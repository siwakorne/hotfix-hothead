#!/bin/ksh

typeset PROJECT_HOME="$(echo $(pwd))"
typeset ngHome="./node_modules/@angular/cli/bin"

cd ${ngHome}

if [ $# -eq 0 ];then
        echo "!!! Please insert build profile (dev/xplink/dtac)"
        exit;
fi;

./ng build --prod --env=$1

typeset srcDir=money-collection-web
typeset destCompressFile=money-collection-web.tar

if [ $# -eq 2 ];then
       destCompressFile=${2}
fi;

cd ${PROJECT_HOME}

rm -rf dist
mkdir -p dist

tar -cvf ${destCompressFile} ${srcDir}
mv ${destCompressFile} dist
rm -rf ${srcDir}

