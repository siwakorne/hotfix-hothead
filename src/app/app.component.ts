import { Component, OnInit } from '@angular/core';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/popper.js/dist/umd/popper.min.js';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
// providers: [VersionCheckService]

export class AppComponent {
    constructor(private router : Router){}
    title = 'app';

    ngOnInit () {
    }

    checkTokenExpired(statusCode){
        if (statusCode == 'B1001' || statusCode == 'B1002' || statusCode == 'B1003' || statusCode == 'B1004' || statusCode == 'B1005' || statusCode == 'B1006') {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        }
    }

    setStatus(status) {
        status = Number(status);
        if (status == '1') {
            return 'Pending';
        } else if (status == '2') {
            return 'Resolved';
        } else if (status == '3') {
            return 'Cancelled';
        } else {
            return 'Default'
        }
    }
}
