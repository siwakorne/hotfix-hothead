import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ng2-currency-mask/src/currency-mask.config";

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoutingModule } from './modules/routing-modules/routing-module';
import { AdminConfModule } from './modules/admin-conf-module/admin-conf.module';
import { AdminSetupModule } from './modules/admin-setup-module/admin-setup.module';
import { ConfirmSalePaymentModule } from './modules/confirm-sale-payment-module/confirm-sale-payment.module';
import { CreditNoteModule } from './modules/credit-note-module/credit-note.module';
import { DashboardModule } from './modules/dashboard-module/dashboard.module';
import { PayInSlipModule } from './modules/pay-in-slip-module/pay-in-slip.module';
import { PostToGlModule } from './modules/post-to-gl-module/post-to-gl.module';
import { ReconcliliationModule } from './modules/reconciliation-module/reconciliation.module';
import { ReportModule } from './modules/report-module/report.module';
import { MenubarComponent } from './views/layout/menubar/menubar.component';
import { FooterComponent } from './views/layout/footer/footer.component';
import { TokenAuthenticateGuard } from './guards/token-authenticate.guard';
import { SalePaymentModule } from './modules/sale-payment-module/sale-payment.module';
import { CustomErrorHandler } from './common/error-handler/custom-error-handler';
import { Endpoint } from './common/const/endpoint';
import { ShareCompModule } from './common/components/share-comp.module';
import { MenuService } from './views/layout/menubar/menu.service';
import { HttpService } from '../../src/app/common/util/http-service';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
    align: "right",
    allowNegative: false,
    decimal: ".",
    precision: 2,
    prefix: "",
    suffix: "",
    thousands: ",",
    
};

@NgModule({
    declarations: [
        AppComponent,
        MenubarComponent,
        FooterComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        CurrencyMaskModule,
        HttpClientModule,
        /* business modules */
        AdminConfModule,
        AdminSetupModule,
        ConfirmSalePaymentModule,
        CreditNoteModule,
        DashboardModule,
        PayInSlipModule,
        PostToGlModule,
        ReconcliliationModule,
        ReportModule,
        SalePaymentModule,
        /* share component module */
        ShareCompModule,
        /* public routing module */
        RoutingModule
    ],
    providers: [
        HttpClient,
        Endpoint,
        MenuService,
        HttpService,
        TokenAuthenticateGuard,
        {
            provide: CURRENCY_MASK_CONFIG, 
            useValue: CustomCurrencyMaskConfig},
        {
            provide: ErrorHandler,
            useClass: CustomErrorHandler
        }
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
    constructor() { }
}
