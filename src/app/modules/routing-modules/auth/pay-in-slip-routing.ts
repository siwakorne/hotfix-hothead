import { Routes } from '@angular/router';
import { PayInSlipComponent } from '../../pay-in-slip-module/components/pay-in-slip/pay-in-slip.component';
import { SearchComponent } from '../../pay-in-slip-module/components/search/search.component';
import { TokenAuthenticateGuard } from '../../../guards/token-authenticate.guard';
import { CancelComponent } from '../../pay-in-slip-module/components/cancel/cancel.component';
import { ReprintComponent } from '../../pay-in-slip-module/components/reprint/reprint.component';
import { PrintComponent } from '../../pay-in-slip-module/components/print/print.component';
import { SlipComponent } from '../../pay-in-slip-module/components/slip/slip.component';

export const PAY_IN_SLIP_ROUTING: Routes = [
    {
        path: 'pay-in-slip', component: PayInSlipComponent,
        children: [
            { path: 'search', component: SearchComponent },
            { path: 'cancel', component: CancelComponent },
            { path: 'reprint', component: ReprintComponent },
            { path: 'print', component: PrintComponent },
            { path: 'slip', component: SlipComponent}
        ],
        // canActivate: [TokenAuthenticateGuard],
        data: {
            role: "admin"
        }
    }
];