import { Routes } from '@angular/router';
import { DashboardComponent } from '../../dashboard-module/components/dashboard/dashboard.component';

export const DASHBOARD_ROUTING: Routes = [
    { path: 'dashboard', component: DashboardComponent }
];