import { Routes } from '@angular/router';
import { ReconciliationComponent } from '../../reconciliation-module/components/reconciliation/reconciliation.component';
import { MainpageArComponent } from '../../reconciliation-module/components/main-page-ar/main-page-ar.component';
import { DetailpageArComponent } from '../../reconciliation-module/components/detail-page-ar/detail-page-ar.component';
import { SearchAdjustArComponent } from '../../reconciliation-module/components/search-adjust-ar/search-adjust-ar.component';


export const RECONCILIATION_ROUTING: Routes = [
    { 
        path: 'reconciliation', component: ReconciliationComponent,
        children: [
            { path: 'main-page-ar', component: MainpageArComponent } ,
            { path: 'detail-page-ar', component: DetailpageArComponent },
            { path: 'search-adjust-ar', component: SearchAdjustArComponent}
        ]
    },
];