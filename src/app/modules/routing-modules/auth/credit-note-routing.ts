import { Routes } from '@angular/router';
import { CreditNoteComponent } from '../../credit-note-module/components/credit-note/credit-note.component';
import { SearchAdjustComponent } from '../../../common/components/search-adjust/search-adjust.component';


export const CREDIT_NOTE_ROUTING: Routes = [
    {
        path: 'credit-note', component: CreditNoteComponent,
        children: [
            { path: 'search', component: SearchAdjustComponent }
        ]
}
];
