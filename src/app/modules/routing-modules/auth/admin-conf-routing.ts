import { Routes } from '@angular/router';
import { AdminConfComponent } from '../../admin-conf-module/components/admin-conf/admin-conf.component';

export const ADMIN_CONF_ROUTING: Routes = [
    {path: 'admin-config', component: AdminConfComponent}
];