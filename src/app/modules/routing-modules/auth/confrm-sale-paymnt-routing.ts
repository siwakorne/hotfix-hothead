import { Routes } from '@angular/router';
// tslint:disable-next-line:max-line-length
import { ConfirmSalePaymentComponent } from '../../confirm-sale-payment-module/components/confirm-sale-payment/confirm-sale-payment.component';
import { FormSalePaymentComponent } from '../../confirm-sale-payment-module/components/form-sale-payment/form-sale-payment.component';
import { ClosingEndOfDayComponent } from '../../confirm-sale-payment-module/components/closing-end-of-day/closing-end-of-day.component';
import { AdjustViewComponent } from '../../confirm-sale-payment-module/components/adjust-view/adjust-view.component';
import { AdjustListComponent } from '../../confirm-sale-payment-module/components/adjust-list/adjust-list.component';
import { SearchAdjustComponent } from '../../../common/components/search-adjust/search-adjust.component';


export const CONFRM_SALE_PAYMNT_ROUTING: Routes = [
    {
        path: 'confirm-sale-payment', component: ConfirmSalePaymentComponent,
        children: [
            { path: 'closing-end-of-day', component: ClosingEndOfDayComponent },
            { path: 'search-adjust', component: SearchAdjustComponent },
            { path: 'form-sale', component: FormSalePaymentComponent },
            { path: 'adjust-view', component: AdjustViewComponent },
            { path: 'adjust-list', component: AdjustListComponent }
        ]
    }
];
