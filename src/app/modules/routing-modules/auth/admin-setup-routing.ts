import { Routes } from '@angular/router';
import { AdminSetupComponent } from '../../admin-setup-module/components/admin-setup/admin-setup.component';
import { UserComponent } from '../../admin-setup-module/components/user/user.component';
import { RoleComponent } from '../../admin-setup-module/components/role/role.component';
import { AdjustTypeComponent } from '../../admin-setup-module/components/adjustType/adjust-type.component';
import { BarcodeComponent } from '../../admin-setup-module/components/barcode/barcode.component';
import { BankComponent } from '../../admin-setup-module/components/bank/bank.component';
import { PaymentTypeComponents } from '../../admin-setup-module/components/payment/payment.component';
import { LocationShopComponents } from '../../admin-setup-module/components/location/location-shop.component';
import { MerchantComponents } from '../../admin-setup-module/components/merchant/merchant.component';
import { LocVsBankComponents } from '../../admin-setup-module/components/locvsbank/locvsbank.component';
import { ManagePendingComponent } from '../../admin-setup-module/components/manage-pending/manage-pending.component';
import { AttachFileComponent } from '../../admin-setup-module/components/attach-file/attach-file.component';
import { AttachSearchComponent } from '../../admin-setup-module/components/attach-search/attach-search.component';
import { AttachUploadComponent } from '../../admin-setup-module/components/attach-upload/attach-upload.component';
import { SuspenseComponent } from '../../admin-setup-module/components/suspense/suspense.component';
import { ConpanyComponent } from '../../admin-setup-module/components/conpany/conpany.component';
import { PayInMappingComponent } from '../../admin-setup-module/components/pay-in-mapping/pay-in-mapping.component';
import { GlobalComponent } from '../../admin-setup-module/components/global/global.component';
import { BankSlaComponent } from '../../admin-setup-module/components/bankSla/bankSla.component';
import { HolidayComponent } from '../../admin-setup-module/components/holiday/holiday.component';
import { OtherAccountComponent } from '../../admin-setup-module/components/other-account/other-account.component';
import { NotificationComponent } from '../../admin-setup-module/components/notification/notification.component';
import { AttachMappingComponent } from '../../admin-setup-module/components/attach-mapping/attach-mapping.component';
import { BankcodeMappingComponent } from '../../admin-setup-module/components/bankcode-mapping/bankcode-mapping.component';
import { BankMasterComponent } from '../../admin-setup-module/components/bank-master/bank-master.component';

export const ADMIN_SETUP_ROUTING: Routes = [
    {
        path: 'admin-setup', component: AdminSetupComponent,
        children: [
            { path: 'user', component: UserComponent },
            { path: 'role', component: RoleComponent },
            { path: 'adjust-type', component: AdjustTypeComponent },
            { path: 'payment-type', component: PaymentTypeComponents },
            { path: 'bank', component: BankComponent },
            { path: 'barcode', component: BarcodeComponent },
            { path: 'location-shop', component: LocationShopComponents },
            { path: 'merchant', component: MerchantComponents},
            { path: 'loc-vs-bank', component: LocVsBankComponents},
            { path: 'manage-pending', component: ManagePendingComponent},
            { path: 'attach-file', component: AttachFileComponent},
            { path: 'attach-search', component: AttachSearchComponent},
            { path: 'attach-upload', component: AttachUploadComponent},
            { path: 'suspense', component: SuspenseComponent},
            { path: 'company', component: ConpanyComponent},
            { path: 'payinLocation', component: PayInMappingComponent},
            { path: 'global', component: GlobalComponent},
            { path: 'bank-sla', component: BankSlaComponent},
            { path: 'holiday', component: HolidayComponent},
            { path: 'mapping-Other-Account', component: OtherAccountComponent},
            { path: 'notification', component: NotificationComponent},
            { path: 'attach-mapping',component: AttachMappingComponent},
            { path: 'bankcode-mapping', component: BankcodeMappingComponent},
            { path: 'bank-master', component: BankMasterComponent}
        ]
    }
];