import { Routes } from '@angular/router';
import { SalePaymentComponent } from '../../sale-payment-module/components/sale-payment/sale-payment.component';

export const SALE_PAYMNT_ROUTING: Routes = [
    { path: 'sale-payment', component: SalePaymentComponent }
];