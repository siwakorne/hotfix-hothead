import { Routes } from '@angular/router';
import { PostToGlComponent } from '../../post-to-gl-module/components/post-to-gl/post-to-gl.component';

export const POST_TO_GL_ROUTING: Routes = [
    { path: 'post-to-gl', component: PostToGlComponent }
];