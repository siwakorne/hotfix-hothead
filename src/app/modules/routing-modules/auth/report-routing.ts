import { Routes } from '@angular/router';
import { ReportComponent } from '../../report-module/components/report/report.component';
import { CoverWhtComponent } from '../../report-module/components/cover-wht/cover-wht.component';
import { SummaryReportComponent } from '../../report-module/components/summary-report/summary-report.component';
import { ReconcileComponent } from '../../report-module/components/reconcile/reconcile.component';
import { ReconcileSaleComponent } from '../../report-module/components/reconcile-sale/reconcile-sale.component';
import { ReconcileBankComponent } from '../../report-module/components/reconcile-bank/reconcile-bank.component';
import { OutstandingAdjustmentReportComponent } from '../../report-module/components/outstanding-adjustment-report/outstanding-adjustment-report.component';
import { ShortageSurplusComponent } from '../../report-module/components/shortage-surplus/shortage-surplus.component';
import { ArAgingComponent } from '../../report-module/components/ar-aging/ar-aging.component';


export const REPORT_ROUTING: Routes = [
    {
        path: 'report', component: ReportComponent,
        children: [
            { path: 'cover-wht', component: CoverWhtComponent },
            { path: 'summary-report', component: SummaryReportComponent },
            {path: 'reconcile-report', component: ReconcileComponent},
            {path: 'Reconcile-Sale-Report', component: ReconcileSaleComponent},
            {path: 'Report-Reconcile-Bank', component: ReconcileBankComponent},
            {path: 'Outstanding-Report', component: OutstandingAdjustmentReportComponent},
            {path: 'Shortage-Surplus', component: ShortageSurplusComponent},
            {path: 'ar-aging' , component: ArAgingComponent }
        ]
    }
];