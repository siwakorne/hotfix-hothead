import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {DialogModule} from 'primeng/dialog';

import { PRIMARY_ROUTING } from './public/primary-routing';
import { LoginComponent } from '../../views/public/login/login.component';
import { PageNotFoundComponent } from '../../views/public/page-not-found/page-not-found.component';


@NgModule({
    imports: [
        CommonModule,
        DialogModule,
        FormsModule,
        RouterModule.forRoot(PRIMARY_ROUTING, { useHash: true }
        )
    ],
    declarations: [
        LoginComponent,
        PageNotFoundComponent
    ],
    exports: [RouterModule]
})
export class RoutingModule { }