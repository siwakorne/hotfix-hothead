import { Routes } from '@angular/router';

import { PageNotFoundComponent } from '../../../views/public/page-not-found/page-not-found.component';
import { LoginComponent } from '../../../views/public/login/login.component';
import { TokenAuthenticateGuard } from '../../../guards/token-authenticate.guard';
import { ParentExampleComponent } from '../../../common/components/parent-example/parent-example.component';

export const PRIMARY_ROUTING: Routes = [
    { path: 'page-not-found', component: PageNotFoundComponent },
    {
        path: 'login', component: LoginComponent,
        // canActivate: [TokenAuthenticateGuard],
        // data: {
        //     role: 'admin'
        // }
    },
    { path: 'exmaple-1', component: ParentExampleComponent },
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: '**', redirectTo: 'page-not-found', pathMatch: 'full' }
];