import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

    constructor(private router: Router) { }

    ngOnInit() {
        this.checkAuth();
    }

    checkAuth() {
        if(!JSON.parse(localStorage.getItem('loginData'))) {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        } else if (JSON.parse(localStorage.getItem('loginData')) && JSON.parse(localStorage.getItem('loginData')).length < 4) {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        } 
    }

}
