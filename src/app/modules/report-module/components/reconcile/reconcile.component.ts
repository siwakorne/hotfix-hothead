import { Component, OnInit } from '@angular/core';
import { Endpoint } from '../../../../common/const/endpoint';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
  selector: 'app-reconcile',
  templateUrl: './reconcile.component.html',
  styleUrls: ['./reconcile.component.css']
})
export class ReconcileComponent implements OnInit {

  constructor(private router: Router, private endpoint: Endpoint,private global: AppComponent, private HttpService: HttpService) { }

  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
  mockLocationId = this.rawMockLocationId[2];
  company: Array<object> = [];
  minMainDocDate = '';
  form: any = {
    search: {
      dateFrom: new Date(),
      dateTo: new Date(),
      companyCode: null,
      paymentTypeCode: null
    }
  }


  ngOnInit() {
  }


  dropDow() {
    // company dropdown

    var headers = {
      'action': 'load',
      'lang': 'EN',
      'navigate': this.router.url,
    }
    this.HttpService.call(this.endpoint.API_LOAD_COMP, 'GET', headers, null, null, null, this.companySuccessCallback.bind(this), null);
  }

  companySuccessCallback(snapData: any){
    this.global.checkTokenExpired(snapData.data.statusCode);
    if (snapData.data.statusCode == 'S0000') {
      this.company = snapData.data.data.companyInfoList.map(snap => {
        return {
          companyCode: snap.companyCode,
        }
      });
    }
  }


  submit(){
    var headers = {
      'action': 'load',
      'lang': 'EN',
      'navigate': this.router.url,
    }
    this.HttpService.call(this.endpoint.API_REPORT_RECONCILE, 'GET', headers, null, null, null, this.submitSuccessCallback.bind(this), null);
  }

  submitSuccessCallback(snapData: any){
    this.global.checkTokenExpired(snapData.data.statusCode);
    if (snapData.data.statusCode == 'S0000') {
      this.company = snapData.data.data.companyInfoList.map(snap => {
        return {
          companyCode: snap.companyCode,
        }
      });
    }
  }
  
}
