import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import * as moment from 'moment';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
  selector: 'app-outstanding-adjustment-report',
  templateUrl: './outstanding-adjustment-report.component.html',
  styleUrls: ['./outstanding-adjustment-report.component.css']
})
export class OutstandingAdjustmentReportComponent implements OnInit {

  constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent, private HttpService: HttpService) { }
  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
  mockLocationId = this.rawMockLocationId[2];

  searchGlDate = '0';
  minGlDate: any;

  locationList = [];

  form: any = {
    adjSrchReq: {
      locationCode: undefined,
      GldateFrom: null,
      action: 'print-detail-'
    }
  };


  ngOnInit() {
    this.dropDow();
  }


  print() {
    let GldateFrom = this.form.adjSrchReq.GldateFrom;
    let GldateTo = this.form.adjSrchReq.GldateTo;

    var headers = {
      'action': this.form.adjSrchReq.action + 'pdf',
      'lang': 'EN',
      'navigate': this.router.url,
      'replyMediaType': 'application/pdf',
      'replyFileName': 'RECONCILE_BY_SHOP_REPORT_' + moment().format('YYYYMMDDhmmss') + '.pdf'
    }
    var params = {
      postGlDate: GldateFrom ? GldateFrom.getTime() : null,
      locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode == 'null' ? null : this.form.adjSrchReq.locationCode : null,
    }
    this.HttpService.call(this.endpoint.API_REPORT_OST, 'GET', headers, 'blob', params, null, this.printSuccessCallback.bind(this), null);
  }

  printSuccessCallback(response: any) {
    this.global.checkTokenExpired(response.data.statusCode);
    if (response.status == 200) {
      let blob = new Blob([response.data], { type: 'application/pdf', });
      let fileURL = URL.createObjectURL(blob);
      window.open(fileURL, '_blank');
    }
  }


  printExcel() {
    let GldateFrom = this.form.adjSrchReq.GldateFrom;
    let GldateTo = this.form.adjSrchReq.GldateTo;
    var headers = {
      'action': this.form.adjSrchReq.action + 'excel',
      'lang': 'EN',
      'navigate': this.router.url,
      'replyMediaType': 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'replyFileName': 'RECONCILE_BY_SHOP_REPORT_' + moment().format('YYYYMMDDhmmss') + '.xlsx'
    }
    var params = {
      postGlDate: GldateFrom ? GldateFrom.getTime() : null,
      locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode == 'null' ? null : this.form.adjSrchReq.locationCode : null,
    }
    this.HttpService.call(this.endpoint.API_REPORT_OST, 'GET', headers, 'blob', params, null, this.printExcelSuccessCallback.bind(this), null);
  }

  printExcelSuccessCallback(response: any) {
    this.global.checkTokenExpired(response.data.statusCode);
    if (response.status == 200) {
      let blob = new Blob([response.data], { type: 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', });
      let fileURL = URL.createObjectURL(blob);
      window.open(fileURL, '_blank');
    }
  }



  dropDow() {
    // location
    let preList = JSON.parse(localStorage.getItem('locationPermiss'));
    preList.map(key => {
      this.locationList.push({
        code: key.code,
        name: key.name
      })
    });
  }





  clear() {
    this.form = {
      adjSrchReq: {
        locationCode: undefined,
        GldateFrom: null,
        GlDateTo: null,
        action: 'print-detail-'
      }
    };
  }

}
