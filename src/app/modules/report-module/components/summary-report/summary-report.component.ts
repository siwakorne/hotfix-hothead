import { Component, OnInit } from '@angular/core';
import { Endpoint } from '../../../../common/const/endpoint';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
  selector: 'app-summary-report',
  templateUrl: './summary-report.component.html',
  styleUrls: ['./summary-report.component.css']
})
export class SummaryReportComponent implements OnInit {
  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
  mockLocationId = this.rawMockLocationId[2];


  filterData: Array<object> = [];
  loading: boolean = false;
  checkData = 1;
  paging = 0;
  yearRanges:string=((new Date()).getFullYear()-2)+':'+ (new Date()).getFullYear();

  constructor(private router: Router, private endpoint: Endpoint,private global : AppComponent, private HttpService: HttpService) { }

  ngOnInit() {
  }

  formSearch: {
    dateFrom: Date,
    dateTo: Date
  } = {
      dateFrom: new Date(),
      dateTo: new Date(),
    }

  paginator: any = {
    currentPage: 0,
    totalPages: 0,
    totalRecords: 0,
    recordPerPage: 3,
  };

  paginate(event) {
    if ((event.page + 1) != this.paginator.currentPage) {
      //   this.loading = true
      this.submit(event.page + 1);
    }
  }

  submit(page) {
    this.filterData = [];
    this.loading = true;
    this.checkData = 1;
    let dateFrom = this.formSearch.dateFrom
    let dateTo = this.formSearch.dateTo
    let config = {
      headers: {
        'action': 'load-all-date',
        'lang': 'EN',
        'navigate': this.router.url,
      }
    }
    var params = {
      dateFrom: dateFrom.getTime() ? dateFrom.getTime() : null,
      dateTo: dateTo.getTime() ? dateTo.getTime() : null
  }
    this.HttpService.call(this.endpoint.API_REPORT_COVER_WHT+'/'+page, 'GET', config.headers, null, params, null, this.submitSuccessCallback.bind(this), null);
  }

  submitSuccessCallback(snapData: any){
    this.global.checkTokenExpired(snapData.data.statusCode);
    if (snapData.data.statusCode == 'S0000') {
      this.filterData = snapData.data.data.dateInfo.map(snap => {
        return {
          docDate: snap.docDate,
        }

      });
      if (snapData.data.data.pagingInfo.totalRecords != 0) {
        this.loading = false;
        this.checkData = 1;
        this.paging = 1
      } else {
        this.loading = false;
        this.checkData = 0;
        this.paging = 0;
      }
      this.paginator = snapData.data.data.pagingInfo;
    }
  }


  loadReport(data) {
    var headers = {
      'lang': 'EN',
      'navigate': this.router.url,
      'replyFileName': 'REPORT_SUMMARY_' + moment().format('YYYYMMDDhmmss') +'.pdf' 
    }
    var params = {
      locationCode:  this.mockLocationId,
      docDate: data.docDate,
      username: this.rawMockLocationId[1]
    }

    this.HttpService.call(this.endpoint.API_REPORT_SUMMARY_PDF, 'GET', headers, 'blob', params, null, this.loadReportSuccessCallback.bind(this), null);
  }

  loadReportSuccessCallback(response: any){
    if (response.status == 200) {
      let blob = new Blob([response.data], { type: 'application/pdf', });
      let fileURL = URL.createObjectURL(blob);
      window.open(fileURL, '_blank');
    }
  }

}
