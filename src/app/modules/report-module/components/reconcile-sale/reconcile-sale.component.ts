import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import * as moment from 'moment';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
    selector: 'app-reconcile-sale',
    templateUrl: './reconcile-sale.component.html',
    styleUrls: ['./reconcile-sale.component.css']
})
export class ReconcileSaleComponent implements OnInit {

    constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent, private HttpService: HttpService) { }
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];


    company: Array<object> = [];
    locationType: Array<object> = [];
    searchTrandoc = '0';
    minTranDate: any;

    searchGlDate = '0';
    minGlDate: any;

    locationList = [];

    form: any = {
        adjSrchReq: {
            whtFlag: '',
            company: null,
            paymentType: null,
            locationCode: undefined,
            adjustType: '',
            status: 1,
            amount: null,
            saleSlipDate: null,
            bankEdc: '',
            merchantId: null,
            inputMerchantId: null,
            wht: '',
            adjustDateTo: null,
            adjustDate: null,
            dateFrom: null,
            DateTo: null,
            GldateFrom: null,
            Ostamount: null,
            locationType: null

        }
    };

    ngOnInit() {
        this.dropDow();
    }


    print() {
        let dateFrom = this.form.adjSrchReq.dateFrom;
        let dateTo = this.form.adjSrchReq.dateTo;
        let GldateFrom = this.form.adjSrchReq.GldateFrom;
        let GldateTo = this.form.adjSrchReq.GldateTo;
        var headers = {
            'action': 'print-pdf',
            'lang': 'EN',
            'navigate': this.router.url,
            'replyMediaType': 'application/pdf',
            'replyFileName': 'RECONCILE_BY_SHOP_REPORT_' + moment().format('YYYYMMDDhmmss') + '.pdf'
        }

        var params = {
            shopDocDateFrom: dateFrom ? dateFrom.getTime() : null,
            shopDocDateTo: dateTo ? dateTo.getTime() : null,
            paymentType: this.form.adjSrchReq.paymentType ? this.form.adjSrchReq.paymentType == 'null'? null : this.form.adjSrchReq.paymentType  : null,
            companyCode: this.form.adjSrchReq.company ? this.form.adjSrchReq.company == 'null'? null : this.form.adjSrchReq.company  : null,
            postGlDate: GldateFrom ? GldateFrom.getTime() : null,
            locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode == 'null'? null : this.form.adjSrchReq.locationCode  : null,
            locationType: this.form.adjSrchReq.locationType ?  this.form.adjSrchReq.locationType == 'null'? null : this.form.adjSrchReq.locationType  : null
        }
        this.HttpService.call(this.endpoint.API_REPORT_RECONCILE_SHOP, 'GET', headers, 'blob', params, null, this.printSuccessCallback.bind(this), null);
    }

    printSuccessCallback(response: any){
        if (response.status == 200) {
            let blob = new Blob([response.data], { type: 'application/pdf', });
            let fileURL = URL.createObjectURL(blob);
            window.open(fileURL, '_blank');
        }
    }


    printExcel() {
        let dateFrom = this.form.adjSrchReq.dateFrom;
        let dateTo = this.form.adjSrchReq.dateTo;
        let GldateFrom = this.form.adjSrchReq.GldateFrom;
        let GldateTo = this.form.adjSrchReq.GldateTo;
        var  headers = {
            'action': 'print-excel',
            'lang': 'EN',
            'navigate': this.router.url,
            'replyMediaType': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'replyFileName': 'RECONCILE_BY_SHOP_REPORT_' + moment().format('YYYYMMDDhmmss') + '.xlsx'
        }
        var params = {
            shopDocDateFrom: dateFrom ? dateFrom.getTime() : null,
            shopDocDateTo: dateTo ? dateTo.getTime() : null,
            paymentType: this.form.adjSrchReq.paymentType ? this.form.adjSrchReq.paymentType == 'null'? null : this.form.adjSrchReq.paymentType  : null,
            companyCode: this.form.adjSrchReq.company ? this.form.adjSrchReq.company == 'null'? null : this.form.adjSrchReq.company  : null,
            postGlDate: GldateFrom ? GldateFrom.getTime() : null,
            locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode == 'null'? null : this.form.adjSrchReq.locationCode  : null,
            locationType: this.form.adjSrchReq.locationType ?  this.form.adjSrchReq.locationType == 'null'? null : this.form.adjSrchReq.locationType  : null

        }
        this.HttpService.call(this.endpoint.API_REPORT_RECONCILE_SHOP, 'GET', headers, 'blob', params, null, this.printExcelSuccessCallback.bind(this), null);
    }

    printExcelSuccessCallback(response: any){
        if (response.status == 200) {
            let blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', });
            let fileURL = URL.createObjectURL(blob);
            window.open(fileURL, '_blank');
        }
    }


    toTranDate() {
        if (this.form.adjSrchReq.dateFrom > this.form.adjSrchReq.dateTo) {
            this.form.adjSrchReq.dateTo = null;
        }

        if (this.form.adjSrchReq.dateFrom) {
            this.minTranDate = this.form.adjSrchReq.dateFrom;
            this.searchTrandoc = '1';
            if (this.form.adjSrchReq.dateTo == null || this.form.adjSrchReq.dateTo == undefined) {
                this.form.adjSrchReq.dateTo = this.form.adjSrchReq.dateFrom;
            }

        } else {
            this.searchTrandoc = '0';
            this.form.adjSrchReq.dateTo = null;
        }
    }


    toGlDate() {
        if (this.form.adjSrchReq.GldateFrom > this.form.adjSrchReq.GldateTo) {
            this.form.adjSrchReq.GldateTo = null;
        }

        if (this.form.adjSrchReq.GldateFrom) {
            this.minGlDate = this.form.adjSrchReq.GldateFrom;
            this.searchGlDate = '1';
            if (this.form.adjSrchReq.GldateTo == null || this.form.adjSrchReq.GldateTo == undefined) {
                this.form.adjSrchReq.GldateTo = this.form.adjSrchReq.GldateFrom;
            }

        } else {
            this.searchGlDate = '0';
            this.form.adjSrchReq.GldateTo = null;
        }
    }





    dropDow() {
        // company dropdown
        var headersCompany = {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_LOAD_COMP, 'GET', headersCompany, null, null, null, this.companySuccessCallback.bind(this), null);

    //location Type
        var headersLocationType = {
            'action': 'load-location-type',
            'lang': 'EN',
            'navigate': this.router.url,
        }
    this.HttpService.call(this.endpoint.API_LOCATION_TYPE, 'GET', headersLocationType, null, null, null, this.locationTypeSuccessCallback.bind(this), null);


        // location
        let preList = JSON.parse(localStorage.getItem('locationPermiss'));
        preList.map(key => {
            this.locationList.push({
                code: key.code,
                name: key.name
            })
        });
    }

    companySuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.company = snapData.data.data.companyInfoList.map(snap => {
                return {
                    companyCode: snap.companyCode,
                }
            });
        }
    }

    locationTypeSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.locationType = snapData.data.data.locationTypeInfoList.map(snap => {
                return {
                    locationType: snap.locationType,
                }
            });
        }
    }



    loadLocationType(){
        this.form.adjSrchReq.paymentType = null
        this.locationType = [];

        var headers = {
            'action': 'load-location-type',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var params = {
            lacationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode == 'null'? null : this.form.adjSrchReq.locationCode  : null
        }
        this.HttpService.call(this.endpoint.API_LOCATION_TYPE, 'GET', headers, null, params, null, this.loadLocationTypeSuccessCallback.bind(this), null);
    }

    loadLocationTypeSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.locationType = snapData.data.data.locationTypeInfoList.map(snap => {
                return {
                    locationType: snap.locationType,
                }
            });
        }
    }



    clear(){
        this.form = {
            adjSrchReq: {
                whtFlag: '',
                company: null,
                paymentType: null,
                locationCode: undefined,
                adjustType: '',
                status: 1,
                amount: null,
                saleSlipDate: null,
                bankEdc: '',
                merchantId: null,
                inputMerchantId: null,
                wht: '',
                adjustDateTo: null,
                adjustDate: null,
                dateFrom: null,
                DateTo: null,
                GldateFrom: null,
                Ostamount: null,
                locationType: null
    
            }
        };
    }



}
