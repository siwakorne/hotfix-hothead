import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import * as moment from 'moment';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
  selector: 'app-shortage-surplus',
  templateUrl: './shortage-surplus.component.html',
  styleUrls: ['./shortage-surplus.component.css']
})
export class ShortageSurplusComponent implements OnInit {

  constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent, private HttpService: HttpService) { }

  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
  mockLocationId = this.rawMockLocationId[2];

  
  company: Array<object> = [];

  searchTrandoc = '0';
  minTranDate: any;
  form: any = {
      adjSrchReq: {
          companyCode: null,
          paymentType: null,
          dateFrom: null,
          DateTo: null,
          locationCodeStart:null,
          locationCodeTo : null,
          wht:null,
      }
  };
  yearRanges:string=((new Date()).getFullYear()-2)+':'+ (new Date()).getFullYear();

  ngOnInit() {
      this.dropDow();

  }


  print() {
      let dateFrom = this.form.adjSrchReq.dateFrom;
      let dateTo = this.form.adjSrchReq.dateTo;
      var headers = {
          'action': 'print-pdf',
          'lang': 'EN',
          'navigate': this.router.url,
          'replyMediaType': 'application/pdf',
          'replyFileName': 'RECONCILE_BY_BANK_REPORT_' + moment().format('YYYYMMDDhmmss') + '.pdf'
      }
      var params = {
          dateFrom: dateFrom ? dateFrom.getTime() : null,
          dateTo: dateTo ? dateTo.getTime() : null,
          transactionCode: this.form.adjSrchReq.paymentType ? this.form.adjSrchReq.paymentType == 'null'? null : this.form.adjSrchReq.paymentType  : null,
          companyCode: this.form.adjSrchReq.companyCode
      }
      this.HttpService.call(this.endpoint.API_REPORT_RECONCILE, 'GET', headers, 'blob', params, null, this.printSuccessCallback.bind(this), null);
  }

  printSuccessCallback(response: any){
      if (response.status == 200) {
          let blob = new Blob([response.data], { type: 'application/pdf', });
          let fileURL = URL.createObjectURL(blob);
          window.open(fileURL, '_blank');
      }
  }


  printExcel() {
      let dateFrom = this.form.adjSrchReq.dateFrom;
      let dateTo = this.form.adjSrchReq.dateTo;
      var headers = {
          'action': 'print-excel',
          'lang': 'EN',
          'navigate': this.router.url,
          'replyMediaType': 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          'replyFileName': 'RECONCILE_BY_BANK_REPORT_' + moment().format('YYYYMMDDhmmss') + '.xlsx'
      }
      var params = {
          dateFrom: dateFrom ? dateFrom.getTime() : null,
          dateTo: dateTo ? dateTo.getTime() : null,
          transactionCode: this.form.adjSrchReq.paymentType ? this.form.adjSrchReq.paymentType == 'null'? null : this.form.adjSrchReq.paymentType  : null,
          companyCode: this.form.adjSrchReq.companyCode
      }
      this.HttpService.call(this.endpoint.API_REPORT_RECONCILE, 'GET', headers, 'blob', params, null, this.printExcelSuccessCallback.bind(this), null);
  }


  printExcelSuccessCallback(response: any){
      if (response.status == 200) {
          let blob = new Blob([response.data], { type: 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', });
          let fileURL = URL.createObjectURL(blob);
          window.open(fileURL, '_blank');
      }
  }


  toTranDate() {
      if (this.form.adjSrchReq.dateFrom > this.form.adjSrchReq.dateTo) {
          this.form.adjSrchReq.dateTo = null;
      }

      if (this.form.adjSrchReq.dateFrom) {
          this.minTranDate = this.form.adjSrchReq.dateFrom;
          this.searchTrandoc = '1';
          if (this.form.adjSrchReq.dateTo == null || this.form.adjSrchReq.dateTo == undefined) {
              this.form.adjSrchReq.dateTo = this.form.adjSrchReq.dateFrom;
          }

      } else {
          this.searchTrandoc = '0';
          this.form.adjSrchReq.dateTo = null;
      }
  }
  locationClear(){
      this.form.adjSrchReq.locationCodeTo = null;
  }


  dropDow() {
      // company dropdown
      var headers = {
          'action': 'load',
          'lang': 'EN',
          'navigate': this.router.url,
      }
      this.HttpService.call(this.endpoint.API_LOAD_COMP, 'GET', headers, null, null, null, this.companySuccessCallback.bind(this), null);
  }


  companySuccessCallback(snapData: any){
      this.global.checkTokenExpired(snapData.data.statusCode);
      if (snapData.data.statusCode == 'S0000') {
          this.company = snapData.data.data.companyInfoList.map(snap => {
              return {
                  companyCode: snap.companyCode,
              }
          });
      }
  }


  clear(){
      this.form = {
          adjSrchReq: {
            companyCode: null,
            paymentType: null,
            dateFrom: null,
            DateTo: null,
            locationCodeStart:null,
            locationCodeTo : null,
            wht:null,
          }
      };
  }
}

