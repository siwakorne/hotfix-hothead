import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../../common/util/http-service';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import { AppComponent } from '../../../../app.component';
import * as moment from 'moment';

@Component({
  selector: 'app-ar-aging',
  templateUrl: './ar-aging.component.html',
  styleUrls: ['./ar-aging.component.css']
})
export class ArAgingComponent implements OnInit {

  constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent, private HttpService: HttpService) { }
  
  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];

    company: Array<object> = [];

    searchTrandoc = '0';
    chanalStatus = '0';
    locationStatus = '0';
    investerStatus = '0';
    reconcileStatus ='0';
    minTranDate: any;
    form: any = {
        adjSrchReq: {
            paymentType: null,
            date: new Date,
            channelTypeFrom: null,
            channelTypeTo: null,
            locationCodeFrom: null,
            locationCodeTo: null,
            investerNameFrom: null,
            investerNameTo: null,
            reconcileByFrom: null,
            reconcileByTo: null,
            wht: 'N',
        }
    };

    ngOnInit() {
        this.dropDow();
        console.log(this.rawMockLocationId);
    }


    print() {
        let dateFrom = this.form.adjSrchReq.dateFrom;
        let dateTo = this.form.adjSrchReq.dateTo;
        var headers = {
            'action': 'print-pdf',
            'lang': 'EN',
            'navigate': this.router.url,
            'replyMediaType': 'application/pdf',
            'replyFileName': 'RECONCILE_BY_BANK_REPORT_' + moment().format('YYYYMMDDhmmss') + '.pdf'
        }
        var params = {
            dateFrom: dateFrom ? dateFrom.getTime() : null,
            dateTo: dateTo ? dateTo.getTime() : null,
            transactionCode: this.form.adjSrchReq.paymentType ? this.form.adjSrchReq.paymentType == 'null'? null : this.form.adjSrchReq.paymentType  : null,
            companyCode: this.form.adjSrchReq.companyCode
        }
        this.HttpService.call(this.endpoint.API_REPORT_RECONCILE, 'GET', headers, 'blob', params, null, this.printSuccessCallback.bind(this), null);
    }

    printSuccessCallback(response: any){
        if (response.status == 200) {
            let blob = new Blob([response.data], { type: 'application/pdf', });
            let fileURL = URL.createObjectURL(blob);
            window.open(fileURL, '_blank');
        }
    }


    printExcel() {
        let dateFrom = this.form.adjSrchReq.dateFrom;
        let dateTo = this.form.adjSrchReq.dateTo;
        var headers = {
            'action': 'print-excel',
            'lang': 'EN',
            'navigate': this.router.url,
            'replyMediaType': 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'replyFileName': 'RECONCILE_BY_BANK_REPORT_' + moment().format('YYYYMMDDhmmss') + '.xlsx'
        }
        var params = {
            dateFrom: dateFrom ? dateFrom.getTime() : null,
            dateTo: dateTo ? dateTo.getTime() : null,
            transactionCode: this.form.adjSrchReq.paymentType ? this.form.adjSrchReq.paymentType == 'null'? null : this.form.adjSrchReq.paymentType  : null,
            companyCode: this.form.adjSrchReq.companyCode
        }
        this.HttpService.call(this.endpoint.API_REPORT_RECONCILE, 'GET', headers, 'blob', params, null, this.printExcelSuccessCallback.bind(this), null);
    }


    printExcelSuccessCallback(response: any){
        if (response.status == 200) {
            let blob = new Blob([response.data], { type: 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', });
            let fileURL = URL.createObjectURL(blob);
            window.open(fileURL, '_blank');
        }
    }

   toTranReconcileBy(){
      this.form.adjSrchReq.reconcileByTo = null;
    }

    toTranChannelType(){
      this.form.adjSrchReq.channelTypeTo = null;
    }

    toTranInvesterName(){
      this.form.adjSrchReq.investerNameTo = null;
    }

    toTranLocationCode(){
      this.form.adjSrchReq.locationCodeTo = null;
    }

    toTranWHT(){
        this.form.adjSrchReq.paymentType = null;
    }

    dropDow() {
        // company dropdown
        var headers = {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_LOAD_COMP, 'GET', headers, null, null, null, this.companySuccessCallback.bind(this), null);
    }


    companySuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.company = snapData.data.data.companyInfoList.map(snap => {
                return {
                    companyCode: snap.companyCode,
                }
            });
        }
    }


    clear(){
      this.searchTrandoc = '0';
      this.chanalStatus = '0';
      this.locationStatus = '0';
      this.investerStatus = '0';
      
      this.reconcileStatus ='0';
        this.form = {
            adjSrchReq: {
              paymentType: null,
              date: new Date,
              channelTypeFrom: null,
              channelTypeTo: null,
              locationCodeFrom: null,
              locationCodeTo: null,
              investerNameFrom: null,
              investerNameTo: null,
              reconcileByFrom: null,
              reconcileByTo: null,
              wht: 'N',
            }
        };
    }
}