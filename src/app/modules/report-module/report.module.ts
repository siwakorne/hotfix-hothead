import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { PaginatorModule } from 'primeng/paginator';

import { ReportComponent } from './components/report/report.component';
import { REPORT_ROUTING } from '../routing-modules/auth/report-routing';
import { CoverWhtComponent } from './components/cover-wht/cover-wht.component';
import { SummaryReportComponent } from './components/summary-report/summary-report.component';
import { ReconcileComponent } from './components/reconcile/reconcile.component';
import { OutstandingAdjustmentReportComponent } from './components/outstanding-adjustment-report/outstanding-adjustment-report.component';
import { ReconcileSaleComponent } from './components/reconcile-sale/reconcile-sale.component';
import { ReconcileBankComponent } from './components/reconcile-bank/reconcile-bank.component';
import { ShortageSurplusComponent } from './components/shortage-surplus/shortage-surplus.component';
import { DialogModule } from 'primeng/dialog';
import { ArAgingComponent } from './components/ar-aging/ar-aging.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(REPORT_ROUTING),
        CalendarModule,
        FormsModule,
        PaginatorModule,
        DialogModule,
        PaginatorModule,
    ],
    declarations: [ReportComponent, CoverWhtComponent, SummaryReportComponent, ReconcileComponent, ReconcileSaleComponent, ReconcileBankComponent, OutstandingAdjustmentReportComponent, ShortageSurplusComponent, ArAgingComponent]
})
export class ReportModule { 
    constructor() { }
}
