import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ADMIN_CONF_ROUTING } from '../routing-modules/auth/admin-conf-routing';
import { AdminConfComponent } from './components/admin-conf/admin-conf.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ADMIN_CONF_ROUTING)
    ],
    declarations: [
        AdminConfComponent
    ],
    exports: [RouterModule]
})
export class AdminConfModule {
    constructor() { }
}
