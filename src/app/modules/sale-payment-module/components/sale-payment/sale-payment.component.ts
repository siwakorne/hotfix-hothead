import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sale-payment',
  templateUrl: './sale-payment.component.html',
  styleUrls: ['./sale-payment.component.css']
})
export class SalePaymentComponent implements OnInit {
    disableBox = false;
    dtac = {
        cheque: '',
        credit: '',
        woCash: '',
        woCheque: '',
        woCredit: ''
    };
    
    dtn = {
        cash: '',
        cheque: '',
        credit: '',
        woCash: '',
        woCheque: '',
        woCredit: ''
    };

    constructor(private router: Router) { }

    onSubmit() {
        // this.routertt.navigate(['/main']);
        this.disableBox = !this.disableBox;
    }
    onCancel() {
        this.router.navigate(['/main']);
    }
    ngOnInit() {
    }

    FloatInput(event, data) {
        if (data === 'CashDtn') {
            if (event.keyCode != 190 && this.dtn.cash.indexOf('.') === -1) {
                this.dtn.cash = this.formatComma(event.target.value)
            }
        } else if (data === 'ChequeDtn') {
            if (event.keyCode != 190 && this.dtn.cheque.indexOf('.') === -1) {
                this.dtn.cheque = this.formatComma(event.target.value)
            }
        } else if (data === 'CreditDtn') {
            if (event.keyCode != 190 && this.dtn.credit.indexOf('.') === -1) {
                this.dtn.credit = this.formatComma(event.target.value)
            }
        } else if (data === 'ChequeDtac') {
            if (event.keyCode != 190 && this.dtac.cheque.indexOf('.') === -1) {
                this.dtac.cheque = this.formatComma(event.target.value)
            }
        } else if (data === 'CreditDtac') {
            if (event.keyCode != 190 && this.dtac.credit.indexOf('.') === -1) {
                this.dtac.credit = this.formatComma(event.target.value)
            }
        } else if (data === 'woCashDtac') {
            if (event.keyCode != 190 && this.dtac.woCash.indexOf('.') === -1) {
                this.dtac.woCash = this.formatComma(event.target.value)
            }
        } else if (data === 'woCheqDtac') {
            if (event.keyCode != 190 && this.dtac.woCheque.indexOf('.') === -1) {
                this.dtac.woCheque = this.formatComma(event.target.value)
            }
        } else if (data === 'woCreditDtac') {
            if (event.keyCode != 190 && this.dtac.woCredit.indexOf('.') === -1) {
                this.dtac.woCredit = this.formatComma(event.target.value)
            }
        } else if (data === 'woCashDtn') {
            if (event.keyCode != 190 && this.dtn.woCash.indexOf('.') === -1) {
                this.dtn.woCash = this.formatComma(event.target.value)
            }
        } else if (data === 'woCheqDtn') {
            if (event.keyCode != 190 && this.dtn.woCheque.indexOf('.') === -1) {
                this.dtn.woCheque = this.formatComma(event.target.value)
            }
        } else if (data === 'woCreditDtn') {
            if (event.keyCode != 190 && this.dtn.woCredit.indexOf('.') === -1) {
                this.dtn.woCredit = this.formatComma(event.target.value)
            }
        }
    }

    formatComma (data) {
        var reverse = data.toString().replace(/,/g, '').split('').reverse().join(''),
            thousand = reverse.match(/\d{1,3}/g);
        var result = thousand.join(',').split('').reverse().join('');
        return result;
    }

}