import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SALE_PAYMNT_ROUTING } from '../routing-modules/auth/sale-paymnt-routing';
import { SalePaymentComponent } from './components/sale-payment/sale-payment.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SALE_PAYMNT_ROUTING),
        FormsModule
    ],
    declarations: [SalePaymentComponent]
})
export class SalePaymentModule { 
    constructor() { }
}
