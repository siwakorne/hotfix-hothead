import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { DialogModule } from 'primeng/dialog';
import { CreditNoteComponent } from './components/credit-note/credit-note.component';
import { CREDIT_NOTE_ROUTING } from '../routing-modules/auth/credit-note-routing';
import { PaginatorModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/calendar';
import { ViewadjustCreditNoteComponent } from './components/view-adjust-credit-note/view-adjust-credit-note.component';




@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        CurrencyMaskModule,
        DialogModule,
        PaginatorModule,
        CalendarModule,

        RouterModule.forChild(CREDIT_NOTE_ROUTING)
    ],
    declarations: [
        CreditNoteComponent,
        ViewadjustCreditNoteComponent
    ],
exports: [
        ViewadjustCreditNoteComponent
]
})
export class CreditNoteModule {
    constructor() { }
}
