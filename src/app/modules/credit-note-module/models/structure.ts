export class searchCreditNote {
    paymentType: string = 'CreditCard';
    company: string = '';
    locationCode: string = '';
    amount: number = 0;
    status: string = '';
    saleSlipDate: any = '';
    bankEdc: string = '';
    merchartId: string = '';
}