import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-view-adjust-credit-note',
    templateUrl: './view-adjust-credit-note.component.html',
    styleUrls: ['./view-adjust-credit-note.component.css']
})
export class ViewadjustCreditNoteComponent implements OnInit {

    @Output() disClose = new EventEmitter<boolean>();
    dateVal = new Date();
    detail: any;
    @Input() data = {
        company: '',
        paymentType: '',
        locationCode: '',
        locationName: '',
        amount: '',
        adjustNo: '',
        adjustType: '',
        status: '',
        bankEdc: '',
        merchartId: '',
        bankDoc: '',
        key: ''
    };
    bankRef = '';

    constructor(private router: Router, private params: ActivatedRoute) { }
    /* onPlusAdjust() {
        this.detail = 1;
    }
    onMinusAdjust() {
        this.detail = 2;
    } */
    onSave() {
        
    }
    onCancel(agreed: boolean) {
        this.disClose.emit(agreed);
    }

    ngOnInit() {
        this.dateVal.setDate(this.dateVal.getDate() - 1);
    }
}
