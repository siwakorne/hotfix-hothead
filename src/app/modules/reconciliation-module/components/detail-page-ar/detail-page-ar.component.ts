import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import * as axios from 'axios';
import * as moment from 'moment';
import { v4 as uuid } from 'uuid';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-detail-page-ar',
    templateUrl: './detail-page-ar.component.html',
    styleUrls: ['./detail-page-ar.component.css']
})
export class DetailpageArComponent implements OnInit {

    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];
    adjustData = [];
    data: any;
    dataCallList: any;
    box: number = 0;
    locationList = [];
    preLocation = [];
    preLocationObj = [];
    dateVal = '';
    conditionPaging = {
        whtFlag: '',
        locationCode: '',
        paymentTypeCode: '',
        dataType: '',
    };
    dateNow = new Date().getTime();
    date: string;
    show: boolean = true;
    display: boolean = false;
    t1Date: number = 0;
    t2Date: number = 0;
    loading: boolean = false;
    url: string;
    msg: string;
    notClosing: Array<any> = [];
    msgUd: any;
    trans = {
        csh: 3,
        chq: 3,
        cc: 3,
        wht: 3
    }
    t3Date: number = 0;
    t4Date: number = 0;
    shops: any = [];
    reconcilePaymentId: number = 0;
    reconcileWhtId: number = 0;
    reconcileCompanyId: number = 0;
    bankDes: any;
    boxedit = 0;
    boxAlert = 0;
    stockDelete = {
        adjustDocId: 0,
        reconcilePaymentId: 0,
        reconcileWhtId: 0,
        paymentTypeCode: '',
        companyCode: '',
        whtFlag: '',
        locationCode: '',
        refAdjustDocId: ''
    }
    filterData = [];
    backupDate: any = {}
    dif: any;
    docSearch = '';
    status = '';
    listCompanyData = [];
    listAdjustData = [];
    listAdjustDetail = {
        company: '',
        dataType: ''
    };
    adjustStatus = [];
    adjustType = [];
    sendData = {
        reconcileId: null,
        reconcileWhtId: null,
        reconcileWhtCompId : null,
        reconcileDetId: null,
        reconcileWhtDetId: null,
        reconcilePaymentId: null,
        reconcileCompanyId: null,
        company: '',
        shopAmt: 0,
        customerName: '',
        adjustDocId: 0,
        diffAmt: 0,
        checkDiffAmt: 0,
        paymentType: '',
        adjustMasterId: undefined,
        adjustMasterType: '',
        chequeNo: '',
        saleSlipDate: '',
        status: '',
        whtFlag: '',
        taxNo: '',
        ostDiff: 0,
        locationCode: null,
        dataType: '',
        paymentTypeCode: '',
        note: '',
        bankAccNo: null,
        postGlFlag: null
    };
    paginator: any = {
        firstPaging: 0,
        pagingInfo: {
            totalRecords: 0,
            recordPerPage: 0,
            currentPage: 0,
            totalPages: 0
        },
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };
    

    constructor(private router: Router, private params: ActivatedRoute, private Endpoint: Endpoint, private global: AppComponent) {
    }

    ngOnInit() {
        let overstring = this.router.url.indexOf('?');
        if (overstring != null) {
            this.url = this.router.url.substr(0, overstring);
        } else {
            this.url = this.router.url;
        }
        this.params.queryParams.subscribe(params => {
            this.date = params['docDate'];
            this.docSearch = params['docSearch'];
            this.status = params['status'];
        });
        this.selectData();
        this.dropDown();
    }

    dropDown() {
        // adjustType dropdown
        axios.default.request({
            url: this.Endpoint.API_LOAD_ADJ_TYPE,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.adjustType = snapData.data.data.adjMastInfoList.map(snap => {
                        return {
                            adjustType: snap.adjustType,
                            adjustDocType: snap.adjustDocType,
                            adjustMasterId: snap.adjustMasterId
                        };
                    });
                }
            });

        // adjustStatus dropdown
        axios.default.request({
            url: this.Endpoint.API_LOAD_ADJ_STTS,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.adjustStatus = snapData.data.data.adjSttsInfoList.map(snap => {
                        return {
                            adjustStatusCode: snap.adjustStatusCode,
                            adjustStatusDesc: snap.adjustStatusDesc
                        };
                    });
                }
            });
    }

    async selectData() {
        this.shops = [];
        this.notClosing = [];
        let config = {
            load: {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'load',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.url,
                    'token': this.rawMockLocationId[3]
                }
            },
            nwht: {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'list-ar-tracking',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.url,
                    'token': this.rawMockLocationId[3]
                }
            },
            wht: {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'list-ar-tracking-wht',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.url,
                    'token': this.rawMockLocationId[3]
                }
            },
            payment: {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'list-myview-shop-payment',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.url,
                    'token': this.rawMockLocationId[3]
                }
            },
            detail: {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'list-myview-shop-det',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.url,
                    'token': this.rawMockLocationId[3]
                }
            }
        }
        let query = '?';
        query += this.date ? 'dateFrom=' + this.date : '';
        await axios.default.request({
            url: this.Endpoint.API_LOAD_BANK_SLA,
            method: 'GET',
            headers: config.load.headers,
        })
            .then(snapShot => {
                this.global.checkTokenExpired(snapShot.data.statusCode);
                snapShot.data.data.infoList.map(snapData => {
                    if (snapData.slaType == "IN") {
                        if (snapData.slaPaymentType == "CSH") {
                            this.trans.csh = snapData.slaDate;
                        }
                        if (snapData.slaPaymentType == "CHQ") {
                            this.trans.chq = snapData.slaDate;
                        }
                        if (snapData.slaPaymentType == "CC") {
                            this.trans.cc = snapData.slaDate;
                        }
                        if (snapData.slaPaymentType == "WHT") {
                            this.trans.wht = snapData.slaDate;
                        }
                    }
                });
            });
        // await axios.default.get(`${this.Endpoint.API_LOAD_AR_DETAIL}${query}`, config.nwht)
        await axios.default.request({
            url: this.Endpoint.API_LOAD_AR_DETAIL,
            method: 'GET',
            headers: config.nwht.headers,
            params: {
                dateFrom: this.date ? this.date : null
            }
        })
            .then(snapShot => {
                this.global.checkTokenExpired(snapShot.data.statusCode);
                this.dateVal = snapShot.data.data.arInfo.docDate;
                this.t1Date = Number(moment(moment(this.dateVal).add(this.trans.csh, 'days')));
                this.t2Date = Number(moment(moment(this.dateVal).add(this.trans.chq, 'days')));
                this.t3Date = Number(moment(moment(this.dateVal).add(this.trans.cc, 'days')));
                this.t4Date = Number(moment(moment(this.dateVal).add(this.trans.wht, 'days')));
                let data = snapShot.data.data.arInfo;
                let unclosing = snapShot.data.data.arTrackingShopEodInfo;
                unclosing.map(unclose => {
                    if (unclose.locationCode) {
                        this.notClosing.push(unclose);
                        this.preLocation.push({
                            locationCode: unclose.locationCode,
                            locationNameEn: unclose.locationNameEn,
                            locationNameTh: unclose.locationNameTh
                        })
                    }
                })
                this.locationList = this.notClosing.map(item => item.locationCode).filter((value, index, self) => self.indexOf(value) === index);
                this.preLocationObj = [];
                this.locationList.map(location => {
                    let preLocationNameEn;
                    let preLocationNameTh;
                    if(location) {
                        this.preLocation.map( locationObj => {
                            if(locationObj.locationCode == location) {
                                preLocationNameEn = locationObj.locationNameEn
                                preLocationNameTh = locationObj.locationNameTh 
                            }
                        })
                    }
                        this.preLocationObj.push({
                            locationCode: location,
                            locationNameEn: preLocationNameEn,
                            locationNameTh: preLocationNameTh
                        })
                });
                if(this.preLocationObj.length) {
                    this.notClosing = this.preLocationObj;
                }
                
                data.arLocTrckInfoList.map(snapShot => {
                    if (this.notClosing.indexOf(snapShot.locationCode) == -1) {
                        let preData = {
                            csh: {
                                shop: {
                                    paymentTypeCode: 'CSH',
                                    shopAmt: null
                                },
                                bank: {
                                    paymentTypeCode: 'CSH',
                                    bankAmt: null,
                                    diffAmt: null,
                                    checkDiffAmt: null,
                                    outstandingDiffAmt: null,
                                    postGlFlag: null,
                                    reconcilePaymentId: null,
                                    reconcileId: null
                                }
                            },
                            chq: {
                                shop: {
                                    paymentTypeCode: 'CHQ',
                                    shopAmt: null
                                },
                                bank: {
                                    paymentTypeCode: 'CHQ',
                                    bankAmt: null,
                                    diffAmt: null,
                                    checkDiffAmt: null,
                                    outstandingDiffAmt: null,
                                    postGlFlag: null,
                                    reconcilePaymentId: null,
                                    reconcileId: null
                                }
                            },
                            cc: {
                                shop: {
                                    paymentTypeCode: 'CC',
                                    shopAmt: null
                                },
                                bank: {
                                    paymentTypeCode: 'CC',
                                    bankAmt: null,
                                    diffAmt: null,
                                    checkDiffAmt: null,
                                    outstandingDiffAmt: null,
                                    postGlFlag: null,
                                    reconcilePaymentId: null,
                                    reconcileId: null
                                }
                            },
                            wht: {
                                shop: {
                                    paymentTypeCode: 'WHT',
                                    shopAmt: null
                                },
                                bank: {
                                    paymentTypeCode: 'WHT',
                                    bankAmt: null,
                                    diffAmt: null,
                                    checkDiffAmt: null,
                                    outstandingDiffAmt: null,
                                    postGlFlag: null,
                                    reconcileWhtId: null,
                                }
                            }
                        };
                        snapShot.arPaymentInfoList.map(list => {
                            if (list.paymentTypeCode === 'CSH') {
                                preData.csh.shop.shopAmt += list.shopAmt;
                                preData.csh.bank.bankAmt += list.bankAmt;
                                preData.csh.bank.checkDiffAmt += list.checkDiffAmt;
                                preData.csh.bank.outstandingDiffAmt += list.outstandingDiffAmt;
                                preData.csh.bank.diffAmt += list.diffAmt;
                                preData.csh.bank.postGlFlag = list.postGlFlag;
                                preData.csh.bank.reconcileId = list.reconcilePaymentId;
                                preData.csh.bank.reconcilePaymentId = list.reconcilePaymentId;
                            } else if (list.paymentTypeCode === 'CHQ') {
                                preData.chq.shop.shopAmt += list.shopAmt;
                                preData.chq.bank.bankAmt += list.bankAmt;
                                preData.chq.bank.checkDiffAmt += list.checkDiffAmt;
                                preData.chq.bank.outstandingDiffAmt += list.outstandingDiffAmt;
                                preData.chq.bank.diffAmt += list.diffAmt;
                                preData.chq.bank.postGlFlag = list.postGlFlag;
                                preData.chq.bank.reconcileId = list.reconcilePaymentId;
                                preData.chq.bank.reconcilePaymentId = list.reconcilePaymentId;
                            } else if (list.paymentTypeCode === 'CC') {
                                preData.cc.shop.shopAmt += list.shopAmt;
                                preData.cc.bank.bankAmt += list.bankAmt;
                                preData.cc.bank.checkDiffAmt += list.checkDiffAmt;
                                preData.cc.bank.outstandingDiffAmt += list.outstandingDiffAmt;
                                preData.cc.bank.diffAmt += list.diffAmt;
                                preData.cc.bank.postGlFlag = list.postGlFlag;
                                preData.cc.bank.reconcileId = list.reconcilePaymentId;
                                preData.cc.bank.reconcilePaymentId = list.reconcilePaymentId;
                            }
                        })
                        this.shops.push({
                            locationCode: snapShot.locationCode,
                            locationNameTh: snapShot.locationNameTh,
                            locationNameEn: snapShot.locationNameEn,
                            arPaymentInfoList: preData
                        })
                    }
                });
            })
        // await axios.default.get(`${this.Endpoint.API_LOAD_AR_DETAIL}${query}`, config.wht)
        await axios.default.request({
            url: this.Endpoint.API_LOAD_AR_DETAIL,
            method: 'GET',
            headers: config.wht.headers,
            params: {
                dateFrom: this.date ? this.date : null

            }
        }).then(snapShot => {
            this.global.checkTokenExpired(snapShot.data.statusCode);
            let data = snapShot.data.data.arInfo;
            data.arLocTrckInfoList.map(snapShot => {
                let wht = {
                    shop: {
                        paymentTypeCode: 'WHT',
                        shopAmt: null
                    },
                    bank: {
                        paymentTypeCode: 'WHT',
                        bankAmt: null,
                        diffAmt: null,
                        checkDiffAmt: null,
                        outstandingDiffAmt: null,
                        postGlFlag: null,
                        reconcileWhtId: null,
                    }
                }
                wht.shop.shopAmt = snapShot.arPaymentInfoList[0].shopAmt;
                wht.bank.bankAmt = snapShot.arPaymentInfoList[0].bankAmt;
                wht.bank.outstandingDiffAmt = snapShot.arPaymentInfoList[0].outstandingDiffAmt;
                wht.bank.diffAmt = snapShot.arPaymentInfoList[0].diffAmt;
                wht.bank.checkDiffAmt = snapShot.arPaymentInfoList[0].checkDiffAmt;
                wht.bank.postGlFlag = snapShot.arPaymentInfoList[0].postGlFlag;
                wht.bank.reconcileWhtId = snapShot.arPaymentInfoList[0].reconcileWhtId;

////////////////////// WHT per Record//////////////////////////
                // snapShot.arPaymentInfoList.map(list => { 
                    // if (list.paymentTypeCode === 'CSH') {
                    //     wht.shop.shopAmt += list.shopAmt;
                    //     wht.bank.bankAmt += list.bankAmt;
                    //     wht.bank.outstandingDiffAmt += list.outstandingDiffAmt;
                    //     wht.bank.diffAmt += list.diffAmt;
                    //     wht.bank.checkDiffAmt += list.checkDiffAmt;
                    //     wht.bank.postGlFlag = list.postGlFlag;
                    //     wht.bank.reconcileWhtId = list.reconcileWhtId;
                    // } else if (list.paymentTypeCode === 'CHQ') {
                    //     wht.shop.shopAmt += list.shopAmt;
                    //     wht.bank.bankAmt += list.bankAmt;
                    //     wht.bank.outstandingDiffAmt += list.outstandingDiffAmt;
                    //     wht.bank.diffAmt += list.diffAmt;
                    //     wht.bank.checkDiffAmt += list.checkDiffAmt;
                    //     wht.bank.postGlFlag = list.postGlFlag;
                    //     wht.bank.reconcileWhtId = list.reconcileWhtId;
                    // } else if (list.paymentTypeCode === 'CC') {
                    //     wht.shop.shopAmt += list.shopAmt;
                    //     wht.bank.bankAmt += list.bankAmt;
                    //     wht.bank.outstandingDiffAmt += list.outstandingDiffAmt;
                    //     wht.bank.diffAmt += list.diffAmt;
                    //     wht.bank.checkDiffAmt += list.checkDiffAmt;
                    //     wht.bank.postGlFlag = list.postGlFlag;
                    //     wht.bank.reconcileWhtId = list.reconcileWhtId;
                    // }
                // })
                if (wht.bank) {
                    wht.bank.outstandingDiffAmt = parseFloat(wht.bank.outstandingDiffAmt).toFixed(2);
                    wht.bank.diffAmt = parseFloat(wht.bank.diffAmt).toFixed(2);
                    wht.bank.bankAmt = parseFloat(wht.bank.bankAmt).toFixed(2);
                    wht.bank.checkDiffAmt = parseFloat(wht.bank.checkDiffAmt).toFixed(2);
                }
                if (wht.shop) {
                    wht.shop.shopAmt = parseFloat(wht.shop.shopAmt).toFixed(2);
                }
                Object.keys(this.shops).forEach(keys => {
                    if (this.shops[keys].locationCode === snapShot.locationCode) {
                        this.shops[keys].arPaymentInfoList.wht = wht
                    }
                })
            });
        });
    }

    showModal(locCode, paymentType) {
        this.adjustData = [];
        if (paymentType == "Cash") {
            let data = {
                locationCode: locCode,
                paymentType: 'CSH',
                saleSlipDate: this.dateVal,
                taxId: '',
                maindocType: 'ar',
                date: moment(this.dateVal).format('DD/MM/YYYY'),
                whtFlag: 'N'
            }
            this.data = data
            this.box = 1
            this.display = true;
        } else if (paymentType == "Cheque") {
            let data = {
                locationCode: locCode,
                paymentType: 'CHQ',
                saleSlipDate: this.dateVal,
                taxId: '',
                maindocType: 'ar',
                date: moment(this.dateVal).format('DD/MM/YYYY'),
                whtFlag: 'N'
            }
            this.data = data;
            this.box = 1;
            this.display = true;
        } else if (paymentType == "Credit Card") {
            let data = {
                locationCode: locCode,
                paymentType: 'CC',
                saleSlipDate: this.dateVal,
                taxId: '',
                maindocType: 'ar',
                date: moment(this.dateVal).format('DD/MM/YYYY'),
                whtFlag: 'N'
            }
            this.data = data;
            this.box = 1;
            this.display = true;
        } else if (paymentType == "wht") {
            let data = {
                locationCode: locCode,
                paymentType: '',
                saleSlipDate: this.dateVal,
                taxId: '',
                maindocType: 'ar',
                date: moment(this.dateVal).format('DD/MM/YYYY'),
                whtFlag: 'Y'
            }
            this.data = data;
            this.box = 1;
            this.display = true;
        }
    }

    adjustBank(code, dataType, shopData, index) {
        this.loading = true;
        this.filterData = [];
        this.reconcilePaymentId = shopData.reconcilePaymentId;
        this.reconcileWhtId = shopData.reconcileWhtId;
        this.backupDate = {
            code: code,
            dataType: dataType,
            shopData: shopData,
            index: index
        }
        this.box = 2;
        this.display = true;
        this.sendData = {
            reconcileId: null,
            reconcileWhtId: null,
            reconcileWhtCompId : null,
            reconcileWhtDetId: null,
            reconcileDetId: null,
            reconcilePaymentId: null,
            reconcileCompanyId: null,
            company: '',
            shopAmt: shopData.shopAmt,
            customerName: '',
            adjustDocId: 0,
            adjustMasterId: undefined,
            adjustMasterType: '',
            status: '',
            whtFlag: '',
            diffAmt: shopData.diffAmt,
            checkDiffAmt: shopData.checkDiffAmt,
            paymentType: '',
            saleSlipDate: this.dateVal,
            taxNo: '',
            chequeNo: '',
            ostDiff: shopData.outstandingDiffAmt,
            locationCode: code,
            dataType: dataType,
            paymentTypeCode: shopData.paymentTypeCode,
            note: '',
            bankAccNo: null,
            postGlFlag: false
        };
        this.conditionPaging.locationCode = code;
        if (dataType == 'wht') {
            this.conditionPaging.whtFlag = 'Y';
        } else {
            this.conditionPaging.whtFlag = 'N';
        }
        this.conditionPaging.paymentTypeCode = shopData.paymentTypeCode;
        this.conditionPaging.dataType = dataType;
        this.loading = false;
        this.searchAdjustBank(1);
        this.paginator.firstPaging = 0;
    }

    searchAdjustBank(page) {
        this.loading = true;
        let config = {
            headers: {
                'Content-Type': 'application/json',
                'action': 'search',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }
        }
        // Everywhere in search add adjustDocType with AR OR SH ** Coding && Testing
        // let query = '?';
        // query += page ? 'currentPage=' + page : '';
        // query += this.dateVal ? '&mainDocDate=' + moment(this.dateVal).add(7, 'hours').format('x') : '';
        // query += this.conditionPaging.locationCode ? '&locationCode=' + this.conditionPaging.locationCode : '';
        // query += this.sendData.paymentTypeCode ? '&paymentTypeCode=' + this.sendData.paymentTypeCode : '';
        // query += this.conditionPaging.whtFlag ? '&whtFlag=' + this.conditionPaging.whtFlag : '';
        // query += '&adjustDocType=AR';
        //axios.default.get(`${this.Endpoint.API_SRCH_ADJ}${query}`, config)
        axios.default.request({
            url: this.Endpoint.API_SRCH_ADJ,
            method: 'GET',
            headers: config.headers,
            params: {
                currentPage: page ? page : null,
                mainDocDate: this.dateVal ? moment(this.dateVal).add(7, 'hours').format('x') : null,
                locationCode: this.conditionPaging.locationCode ? this.conditionPaging.locationCode : null,
                paymentTypeCode: this.sendData.paymentTypeCode ? this.sendData.paymentTypeCode == 'WHT' ? null : this.sendData.paymentTypeCode : null,
                whtFlag: this.conditionPaging.whtFlag ? this.conditionPaging.whtFlag : null,
                adjustDocType: 'AR'
            }
        }).then(snapShot => {
            this.global.checkTokenExpired(snapShot.data.statusCode);
            this.loading = false;
            let data = snapShot.data.data;
            data.adjSrchRespList.map(data => {
                data.adjustStatus = this.global.setStatus(data.adjustStatus);
                this.adjustType.map(type => {
                    if (type.adjustMasterId == data.adjustMasterId) {
                        data.adjustMasterType = type.adjustType;
                    }
                })
            })
            this.filterData = data.adjSrchRespList;
            data.adjSrchRespList.map(snap => {
                this.sendData.postGlFlag = snap.postGlDate != null ? true : false;
            })
            this.paginator.pagingInfo = data.pagingInfo
        });
    }

    decrease(event) {
        this.selectData();
    }

    async addAdjustBank() {
        this.sendData.note = '';
        let config = {
            headers: {
                'Content-Type': 'application/json',
                'action': 'list',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }
        }
        if (this.sendData.dataType == 'Cash') {
            this.box = 3;
            this.sendData.company = 'DTN';
            this.sendData.reconcilePaymentId = this.reconcilePaymentId;
            // let query = '?';
            // query += this.dateVal ? 'docDate=' + this.dateVal : '';
            // query += this.conditionPaging.locationCode ? '&locationCode=' + this.conditionPaging.locationCode : '';
            // query += this.sendData.paymentTypeCode ? '&paymentTypeCode=' + this.sendData.paymentTypeCode : '';
            // query += '&whtFlag=N';
            // axios.default.get(`${this.Endpoint.API_LOAD_AR_COMPANY}${query}`, config)
            // .then(snapShot => {
            //     snapShot.data.data.arReclCompInfoList.map(data => {
            //         this.sendData.adjustMasterId
            //     });
            // })
        }else {
            let vm = this;
            this.box = 4;
            this.listCompanyData = [];
            // Everywhere in search add adjustDocType with AR OR SH ** Coding && Testing
            // let query = '?';
            // query += this.dateVal ? 'docDate=' + this.dateVal : '';
            // query += this.conditionPaging.locationCode ? '&locationCode=' + this.conditionPaging.locationCode : '';
            // query += this.sendData.paymentTypeCode ? '&paymentTypeCode=' + this.sendData.paymentTypeCode : '';
            // query += this.sendData.dataType == 'wht' ? '&whtFlag=Y' : '&whtFlag=N';
            // axios.default.get(`${this.Endpoint.API_LOAD_AR_COMPANY}${query}`, config)
            await axios.default.request({
                url: this.Endpoint.API_LOAD_AR_COMPANY,
                method: 'GET',
                headers: config.headers,
                params: {
                    docDate: this.dateVal ? this.dateVal : null,
                    locationCode: this.conditionPaging.locationCode ? this.conditionPaging.locationCode : null,
                    paymentTypeCode: this.sendData.paymentTypeCode ? this.sendData.paymentTypeCode : null,
                    whtFlag: this.sendData.dataType == 'wht' ? 'Y' : 'N'
                }
            })
                .then(snapShot => {
                    this.global.checkTokenExpired(snapShot.data.statusCode);
                    snapShot.data.data.arReclCompInfoList.map(data => {
                        if ((data.outstandingDiffAmt != null && data.outstandingDiffAmt != 0) || (data.checkDiffAmt != 0  && (this.sendData.dataType == 'Cheque' || this.sendData.dataType == 'wht'))) {
                            if (this.sendData.dataType == 'Credit Card') {
                                this.listCompanyData.push(
                                    {
                                        reconcileId: data.reconcileId,
                                        reconcilePaymentId: this.reconcilePaymentId,
                                        reconcileCompanyId: data.reconcileCompanyId,
                                        companyCode: data.companyCode,
                                        shopAmt: data.shopAmt,
                                        dataType: "Credit Card",
                                        diffAmt: data.outstandingDiffAmt,
                                        locationCode: this.conditionPaging.locationCode,
                                        note: data.note,
                                        outstandingDiffAmt: data.outstandingDiffAmt,
                                        paymentTypeCode: data.paymentTypeCode,
                                        saleSlipDate: this.dateVal,
                                    }
                                )
                            } else if (this.sendData.dataType == 'Cheque') {
                                this.listCompanyData.push(
                                    {
                                        reconcileId: data.reconcileId,
                                        reconcilePaymentId: this.reconcilePaymentId,
                                        reconcileCompanyId: data.reconcileCompanyId,
                                        companyCode: data.companyCode,
                                        shopAmt: data.shopAmt,
                                        dataType: "Cheque",
                                        chequeNo: data.chequeNo,
                                        diffAmt: data.outstandingDiffAmt,
                                        locationCode: this.conditionPaging.locationCode,
                                        note: data.note,
                                        outstandingDiffAmt: data.outstandingDiffAmt,
                                        paymentTypeCode: data.paymentTypeCode,
                                        saleSlipDate: vm.dateVal
                                    },
                                )
                            } else if (this.sendData.dataType == 'wht') {
                                if((data.outstandingDiffAmt != null && data.outstandingDiffAmt != 0))
                                {
                                    this.listCompanyData.push(
                                        {
                                            reconcileId: data.reconcileId,
                                            reconcilePaymentId: this.reconcilePaymentId,
                                            reconcileCompanyId: data.reconcileCompanyId,
                                            companyCode: data.companyCode,
                                            shopAmt: data.shopAmt,
                                            taxNo: data.taxNo,
                                            customerName: data.custName,
                                            paymentType: data.paymentTypeCode == 'CHQ' ? 'Cheque' : data.paymentTypeCode == 'CC' ? 'Credit Card' : 'Cash',
                                            dataType: "wht",
                                            diffAmt: data.outstandingDiffAmt,
                                            locationCode: this.conditionPaging.locationCode,
                                            outstandingDiffAmt: data.outstandingDiffAmt,
                                            paymentTypeCode: data.paymentTypeCode,
                                            saleSlipDate: this.dateVal
                                        },
                                    )
                                }
                                else{
                                }
                            }
                        }
                    });
                });
            // Waiting API Adjust Bank WHT!
        }
        this.display = true;
    }

    view(data) {
        this.sendData = {
            reconcileId: 0,
            reconcileWhtId: 0,
            reconcileWhtCompId: 0,
            reconcileDetId: 0,
            reconcileWhtDetId: 0,
            reconcilePaymentId: 0,
            reconcileCompanyId: 0,
            shopAmt: data.adjustAmt,
            saleSlipDate: data.adjustDate,
            adjustDocId: data.adjustDocId,
            adjustMasterId: data.adjustMasterId,
            adjustMasterType: data.adjustMasterType,
            note: data.adjustNote,
            status: data.adjustStatus,
            company: data.companyCode,
            customerName: data.customerName,
            locationCode: data.locationCode,
            paymentType: data.paymentTypeCode,
            whtFlag: data.whtFlag,
            diffAmt: data.adjustAmt,
            checkDiffAmt: data.checkDiffAmt,
            chequeNo: data.chequeNo,
            taxNo: data.taxNo,
            ostDiff: data.adjustAmt,
            dataType: data.paymentTypeCode == 'CSH' ? 'Cash' : data.paymentTypeCode == 'CHQ' ? 'Cheque' : data.paymentTypeCode == 'CC' ? 'Credit Card' : 'wht',
            paymentTypeCode: data.paymentTypeCode,
            bankAccNo: data.bankAccNo,
            postGlFlag: data.postGlFlag
        }
        this.setBank(data);
        this.box = 5;
        this.display = true;
    }



    setBank(data) {
        axios.default.request({
            url: this.Endpoint.API_BANK_ADJUST_AR,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-by-id',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }, params: {
                adjustMasterId: data.adjustMasterId
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.bankDes = snapData.data.data.adjMastInfoList.map(snap => {
                        return {
                            bankAccountName: snap.bankAccountName,
                            bankAccountNo: snap.bankAccountNo,
                            glAccount: snap.glAccount,
                            glRc: snap.glRc,
                            glProduct: snap.glProduct,
                            glProject: snap.glProject,
                            glCs: snap.glCs,
                            glFs: snap.glFs,
                            glSs: snap.glSs,
                            glRs: snap.glRs
                        };
                    });
                }
            });
    }

    edit(data) {
        this.sendData = {
            reconcileId: data.reconcileId,
            reconcileWhtId: this.reconcileWhtId,
            reconcileWhtCompId : data.reconcileWhtCompId,
            reconcileWhtDetId: data.reconcileWhtDetId,
            reconcileDetId: data.reconcileDetId,
            reconcilePaymentId: this.reconcilePaymentId,
            reconcileCompanyId: null,
            shopAmt: data.adjustAmt,
            diffAmt: this.sendData.diffAmt,
            checkDiffAmt: this.sendData.checkDiffAmt,
            saleSlipDate: data.adjustDate,
            adjustDocId: data.adjustDocId,
            adjustMasterId: data.adjustMasterId,
            adjustMasterType: data.adjustMasterType,
            note: data.adjustNote,
            status: data.adjustStatus,
            company: data.companyCode,
            customerName: data.customerName,
            locationCode: data.locationCode,
            paymentType: data.paymentTypeCode,
            whtFlag: data.whtFlag,
            chequeNo: data.chequeNo,
            taxNo: data.taxNo,
            ostDiff: data.adjustAmt,
            dataType: data.paymentTypeCode == 'CSH' ? 'Cash' : data.paymentTypeCode == 'CHQ' ? 'Cheque' : data.paymentTypeCode == 'CC' ? 'Credit Card' : 'wht',
            paymentTypeCode: data.paymentTypeCode,
            bankAccNo: data.bankAccNo,
            postGlFlag: data.postGlFlag
        }
        this.box = 3;
        this.display = true;
    }

    callList(data, page) {
        this.dataCallList = data;
        this.box = 6;
        this.listAdjustData = [];
        let config = {
            headers: {
                'Content-Type': 'application/json',
                'action': 'list-by-pmnt-comp',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }
        }
        axios.default.request({
            url: this.Endpoint.API_LOAD_AR_COMPANY,
            method: 'GET',
            headers: config.headers,
            params: {
                currentPage: page,
                docDate: this.dateVal ? this.dateVal : null,
                locationCode: this.conditionPaging.locationCode ? this.conditionPaging.locationCode : null,
                paymentTypeCode: this.conditionPaging.paymentTypeCode ? this.conditionPaging.paymentTypeCode : null,
                companyCode: data.companyCode ? data.companyCode : null,
                whtFlag: this.conditionPaging.whtFlag
            }
        })
            .then(snapShot => {
                this.global.checkTokenExpired(snapShot.data.statusCode);
                snapShot.data.data.arReclPmntCompInfoList.map(snapData => {
                    let preData = snapData;
                    preData = Object.assign({}, preData, {
                        saleSlipDate: data.saleSlipDate,
                        /*    docDate: this.closingData.docDate */
                    });
                    this.listAdjustDetail.company = snapData.companyCode;
                    this.listAdjustDetail.dataType = data.dataType;
                    // if (snapData.paymentTypeCode == 'CHQ') {
                    //     this.listAdjustDetail.dataType = 'Cheque';
                    // } else if (snapData.paymentTypeCode == 'CSH') {
                    //     this.listAdjustDetail.dataType = 'Cash';
                    // } else if (snapData.paymentTypeCode == 'CC') {
                    //     this.listAdjustDetail.dataType = 'Credit Card'
                    // }
                    this.listAdjustData.push(preData);
                })
                this.paginator = snapShot.data.data.pagingInfo;
            });
    }

    async callAdjust(data) {
        let config = {
            whtsummary: {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'list-by-comp-wht',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.url,
                    'token': this.rawMockLocationId[3]
                }
            }
        }
        if(data.dataType == 'wht'){
            await axios.default.request({
                url: this.Endpoint.API_LOAD_AR_COMPANY,
                method: 'GET',
                headers: config.whtsummary.headers,
                params: {
                    locationCode: this.conditionPaging.locationCode ? this.conditionPaging.locationCode : null,
                    companyCode: data.companyCode,
                    docDate: this.dateVal ? moment(this.dateVal).add(7, 'hours').format('x') : null,
                }
            })
            .then(snapShot => {
                this.global.checkTokenExpired(snapShot.data.statusCode);
                this.sendData = {
                    reconcileId: snapShot.data.data.arReclCompInfo.reconcileId,
                    reconcileWhtId: this.reconcileWhtId,
                    reconcileWhtCompId : snapShot.data.data.arReclCompInfo.reconcileWhtCompId,
                    reconcileWhtDetId: data.reconcileWhtDetId,
                    reconcileDetId: data.reconcileDetId,
                    reconcilePaymentId: this.reconcilePaymentId,
                    reconcileCompanyId:data.reconcileCompanyId,
                    company: snapShot.data.data.arReclCompInfo.companyCode,
                    shopAmt: snapShot.data.data.arReclCompInfo.shopAmt,
                    customerName: data.customerName,
                    diffAmt: snapShot.data.data.arReclCompInfo.outstandingDiffAmt,
                    checkDiffAmt: data.checkDiffAmt,
                    chequeNo: data.chequeNo,
                    adjustDocId: data.adjustDocId,
                    adjustMasterId: data.adjustMasterId,
                    adjustMasterType: '',
                    status: '',
                    whtFlag: data.dataType == 'wht' ? 'Y' : 'N',
                    paymentType: data.paymentTypeCode,
                    saleSlipDate: this.dateVal,
                    taxNo: data.taxId,
                    ostDiff: snapShot.data.data.arReclCompInfo.outstandingDiffAmt,
                    locationCode: this.conditionPaging.locationCode,
                    dataType: data.dataType == 'wht' ? data.dataType  : data.paymentTypeCode == 'CSH' ? 'Cash' : data.paymentTypeCode == 'CC' ? 'Credit Card' : data.paymentTypeCode == 'CHQ' ? 'Cheque' : '',
                    //////////////////////////// WHT per Record ///////////////////////////////////////
                    // dataType: this.listAdjustDetail.dataType == 'WHT' ? this.listAdjustDetail.dataType : data.paymentTypeCode == 'CSH' ? 'Cash' : data.paymentTypeCode == 'CC' ? 'Credit Card' : data.paymentTypeCode == 'CHQ' ? 'Cheque' : '',
                    paymentTypeCode: snapShot.data.data.arReclCompInfo.paymentTypeCode,
                    note: data.note,
                    bankAccNo: data.bankAccNo,
                    postGlFlag: data.postGlFlag
                };
            })
        }else{
        this.sendData = {
            reconcileId: data.reconcileId,
            reconcileWhtId: this.reconcileWhtId,
            reconcileWhtCompId : data.reconcileWhtCompId,
            reconcileWhtDetId: data.reconcileWhtDetId,
            reconcileDetId: data.reconcileDetId,
            reconcilePaymentId: this.reconcilePaymentId,
            reconcileCompanyId: data.reconcileCompanyId,
            company: data.companyCode,
            shopAmt: data.shopAmt,
            customerName: data.customerName,
            diffAmt: data.outstandingDiffAmt,
            checkDiffAmt: data.checkDiffAmt,
            chequeNo: data.chequeNo,
            adjustDocId: data.adjustDocId,
            adjustMasterId: data.adjustMasterId,
            adjustMasterType: '',
            status: '',
            whtFlag: data.dataType == 'wht' ? 'Y' : 'N',
            paymentType: data.paymentTypeCode,
            saleSlipDate: this.dateVal,
            taxNo: data.taxId,
            ostDiff: data.outstandingDiffAmt,
            locationCode: this.conditionPaging.locationCode,
            dataType: data.dataType == 'wht' ? data.dataType  : data.paymentTypeCode == 'CSH' ? 'Cash' : data.paymentTypeCode == 'CC' ? 'Credit Card' : data.paymentTypeCode == 'CHQ' ? 'Cheque' : '',
            //////////////////////////// WHT per Record ///////////////////////////////////////
            // dataType: this.listAdjustDetail.dataType == 'WHT' ? this.listAdjustDetail.dataType : data.paymentTypeCode == 'CSH' ? 'Cash' : data.paymentTypeCode == 'CC' ? 'Credit Card' : data.paymentTypeCode == 'CHQ' ? 'Cheque' : '',
            paymentTypeCode: data.paymentTypeCode,
            note: data.note,
            bankAccNo: data.bankAccNo,
            postGlFlag: data.postGlFlag
        };
    }
        this.box = 3;
        this.display = true;
    }

    paginate(event) {
        if (this.paginator.firstPaging != event.first) {
            this.paginator.firstPaging = event.first;
            this.searchAdjustBank(event.page + 1);
        }

        if ((event.page + 1) != this.paginator.currentPage) {
            this.callList(this.dataCallList, event.page + 1);
        }
    }

    paginateAdj(event) {
        if (this.paginator.firstPaging != event.first) {
            this.paginator.firstPaging = event.first;
            this.searchAdjustBank(event.page + 1);
        }
    }

    async onClose(agreed: boolean) {
        agreed ? this.display = false : this.display = true;
        this.display = true;
        await this.searchAdjustBank((this.paginator.firstPaging / 10) + 1);
        await this.selectData();
        var fetchData = this.backupDate.shopData.paymentTypeCode == 'wht' ? this.shops[this.backupDate.index].arPaymentInfoList.wht.bank : this.backupDate.shopData.paymentTypeCode == 'CSH' ? this.shops[this.backupDate.index].arPaymentInfoList.csh.bank : this.backupDate.shopData.paymentTypeCode == 'CHQ' ? this.shops[this.backupDate.index].arPaymentInfoList.chq.bank : this.backupDate.shopData.paymentTypeCode == 'CC' ? this.shops[this.backupDate.index].arPaymentInfoList.cc.bank : [];
        await this.adjustBank(this.backupDate.code, this.backupDate.dataType, fetchData, this.backupDate.index);
    }

    goBack() {
        this.router.navigate(['/reconciliation/main-page-ar'], { queryParams: { docSearch: this.docSearch, status: this.status } });
    }

    stockData(data) {
        if (data.whtFlag == 'Y') {
            this.stockDelete = {
                reconcileWhtId: this.reconcileWhtId,
                adjustDocId: data.adjustDocId,
                reconcilePaymentId: this.reconcilePaymentId,
                paymentTypeCode: undefined,
                companyCode: data.companyCode,
                whtFlag: data.whtFlag,
                locationCode: data.locationCode,
                refAdjustDocId: data.refAdjustDocId ? data.refAdjustDocId == 0 ? null : data.refAdjustDocId : null
            }
        } else {
            this.stockDelete = {
                reconcileWhtId: undefined,
                adjustDocId: data.adjustDocId,
                reconcilePaymentId: this.reconcilePaymentId,
                paymentTypeCode: data.paymentTypeCode,
                companyCode: data.companyCode,
                whtFlag: data.whtFlag,
                locationCode: data.locationCode,
                refAdjustDocId: data.refAdjustDocId ? data.refAdjustDocId == 0 ? null : data.refAdjustDocId : null
            }
        }
        this.boxAlert = 1;
    }

    cancel() {
        this.boxAlert = 0;
        this.stockDelete = {
            reconcilePaymentId: 0,
            reconcileWhtId: 0,
            adjustDocId: 0,
            paymentTypeCode: '',
            companyCode: '',
            whtFlag: '',
            locationCode: '',
            refAdjustDocId: ''
        }
    }

    delete() {
        this.msg = '';
        let data = this.stockDelete;
        let config = {
            headers: {
                'Content-Type': 'application/json',
                'action': 'remove',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }
        }
        axios.default.request({
            url: this.Endpoint.API_POST_AR_ADJUST,
            headers: config.headers,
            method: 'DELETE',
            data: data
        })
            .then(response => {
                this.global.checkTokenExpired(response.data.statusCode);
                if (response.data.statusCode == 'S0000') {
                    this.msg = 'success';
                    this.onClose(true);
                } else if (response.data.statusCode == 'B0014') {
                    this.msg = 'isref';
                } else {
                    this.msg = 'fail';
                }
            })
        this.boxAlert = 0;
    }

    checkEdit(data) {
        this.boxedit = 0;
        this.msgUd = '';
        axios.default.request({
            url: this.Endpoint.API_ADD_ADJ,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'action': 'valid-upd-ref',
                'transactionId': uuid(),
                'username': this.rawMockLocationId[1],
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }, data: {
                adjUpdInfo: {
                    adjustDocId: data.adjustDocId
                }
            }
        }).then(snap => {
            if (snap.data.data.adjustRefFlag == true) {
                this.edit(data)
            }else{
                this.boxedit = 1;
                this.display = true;
                this.msgUd = 'false';
            }
        })

        // this.editAdjust = data
        // this.box = 3;
        // this.display = true;
    }

    cancelEdit() {
        
        this.boxedit = 0;
        var fetchData = this.backupDate.shopData.paymentTypeCode == 'wht' ? this.shops[this.backupDate.index].arPaymentInfoList.wht.bank : this.backupDate.shopData.paymentTypeCode == 'CSH' ? this.shops[this.backupDate.index].arPaymentInfoList.csh.bank : this.backupDate.shopData.paymentTypeCode == 'CHQ' ? this.shops[this.backupDate.index].arPaymentInfoList.chq.bank : this.backupDate.shopData.paymentTypeCode == 'CC' ? this.shops[this.backupDate.index].arPaymentInfoList.cc.bank : [];
        this.adjustBank(this.backupDate.code, this.backupDate.dataType, fetchData, this.backupDate.index);
        this.display = true;
    }


    async onSwitch(agreed: any) {
        await this.searchAdjustBank((this.paginator.firstPaging / 10) + 1);
        await this.selectData();
        this.sendData.dataType = agreed;
        if (agreed == 'Credit Card') {
            this.box = 4;
            this.display = true;
            this.addAdjustBank();
        } else if (agreed == 'Cheque') {
            this.box = 6;
            this.display = true;
            this.callList(this.dataCallList, 1);
        } else if (agreed == 'Cash' || agreed == 'Update') {
            this.box = 2;
            this.display = true;
            var fetchData = this.backupDate.shopData.paymentTypeCode == 'wht' ? this.shops[this.backupDate.index].arPaymentInfoList.wht.bank : this.backupDate.shopData.paymentTypeCode == 'CSH' ? this.shops[this.backupDate.index].arPaymentInfoList.csh.bank : this.backupDate.shopData.paymentTypeCode == 'CHQ' ? this.shops[this.backupDate.index].arPaymentInfoList.chq.bank : this.backupDate.shopData.paymentTypeCode == 'CC' ? this.shops[this.backupDate.index].arPaymentInfoList.cc.bank : [];
            this.adjustBank(this.backupDate.code, this.backupDate.dataType, fetchData, this.backupDate.index);
        } else if (agreed == 'wht') {
            this.box = 4;
            this.display = true;
            this.addAdjustBank();
////////////////// WHT per Record///////////////////////////////////////
            // this.box = 6;
            // this.callList(this.dataCallList, 1)
            // this.display = true;

        }
    }
}
