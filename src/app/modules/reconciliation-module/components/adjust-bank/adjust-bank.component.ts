import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import * as axios from 'axios';
import { v4 as uuid } from 'uuid';
import * as moment from 'moment';
import { Endpoint } from '../../../../common/const/endpoint';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-adjust-bank',
    templateUrl: './adjust-bank.component.html',
    styleUrls: ['./adjust-bank.component.css']
})
export class AdjustBankComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];
    @Output() disClose = new EventEmitter<boolean>();
    @Output() decreaseAmount = new EventEmitter<number>();
    @Output() disSwitch = new EventEmitter<any>();
    display: boolean = false;
    type: any = [];
    company: Array<object> = [];
    adjustType: Array<any> = [];
    adjustStatus: Array<object> = [];
    resolve: boolean = false;
    merchant: Array<any> = [];
    disableCancel: boolean = false;
    disableSave: boolean = false;
    status: any = 1;
    locationList = [];
    bank: Array<any> = [];
    errorStatus: boolean = false;
    errorMessage: string = '';
    merchantId: any = [];
    adjustData = [];
    cnFlag: string = '';
    filterData = [];
    dateVal = Date.now();
    detail: any;
    key = '';
    url: any;
    msg: any;
    ostDiff: any;
    amount: any;
    fixAccountFlag: string = null;
    bankDes: Array<any> = [];
    bankAccNo: any;
    checkAmtFlag: any;
    checkAmtValue: any;
    active: any;
    dataAdjustType: string = '';
    docType:any;
    checkData = 1;
    paymentType: Array<any> =[];
    //paging = 0;

    data: any;
    form: any = {
        pagingInfo: {
            currentPage: 1
        },
        adjSrchReq: {
            company: null,
            paymentType: null,
            locationCode: '',
            adjustType: undefined,
            status: 1,
            amount: null,
            saleSlipDate: null,
            bankEdc: null,
            merchantId: null,
            ChequeNo: null,
            adjustNo: null

        }
    };
    filterPagination: Array<object> = [];
    paginator: any = {
        pageCount: 0,
        rows: 3,
        data: [],
        offset: 0
    };
    backupAmount: number = 0;
    @Input() rawData = {
        reconcileId: 0,
        reconcileWhtId: 0,
        reconcileWhtCompId :0,
        reconcileWHTCompanyId: 0,
        reconcileWhtDetId: 0,
        mainDocDate: 0,
        reconcileDetId: 0,
        reconcileCompanyId: 0,
        adjustDocId: 0,
        key: '',
        paymentTypeCode: '',
        adjustMasterId: null,
        adjustMasterType: null,
        ostDiff: 0,
        company: '',
        diff: 0,
        saleSlipDate: '',
        dataType: '',
        amount: null,
        note: '',
        last4Digi: null,
        merchartId: undefined,
        bankEdc: undefined,
        status: 1,
        locationCode: 0,
        adjustNo: 0,
        taxNo: '',
        customerName: '',
        whtFlag: '',
        companyCode: '',
        reconcilePaymentId: 0,
        bankAccNo: null,
        glAccount: null,
        glCs: null,
        glFs: null,
        glProduct: null,
        glProject: null,
        glRc: null,
        glRs: null,
        glSs: null,
        chequeNo: null
    };

    Location = [
        { code: '02131', name: 'Dtac Hall Future Park Rangsit' },
        { code: '02668', name: 'Dtac Hall CentralPlaza Chaengwattana' },
        { code: '02951', name: 'Dtac Hall Lotus Bangyai' },
        { code: '02705', name: 'Dtac Hall Paradise Park' }
    ];
    constructor(private router: Router, private global: AppComponent, private endpoint: Endpoint) { }


    checkAmtWithFlag(type){
        this.msg = '';
        let amt = Number(this.amount.replace(/,/g, ''));
        if(this.checkAmtFlag == 'Y'){
            if(this.checkAmtValue < Math.abs(amt)){
                this.msg = 'overflow';
            } else {
               this.onSave(type);
            }
          }else{
            this.onSave(type);
          }
    }


    onSave(type){
        if(this.fixAccountFlag == 'N'){
            if (this.rawData.adjustMasterId == null ||
                this.rawData.adjustMasterId == undefined ||
                this.rawData.adjustMasterId == 'undefined' ||
                this.rawData.adjustMasterId == ''  || 
                this.status == null ||
                this.status == undefined ||
                this.status == '' ||
                this.amount == null ||
                this.amount == undefined ||
                this.amount == '' || 
                this.amount == 0 || 
                this.bankAccNo == null ||
                this.bankAccNo == undefined ||
                this.bankAccNo == 'null' ||
                this.bankAccNo == ''
            ) {
                this.msg = 'fail';
            } else {
                this.save(type)
            }
        } else {
            if (this.rawData.adjustMasterId == null ||
                this.rawData.adjustMasterId == undefined ||
                this.rawData.adjustMasterId == 'undefined' ||
                this.rawData.adjustMasterId == ''  || 
                this.status == null ||
                this.status == undefined ||
                this.status == '' ||
                this.amount == 0 || 
                this.amount == null ||
                this.amount == undefined ||
                this.amount == ''
            ) {
                this.msg = 'fail';
            } else {
                this.save(type)
            }
        }
       
    }

    save(type) {
        this.disableSave = true;
        this.msg = '';
        if (type == 'save') {
            // if (Math.abs(this.rawData.ostDiff) < Math.abs(this.amount)) {
            //     this.msg = 'overflow';
            // } else {
            let config = {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'add',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.url,
                    'token': this.rawMockLocationId[3]
                }
            }
            if (this.rawData.whtFlag == 'Y') {
                let wht = {
                    reconcileId: this.rawData.reconcileId,
                    reconcileWhtId: this.rawData.reconcileId,
                    reconcileWhtCompId: this.rawData.reconcileWhtCompId,
                    reconcileWhtDetId: this.rawData.reconcileWhtDetId,
                    reconcileCompanyId: this.rawData.reconcileCompanyId,
                    locationCode: this.rawData.locationCode,
                    companyCode: this.rawData.company,
                    adjustMasterId: Number(this.rawData.adjustMasterId),
                    adjustAmt: Number(this.amount.replace(/,/g, '')),
                    adjustNote: this.rawData.note ? this.rawData.note : '',
                    whtFlag: 'Y',
                    adjustStatus: this.rawData.adjustNo ? '2' : this.status.toString(),
                    bankAccountNo: this.bankAccNo,
                    glAccount: this.rawData.glAccount,
                    glCs: this.rawData.glCs,
                    glFs: this.rawData.glFs,
                    glProduct: this.rawData.glProduct,
                    glProject: this.rawData.glProject,
                    glRc: this.rawData.glRc,
                    glRs: this.rawData.glRs,
                    glSs: this.rawData.glSs,
                    chequeNo: this.rawData.chequeNo ? this.rawData.chequeNo : null,
                    refAdjustDocId: this.rawData.adjustNo
                }
                axios.default.request({
                    url: this.endpoint.API_POST_AR_ADJUST,
                    method: 'POST',
                    data: wht,
                    headers: config.headers
                })
                    .then(snap => {
                    this.global.checkTokenExpired(snap.data.statusCode);
                        if(snap.data.statusCode == 'S0000'){
                            this.errorStatus = false;
                            this.errorMessage = '';
                        } else {
                            this.disableSave = false;
                            this.errorStatus = true;
                            this.errorMessage = 'ไม่สามารถเพิ่มรายการได้';
                        }
                        
                    }).catch((err) => {
                        this.disableSave = false;
                        this.errorStatus = true;
                        this.errorMessage = 'ไม่สามารถเพิ่มรายการได้';
                    })
            } else {
                let data = {
                    reconcileDetId: this.rawData.reconcileDetId,
                    locationCode: this.rawData.locationCode,
                    paymentTypeCode: this.rawData.paymentTypeCode,
                    companyCode: this.rawData.company,
                    adjustMasterId: Number(this.rawData.adjustMasterId),
                    adjustAmt: Number(this.amount.replace(/,/g, '')),
                    adjustNote: this.rawData.note ? this.rawData.note : '',
                    whtFlag: 'N',
                    adjustStatus: this.rawData.adjustNo ? '2' : this.status.toString(),
                    reconcilePaymentId: this.rawData.reconcilePaymentId,
                    reconcileCompanyId: this.rawData.reconcileCompanyId,
                    edcBank: this.rawData.bankEdc,
                    merchantId: this.rawData.merchartId,
                    saleSlipDate: this.rawData.saleSlipDate,
                    creditCardNo: this.rawData.last4Digi,
                    bankAccountNo: this.bankAccNo,
                    glAccount: this.rawData.glAccount,
                    glCs: this.rawData.glCs,
                    glFs: this.rawData.glFs,
                    glProduct: this.rawData.glProduct,
                    glProject: this.rawData.glProject,
                    glRc: this.rawData.glRc,
                    glRs: this.rawData.glRs,
                    glSs: this.rawData.glSs,
                    chequeNo: this.rawData.chequeNo ? this.rawData.chequeNo : null,
                    refAdjustDocId: this.rawData.adjustNo

                }
                axios.default.request({
                    url: this.endpoint.API_POST_AR_ADJUST,
                    method: 'POST',
                    data: data,
                    headers: config.headers
                })
                    .then(snap => {
                        this.global.checkTokenExpired(snap.data.statusCode);
                        if(snap.data.statusCode == 'S0000'){
                            this.errorStatus = false;
                            this.errorMessage = ''; 
                        } else {
                            this.errorStatus = true;
                            this.disableSave = false;
                            this.errorMessage = 'ไม่สามารถเพิ่มรายการได้'; 
                        }
                        
                    })
                    .catch((err) => {
                        this.disableSave = false;
                        this.errorStatus = true;
                        this.errorMessage = 'ไม่สามารถเพิ่มรายการได้';
                    })
            }
            // }
        } else if (type == 'update') {
            if(Number(this.backupAmount.toString().replace(/,/g, '')) >= Number(this.amount.toString().replace(/,/g, ''))) {
                let config = {
                    headers: {
                        'Content-Type': 'application/json',
                        'action': 'update',
                        'transactionId': uuid(),
                        'locationId': this.mockLocationId,
                        'lang': 'EN',
                        'username': this.rawMockLocationId[1],
                        'navigate': this.url,
                        'token': this.rawMockLocationId[3]
                    }
                }
                if (this.rawData.whtFlag == 'Y') {
                    let data = {
                        adjustDocId: this.rawData.adjustDocId,
                        adjustMasterId: Number(this.rawData.adjustMasterId),
                        adjustAmt: Number(this.amount.replace(/,/g, '')),
                        locationCode: this.rawData.locationCode,
                        companyCode: this.rawData.company,
                        whtFlag: this.rawData.whtFlag,
                        adjustStatus: this.status.toString(),
                        adjustNote: this.rawData.note,
                        reconcilePaymentId: this.rawData.reconcilePaymentId,
                        reconcileWhtId: this.rawData.reconcileWhtId,
                        bankAccountNo: this.bankAccNo,
                        glAccount: this.rawData.glAccount,
                        glCs: this.rawData.glCs,
                        glFs: this.rawData.glFs,
                        glProduct: this.rawData.glProduct,
                        glProject: this.rawData.glProject,
                        glRc: this.rawData.glRc,
                        glRs: this.rawData.glRs,
                        glSs: this.rawData.glSs
                    }
                    axios.default.request({
                        url: this.endpoint.API_POST_AR_ADJUST,
                        headers: config.headers,
                        method: 'PUT',
                        data: data
                    }).then(snap => {
                        this.global.checkTokenExpired(snap.data.statusCode);
                        if(snap.data.statusCode == 'S0000'){
                            this.errorStatus = false;
                            this.errorMessage = '';
                        } else {
                            this.disableSave = false;
                            this.errorStatus = true;
                            this.errorMessage = '';
                        }
                    }).catch(err => {
                        this.disableSave = false;
                        this.errorStatus = true;
                        this.errorMessage = 'ไม่สามารถแก้ไขรายการได้';
                    })
                } else {
                    let data = {
                        adjustDocId: this.rawData.adjustDocId,
                        adjustMasterId: Number(this.rawData.adjustMasterId),
                        adjustAmt: Number(this.amount.replace(/,/g, '')),
                        locationCode: this.rawData.locationCode,
                        paymentTypeCode: this.rawData.paymentTypeCode,
                        companyCode: this.rawData.company,
                        whtFlag: this.rawData.whtFlag,
                        adjustStatus: this.status.toString(),
                        adjustNote: this.rawData.note,
                        reconcilePaymentId: this.rawData.reconcilePaymentId,
                        bankAccountNo: this.bankAccNo,
                        glAccount: this.rawData.glAccount,
                        glCs: this.rawData.glCs,
                        glFs: this.rawData.glFs,
                        glProduct: this.rawData.glProduct,
                        glProject: this.rawData.glProject,
                        glRc: this.rawData.glRc,
                        glRs: this.rawData.glRs,
                        glSs: this.rawData.glSs
                    }
                    axios.default.request({
                        url: this.endpoint.API_POST_AR_ADJUST,
                        headers: config.headers,
                        method: 'PUT',
                        data: data
                    }).then(snap => {
                        this.global.checkTokenExpired(snap.data.statusCode);
                        if(snap.data.statusCode == 'S0000'){
                            this.errorStatus = false;
                            this.errorMessage = '';
                        } else {
                            this.disableSave = false;
                            this.errorStatus = true;
                            this.errorMessage = '';
                        }
                    })
                    .catch(err => {
                        this.disableSave = false;
                        this.errorStatus = true;
                        this.errorMessage = 'ไม่สามารถแก้ไขรายการได้';
                    })
                }
            } else {
                this.disableSave = false;
                this.errorStatus = true;
                this.errorMessage = 'ไม่สามารถแก้ไขรายการได้';
            }
        }
    }

    showDialog() {
        this.display = true;
        this.form.adjSrchReq.locationCode = '';
        if (this.rawData.ostDiff > 0) {
            this.form.adjSrchReq.amount = (Number(this.amount.replace(/,/g, '')) * (-1)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        } else {
            this.form.adjSrchReq.amount = (this.amount.replace(/,/g, '')).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        }

    }

    paginate(event) {
        this.form.pagingInfo.currentPage = (event.page + 1);
        this.onSearch(event.page+1);
    }

    onSearch(page) {
        let form = this.form;
        this.filterData = [];
        this.checkData = 1;
        form.adjSrchReq.status = Number(form.adjSrchReq.status);
        // if (this.paginator.currentPage != form.pagingInfo.currentPage) {
            axios.default.request({
                url: this.endpoint.API_SRCH_ADJ,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'search',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.url,
                    'token': this.rawMockLocationId[3]
                },
                params: {
                    currentPage: form.pagingInfo.currentPage,
                    companyCode: form.adjSrchReq.company? form.adjSrchReq.company : null,
                    adjustMasterId: form.adjSrchReq.adjustType ? form.adjSrchReq.adjustType == undefined ?  null :  form.adjSrchReq.adjustType == 'undefined' ? null:  form.adjSrchReq.adjustType : null,
                    paymentTypeCode: form.adjSrchReq.paymentType,
                    adjustStatus: form.adjSrchReq.status,
                    merchantId: form.adjSrchReq.merchantId,
                    edcBank: form.adjSrchReq.bankEdc,
                    saleSlipDate: form.adjSrchReq.saleSlipDate,
                    adjustAmt: form.adjSrchReq.amount.replace(/,/g, ''),
                    adjustDocType: form.adjSrchReq.doctype? form.adjSrchReq.doctype == undefined ?  null :  form.adjSrchReq.doctype == 'undefined' ? null:  form.adjSrchReq.doctype : null,
                    chequeNo: form.adjSrchReq.chequeNo,
                    adjustDocId: form.adjSrchReq.adjustNo ? form.adjSrchReq.adjustNo : null,
                    locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode != 'undefined' ? this.form.adjSrchReq.locationCode : null : null,
                }
            }).then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.filterData= snapData.data.data.adjSrchRespList.map(snap => {
                        let adjustType = '';
                        this.adjustType.map(type => {
                            if (type.adjustMasterId == snap.adjustMasterId && type.adjustDocType == 'AR') {
                                adjustType = type.adjustType;
                            }
                        })

                        Object.keys(this.adjustType).forEach(keys => {
                            if (snap.adjustMasterId == this.adjustType[keys].adjustMasterId) {
                                this.dataAdjustType = this.adjustType[keys].adjustType;
                            }
                        });
                        return {
                            adjustNo: snap.adjustDocId,
                            company: snap.companyCode,
                            locationCode: snap.locationCode,
                            adjustType: this.dataAdjustType,
                            paymentType: snap.paymentTypeCode === 'CSH' ? 'Cash' : snap.paymentTypeCode === 'CHQ' ? 'Cheque' : snap.paymentTypeCode === 'CC' ? 'Credit Card' : '',
                            status: this.global.setStatus(snap.adjustStatus),
                            amount: snap.adjustAmt ? snap.adjustAmt : 0,
                            merchantId: snap.merchantId,
                            edcBank: snap.edcBank,
                            saleSlipDate: snap.saleSlipDate,
                            adjustNote: snap.adjustNote,
                            last4Digits: snap.last4Digits,
                            adjustDate: snap.adjustDate,
                            adjustMasterId: snap.adjustMasterId,
                            outstandingAmt: snap.outstandingAmt

                        }
                    });
                    if (snapData.data.data.pagingInfo.totalRecords != 0) {
                        this.checkData = 1;
                        //this.paging = 1;
                    } else {
                        this.checkData = 0;
                        //this.paging = 0;
                    }
                    this.active = 'search'
                    this.paginator = snapData.data.data.pagingInfo;
                }
                this.filterPagination = this.filterData;
            });
        // }
    }

    add(data) {
        this.rawData.adjustNo = data.adjustNo;
        this.rawData.adjustMasterId = data.adjustMasterId;
        this.display = false;
        this.form.adjustNo = data.adjustNo;
        this.form.key = data.key;
        this.status = '2';
        // this.data.adjustType = data.adjustType
        // this.data.status = 'Resolve'
        // this.amount = (Math.abs(parseFloat(data.amount)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
        this.setCnFlag();
    }

    ngOnInit() {
        this.rawData.saleSlipDate = moment(this.rawData.saleSlipDate).format('DD/MM/YYYY');
        this.rawData.mainDocDate = Number(moment(this.rawData.saleSlipDate).format('x'));
        let overstring = this.router.url.indexOf('?');
        if(this.mockLocationId) {
            if (overstring) {
                this.url = this.router.url.substr(0, overstring);
            } else {
                this.url = this.router.url;
            }
            if (this.rawData.company == null) {
                this.rawData.company = '';
            }
        }
        this.ostDiff = this.rawData.ostDiff;
        this.amount = (Math.abs(this.rawData.ostDiff).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
        this.data = this.rawData
        this.data.amount = this.rawData.ostDiff;
        this.backupAmount = this.amount;
        this.bankAccNo = this.rawData.bankAccNo;
        this.dropDown();
    }

    dropDown() {
        // adjustType dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_TYPE,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.adjustType = snapData.data.data.adjMastInfoList.map(snap => {
                        return {
                            adjustType: snap.adjustType,
                            adjustDocType: snap.adjustDocType,
                            adjustMasterId: snap.adjustMasterId,
                            adjustCnFlag: snap.cnFlag == 'Y' ? snap.cnFlag : 'N',
                            fixAccountFlag: snap.fixAccountFlag,
                            defaultStatus: snap.defaultStatus,
                            checkAmtFlag: snap.checkAmtFlag,
                            checkAmtValue: snap.checkAmtValue,
                            glAccount: snap.glAccount,
                            glCs: snap.glCs,
                            glFs: snap.glFs,
                            glProduct: snap.glProduct,
                            glProject: snap.glProject,
                            glRc: snap.glRc,
                            glRs: snap.glRs,
                            glSs: snap.glSs,
                            autoFlag: snap.autoFlag
                        };
                    });
                    if (this.adjustType.length) {
                        this.setCnFlag();
                    }
                }
            });

        // adjustStatus dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_STTS,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.adjustStatus = snapData.data.data.adjSttsInfoList.map(snap => {
                        return {
                            adjustStatusCode: snap.adjustStatusCode,
                            adjustStatusDesc: snap.adjustStatusDesc
                        };
                    });
                }
            });

        // company dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_COMP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.company = snapData.data.data.companyInfoList.map(snap => {
                        return {
                            companyCode: snap.companyCode,
                        };
                    });
                }
            });
            // paymentType dropdown
axios.default.request({
    url: this.endpoint.API_LOAD_PAYMENTTYPE,
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'action': 'load',
        'transactionId': uuid(),
        'locationId': this.mockLocationId,
        'lang': 'EN',
        'username': this.rawMockLocationId[1],
        'navigate': this.router.url,
        'token': this.rawMockLocationId[3],
    }

})
    .then(snapData => {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.paymentType = snapData.data.data.paymentTypeInfoList.map(snap => {
                return {
                    paymentTypeCode: snap.paymentTypeCode,
                    paymentTypeDesc: snap.paymentTypeDesc,
                    paymentTypeStatus: snap.paymentTypeStatus,
                    paymentMethod: snap.paymentMethod

                }
            });
        }
    })

        // location
        /* axios.default.get(`https://us-central1-mockup-moneycollection.cloudfunctions.net/api/loadLocation`).then(data => {
            this.locationList = []; */
        let preList = JSON.parse(localStorage.getItem('locationPermiss'));
        preList.map(key => {
            this.locationList.push({
                code: key.code,
                name: key.name
            })
        });
        /* }); */

        //bank
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK_MERCHANT,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.bank = [];
                    snapData.data.data.locMrchntInfoList.map(snap => {
                        this.bank.push({
                            bankName: snap.bankName
                        })
                    });
                    //document type dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_DOC_TYPE,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
    }, 
            }).then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.docType = snapData.data.data.docTypeInfoList.map(snap => {
                        return {
                            adjustDocType: snap.adjustDocType,
                            docTypeDesc: snap.docTypeDesc
                        };
                    });
                }
            });
                }
            });
    }
                 
    loadMerchant() {
        //load merchant
        this.rawData.merchartId = undefined;
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK_MERCHANT,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-merchant',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                bankName: this.data.bankEdc
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.merchant = snapData.data.data.locMrchntInfoList.map(snap => {
                        return {
                            merchantId: snap.merchantId,
                            merchantType: snap.merchantType
                        };
                    });
                }
            });
    }

    setCnFlag() {
        // this.cnFlag = 'N';
        this.checkAmtValue = null;
        this.checkAmtFlag = null;
        this.adjustType.map(data => {
            if (this.rawData.adjustMasterId == data.adjustMasterId && data.adjustCnFlag) {
                this.cnFlag = data.adjustCnFlag;
            }
            if(this.rawData.adjustMasterId == data.adjustMasterId){
                this.checkAmtFlag = data.checkAmtFlag;
                this.checkAmtValue = data.checkAmtValue;
            }
        })
        if (this.cnFlag == 'N') {
            this.rawData.bankEdc = undefined;
            this.rawData.merchartId = undefined;
            this.rawData.last4Digi = 0;
        }
        this.setStatus();
        this.setBank();
        this.setGlAcc()
        
    }

    setStatus() {
        this.adjustType.map(snap => {
            if (this.rawData.adjustMasterId == snap.adjustMasterId && !this.rawData.adjustNo) {
                this.status = snap.defaultStatus;
            } 
        }); 
    }

    setData(){
        if(this.fixAccountFlag == 'N'){
            this.bankAccNo = this.rawData.bankAccNo ? this.rawData.bankAccNo : null;
        } else {
            this.bankAccNo = null;
        }
      
    }


    setGlAcc() {
        this.rawData.glAccount = null,
        this.rawData.glCs = null,
        this.rawData.glFs = null,
        this.rawData.glProduct = null,
        this.rawData.glProject = null,
        this.rawData.glRc = null,
        this.rawData.glRs = null,
        this.rawData.glSs = null
        if (this.bankAccNo == null || this.bankAccNo == undefined || this.bankAccNo == 'undefined') {
            this.adjustType.map(snap => {
                if (this.rawData.adjustMasterId == snap.adjustMasterId) {
                        this.rawData.glAccount = snap.glAccount,
                        this.rawData.glCs = snap.glCs,
                        this.rawData.glFs = snap.glFs,
                        this.rawData.glProduct = snap.glProduct,
                        this.rawData.glProject = snap.glProject,
                        this.rawData.glRc = snap.glRc,
                        this.rawData.glRs = snap.glRs,
                        this.rawData.glSs = snap.glSs
                }

            })
        } else {
            this.bankDes.map(snap => {
                if (this.bankAccNo == snap.bankAccountNo) {
                    this.rawData.glAccount = snap.glAccount,
                        this.rawData.glCs = snap.glCs,
                        this.rawData.glFs = snap.glFs,
                        this.rawData.glProduct = snap.glProduct,
                        this.rawData.glProject = snap.glProject,
                        this.rawData.glRc = snap.glRc,
                        this.rawData.glRs = snap.glRs,
                        this.rawData.glSs = snap.glSs
                }
            })
        }
    }

    setBank() {
        // adjustType dropdown

        this.adjustType.map(snap => {
            if (this.rawData.adjustMasterId == snap.adjustMasterId) {
                this.fixAccountFlag = snap.fixAccountFlag;
            }
        });

        this.setData()

        // adjustType dropdown
        axios.default.request({
            url: this.endpoint.API_BANK_ADJUST_AR,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-by-id',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.url,
                'token': this.rawMockLocationId[3]
            }, params: {
                adjustMasterId: this.rawData.adjustMasterId
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.bankDes = snapData.data.data.adjMastInfoList.map(snap => {
                        return {
                            bankAccountName: snap.bankAccountName,
                            bankAccountNo: snap.bankAccountNo,
                            glAccount: snap.glAccount,
                            glRc: snap.glRc,
                            glProduct: snap.glProduct,
                            glProject: snap.glProject,
                            glCs: snap.glCs,
                            glFs: snap.glFs,
                            glSs: snap.glSs,
                            glRs: snap.glRs
                        };
                    });
                }
            });
    }

    clearData() {
        this.form.pagingInfo. currentPage = 1;
        this.form.adjSrchReq.locationCode = '';
        this.form.adjSrchReq.company = '';
        this.form.adjSrchReq.paymentType = null;
        this.form.adjSrchReq.adjustType = undefined;
        this.form.adjSrchReq.status = 1;
        this.form.adjSrchReq.saleSlipDate = null;
        this.form.adjSrchReq.merchantId = null;
        this.form.adjSrchReq.chequeNo = null;
        this.form.adjSrchReq.adjustNo = null;
        this.form.adjSrchReq.doctype = undefined;

        if (this.rawData.ostDiff > 0) {
            this.form.adjSrchReq.amount = (Number(this.amount.replace(/,/g, '')) * (-1)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        } else {
            this.form.adjSrchReq.amount = (this.amount.replace(/,/g, '')).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        }
    }

    onClose(agreed: boolean) {
        this.resolve = false;
        this.disableCancel = true;
        this.data = [];
        this.rawData.dataType = null;
        this.disClose.emit(agreed);
    }

    success(agreed: boolean){
        this.disSwitch.emit(this.rawData.adjustMasterType ? 'Update' : this.rawData.whtFlag == 'Y' ? this.rawData.dataType : this.rawData.paymentTypeCode == 'CSH' ? 'Cash' : this.rawData.paymentTypeCode == 'CC' ? 'Credit Card' : 'Cheque' );
    }

    isOnlyNumber(event, data) {
        if(data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                let minus = 1;
                data = data.replace(/,/g, '');
                if((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    this.amount = Math.abs(this.setDecimal(parseFloat(data) * minus)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                }
            }
        }
    }

    setDecimal(number: number) {
        number = isNaN(number) ? 0 : number;
        if(number) {
            var digit = number.toString().indexOf('.');
            if(digit != -1) {
                number = parseFloat(number.toString().substr(0,(digit+3)));
            }
        }
        return number;
    }
}
