import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import * as moment from 'moment';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
    selector: 'app-main-page-ar',
    templateUrl: './main-page-ar.component.html',
    styleUrls: ['./main-page-ar.component.css']
})
export class MainpageArComponent implements OnInit {

    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];
    url: string;
    formSearch: {
        dateFrom: Date,
        dateTo: Date,
        status: string
    } = {
            dateFrom: new Date(),
            dateTo: new Date(),
            status: ''
        };
    dateNow = (new Date()).getTime();
    yearRanges:string=((new Date()).getFullYear()-2)+':'+ (new Date()).getFullYear();
    closingList = [];
    isLoading = true;

    constructor(private router: Router, private params: ActivatedRoute, private Endpoint: Endpoint, private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        let overstring = this.router.url.indexOf('?');
        if (overstring != -1) {
            this.url = this.router.url.substr(0, overstring);
        } else {
            this.url = this.router.url;
        }
        this.params.queryParams.subscribe(params => {
            this.formSearch.dateTo = params['docSearch'] ? new Date(params['docSearch']) : new Date();
            this.formSearch.status = params['status'] ? params['status'] : '';
        });
        if (this.formSearch.dateTo == undefined || !this.formSearch.dateTo) {
            this.formSearch.dateTo = new Date();
        }
        this.searchData();
    }

    async searchData() {
        this.isLoading = true;
        let config = {
            nwht: {
                headers: {
                    'action': 'list-myview-ar',
                    'lang': 'EN',
                    'navigate': this.url,
                }
            },
            wht: {
                headers: {
                    'action': 'list-myview-ar-wht',
                    'lang': 'EN',
                    'navigate': this.url,
                }
            }
        }
        let date = this.formSearch.dateTo;
        let month = moment(new Date(date.getFullYear(), date.getMonth(), 1, 0, 0, 0, 0)).add(7, 'hours').format('x');
        // let day = month;
        let query = '?';
        query += month ? 'dateFrom=' + month : '';
        query += this.formSearch.status ? '&docStatus=' + this.formSearch.status : '';
        // await axios.default.get(`${this.Endpoint.API_LOAD_AR_DETAIL}${query != '?' ? query : ''}`, config.nwht)

        var paramsArNWHT = {
            dateFrom: month ? month : null,
            docStatus: this.formSearch.status ? this.formSearch.status : null
        }
        await this.HttpService.call(this.Endpoint.API_LOAD_AR_DETAIL, 'GET', config.nwht.headers, null, paramsArNWHT, null, this.arNWhtSuccessCallback.bind(this), this.arNWhtFailCallback.bind(this));

        // await axios.default.get(`${this.Endpoint.API_LOAD_AR_DETAIL}${query != '?' ? query : ''}`, config.wht)
        var paramsWht = {
            dateFrom: month ? month : null,
            docStatus: this.formSearch.status ? this.formSearch.status : null
        }
        await  this.HttpService.call(this.Endpoint.API_LOAD_AR_DETAIL, 'GET', config.wht.headers, null, paramsWht, null, this.arWhtSuccessCallback.bind(this), this.arWhtFailCallback.bind(this));
    }

    arNWhtSuccessCallback(snapShot: any){
        this.global.checkTokenExpired(snapShot.data.statusCode);
        let data = snapShot.data.data.myViewArInfoList;
        this.closingList = data.map(list => {
            let csh = {
                bankAmt: null,
                diffAmt: null,
                outstandingDiffAmt: null,
                shopAmt: null,
                paymentTypeCode: 'CSH'
            };
            let chq = {
                bankAmt: null,
                diffAmt: null,
                outstandingDiffAmt: null,
                shopAmt: null,
                paymentTypeCode: 'CHQ'
            };
            let cc = {
                bankAmt: null,
                diffAmt: null,
                outstandingDiffAmt: null,
                shopAmt: null,
                paymentTypeCode: 'CC'
            }
            let wht = {
                bankAmt: null,
                diffAmt: null,
                outstandingDiffAmt: null,
                shopAmt: null,
                paymentTypeCode: 'WHT'
            }
            list.payments.forEach(payment => {
                if (payment.paymentTypeCode === 'CC') {
                    cc.shopAmt += payment.shopAmt;
                    cc.bankAmt += payment.bankAmt;
                    cc.diffAmt += payment.diffAmt;
                    cc.outstandingDiffAmt +=  payment.outstandingDiffAmt ;//payment.shopAmt == (payment.bankAmt + payment.diffAmt) ? payment.outstandingDiffAmt : payment.shopAmt - payment.bankAmt;
                } else if (payment.paymentTypeCode === 'CHQ') {
                    chq.shopAmt += payment.shopAmt;
                    chq.bankAmt += payment.bankAmt;
                    chq.diffAmt += payment.diffAmt;
                    chq.outstandingDiffAmt += payment.outstandingDiffAmt ;//payment.shopAmt == (payment.bankAmt + payment.diffAmt) ? payment.outstandingDiffAmt : payment.shopAmt - payment.bankAmt;
                } else if (payment.paymentTypeCode === 'CSH') {
                    csh.shopAmt += payment.shopAmt;
                    csh.bankAmt += payment.bankAmt;
                    csh.diffAmt += payment.diffAmt;
                    csh.outstandingDiffAmt += payment.outstandingDiffAmt ;// payment.shopAmt == (payment.bankAmt + payment.diffAmt) ? payment.outstandingDiffAmt : payment.shopAmt - payment.bankAmt;
                }
            })
            list.payments = [];
            list.payments.push(csh, chq, cc, wht);
            return list;
        })
        this.isLoading = false;
    }

    arNWhtFailCallback(snapShot: any){
        this.isLoading = false;
    }


    arWhtSuccessCallback(snapShot: any){
        this.global.checkTokenExpired(snapShot.data.statusCode);
        let data = snapShot.data.data.myViewArInfoList;
        if (data.length) {
            data.map(list => {
                let wht = {
                    bankAmt: null,
                    diffAmt: null,
                    outstandingDiffAmt: null,
                    shopAmt: null,
                    paymentTypeCode: 'WHT'
                }
                if (list) {
                    list.payments.forEach(payment => {
                        wht.shopAmt += payment.shopAmt;
                        wht.bankAmt += payment.bankAmt;
                        wht.diffAmt += payment.diffAmt;
                        wht.outstandingDiffAmt += payment.outstandingDiffAmt;
                    })
                    Object.keys(this.closingList).forEach(key => {
                        if (this.closingList[key].docDate === list.docDate) {
                            this.closingList[key].payments[3] = wht;
                        }
                    })
                }
            })
        }
        this.isLoading = false;
    }

    arWhtFailCallback(snapShot: any){
        this.isLoading = false;
    }

    onGo(docDate) {
        this.router.navigate(['/reconciliation/detail-page-ar'], { queryParams: { docDate: docDate, docSearch: this.formSearch.dateTo, status: this.formSearch.status } });
    }
}
