import { Component, OnInit, Input, OnChanges } from '@angular/core';
import * as axios from 'axios';
import { Router } from '@angular/router';
import { v4 as uuid } from 'uuid';
import * as moment from 'moment';
import { Endpoint } from '../../../../common/const/endpoint'
import { AppComponent } from '../../../../app.component';
@Component({
    selector: 'app-adjust-shop',
    templateUrl: './adjust-shop.component.html',
    styleUrls: ['./adjust-shop.component.css']
})
export class AdjustShopComponent implements OnInit, OnChanges {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];
    showSearch: boolean
    config = {
        headers: {
            'Content-Type': 'application/json',
            'action': 'search',
            'transactionId': uuid(),
            'locationId': this.mockLocationId,
            'lang': 'EN',
            'username': this.rawMockLocationId[1],
            'navigate': this.router.url,
            'token': this.rawMockLocationId[3]
        }
    };
    display: boolean = false;
    url: string;
    urlHead: string;
    listData = []
    search: boolean;
    adjustData: Array<any> = [];
    filterData: Array<any> = [];
    filterPagination: Array<object> = [];
    company: Array<object> = [];
    adjustType: Array<object> = [];
    adjustStatus: Array<object> = [];
    paymentType: Array<any> =[];
    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    }
    @Input() data = {
        locationCode: '',
        paymentType: '',
        saleSlipDate: '',
        taxId: '',
        maindocType: 'ar',
        date: '',
        whtFlag: ''
    }

    form: any = {
        pagingInfo: {
            currentPage: 1
        },
        adjSrchReq: {
            company: '',
            paymentType: null,
            locationCode: null,
            adjustType: '',
            status: '',
            amount: null,
            saleSlipDate: null,
            bankEdc: null,
            merchantId: null,
            Ostamount: null

        }
    }

    Location: Array<object> = [
        { code: '10001', name: 'Dtac Hall Future Park Rangsit' },
        { code: '02131', name: 'Dtac Hall Future Park Rangsit' },
        { code: '02668', name: 'Dtac Hall CentralPlaza Chaengwattana' },
        { code: '02951', name: 'Dtac Hall Lotus Bangyai' },
        { code: '02705', name: 'Dtac Hall Paradise Park' }
    ]

    banks: Array<object> = [
        { bank: 'kbank' },
        { bank: 'scb' },
    ]

    constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent) { }

    ngOnInit() {
        let overstring = this.router.url.indexOf('?');
        if (overstring) {
            this.urlHead = this.router.url.substr(0, overstring);
        } else {
            this.urlHead = this.router.url;
        }
        // this.showSearch = false;
        // if (this.router.url === '/confirm-sale-payment/search-adjust') {
        //     this.url = 'shop'
        // } else if (this.router.url === '/credit-note/search') {
        //     this.url = 'Treasury'
        // }
        // this.dropDow()
        // this.onSearch('1');
        // this.clear();
    }

    ngOnChanges() {
        let overstring = this.router.url.indexOf('?');
        if (overstring) {
            this.urlHead = this.router.url.substr(0, overstring);
        } else {
            this.urlHead = this.router.url;
        }
        this.showSearch = false;
        if (this.router.url === '/confirm-sale-payment/search-adjust') {
            this.url = 'shop'
        } else if (this.router.url === '/credit-note/search') {
            this.url = 'Treasury'
        }
        if(this.urlHead) {
            this.dropDow()
            this.onSearch('1');
            this.clear();
        }
    }

    dropDow() {
        // company dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_COMP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.urlHead,
                'token': this.rawMockLocationId[3]
            }, /* params: {
                searchAdjust: true
            } */

        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.company = snapData.data.data.companyInfoList.map(snap => {
                        return {
                            companyCode: snap.companyCode,
                        }
                    });
                }
            })

        // adjustType dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_TYPE,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.urlHead,
                'token': this.rawMockLocationId[3]
            },/*  params: {
                searchAdjust: true
            } */

        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.adjustType = snapData.data.data.adjMastInfoList.map(snap => {
                        return {
                            adjustType: snap.adjustType,
                            adjustDocType: snap.adjustDocType,
                            adjustMasterId: snap.adjustMasterId
                        }
                    });
                }
            })
        // adjustStatus dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_STTS,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.urlHead,
                'token': this.rawMockLocationId[3]
            },/*  params: {
                searchAdjust: true
            } */

        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.adjustStatus = snapData.data.data.adjSttsInfoList.map(snap => {
                        return {
                            adjustStatusCode: snap.adjustStatusCode,
                            adjustStatusDesc: snap.adjustStatusDesc
                        }
                    });
                }
            })
            // paymentType dropdown
axios.default.request({
    url: this.endpoint.API_LOAD_PAYMENTTYPE,
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'action': 'load',
        'transactionId': uuid(),
        'locationId': this.mockLocationId,
        'lang': 'EN',
        'username': this.rawMockLocationId[1],
        'navigate': this.router.url,
        'token': this.rawMockLocationId[3],
    }

})
    .then(snapData => {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.paymentType = snapData.data.data.paymentTypeInfoList.map(snap => {
                return {
                    paymentTypeCode: snap.paymentTypeCode,
                    paymentTypeDesc: snap.paymentTypeDesc,
                    paymentTypeStatus: snap.paymentTypeStatus,
                    paymentMethod: snap.paymentMethod

                }
            });
        }
    })
    }

    paginate(event) {
        if (((event.page + 1) != this.paginator.currentPage) && this.paginator.currentPage !== 0) {
            this.onSearch(event.page + 1);
        }
    }

    view(data) {
        this.display = true;
        this.listData = data
    }

    onSearch(page) {
        this.search = true;
        this.filterData = [];
        let date = Number(this.data.saleSlipDate)
        let config = {
            headers: {
                'Content-Type': 'application/json',
                'action': 'search',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.urlHead,
                'token': this.rawMockLocationId[3]
            }
        }
        // let query = "?";
        // query += page ? `currentPage=${page}` : '';
        // query += this.data.locationCode ? `&locationCode=${this.data.locationCode}` : '';
        // //query += this.data.whtFlag ? `&whtFlag=${this.data.whtFlag}` : '';
        // query += this.form.adjSrchReq.status ? `&adjustStatus=${this.form.adjSrchReq.status}` : '';
        // query += this.data.paymentType ? `&paymentTypeCode=${this.data.paymentType}` : '';
        // query += date ? `&mainDocDate=${moment(date).add(7, 'hours').format('x')}` : '';
        // query += this.form.adjSrchReq.amount ? '&adjustAmt=' + this.form.adjSrchReq.amount.replace(/,/g, '') : '';
        // query += this.form.adjSrchReq.company ? `&companyCode=${this.form.adjSrchReq.company}` : '';
        // query += this.data.whtFlag ? `&whtFlag=${this.data.whtFlag}`: '';
        // query += '&adjustDocType=SH';
        if (this.adjustType) {
            // axios.default.get(`${this.endpoint.API_SRCH_ADJ}${query}`, config)
            axios.default.request({
                url: this.endpoint.API_SRCH_ADJ,
                method: 'GET',
                headers: config.headers,
                params: {
                    currentPage: page ? page : null,
                    locationCode: this.data.locationCode ? this.data.locationCode : null,
                    adjustStatus: this.form.adjSrchReq.status ? this.form.adjSrchReq.status : null,
                    whtFlag: this.data.whtFlag ? this.data.whtFlag: null,
                    paymentTypeCode: this.data.paymentType ? this.data.paymentType : null,
                    mainDocDate: date ? moment(date).add(7, 'hours').format('x') : null,
                    adjustAmt: this.form.adjSrchReq.amount ? this.form.adjSrchReq.amount.replace(/,/g, '') : null,
                    companyCode: this.form.adjSrchReq.company ? this.form.adjSrchReq.company : null,
                    adjustDocType: 'SH',
                    adjustMasterId: this.form.adjSrchReq.adjustType ? this.form.adjSrchReq.adjustType : null

                }
            })
                .then(snapData => {
                    this.global.checkTokenExpired(snapData.data.statusCode);
                    if (snapData.data.statusCode == 'S0000') {
                        this.filterData = snapData.data.data.adjSrchRespList.map(snap => {
                            let adjust = '';
                            Object.keys(this.adjustType).map(key => {
                                if (this.adjustType[key].adjustMasterId === snap.adjustMasterId) {
                                    adjust = this.adjustType[key].adjustType;
                                }
                            })
                            return {
                                adjustNo: snap.adjustDocId,
                                company: snap.companyCode,
                                locationCode: snap.locationCode,
                                adjustType: adjust,
                                paymentType: snap.paymentTypeCode === 'CSH' ? 'Cash' : snap.paymentTypeCode === 'CHQ' ? 'Cheque' : snap.paymentTypeCode === 'CC' ? 'Credit Card' : '',
                                status: this.global.setStatus(snap.adjustStatus),
                                amount: snap.adjustAmt ? snap.adjustAmt : 0,
                                Ostamount: snap.outstandingAmt? snap.outstandingAmt  : 0
                            }
                        });
                        this.paginator = snapData.data.data.pagingInfo;
                    }

                    this.search = true;
                    // if (form.adjSrchReq.status === null) {
                    //     form.adjSrchReq.status = -1;
                    // }
                })
        }
    }

    searchAdjust() {
        if (this.showSearch == false) {
            this.showSearch = true
        } else {
            this.showSearch = false
            this.form = {
                adjSrchReq: {
                    company: '',
                    paymentType: '',
                    locationCode: null,
                    adjustType: '',
                    status: '',
                    amount: null,
                    saleSlipDate: null,
                    bankEdc: null,
                    merchantId: null,
                    Ostamount: null
    
                }
            }
        }

    }

    clear() {
        if(this.data.whtFlag == 'Y'){
            this.data.paymentType = null;
        }
        this.form = {
            adjSrchReq: {
                company: '',
                paymentType: '',
                locationCode: null,
                adjustType: '',
                status: '',
                amount: null,
                saleSlipDate: null,
                bankEdc: null,
                merchantId: null,
                Ostamount: null
                
            }
        }
        this.search = false;
    }

    onClose(agreed: boolean) {
        agreed ? this.display = false : this.display = true
    }

    isOnlyNumber(event, data) {
        if(data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                let minus = 1;
                let minusString = data.indexOf('-')
                if (minusString != -1) {
                    minus = -1;
                    data = data.replace(/-/g, '');
                }
                data = data.replace(/,/g, '');
                if((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    this.form.adjSrchReq.amount = data ? this.setDecimal(parseFloat(data)*minus).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null;
                }
            }
        }
    }

    isOnlyNumberOst(event, data) {
        if(data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                let minus = 1;
                let minusString = data.indexOf('-')
                if (minusString != -1) {
                    minus = -1;
                    data = data.replace(/-/g, '');
                }
                data = data.replace(/,/g, '');
                if((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    this.form.adjSrchReq.Ostamount = data ? this.setDecimal(parseFloat(data)*minus).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null;
                }
            }
        }
    }

    setDecimal(number: number) {
        number = isNaN(number) ? 0 : number;
        if(number) {
            var digit = number.toString().indexOf('.');
            if(digit != -1) {
                number = parseFloat(number.toString().substr(0,(digit+3)));
            }
        }
        return number;
    }
}
