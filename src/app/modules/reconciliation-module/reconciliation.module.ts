import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { KeyFilterModule } from 'primeng/keyfilter';

import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ReconciliationComponent } from './components/reconciliation/reconciliation.component';
import { RECONCILIATION_ROUTING } from '../routing-modules/auth/reconciliation-routing';
import { MainpageArComponent } from './components/main-page-ar/main-page-ar.component';
import { DetailpageArComponent } from './components/detail-page-ar/detail-page-ar.component';
import { ConfirmSalePaymentModule } from '../confirm-sale-payment-module/confirm-sale-payment.module';
import { AdjustBankComponent } from './components/adjust-bank/adjust-bank.component';
import { AdjustShopComponent } from './components/adjust-shop/adjust-shop.component';
import { PaginatorModule } from 'primeng/primeng';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SearchAdjustArComponent } from './components/search-adjust-ar/search-adjust-ar.component';


@NgModule({
    imports: [
        CalendarModule,
        CommonModule,
        FormsModule,
        DialogModule,
        PaginatorModule,
        RouterModule.forChild(RECONCILIATION_ROUTING),
        ProgressSpinnerModule,
        ConfirmSalePaymentModule,
        CurrencyMaskModule,
        ScrollPanelModule,
        KeyFilterModule,
    ],
    declarations: [
        ReconciliationComponent,
        MainpageArComponent,
        DetailpageArComponent,
        AdjustBankComponent,
        AdjustShopComponent,
        SearchAdjustArComponent
    ]
})
export class ReconcliliationModule { 
    constructor() { }
}
