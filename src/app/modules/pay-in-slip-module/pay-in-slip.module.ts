import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CalendarModule } from 'primeng/calendar';
import {TooltipModule} from 'primeng/tooltip';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';
import { KeyFilterModule } from 'primeng/keyfilter';

import { PayInSlipComponent } from './components/pay-in-slip/pay-in-slip.component';
import { SearchComponent } from './components/search/search.component';
import { CancelComponent } from './components/cancel/cancel.component';
import { ReprintComponent } from './components/reprint/reprint.component';
import { PrintComponent } from './components/print/print.component';
import { PAY_IN_SLIP_ROUTING } from '../routing-modules/auth/pay-in-slip-routing';
import { SlipComponent } from './components/slip/slip.component';
import { CurrencyMaskModule } from "ng2-currency-mask";

@NgModule({
    imports: [
        CommonModule,
        CalendarModule,
        FormsModule,
        TooltipModule,
        DialogModule,
        PaginatorModule,
        RouterModule.forChild(PAY_IN_SLIP_ROUTING),
        CurrencyMaskModule,
        DialogModule,
        KeyFilterModule
    ],
    declarations: [
        PayInSlipComponent,
        SearchComponent,
        CancelComponent,
        ReprintComponent,
        PrintComponent,
        SlipComponent
    ],
})
export class PayInSlipModule { 
    constructor() { }
}
