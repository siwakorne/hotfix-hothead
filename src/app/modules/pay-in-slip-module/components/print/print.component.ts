import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import { wrapIntoObservable } from '@angular/router/src/utils/collection';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';
@Component({
    selector: 'app-print',
    templateUrl: './print.component.html',
    styleUrls: ['./print.component.css']
})
export class PrintComponent implements OnInit {

    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];
    cash: any = 0;
    cheque: any = 0;
    timeCash: any = 0;
    timeCheque = 0;
    adjustData = [];
    filterData: any;
    msg = '';
    msgCheque = '';
    msgCash = '';
    lastPrintCash: any = 0;
    lastPrintCheque: any = 0;
    bank = [];
    bankCSH = [];
    bankMasterIdCSH: string;
    bankDescCSH: string;
    space: string = ' ';

    company = [];
    form = {
        bank: null,
        companyCode: 'DTN',
        bankNameThai: '',
        cheque: {
            bank: null,
            companyCode: null,
            bankNameThai: '',

        }
    };
    data = {
        company: '',
        acctNo: '',
        amount: '',
        bankName: '',
        compCode: '',
        locCode: '',
        locName: '',
        pmntType: '',
        barcode: ''
    };
    constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        this.checkLastPrint();
        this.dropDow();
        this.checkDataCheque()
        this.chackDataCash();

    }

    checkLastPrint() {
        var headers = {
            'lang': 'EN',
            'navigate': this.router.url,
            'action': 'last-print'
        }
        var params = {
            locationCode: JSON.parse(localStorage.getItem('loginData'))[2],
        }
        this.HttpService.call(this.endpoint.API_CHECK_LASTPRINT,'GET', headers,null, params , null , this.checkLastPrintSuccessCallback.bind(this),null);
    }

    checkLastPrintSuccessCallback(response: any){
        this.global.checkTokenExpired(response.data.statusCode);
        if (response.data.statusCode == 'S0000') {
            response.data.data.payinSlipInfoList.map(snap => {
                if (snap.paymentType == 'CSH') {
                    this.lastPrintCash = moment(snap.payinDate);
                } else if (snap.paymentType == 'CHQ') {
                    this.lastPrintCheque = moment(snap.payinDate);
                }
            })
        }
    }

    chackDataCash() {
        //cash
         var headers = {
            'action': 'check-data',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var params = {
            // bankMasterId: this.form.bank,
            paymentType: 'CSH',
        }
        this.HttpService.call(this.endpoint.API_CHECK_DATA_PRINT,'POST', headers,null, params , null , this.chackDataCashSuccessCallback.bind(this),null);
    }

    chackDataCashSuccessCallback(snapData: any){
        if (snapData.data.data.updAffected.chk_data_flag == 'true') {
            this.msgCash = 'success';
        } else {
            this.msgCash = 'fail';
        }
    }

    checkDataCheque() {
        //cheque
        var headers = {
            'action': 'check-data',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var params = {
            // bankMasterId: this.form.bank,
            paymentType: 'CHQ',
        }
        this.HttpService.call(this.endpoint.API_CHECK_DATA_PRINT,'POST', headers,null, params , null , this.checkDataChequeSuccessCallback.bind(this),this.checkDataChequeFailCallback.bind(this));
    }

    checkDataChequeSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.data.updAffected.chk_data_flag == 'true') {
            this.msgCheque = 'success';
        } else {
            this.msgCheque = 'fail';
        }
    }

    checkDataChequeFailCallback(err: any){
        if (err.response.data.statusCode == 'S0000') {
            this.msgCheque = 'success';
        } else {
            this.msgCheque = 'fail';
        }
    }


    onCash() {
        this.timeCash = moment(Date.now()).add(+7, 'hours')
        let printAgainCash = this.timeCash - this.lastPrintCash;
        if (this.form.bank == null) {
            this.msgCash = 'fail';
        } else {
            var headers = {
                'lang': 'EN',
                'navigate': this.router.url,
                'replyContentType': 'application/pdf',
                'replyFileName': 'PAYIN_SLIP_CASH_' + moment().format('YYYYMMDDhmmss') +'.pdf' 
            }

            var params = {
                paymentType: 'CSH',
                bankMasterId: this.form.bank,
                locationCode: this.mockLocationId,
                companyCode: this.form.companyCode,
                payinStatus: 'P',
                user: this.rawMockLocationId[1],
            }
            this.HttpService.call(this.endpoint.API_PRINT_SLIP_load,'GET', headers,'blob', params , null , this.onCashSuccessCallback.bind(this),this.onCashFailCallback.bind(this));
        }
    }

    onCashSuccessCallback(response: any){
        if (response.status == 200) {
            let blob = new Blob([response.data], { type: 'application/pdf', });
            let fileURL = URL.createObjectURL(blob);
            window.open(fileURL, '_blank');
            this.msg = 'success';
            this.checkLastPrint();
        }
    }

    onCashFailCallback(err: any){
        this.msg = 'notsuccess';
    }

    onCheque() {
        this.cheque = moment(Date.now()).format('DD/MM/YYYY, h:mm:ss a');
        this.timeCheque = Date.now();
        let printAgainCheque = this.timeCheque - this.lastPrintCheque;
        let date = new Date();
        if (this.form.cheque.bank === null) {
            this.msgCheque = 'fail';
        } else {
            var headers = {
                'lang': 'EN',
                'navigate': this.router.url,
                'replyContentType': 'application/pdf',
                'replyFileName': 'PAYIN_SLIP_CHEQUE_' + moment().format('YYYYMMDDhmmss') +'.pdf' 
            }

            var params = {
                paymentType: 'CHQ',
                bankMasterId: this.form.cheque.bank,
                locationCode: this.mockLocationId,
                companyCode: this.form.cheque.companyCode,
                payinStatus: 'P',
                user: this.rawMockLocationId[1],
            }
            this.HttpService.call(this.endpoint.API_PRINT_SLIP_load,'GET', headers,'blob', params , null , this.onChequeSuccessCallback.bind(this),this.onChequeFailCallback.bind(this));
        }
    }


    onChequeSuccessCallback(response: any){
        if (response.status == 200) {
            let blob = new Blob([response.data], { type: 'application/pdf', });
            let fileURL = URL.createObjectURL(blob);
            window.open(fileURL, '_blank');
            this.msg = 'success';
            this.checkLastPrint();
        }
    }

    onChequeFailCallback(err: any){
        this.msg = 'notsuccess';
    }

    async dropDow() {
        //bankCSH
        var headers = {
            'action': 'load-bank',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_LOAD_BANK,'GET', headers, null, null , null , this.bankCSHSuccessCallback.bind(this),null);

        let company = [];
        //company
        var headersCompany = {
            'action': 'load-comp',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var paramsCompany = {
            paymentTypeCode: 'CHQ'
        }
        this.HttpService.call(this.endpoint.API_LOAD_COMP_PRINT,'GET', headersCompany, null,  paramsCompany , null , this.companySuccessCallback.bind(this),null);
    }

    async dropDownBank() {
        //bankCHQ
        var headersBank = {
            'action': 'load-bank',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var paramsBank = {
            companyCode: this.form.cheque.companyCode
        }
        await this.HttpService.call(this.endpoint.API_LOAD_BANK,'GET', headersBank, null,  paramsBank , null , this.bankSuccessCallback.bind(this),null);
    }


    bankCSHSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.bankCSH = snapData.data.data.bankInfoList.map(snap => {
                if (snap.paymentTypeCode == 'CSH' && snap.default_bank == 'Y') {
                    this.form.bank = snap.bankMasterId;
                } else if (snap.paymentTypeCode == 'CSH' && this.form.bank == null) {
                    this.form.bank = snap.bankMasterId;
                }
                return {
                    bankDesc: snap.bankDesc,
                    bankMasterId: snap.bankMasterId,
                    bankShortName: snap.bankShortName,
                    paymentTypeCode: snap.paymentTypeCode,
                    companyCode: snap.companyCode,
                    bankCode: snap.bankCode
                };
            });
        }
    }

    companySuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.company = snapData.data.data.bankInfoList.map(snap => {
                if (this.form.cheque.companyCode == null) {
                    this.form.cheque.companyCode = snap.companyCode;
                }
                this.dropDownBank()
                return {
                    companyCode: snap.companyCode,
                };
            });
        }
    }

    bankSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.bank = snapData.data.data.bankInfoList.map(snap => {
                if (snap.paymentTypeCode == 'CHQ' && snap.default_bank == 'Y' && this.form.cheque.companyCode == snap.companyCode) {
                    this.form.cheque.bank = snap.bankMasterId;
                } else if (snap.paymentTypeCode == 'CHQ' && this.form.cheque.companyCode == snap.companyCode && this.form.cheque.bank == null) {
                    this.form.cheque.bank = snap.bankMasterId;
                }
                return {
                    bankDesc: snap.bankDesc,
                    bankMasterId: snap.bankMasterId,
                    bankShortName: snap.bankShortName,
                    paymentTypeCode: snap.paymentTypeCode,
                    companyCode: snap.companyCode,
                    bankCode: snap.bankCode
                };
            });
        }
    }


    loadBank() {
        this.form.cheque.bank = null;
        this.bank = [];
        var headersLoadBank = {
            'action': 'load-bank',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var paramsLoadBank = {
            companyCode: this.form.cheque.companyCode,
            locationCode: JSON.parse(localStorage.getItem('loginData'))[2]
        }
        this.HttpService.call(this.endpoint.API_LOAD_BANK,'GET', headersLoadBank, null,  paramsLoadBank , null , this.loadBankankSuccessCallback.bind(this),null);
    }


    loadBankankSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
           /*  this.bank = */ snapData.data.data.bankInfoList.map(snap => {
                if (snap.paymentTypeCode == 'CHQ' && snap.default_bank == 'Y' && this.form.cheque.companyCode == snap.companyCode) {
                    this.form.cheque.bank = snap.bankMasterId;
                } else if (snap.paymentTypeCode == 'CHQ' && this.form.cheque.companyCode == snap.companyCode) {
                    this.form.cheque.bank = snap.bankMasterId;
                }
                if (snap.paymentTypeCode == 'CHQ') {
                    this.bank.push({
                        bankDesc: snap.bankDesc,
                        bankMasterId: snap.bankMasterId,
                        bankShortName: snap.bankShortName,
                        paymentTypeCode: snap.paymentTypeCode,
                        companyCode: snap.companyCode,
                        bankCode: snap.bankCode
                    })
                }

            });
        }
    }

    reload() {
        this.checkLastPrint();
    }
}






