import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import * as moment from 'moment';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
    selector: 'app-cancel',
    templateUrl: './cancel.component.html',
    styleUrls: ['./cancel.component.css']
})
export class CancelComponent implements OnInit {

    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];
    adjustData: Array<any> = [];
    filterData: Array<any> = [];
    loading: boolean = true;
    page = 1;
    paging = 0;
    checkData = 1;
    msg: string;
    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };
    form: any = {
        cancleReason: '',
        payinSlipId: '',
        paymentType: '',
        company: '',
        status: 'P',
        cancelStatus: 'C',
        bankDesc: '',
        paymentTypeDesc: '',
        amount: ''
    };
    data: any = {
        payinSlipId: '',
        saleSlipDate: '',
        company: '',
        edcBank: '',
        paymentType: '',
        amount: '',
        paymentTypeCode: ''
    };

    constructor(private router: Router, private endpoint: Endpoint, private global:AppComponent, private HttpService: HttpService) { }

    onOk(data) {
        this.data = data;
        //this.route.navigate(['/pay-in-slip/cancel']);
    }


    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.loading = true
            this.query(event.page + 1);
        }
    }

    confirm() {
        if (this.form.cancleReason == '') {
            this.msg = 'fail';
        } else {

            var headers = {
                'action': 'cancel',
                'lang': 'EN',
                'navigate': this.router.url,
            }

            var data = {
                payinSlipId: this.data.payinSlipId,
                payinSlipStatus: this.form.cancelStatus,
                reasonDesc: this.form.cancleReason,
                paymentType: this.data.paymentTypeCode,
                username: this.rawMockLocationId[1]
            }
            this.HttpService.call(this.endpoint.API_CANCLE_SLIP,'PUT', headers,null, null , data , this.conFirmSuccessCallback.bind(this),null);
            this.form.cancleReason = '';
        }
    }

    conFirmSuccessCallback(Response: any){
        this.global.checkTokenExpired(Response.data.statusCode);
        if (Response.data.statusCode == 'S0000') {
            this.msg = 'success';
        } else {
            this.msg = 'fail';
        }
        this.query(this.page)
    }

    query(page) {
        this.checkData = 1;
        let date = new Date(this.form.valueDate);
        var headers = {
            'action': 'list',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var params = {
            payinSlipStatus: this.form.status ? this.form.status : undefined,
            payinSlipId: this.form.payinSlipId ? this.form.payinSlipId : undefined,
            paymentTypeCode: this.form.paymentType ? this.form.paymentType : undefined,
            companyCode: this.form.company ? this.form.company : undefined,
            bankDesc: this.form.bankDesc ? this.form.bankDesc : undefined,
            paymentTypeDesc: this.form.paymentTypeDesc ? this.form.paymentTypeDesc : undefined,
            payinDate: date.getTime() ? date.getTime() : undefined,
            payinAmt: this.form.amount ? this.form.amount : undefined,
        }
        this.HttpService.call(`${this.endpoint.API_CANCLE_SLIP}/${page}`,'GET', headers,null, params , null , this.querySuccessCallback.bind(this),null);
    }

    querySuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.loading = false;
            this.filterData = snapData.data.data.payinSlipInfoList.map(snap => {
                return {
                    company: snap.companyCode,
                    paymentType: snap.paymentTypeDesc,
                    amount: snap.payinAmt ? snap.payinAmt : 0,
                    edcBank: snap.bankDesc,
                    payinDate: snap.payinDate ? moment(snap.payinDate): null,
                    payinSlipId: snap.payinSlipId,
                    tranDate: snap.tranDate,
                    paymentTypeCode: snap.paymentTypeDesc == 'Cheque' ? 'CHQ' : snap.paymentTypeDesc == 'Cash' ? 'CSH' : ''

                }
            })
            if (snapData.data.data.paging.totalRecords != 0) {
                this.loading = false;
                this.checkData = 1;
                this.paging = 1
            } else {
                this.loading = false;
                this.checkData = 0;
                this.paging = 0;
            }
            this.paginator = snapData.data.data.paging;
        }
    }

    ngOnInit() {
        this.loading = true;
        this.checkData = 1;
        this.query(this.page);
    }

    cancel() {
        this.form.cancleReason = '';

        this.router.navigate(['/pay-in-slip/cancel']);
    }

}
