import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { v4 as uuid } from 'uuid';
import * as axios from 'axios';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpClient } from '@angular/common/http';
import { AppComponent } from '../../../../app.component';
@Component({
    selector: 'app-reprint',
    templateUrl: './reprint.component.html',
    styleUrls: ['./reprint.component.css']
})
export class ReprintComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    search: boolean;
    filterData: Array<object> = [];
    filterPagination: Array<object> = [];
    adjustData: Array<any> = [];
    company: Array<object> = [];
    bank: any;
    mockLocationId = this.rawMockLocationId[2];
    loading: boolean = true;
    checkData = 1;
    page: any;
    printDate: any;
    print: any;
    paymentType: Array<any> =[];
    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };
    msg: string = '';
    box: number = 0;
    display: boolean = false;
    form: any = {
        cash: 0,
        cheque: 0,
        printDate: '',
        tranDate: '',
        paymentType: '',
        bank: '',
        company: '',
        amount: null,
        rePrintReason: '',
        status: 'P',
    };
    data = {
        key: '',
        date: '',
        company: '',
        acctNo: '',
        amount: '',
        bankName: '',
        compCode: '',
        locCode: '',
        locName: '',
        pmntType: '',
        bankNameThai: '',
        moneyText: '',
        status: '',
        bankMasterId: '',
        paymentType: '',
        companyCode: '',
        payinDate: null,
        paymentTypeCode: null,
        payinSlipId: ''
    };
    constructor(private router: Router, private endpoint: Endpoint, private http: HttpClient, private global: AppComponent) { }

    ngOnInit() {
        this.form.printDate = new Date();
        this.dropDow();
    }

    dropDow() {
        axios.default.request({
            url: this.endpoint.API_LOAD_COMP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }, params: {
                searchAdjust: true
            }

        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.company = snapData.data.data.companyInfoList.filter(snap => {
                    return {
                        companyCode: snap.companyCode != 'PAYSBUY',
                    };
                });
            }
        });

        //bank
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }, params: {
                companyCode: this.form.company ? this.form.company : null,
                paymentTypeCode: this.form.paymentTypeDesc ? this.form.paymentTypeDesc : null
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.bank = snapData.data.data.bankInfoList.map(snap => {
                    return {
                        bankDesc: snap.bankDesc,
                        bankMasterId: snap.bankMasterId,
                        bankShortName: snap.bankShortName
                    };
                });
            }
        });
        // paymentType dropdown
axios.default.request({
    url: this.endpoint.API_LOAD_PAYMENTTYPE,
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'action': 'load',
        'transactionId': uuid(),
        'locationId': this.mockLocationId,
        'lang': 'EN',
        'username': this.rawMockLocationId[1],
        'navigate': this.router.url,
        'token': this.rawMockLocationId[3],
    }

})
    .then(snapData => {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.paymentType = snapData.data.data.paymentTypeInfoList.map(snap => {
                return {
                    paymentTypeCode: snap.paymentTypeCode,
                    paymentTypeDesc: snap.paymentTypeDesc,
                    paymentTypeStatus: snap.paymentTypeStatus,
                    paymentMethod: snap.paymentMethod

                }
            });
        }
    })


    }

    reprint(data) {
        this.form.rePrintReason = '';
        this.data = data;
        this.display = true;
        this.box = 1;
    }

    confirm() {
        this.box = 2;
        this.display = true;
        if (this.form.rePrintReason == '') {
            this.msg = 'fail';
        } else {
            axios.default.request({
                url: this.endpoint.API_REPRINT_SLIP,
                method: 'GET',
                responseType: 'blob',
                headers: {
                    'Content-Type': 'application/json',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.router.url,
                    'token': this.rawMockLocationId[3],
                    'replyContentType': 'application/pdf',
                    'replyFileName': (this.data.paymentTypeCode == 'CSH' ? 'PAYIN_SLIP_CASH_' : 'PAYIN_SLIP_CHEQUE_') + moment().format('YYYYMMDDhmmss') + '.pdf'
                },
                params: {
                    payInSlipId: this.data.payinSlipId,
                    payinSlipStatus: 'R',
                    reasonDesc: this.form.rePrintReason,
                    locationCode: this.mockLocationId,

                }
            }).then(response => {
                if (response.status == 200) {
                    this.msg = 'success';
                    let blob = new Blob([response.data], { type: 'application/pdf', });
                    let fileURL = URL.createObjectURL(blob);
                    window.open(fileURL, '_blank');
                    this.Search(this.page, 'paging');
                    this.cancel();
                } else{
                    this.msg = 'fail'; 
                }
            }).catch(err => {
                this.msg = 'fail'; 
            })
        }
    }

    cancel() {
        this.form.rePrintReason = '';
        this.display = false;
    }
    reOpen() {
        this.box = 1
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.loading = true
            this.Search(event.page + 1, 'paging');
            this.search = true;
        }
    }

    clear() {
        this.filterData = [];
        this.form = {
            cash: 0,
            cheque: 0,
            valueDate: '',
            paymentType: '',
            bank: '',
            company: '',
            amount: null,
            rePrintReason: '',
            status: 'P'
        };
        this.loadBank();
        this.form.printDate = new Date();
        this.search = false;
    }

    Search(page, click) {
        this.loading = true;
        this.page = page;
        this.search = true;
        this.printDate = null;
        this.print = null;
        if (click == 'paging') {
            this.checkData = 1;
        } else {
            this.checkData = 1;
            this.filterData = [];
        }
        if (this.form.printDate == null) {
            this.print = null;
        } else {
            this.printDate = new Date(this.form.printDate.getFullYear(), this.form.printDate.getMonth(), this.form.printDate.getDate());
            this.print = moment(this.printDate).format('x');
        }
        this.search = true;
        this.form.amount = this.form.amount != null ? this.form.amount.replace(/,/g, '') : null;
        axios.default.request({
            url: this.endpoint.API_SEARCH_SLIP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'search',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                currentPage: page ? page : undefined,
                paymentTypeCode: this.form.paymentType ? this.form.paymentType : undefined,
                companyCode: this.form.company ? this.form.company : undefined,
                payinSlipStatus: this.form.status ? this.form.status : undefined,
                payinDate: this.print ? this.print : undefined,
                ref2: this.form.tranDate ? this.form.tranDate : undefined,
                locationCode: this.mockLocationId ? this.mockLocationId : undefined,
                bankMasterId: this.form.bank ? this.form.bank : undefined,
                payinAmt: this.form.amount ? this.form.amount : undefined
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.filterData = snapData.data.data.payinSlipInfoList.map(snap => {
                        return {
                            bankMasterId: snap.bankMasterId,
                            company: snap.companyCode,
                            paymentType: snap.paymentTypeDesc,
                            paymentTypeCode: snap.paymentTypeDesc == 'Cash' ? 'CSH' : snap.paymentTypeDesc == 'Cheque' ? 'CHQ' : 'Default',
                            amount: snap.payinAmt ? snap.payinAmt : 0,
                            edcBank: snap.bankDesc,
                            payinDate: moment(snap.payinDate),
                            payinSlipId: snap.payinSlipId,
                            tranDate: snap.tranDate
                        }
                    })
                    if (snapData.data.data.paging.totalRecords == 0) {
                        this.checkData = 0;
                        this.loading = false
                    } else {
                        this.loading = false
                        this.checkData = 1;
                    }
                    this.paginator = snapData.data.data.paging;
                } else {
                    this.loading = false;
                    this.checkData = 0;
                }
            }).catch(err => {
                this.checkData = 0;
                this.loading = false;
            })
    }


    loadBank() {
        this.bank = [];
        //bank
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }, params: {
                companyCode: this.form.company ? this.form.company : null,
                paymentTypeCode: this.form.paymentType ? this.form.paymentType : null
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.bank = snapData.data.data.bankInfoList.map(snap => {
                    return {
                        bankDesc: snap.bankDesc,
                        bankMasterId: snap.bankMasterId,
                        bankShortName: snap.bankShortName
                    };
                });
            }
        });
    }

    isOnlyNumber(event, data, type) {
        if (data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                let minus = 1;
                let minusString = data.indexOf('-')
                if (minusString != -1) {
                    minus = -1;
                    data = data.replace(/-/g, '');
                }
                data = data.replace(/,/g, '');
                if ((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    if(type == 'amount') {
                        this.form.amount = data ? this.setDecimal(parseFloat(data) * minus).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null;
                    } else if (type == 'tranDate') {
                        this.form.tranDate = isNaN(Math.abs(data)) ? null : Math.abs(data) != 0 ? Math.abs(data).toString() : null;
                    }
                }
            }
        }
    }

    setDecimal(number: number) {
        number = isNaN(number) ? 0 : number;
        if (number) {
            var digit = number.toString().indexOf('.');
            if (digit != -1) {
                number = parseFloat(number.toString().substr(0, (digit + 3)));
            }
        }
        return number;
    }
}
