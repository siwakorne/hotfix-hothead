import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import * as axios from 'axios';
import { Endpoint } from '../../../../common/const/endpoint';
import { v4 as uuid } from 'uuid';
import { AppComponent } from '../../../../app.component';
@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];
    search: boolean;
    loading: boolean = true;
    checkData = 1;
    bank: any;
    company: Array<object> = []
    filterData: Array<object> = [];
    printDate: any;
    print: any;
    paymentType: Array<any> =[];
    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };

    form: any = {
        pagingInfo: {
            currentPage: 1
        },
        printDate: '',
        tranDate: '',
        paymentTypeDesc: '',
        bank: '',
        company: '',
        status: '',
        amount: ''
    };

    constructor(private endpoint: Endpoint, private router: Router, private global : AppComponent) { }


    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.loading = true
            this.onSearch(event.page + 1, 'paging');
        }
    }


    onSearch(page, click) {
        this.loading = true;
        this.filterData = [];
        this.search = true;
        this.printDate = null;
        this.print =  null;
        if (click == 'paging') {
            this.checkData = 1;
        } else {
            this.checkData = 1;
        }

        if(this.form.printDate == null){
            this.print = null;
        } else{
            this.printDate = new Date(this.form.printDate.getFullYear(), this.form.printDate.getMonth(), this.form.printDate.getDate());
            this.print = moment(this.printDate).format('x');
        }
           
       this.form.amount = this.form.amount.replace(/,/g, '');

        axios.default.request({
            url: this.endpoint.API_SEARCH_SLIP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'search',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                currentPage: page ? page : undefined,
                companyCode: this.form.company ? this.form.company : undefined,
                bankMasterId: this.form.bank ? this.form.bank : undefined,
                payinSlipStatus: this.form.status ? this.form.status != 'R' ? this.form.status : undefined : undefined,
                paymentTypeCode: this.form.paymentTypeDesc ? this.form.paymentTypeDesc : undefined,
                locationCode : this.mockLocationId ? this.mockLocationId : undefined,
                payinDate: this.print ? this.print : undefined,
                ref2: this.form.tranDate ? this.form.tranDate : undefined,
                payinAmt: this.form.amount ? parseFloat(this.form.amount) : undefined,
                reprintFlag: this.form.status == 'R' ? 'Y' : undefined
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.loading = false;
                this.filterData = snapData.data.data.payinSlipInfoList.map(snap => {
                    return {
                        adjustNo: snap.adjustDocId,
                            company: snap.companyCode,
                            // locationCode: snap.locationCode,
                            // adjustType: snap.adjustType,
                            paymentType: snap.paymentTypeDesc,
                            status: snap.payinStatus == 'P' ? 'Printed' : snap.payinStatus == 'R' ? 'Reprinted' : snap.payinStatus == 'C' ? 'Cancel' : 'Default',
                            amount: snap.payinAmt ? snap.payinAmt : 0,
                            // merchantId: snap.merchantId,
                            edcBank: snap.bankDesc,
                            payinDate: moment(snap.payinDate),
                            updatedBy: snap.updatedBy,
                            reasonDesc: snap.reasonDesc,
                            reprintFlag: snap.reprintFlag,
                            tranDate: snap.tranDate
                    }
                })
                if (snapData.data.data.paging.totalRecords != 0) {
                    this.loading = false;
                    this.checkData = 1;
                } else {
                    this.loading = false;
                    this.checkData = 0;
                }
                this.paginator = snapData.data.data.paging;
            }
        });
    }

    onClear() {
        this.filterData = [];
        this.form = {
            date: '',
            paymentTypeDesc: '',
            bank: '',
            company: '',
            status: '',
            amount: ''
        };
        this.loadBank();
        this.form.printDate = new Date();
        this.search = false;
    }

    ngOnInit() {
        let preDate = new Date();
        this.dropDow();
        this.form.printDate = new Date();
    }

    dropDow() {
        //company
        axios.default.request({
            url: this.endpoint.API_LOAD_COMP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                searchAdjust: true
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.company = snapData.data.data.companyInfoList.map(snap => {
                    return {
                        companyCode: snap.companyCode,
                    };
                });
            }
        });

        //bank
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.bank = snapData.data.data.bankInfoList.map(snap => {
                    return {
                        bankDesc: snap.bankDesc,
                        bankMasterId: snap.bankMasterId,
                        bankShortName: snap.bankShortName
                    };
                });
            }
        });
        // paymentType dropdown
axios.default.request({
    url: this.endpoint.API_LOAD_PAYMENTTYPE,
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'action': 'load',
        'transactionId': uuid(),
        'locationId': this.mockLocationId,
        'lang': 'EN',
        'username': this.rawMockLocationId[1],
        'navigate': this.router.url,
        'token': this.rawMockLocationId[3],
    }

})
    .then(snapData => {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.paymentType = snapData.data.data.paymentTypeInfoList.map(snap => {
                return {
                    paymentTypeCode: snap.paymentTypeCode,
                    paymentTypeDesc: snap.paymentTypeDesc,
                    paymentTypeStatus: snap.paymentTypeStatus,
                    paymentMethod: snap.paymentMethod

                }
            });
        }
    })
    }



    loadBank(){
        this.bank = [];
           //bank
           axios.default.request({
            url: this.endpoint.API_LOAD_BANK,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },params: {
                companyCode: this.form.company ? this.form.company : null,
                paymentTypeCode: this.form.paymentTypeDesc ? this.form.paymentTypeDesc : null
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.bank = snapData.data.data.bankInfoList.map(snap => {
                    return {
                        bankDesc: snap.bankDesc,
                        bankMasterId: snap.bankMasterId,
                        bankShortName: snap.bankShortName
                    };
                });
            }
        });
    }
    

    isOnlyNumber(event, data, type) {
        if(data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                let minus = 1;
                let minusString = data.indexOf('-')
                if(minusString != -1) {
                    minus = -1;
                    data = data.replace(/-/g, '');
                }
                data = data.replace(/,/g, '');
                if((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    if(type == 'amount') {
                        this.form.amount = data ? this.setDecimal(parseFloat(data) * minus).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null;
                    } else if (type == 'tranDate') {
                        this.form.tranDate = isNaN(Math.abs(data)) ? null : Math.abs(data) != 0 ? Math.abs(data).toString() : null;
                    }
                }
            }
        }
    }

    setDecimal(number: number) {
        number = isNaN(number) ? 0 : number;
        if(number) {
            var digit = number.toString().indexOf('.');
            if(digit != -1) {
                number = parseFloat(number.toString().substr(0,(digit+3)));
            }
        }
        return number;
    }
}
