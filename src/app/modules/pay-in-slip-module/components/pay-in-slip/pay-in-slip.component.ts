import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-pay-in-slip',
    templateUrl: './pay-in-slip.component.html',
    styleUrls: ['./pay-in-slip.component.css']
})
export class PayInSlipComponent implements OnInit {

    constructor(private router: Router) { }

    ngOnInit() {
        this.checkAuth();
    }

    checkAuth() {
        if(!JSON.parse(localStorage.getItem('loginData'))) {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        } else if (JSON.parse(localStorage.getItem('loginData')) && JSON.parse(localStorage.getItem('loginData')).length < 4) {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        } 
    }
}
