import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as ThaiBaht from 'thai-baht-text';
import * as moment from 'moment';

@Component({
    selector: 'app-slip',
    templateUrl: './slip.component.html',
    styleUrls: ['./slip.component.css']
})
export class SlipComponent implements OnInit {

    constructor(private params: ActivatedRoute) { }

    data = {
        date: '',
        company: '',
        acctNo: '',
        amount: '',
        bankName: '',
        compCode: '',
        locCode: '',
        locName: '',
        pmntType: '',
        bankNameThai: '',
        moneyText: '',
        status: '',
        customer: '',
        barcode: ''
    }

    ngOnInit() {
        this.params.queryParams.subscribe(params => {
            this.data.date = moment().format('l');
            this.data.company = params['company']
            this.data.amount = params['amount']
            this.data.bankNameThai = params['bankNameThai']
            this.data.locName = params['locName']
            this.data.locCode = params['locCode']
            this.data.acctNo = params['acctNo']
            this.data.bankName = params['bankName']
            this.data.compCode = params['compCode']
            this.data.pmntType = params['pmntType']
            this.data.status = params['status']
            this.data.customer = params['customer']
            this.data.barcode = params['barcode']
        });
        let money = this.data.amount;
        this.data.moneyText = ThaiBaht(money);
        this.printSlip();
    }
    
    printSlip() {
        
    }
}
