import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms'
import { GrowlModule } from 'primeng/growl';
import { CalendarModule } from 'primeng/calendar'
import { MenuModule } from 'primeng/menu';
import { TreeModule } from 'primeng/tree';
import { DialogModule } from 'primeng/dialog';
import { FileUploadModule } from 'primeng/fileupload';
import { MessagesModule } from 'primeng/messages';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

import { AdminSetupComponent } from './components/admin-setup/admin-setup.component';
import { ADMIN_SETUP_ROUTING } from '../routing-modules/auth/admin-setup-routing';
import { UserComponent } from './components/user/user.component'
import { RoleComponent } from './components/role/role.component'
import { AdjustTypeComponent } from './components/adjustType/adjust-type.component';
import { BankComponent } from './components/bank/bank.component';
import { BarcodeComponent } from './components/barcode/barcode.component';
import { PaymentTypeComponents } from './components/payment/payment.component';
import { LocationShopComponents } from './components/location/location-shop.component';
import { MerchantComponents } from './components/merchant/merchant.component';
import { CheckboxModule } from 'primeng/checkbox';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { PickListModule } from 'primeng/picklist';
import { LocVsBankComponents } from './components/locvsbank/locvsbank.component';
import { ManagePendingComponent } from './components/manage-pending/manage-pending.component';
import { PaginatorModule } from 'primeng/primeng';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { KeyFilterModule } from 'primeng/keyfilter';
import { AttachSearchComponent } from './components/attach-search/attach-search.component';
import { AttachUploadComponent } from './components/attach-upload/attach-upload.component';
import { AttachFileComponent } from './components/attach-file/attach-file.component';
import { SuspenseComponent } from './components/suspense/suspense.component';
import { ConpanyComponent } from './components/conpany/conpany.component';
import { PayInMappingComponent } from './components/pay-in-mapping/pay-in-mapping.component';
import { GlobalComponent } from './components/global/global.component';
import { BankSlaComponent } from './components/bankSla/bankSla.component';
import { HolidayComponent } from './components/holiday/holiday.component';
import { OtherAccountComponent } from './components/other-account/other-account.component';
import { NotificationComponent } from './components/notification/notification.component';
import {MultiSelectModule} from 'primeng/multiselect';
import { AttachMappingComponent } from './components/attach-mapping/attach-mapping.component';
import { BankcodeMappingComponent } from './components/bankcode-mapping/bankcode-mapping.component';
import { BankMasterComponent } from './components/bank-master/bank-master.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ADMIN_SETUP_ROUTING),
        FormsModule,
        CalendarModule,
        GrowlModule,
        MenuModule,
        TreeModule,
        DialogModule,
        CheckboxModule,
        PickListModule,
        CurrencyMaskModule,
        PaginatorModule,
        ScrollPanelModule,
        KeyFilterModule,
        FileUploadModule,
        MessagesModule,
        ConfirmDialogModule,
        MultiSelectModule
    ],
    exports: [RouterModule],
    declarations: [
        AdminSetupComponent,
        UserComponent,
        RoleComponent,
        AdjustTypeComponent,
        BankComponent,
        BarcodeComponent,
        PaymentTypeComponents,
        LocationShopComponents,
        MerchantComponents,
        LocVsBankComponents,
        ManagePendingComponent,
        AttachSearchComponent,
        AttachUploadComponent,
        AttachFileComponent,
        SuspenseComponent,
        ConpanyComponent,
        PayInMappingComponent,
        GlobalComponent,
        BankSlaComponent,
        HolidayComponent,
        OtherAccountComponent,
        NotificationComponent,
        AttachMappingComponent,
        BankcodeMappingComponent,
        BankMasterComponent,
    ]
})
export class AdminSetupModule { }
