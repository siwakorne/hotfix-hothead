import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-admin-setup',
    templateUrl: './admin-setup.component.html',
    styleUrls: ['./admin-setup.component.css']
})
export class AdminSetupComponent implements OnInit {

    constructor(private router: Router) { }

    ngOnInit() {
        this.checkAuth();
    }

    checkAuth() {
        if(!JSON.parse(localStorage.getItem('loginData'))) {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        } else if (JSON.parse(localStorage.getItem('loginData')) && JSON.parse(localStorage.getItem('loginData')).length < 4) {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        } 
    }

}
