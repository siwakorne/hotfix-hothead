import { Component, OnInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import * as axios from 'axios';
import { v4 as uuid } from 'uuid';
import * as moment from 'moment';
import { Endpoint } from '../../../../common/const/endpoint';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-manage-pending',
    templateUrl: './manage-pending.component.html',
    styleUrls: ['./manage-pending.component.css']
})
export class ManagePendingComponent implements OnInit, OnChanges {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];
    config = {
        headers: {
            'Content-Type': 'application/json',
            'action': 'search',
            'transactionId': uuid(),
            'locationId': this.mockLocationId,
            'lang': 'EN',
            'username': this.rawMockLocationId[1],
            'navigate': this.router.url,
            'token': this.rawMockLocationId[3]
        }
    };
    loading: boolean = true;
    display: boolean = false;
    url: string;
    listData: any;
    checkData = 1;
    beforePaging = 1;
    paging = 0;
    deleteRef = null;
    box = 0;
    page: number = 0;
    search: boolean;
    dataAdjustType: string = '';
    dataLocationName: string = '';
    adjustData: Array<any> = [];
    adjustStatus: Array<any> = [];
    errorStatus: boolean = false;
    errorMessage: string = '';
    filterData: Array<object> = [];
    filterPagination: Array<object> = [];
    company: Array<object> = [];
    adjustType: Array<any> = [];
    adjustAmountList: any = [];
    bank: any;
    merchant: any;
    searchMaindoc = '0';
    searchAdjustdoc = '0';
    checkSave: boolean = false;
    minMainDocDate: any;
    mintoDocDate: any;
    paymentType: Array<any> =[];
    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };

    form: any = {
        pagingInfo: {
            currentPage: 1
        },
        adjSrchReq: {
            company: '',
            paymentType: '',
            locationCode: '',
            adjustType: '',
            status: 1,
            amount: null,
            saleSlipDate: null,
            bankEdc: '',
            merchantId: null,
            adjustDateTo: null,
            adjustDate: null,
            mainDocDate: null,
            mainDocDateTo: null,
            wht: ''
        }
    };

    locationList = [];

    constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent) { }

    ngOnInit() {
        this.loading = true;
        this.checkData = 1;
        this.dropDown();
    }

    ngOnChanges() {
        this.loading = true;
        this.checkData = 1;
        this.dropDown();
    }

    dropDown() {
        // company dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_COMP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }

        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.company = snapData.data.data.companyInfoList.map(snap => {
                        return {
                            companyCode: snap.companyCode,
                        }
                    });
                }
            })

        // adjustType dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_TYPE,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        })
            .then(snapData => {
                if (snapData.data.statusCode == 'S0000') {
                    snapData.data.data.adjMastInfoList.map(snap => {
                        this.adjustType.push({
                            adjustType: snap.adjustType,
                            adjustDocType: snap.adjustDocType,
                            adjustMasterId: snap.adjustMasterId,
                            adjustStatus: snap.defaultStatus,
                            fixAccountFlag: snap.fixAccountFlag,
                            glAccount: snap.glAccount,
                            glCs: snap.glCs,
                            glFs: snap.glFs,
                            glProduct: snap.glProduct,
                            glProject: snap.glProject,
                            glRc: snap.glRc,
                            glRs: snap.glRs,
                            glSs: snap.glSs
                        });
                    });
                }
            })

        // adjustStatus dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_STTS,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }

        })
            .then(snapData => {
                if (snapData.data.statusCode == 'S0000') {
                    this.adjustStatus = snapData.data.data.adjSttsInfoList.map(snap => {
                        return {
                            adjustStatusCode: snap.adjustStatusCode,
                            adjustStatusDesc: snap.adjustStatusDesc
                        }
                    });

                }
            });

        // Bank dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_BANKCODE,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank-code',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        }).then(snapData => {
            if (snapData.data.statusCode == 'S0000') {
                this.bank = snapData.data.data.bankCodeInfoList.map(snap => {
                    return {
                        bankCode: snap.bankCode,
                        bankNameTh: snap.bankNameTh,
                        initialName: snap.initialName
                    }
                });
            }
        });

        //Location dropdown
        let preList = JSON.parse(localStorage.getItem('locationPermiss'));
        preList.map(key => {
            this.locationList.push({
                code: key.code,
                name: key.name
            })
        });
        // paymentType dropdown
axios.default.request({
    url: this.endpoint.API_LOAD_PAYMENTTYPE,
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'action': 'load',
        'transactionId': uuid(),
        'locationId': this.mockLocationId,
        'lang': 'EN',
        'username': this.rawMockLocationId[1],
        'navigate': this.router.url,
        'token': this.rawMockLocationId[3],
    }

})
    .then(snapData => {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.paymentType = snapData.data.data.paymentTypeInfoList.map(snap => {
                return {
                    paymentTypeCode: snap.paymentTypeCode,
                    paymentTypeDesc: snap.paymentTypeDesc,
                    paymentTypeStatus: snap.paymentTypeStatus,
                    paymentMethod: snap.paymentMethod

                }
            });
        }
    })
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.loading = true
            this.Search(event.page + 1, 'paging');
        }
    }

    edit(data) {
        this.checkSave = false;
        this.adjustAmountList = [];
        this.box = 1;
        this.display = true;
        this.listData = data
    }

    Search(page, click) {
        this.filterData = [];
        this.loading = true;
        this.page = page;
        this.checkData = 1;
        if (click != 'paging') {
            this.paging = 0;
        }
        this.search = true;
        let date = '';
        if (this.form.adjSrchReq.saleSlipDate) {
            let preData = new Date(this.form.adjSrchReq.saleSlipDate);
            date = moment(preData).add(-7, 'hours').format('x');
        }
        let dateAdjust = '';
        if (this.form.adjSrchReq.adjustDate) {
            let preDataAdjust = new Date(this.form.adjSrchReq.adjustDate);
            dateAdjust = moment(preDataAdjust).add(-7, 'hours').format('x');
        }

        axios.default.request({
            url: this.endpoint.API_SRCH_ADJ,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'search',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                currentPage: page ? page : undefined,
                paymentTypeCode: this.form.adjSrchReq.paymentType ? this.form.adjSrchReq.paymentType : undefined,
                adjustMasterId: this.form.adjSrchReq.adjustType ? this.form.adjSrchReq.adjustType : undefined,
                locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode : undefined,
                companyCode: this.form.adjSrchReq.company ? this.form.adjSrchReq.company : undefined,
                adjustStatus: this.form.adjSrchReq.status ? this.form.adjSrchReq.status : undefined,
                edcBank: this.form.adjSrchReq.bankEdc ? this.form.adjSrchReq.bankEdc : undefined,
                adjustAmt: this.form.adjSrchReq.amount ? this.form.adjSrchReq.amount.replace(/,/g, '') : undefined,
                merchantId: this.form.adjSrchReq.merchantId ? this.form.adjSrchReq.merchantId : undefined,
                saleSlipDate: date ? date : undefined,
                whtFlag: this.form.adjSrchReq.wht ? this.form.adjSrchReq.wht : null,
                // adjustDate: dateAdjust ? dateAdjust : undefined,
                adjustDate: this.form.adjSrchReq.adjustDate ? moment(new Date(this.form.adjSrchReq.adjustDate)).add(+7, 'hours').format('x') : null,
                adjustDateTo: this.form.adjSrchReq.adjustDateTo == this.form.adjSrchReq.adjustDate ? null : this.form.adjSrchReq.adjustDateTo ? moment(new Date(this.form.adjSrchReq.adjustDateTo)).add(+7, 'hours').format('x') : null,
                mainDocDate: this.form.adjSrchReq.mainDocDate ? moment(new Date(this.form.adjSrchReq.mainDocDate)).add(+7, 'hours').format('x') : null,
                mainDocDateTo: this.form.adjSrchReq.mainDocDateTo == this.form.adjSrchReq.mainDocDate ? null : this.form.adjSrchReq.mainDocDateTo ? moment(new Date(this.form.adjSrchReq.mainDocDateTo)).add(+7, 'hours').format('x') : null,
                adjustDocType: 'AR',
                postGl: 'RE',
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.filterData = snapData.data.data.adjSrchRespList.map(snap => {
                    this.loading = false;
                    Object.keys(this.locationList).forEach(keys => {
                        if (snap.locationCode == this.locationList[keys].code) {
                            this.dataLocationName = this.locationList[keys].name;
                        }
                    });
                    Object.keys(this.adjustType).forEach(keys => {
                        if (snap.adjustMasterId == this.adjustType[keys].adjustMasterId) {
                            this.dataAdjustType = this.adjustType[keys].adjustType;
                        }
                    });
                    return {
                        adjustDocId: snap.adjustDocId,
                        adjustNo: snap.adjustDocId,
                        company: snap.companyCode,
                        locationCode: snap.locationCode,
                        adjustType: this.dataAdjustType,
                        paymentType: snap.paymentTypeCode === 'CSH' ? 'Cash' : snap.paymentTypeCode === 'CHQ' ? 'Cheque' : snap.paymentTypeCode === 'CC' ? 'Credit Card' : '',
                        status: this.global.setStatus(snap.adjustStatus),
                        amount: snap.adjustAmt ? snap.adjustAmt : 0,
                        merchantId: snap.merchantId,
                        edcBank: snap.edcBank,
                        saleSlipDate: moment(snap.saleSlipDate).add(7, 'hours'),
                        locationName: this.dataLocationName,
                        adjustNote: snap.adjustNote,
                        last4Digits: snap.last4Digits,
                        adjustDate: snap.adjustDate,
                        mainDocDate: snap.mainDocDate,
                        refAdjustDocId: snap.refAdjustDocId,
                        postGlFlag: snap.postGlFlag,
                        postGlDate: snap.postGlDate,
                        whtFlag: snap.whtFlag
                    }
                });
                this.paginator = snapData.data.data.pagingInfo;
            }
            if (snapData.data.data.pagingInfo.currentPage != 0) {
                this.loading = false;
                this.checkData = 1;
                this.paging = 1
            } else {
                this.loading = false;
                this.checkData = 0;
                this.paging = 0;
            }
        })
            .catch(() => {
                this.loading = false;
            });
    }

    clear() {
        this.filterData = [];
        this.paging = 0;
        this.form = {
            pagingInfo: {
                currentPage: 1
            },
            adjSrchReq: {
                company: '',
                paymentType: '',
                locationCode: '',
                adjustType: '',
                status: 1,
                amount: null,
                saleSlipDate: null,
                bankEdc: '',
                merchantId: null,
                adjustDateTo: null,
                adjustDate: null,
                mainDocDate: null,
                mainDocDateTo: null,
                wht: ''
            }
        };
        this.searchMaindoc = '0';
        this.searchAdjustdoc = '0';
    }

    onClose(agreed: boolean) {
        agreed ? this.display = false : this.display = true;
        this.loading = true
        this.box = 0;
        this.Search(this.page, 'paging');
    }

    isOnlyNumber(event, data, index, type) {
        data = data ? data.toString() : undefined;
        if (data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                let minus = 1;
                let minusString = data.indexOf('-');
                if (minusString != -1) {
                    minus = -1;
                    data = data.replace(/-/g, '');
                }
                data = data.replace(/,/g, '');
                if ((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    var preProcess = data ? this.setDecimal(parseFloat(data) * minus).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null;
                    if (type == 'form') {
                        this.form.adjSrchReq.amount = preProcess;
                    } else {
                        this.adjustAmountList[index].adjustAmount = preProcess;
                    }
                }
            }
        }
    }

    setDecimal(number: number) {
        number = isNaN(number) ? 0 : number;
        if (number) {
            var digit = number.toString().indexOf('.');
            if (digit != -1) {
                number = parseFloat(number.toString().substr(0, (digit + 3)));
            }
        }
        return Math.abs(number);
    }

    submit(amount, type) {
        this.checkSave = true;
        let total = 0;
        let failAdjust = false;
        this.errorStatus = false;
        this.errorMessage = '';
        let dataManage = [];
        this.adjustAmountList.map(data => {
            if (data.adjustAmount != null) {
                total += Number(parseFloat(data.adjustAmount.toString().replace(/,/g, '')).toFixed(2));
                let objectAdjust = [];
                let objectBank = [];
                objectBank = data.bank ? data.bank.filter(bank => bank.bankAccountNo == data.adjustBank) : [];
                objectAdjust = this.adjustType.filter(adjust => adjust.adjustMasterId == data.adjustType);
                if (objectAdjust.length || objectBank.length) {
                    dataManage.push({
                        adjustDocId: data.adjustDocId,
                        adjustMasterId: Number(data.adjustType),
                        adjustStatus: objectAdjust.length ? objectAdjust[0].adjustStatus : undefined,
                        bankAccountNo: objectBank.length ? objectBank[0].bankAccountNo : undefined,
                        glAccount: objectBank.length ? objectBank[0].glAccount : objectAdjust.length ? objectAdjust[0].glAccount : undefined,
                        glRc: objectBank.length ? objectBank[0].glRc : objectAdjust.length ? objectAdjust[0].glRc : undefined,
                        glProduct: objectBank.length ? objectBank[0].glProduct : objectAdjust.length ? objectAdjust[0].glProduct : undefined,
                        glProject: objectBank.length ? objectBank[0].glProject : objectAdjust.length ? objectAdjust[0].glProject : undefined,
                        glCs: objectBank.length ? objectBank[0].glCs : objectAdjust.length ? objectAdjust[0].glCs : undefined,
                        glFs: objectBank.length ? objectBank[0].glFs : objectAdjust.length ? objectAdjust[0].glFs : undefined,
                        glSs: objectBank.length ? objectBank[0].glSs : objectAdjust.length ? objectAdjust[0].glSs : undefined,
                        glRs: objectBank.length ? objectBank[0].glRs : objectAdjust.length ? objectAdjust[0].glRs : undefined,
                        adjustAmt: Number(data.adjustAmount.toString().replace(/,/g, ''))
                    })
                }
            }
            if (data.adjustType == null || (!data.adjustBank && data.fixAccountFlag == 'N')) failAdjust = true;
        })
        if (total != 0 && !failAdjust) {
            if (total != Math.abs(amount)) {
                this.checkSave = false;
                this.errorStatus = true;
                this.errorMessage = 'Adjust amount not equal to Adjustment';
            } else {
                if (type == 'submit') {
                    axios.default.request({
                        url: this.endpoint.API_MANAGE_PENDING,
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'action': 'add-postgl',
                            'transactionId': uuid(),
                            'username': this.rawMockLocationId[1],
                            'locationId': this.mockLocationId,
                            'lang': 'EN',
                            'navigate': this.router.url,
                            'token': this.rawMockLocationId[3]
                        },
                        data: {
                            // Waiting Data to check adjustDoc
                            adjustDocId: Number(this.listData.adjustDocId),
                            postGlFlag: 'RE',
                            managePendingInfo: dataManage
                        }
                    })
                        .then(snapData => {
                            this.global.checkTokenExpired(snapData.data.statusCode);                            
                            if (snapData.data.statusCode == 'S0000') {
                                this.errorStatus = false;
                                this.errorMessage = '';
                            } else {
                                this.checkSave = false;
                                this.errorStatus = true;
                                this.errorMessage = snapData.data.statusMessage;
                            }
                        })
                        .catch((err) => {
                            this.checkSave = false;
                            this.errorStatus = true;
                            this.errorMessage = err.response.data.statusMessage;
                        })
                } else if (type == 'update') {
                    axios.default.request({
                        url: this.endpoint.API_MANAGE_PENDING,
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                            'action': 'update-postgl',
                            'transactionId': uuid(),
                            'username': this.rawMockLocationId[1],
                            'locationId': this.mockLocationId,
                            'lang': 'EN',
                            'navigate': this.router.url,
                            'token': this.rawMockLocationId[3]
                        },
                        data: {
                            // Waiting Data to check adjustDoc
                            refAdjustDocId: this.listData.refAdjustDocId ? Number(this.listData.refAdjustDocId) : undefined,
                            postGlFlag: 'RE',
                            managePendingInfo: dataManage
                        }
                    })
                    .then(snapData => {
                        this.global.checkTokenExpired(snapData.data.statusCode);
                        if (snapData.data.statusCode == 'S0000') {
                            this.errorStatus = false;
                            this.errorMessage = '';
                        } else {
                            this.checkSave = false;
                            this.errorStatus = true;
                            this.errorMessage = snapData.data.statusMessage;
                        }
                    })
                    .catch((err) => {
                        this.checkSave = false;
                        this.errorStatus = true;
                        this.errorMessage = err.response.data.statusMessage;
                    })
                }
            }
        } else {
            this.checkSave = false;
            this.errorStatus = true;
            this.errorMessage = 'Please input data';
        }
    }

    addList() {
        this.adjustAmountList.push({
            adjustType: null,
            adjustBank: null,
            adjustAmount: null,
            bank: null,
            fixAccountFlag: null
        })
    }

    setBank(index) {
        // adjustType dropdown
        this.adjustAmountList[index].adjustBank = null;
        this.adjustType.map(snap => {
            if (this.adjustAmountList[index].adjustType == snap.adjustMasterId) {
                this.adjustAmountList[index].fixAccountFlag = snap.fixAccountFlag ? snap.fixAccountFlag : null;
            }
        });
        axios.default.request({
            url: this.endpoint.API_BANK_ADJUST_AR,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-by-id',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                adjustMasterId: this.adjustAmountList[index].adjustType
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.adjustAmountList[index].bank = snapData.data.data.adjMastInfoList.map(snap => {
                        return {
                            bankAccountName: snap.bankAccountName,
                            bankAccountNo: snap.bankAccountNo,
                            glAccount: snap.glAccount,
                            glRc: snap.glRc,
                            glProduct: snap.glProduct,
                            glProject: snap.glProject,
                            glCs: snap.glCs,
                            glFs: snap.glFs,
                            glSs: snap.glSs,
                            glRs: snap.glRs
                        };
                    });
                }
            });
    }

    deleteList(index) {
        if (this.adjustAmountList[index].adjustDocId) {
            this.adjustAmountList[index].adjustAmount = 0;
            this.adjustAmountList[index].deleteFlag = true;
        } else {
            this.adjustAmountList.splice(index, 1);
        }
    }

    view(data) {
        this.checkSave = false;
        this.adjustAmountList = [];
        this.display = true;
        this.box = 2;
        axios.default.request({
            url: this.endpoint.API_SRCH_ADJ,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'search-managepending',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                adjustDocType: 'AR',
                postGl: 'RE',
                refAdjustDocId: data.refAdjustDocId
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.listData = {
                    amount: 0.00,
                    listAdjustDocId: undefined,
                    companyCode: undefined,
                    mainDocDate: undefined,
                    locationCode: undefined,
                    paymentType: undefined,
                    adjustStatus: undefined
                };
                snapData.data.data.adjSrchRespList.map(snap => {
                    let fixFlag = null;
                    let bank = null;
                    this.loading = false;
                    this.listData.amount = snap.adjustStatus != 3 ? parseFloat(this.listData.amount) + parseFloat(snap.adjustAmt) : parseFloat(this.listData.amount) + 0;
                    this.listData.refAdjustDocId = snap.refAdjustDocId;
                    this.listData.companyCode = snap.companyCode;
                    this.listData.mainDocDate = snap.mainDocDate;
                    this.listData.locationCode = snap.locationCode;
                    this.listData.paymentTypeCode = snap.paymentTypeCode ? snap.paymentTypeCode == 'CSH' ? 'Cash' : snap.paymentTypeCode == 'CHQ' ? 'Cheque' : snap.paymentTypeCode == 'CC' ? 'Credit Card' : 'Unknwon' : 'Unknwon';
                    this.adjustType.map(adjustList => {
                        if (adjustList.adjustMasterId == snap.adjustMasterId && snap.adjustStatus != 3) {
                            fixFlag = adjustList.fixAccountFlag != undefined ? adjustList.fixAccountFlag : null;
                        }
                    });
                    if (fixFlag && snap.adjustStatus != 3) {
                        axios.default.request({
                            url: this.endpoint.API_BANK_ADJUST_AR,
                            method: 'GET',
                            headers: {
                                'Content-Type': 'application/json',
                                'action': 'load-by-id',
                                'transactionId': uuid(),
                                'locationId': this.mockLocationId,
                                'lang': 'EN',
                                'username': this.rawMockLocationId[1],
                                'navigate': this.router.url,
                                'token': this.rawMockLocationId[3]
                            },
                            params: {
                                adjustMasterId: snap.adjustMasterId
                            }
                        })
                            .then(async bankData => {
                                if (bankData.data.statusCode == 'S0000') {
                                    bank = await bankData.data.data.adjMastInfoList.map(bank => {
                                        return {
                                            bankAccountName: bank.bankAccountName,
                                            bankAccountNo: bank.bankAccountNo,
                                            glAccount: bank.glAccount,
                                            glRc: bank.glRc,
                                            glProduct: bank.glProduct,
                                            glProject: bank.glProject,
                                            glCs: bank.glCs,
                                            glFs: bank.glFs,
                                            glSs: bank.glSs,
                                            glRs: bank.glRs
                                        };
                                    });
                                    this.adjustAmountList.push({
                                        bank: bank,
                                        adjustBank: snap.bankAccNo,
                                        fixAccountFlag: fixFlag,
                                        adjustAmount: snap.adjustAmt ? Math.abs(Number(parseFloat(snap.adjustAmt).toFixed(2))).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : 0,
                                        adjustType: snap.adjustMasterId,
                                        adjustDocId: snap.adjustDocId
                                    });
                                }
                            });
                    } else if (snap.adjustStatus != 3) {
                        this.adjustAmountList.push({
                            bank: bank,
                            adjustBank: null,
                            fixAccountFlag: fixFlag,
                            adjustAmount: snap.adjustAmt ? Math.abs(Number(parseFloat(snap.adjustAmt).toFixed(2))).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : 0,
                            adjustType: snap.adjustMasterId,
                            adjustDocId: snap.adjustDocId
                        });
                    }
                });
                // this.paginator.detail = {
                //     currentPage: snapData.data.data.pagingInfo.currentPage,
                //     totalPages: snapData.data.data.pagingInfo.totalPages,
                //     totalRecords: snapData.data.data.pagingInfo.totalRecords,
                //     recordPerPage: snapData.data.data.pagingInfo.recordPerPage
                // };
            }
            // if (snapData.data.data.pagingInfo.currentPage != 0) {
            //     this.loading = false;
            //     this.checkData = 1;
            //     this.paging = 1
            // } else {
            //     this.loading = false;
            //     this.checkData = 0;
            //     this.paging = 0;
            // }
        })
            .catch(() => {
                this.loading = false;
            });
    }

    toDocDate() {
        if (this.form.adjSrchReq.adjustDate > this.form.adjSrchReq.adjustDateTo) {
            this.form.adjSrchReq.adjustDateTo = null;
        }
        if (this.form.adjSrchReq.adjustDate) {
            this.mintoDocDate = this.form.adjSrchReq.adjustDate
            this.searchAdjustdoc = '1';
            if (this.form.adjSrchReq.adjustDateTo == null || this.form.adjSrchReq.adjustDateTo == undefined) {
                this.form.adjSrchReq.adjustDateTo = this.form.adjSrchReq.adjustDate;
            }
        } else {
            this.searchAdjustdoc = '0';
            this.form.adjSrchReq.adjustDateTo = null;
        }
    }

    toMainDocDate() {
        if (this.form.adjSrchReq.mainDocDate > this.form.adjSrchReq.mainDocDateTo) {
            this.form.adjSrchReq.mainDocDateTo = null;
        }
        if (this.form.adjSrchReq.mainDocDate) {
            this.minMainDocDate = this.form.adjSrchReq.mainDocDate;
            this.searchMaindoc = '1';
            if (this.form.adjSrchReq.mainDocDateTo == null || this.form.adjSrchReq.mainDocDateTo == undefined) {
                this.form.adjSrchReq.mainDocDateTo = this.form.adjSrchReq.mainDocDate;
            }

        } else {
            this.searchMaindoc = '0';
            this.form.adjSrchReq.mainDocDateTo = null;
        }
    }

    preDelete(refId) {
        this.box = 3;
        this.deleteRef = refId;
    }

    cancel() {
        this.box = 2;
        this.deleteRef = null;
    }

    delete(refId) {
        axios.default.request({
            url: this.endpoint.API_MANAGE_PENDING,
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'action': 'delete-postgl',
                'transactionId': uuid(),
                'username': this.rawMockLocationId[1],
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            data: {
                refAdjustDocId: refId
            }
        })
        .then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            this.errorStatus = false;
            this.errorMessage = '';
        })
        .catch((err) => {
            this.errorStatus = true;
            this.errorMessage = err.response.data.statusMessage;
        })
    }

}
