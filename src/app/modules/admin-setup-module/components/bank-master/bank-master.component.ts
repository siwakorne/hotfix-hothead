import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from 'primeng/api';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import { AppComponent } from '../../../../app.component';

@Component({
  selector: 'app-bank-master',
  templateUrl: './bank-master.component.html',
  styleUrls: ['./bank-master.component.css']
})
export class BankMasterComponent implements OnInit {
  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    locationName = '';
    formActive = false;
    bankActive = true;
    edit = 0;
    status = '';
    msg = '';
    displayAddbank= false;
    updateStatus = false;
    addStatus = false;
    company = [];
    paymentType: Array<object> = [];
    bankShortName: Array<object> = [];
    dataSearch = {
        findBy: null,
        search: null
    };
    filterBank = [];
    dbDataBank = [];
    errorDesc = '';
    errorCode = '';
    deleteData = '';
    dpsType = '';
    formDataBank = {
        bankMasterId: 0,
        companyCode: '',
        bankShortName:'',
        bankAccountNo: '',
        bankDesc:'',
        bankCode:'',
        compServiceCode: '',
        paymentTypeCode: '',
        depositType: '',
        payinLine1: '',
        payinLine2: '',
        payinLine3: '',
        bankBranch:'',
        bankAccountName:'',
    };
    paginator = {
        totalRecords: 0,
        recordPerPage: 3,
        currentPage: 0,
        totalPages: 0
    };
    rawData: object;
    // form: any = {

    //     dropdownReq: {
    //         company: '',
    //         paymentTypeCode: '',
    //         paymentTypeDesc:'',
    //         bankShortName:'',

    //     }
    // };

  constructor(private endpoint: Endpoint, private router: Router,private global : AppComponent, private HttpService: HttpService) { }

  ngOnInit() {
    this.queryData(1);
    this.dropdown();
  }
  queryData(page) {
    this.HttpService.call(this.endpoint.API_MASTER_BANK_MASTER, 'GET', 
    {
        'action': 'load-all',
        'lang': 'EN',
        'navigate': this.router.url,
    },
    null, {currentPage: page}, null,this.querySuccessCallback.bind(this), null);
    
   
}

querySuccessCallback(snap: any) {
    this.global.checkTokenExpired(snap.data.statusCode);
    if(snap.data.statusCode === 'S0000') {
        let data = snap.data.data;
        this.dbDataBank = data.masterBankMasterInfoList;
        this.paginator = data.pagingInfo;
    }
}

dropdown() {
    // company dropdown
    this.HttpService.call(this.endpoint.API_LOAD_COMP, 'GET', 
    {
        'action': 'load',
        'lang': 'EN',
        'navigate': this.router.url,
    },null, null, null,this.companyDropdownSuccessCallback.bind(this), null);
    // payment type dropdown
    this.HttpService.call(this.endpoint.API_PAYMENTTYPE_MASTER, 'GET', 
    {
        'action': 'load-all',
        'lang': 'EN',
        'navigate': this.router.url,
    },null, null, null,this.paymentTypeDropdownSuccessCallback.bind(this), null);

    //bank short name dropdown
    this.HttpService.call(this.endpoint.API_LOAD_BANK_SHORTNAME, 'GET', 
    {
        'action': 'load',
        'lang': 'EN',
        'navigate': this.router.url,
    },null, null, null,this.bankShortNameDropdownSuccessCallback.bind(this), null);

    
}

companyDropdownSuccessCallback(snapData:any) {
    this.global.checkTokenExpired(snapData.data.statusCode);
    if (snapData.data.statusCode == 'S0000') {
        this.company = snapData.data.data.companyInfoList.map(snap => {
            return {
                companyCode: snap.companyCode,
                companyName: snap.companyName,
            }
        });
    }
}

paymentTypeDropdownSuccessCallback(snapData:any) {
    this.global.checkTokenExpired(snapData.data.statusCode);
    if (snapData.data.statusCode == 'S0000') {
        this.paymentType = snapData.data.data.paymentTypeMasterInfoList.map(snap => {
            return {
                paymentTypeCode: snap.paymentTypeCode,
                paymentTypeDesc: snap.paymentTypeDesc,
            }
        });
    }
}
bankShortNameDropdownSuccessCallback(snapData:any) {
    this.global.checkTokenExpired(snapData.data.statusCode);
    if (snapData.data.statusCode == 'S0000') {
        this.bankShortName = snapData.data.data.infoList.map(snap => {
        
            return {
                bankShortName: snap.bankShortName,
            }
        });
    }
}

formBank() {
  this.bankActive = false;
  this.formActive = true;
}
displayAddBankModal(){
    this.displayAddbank = true;
    this.edit = 0;
    this.addStatus = true;
    this.updateStatus = false;
}

editBank(data) {
  this.displayAddBankModal();
  this.formDataBank = data;
  this.edit = 1;
  this.updateStatus = true;
  this.addStatus = false;
}

checkCompany(){
    this.company.map(snap => {
            if(snap.companyCode == this.formDataBank.companyCode){
            this.formDataBank.bankAccountName = snap.companyName;
            }
       })
      
    }

// update(){
//   this.displayAddbank = false;
//   this.errorCode = '';
//   this.errorDesc = '';
//   if(this.formDataBank.bankMasterId == 0)
//       this.formDataBank.bankMasterId = undefined;
//   if(this.formDataBank.companyCode.indexOf(')') != -1 
//      && this.formDataBank.bankShortName
//      && this.formDataBank.depositType
//      && this.formDataBank.bankCode
//      && this.formDataBank.bankBranch
//      && this.formDataBank.bankDesc
//      && this.formDataBank.bankAccountNo
//      && this.formDataBank.compServiceCode
//      && this.formDataBank.paymentTypeCode
//      && this.formDataBank.payinLine1
//      && this.formDataBank.payinLine2
//      && this.formDataBank.payinLine3)
//      {
//          var header = {
//           'action': this.formDataBank.bankMasterId == undefined ? 'add' : 'update',
//           'lang':'EN',
//           'navigate': this.router.url
//       }
//       this.HttpService.call(this.endpoint.API_MASTER_BANK_MASTER, 'POST', header,null,null, this.formDataBank,this.updateSuccessCallback.bind(this),null);
//   } else {
//       this.displayAddbank = false;
//       this.errorCode = 'inComplete';
//       this.errorDesc = 'กรอกข้อมูลไม่ครบถ้วน';
//   }
// }

// updateSuccessCallback(snap:any) {
//   this.global.checkTokenExpired(snap.data.statusCode);
//   if(snap.data.statusCode != 'S0000') {
//       this.errorCode = snap.data.statusCode;
//       this.errorDesc = snap.data.statusMessage;
//   }
// }
addBankMaster(data){
    this.checkCompany();
    this.displayAddbank = false;
    this.errorDesc = '';
        this.errorCode = '';
        if ((this.formDataBank.companyCode == '' || null || undefined) ||
            (this.formDataBank.bankAccountNo == '' || null || undefined) ||
            (this.formDataBank.bankShortName == '' || null || undefined) ||
            (this.formDataBank.bankDesc == '' || null || undefined) ||
            (this.formDataBank.bankCode == '' || null || undefined) ||
            (this.formDataBank.compServiceCode == '' || null || undefined) ||
            (this.formDataBank.paymentTypeCode == '' || null || undefined) ||
            (this.formDataBank.depositType == '' || null || undefined) ||
            (this.formDataBank.payinLine1 == '' || null || undefined) ||
            (this.formDataBank.payinLine2 == '' || null || undefined) ||
            (this.formDataBank.payinLine3 == '' || null || undefined) ||
            (this.formDataBank.bankBranch == '' || null || undefined)) {
                this.errorCode = 'inComplete';
                this.errorDesc = 'กรอกข้อมูลไม่ครบถ้วน';
            } else {
    var headers = {
        'action': 'add',
        'lang': 'EN',
        'navigate': this.router.url,
    }
    var datas= {
        'companyCode': data.companyCode,
        'bankShortName': data.bankShortName,
        'bankAccountNo': data.bankAccountNo,
        'bankDesc': data.bankDesc,
        'bankCode':data.bankCode,
        'compServiceCode': data.compServiceCode,
        'paymentTypeCode': data.paymentTypeCode,
        'depositType': data.depositType,
        'payinLine1': data.payinLine1,
        'payinLine2': data.payinLine2,
        'payinLine3': data.payinLine3,
        'bankBranch':data.bankBranch,
        'bankAccountName':data.bankAccountName
    }
    this.HttpService.call(this.endpoint.API_MASTER_BANK_MASTER,'POST', headers,null, null , datas ,this.addBankMasterSuccessCallback.bind(this) ,null)
}}

addBankMasterSuccessCallback(snapData: any){
    this.global.checkTokenExpired(snapData.data.statusCode);
    if (snapData.data.statusCode == 'S0000') {
        this.formDataBank = snapData.data.data.masterBankMasterInfoList
        this.msg = 'pass';
    }else{
        this.msg = 'fail';
    }
}

updateBankMaster(data){
    this.displayAddbank = false;
    this.checkCompany();
    this.errorDesc = '';
        this.errorCode = '';
        if ((this.formDataBank.companyCode == '' || null || undefined) ||
            (this.formDataBank.bankAccountNo == '' || null || undefined) ||
            (this.formDataBank.bankShortName == '' || null || undefined) ||
            (this.formDataBank.bankDesc == '' || null || undefined) ||
            (this.formDataBank.bankCode == '' || null || undefined) ||
            (this.formDataBank.compServiceCode == '' || null || undefined) ||
            (this.formDataBank.paymentTypeCode == '' || null || undefined) ||
            (this.formDataBank.depositType == '' || null || undefined) ||
            (this.formDataBank.payinLine1 == '' || null || undefined) ||
            (this.formDataBank.payinLine2 == '' || null || undefined) ||
            (this.formDataBank.payinLine3 == '' || null || undefined) ||
            (this.formDataBank.bankBranch == '' || null || undefined)) {
                this.errorCode = 'inComplete';
                this.errorDesc = 'กรอกข้อมูลไม่ครบถ้วน';
            } else {
    var headers = {
        'action': 'update',
        'lang': 'EN',
        'navigate': this.router.url,
    }
    var datas = data
    this.HttpService.call(this.endpoint.API_MASTER_BANK_MASTER,'PUT', headers,null, null , datas ,this.updateBankMasterSuccessCallback.bind(this) ,null)
}}

updateBankMasterSuccessCallback(snapData: any){
    this.global.checkTokenExpired(snapData.data.statusCode);
    if (snapData.data.statusCode == 'S0000') {
        this.formDataBank = snapData.data.data.masterBankMasterInfoList
        this.msg = 'pass';
    }else{
        this.msg = 'fail';
    }
}

clearForm() {
  this.locationName = '';
  this.dpsType = '';
  this.formDataBank = {
    bankMasterId: 0,
        companyCode: '',
        bankShortName:'',
        bankAccountNo: '',
        bankDesc:'',
        bankCode:'',
        compServiceCode: '',
        paymentTypeCode: '',
        depositType: '',
        payinLine1: '',
        payinLine2: '',
        payinLine3: '',
        bankBranch:'',
        bankAccountName:'',
  };
//   this.form = {
//     dropdownReq: {
//         company: '',
//         paymentTypeCode: '',
//         paymentTypeDesc:'',
//         bankShortName:'',
//     }
//   };
  this.paginator = {
      totalRecords: 0,
      recordPerPage: 3,
      currentPage: 0,
      totalPages: 0
  };
}


cancel() {
  this.clearForm();
  this.queryData(1);
  this.bankActive = true;
  this.formActive = false;
  this.displayAddbank = false;
}
success() {
  this.bankActive = true;
  this.formActive = false;
  this.queryData(this.paginator.currentPage);
  this.clearForm();
}

delete(data){
  this.deleteData = data;
}

deleteBank(){
  var header = {
      'action': 'delete',
      'lang':'EN',
      'navigate': this.router.url
  };
  this.HttpService.call(this.endpoint.API_MASTER_BANK_MASTER, 'DELETE', header,null, null, {bankMasterId: this.deleteData}, this.deleteSuccessCallback.bind(this), null);
}

deleteSuccessCallback(snap:any) {
  this.global.checkTokenExpired(snap.data.statusCode);
  this.queryData(1);
}

paginate(event) {
  if ((event.page + 1) != this.paginator.currentPage) {
      this.queryData(event.page + 1);
  }
}

}
