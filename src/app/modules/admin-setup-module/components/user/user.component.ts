import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { v4 as uuid } from 'uuid';
import * as moment from 'moment';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import { Router } from '@angular/router';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    formActive = false;
    findBy: string = null;
    deleteUsername: string = '';
    userActive = true;
    searchValue: string = '';
    errorDesc = '';
    edit = '';
    showPaging: boolean = false;
    status = '';
    alert = {
        username: '',
        name: '',
        ip: '',
    };
    filterStatus:boolean = false;
    filterUser = [];
    dbDataUser = [];
    listRole = [];
    oldListRole = [];
    formDataUser = {
        userName: '',
        firstName: '',
        lastName: '',
        effectiveDate: '',
        expiredDate: '',
        requestNo: ''
    };
    paginator = {
        totalRecords: 0,
        recordPerPage: 3,
        currentPage: 0,
        totalPages: 0
    };
    rawData: object;
    roleData = [];

    constructor(private endpoint: Endpoint, private HttpService: HttpService,private global: AppComponent, private router: Router) { }

    ngOnInit() {
        this.queryData(this.filterStatus, this.paginator.currentPage+1);
    }

    queryUserSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.data) {
            this.dbDataUser = snap.data.data.userInfoList;
            this.paginator = snap.data.data.pagingInfo;
            this.showPaging = true;
        }
    }

    queryData(filter:any, page:any) {
        this.edit = '';
        this.rawData=[];
        this.roleData = [];
        this.dbDataUser = [];
        let header = {
            'action': 'list',
            'lang': 'en',
            'navigate': this.router.url
        }
        if(filter) {
            this.HttpService.call(this.endpoint.API_USER_MASTER, 'GET',
            header,
            null,
            {currentPage: page, searchBy: this.findBy, searchValue: this.searchValue},
            null,
            this.queryUserSuccessCallback.bind(this),
            null)
        } else {
            this.HttpService.call(this.endpoint.API_USER_MASTER, 'GET',
            header,
            null,
            {currentPage: page},
            null,
            this.queryUserSuccessCallback.bind(this),
            null)
        }
    }

    formSearch() {
        this.showPaging = false;
        this.clearForm();
        this.findBy = this.findBy != 'null' ? this.findBy : null;
        if(this.findBy != null) {
            this.filterStatus = true;
            this.queryData(this.filterStatus, 1);
        } else {
            this.filterStatus = false;
            this.queryData(this.filterStatus, 1);
        }
        this.userActive = true;
        this.formActive = false;
    }

    formUser() {
        this.userActive = false;
        this.formActive = true;
    }

    addUser() {
        this.errorDesc = '';
        let effectiveDate = '';
        let expiredDate = ''
        if(this.formDataUser.effectiveDate) {
            if (this.formDataUser.effectiveDate.length === 10) {
                let splitDate = this.formDataUser.effectiveDate.split('/');
                let newDate = splitDate[1] + ',' + splitDate[0] + ',' + splitDate[2];
                effectiveDate = moment(new Date(newDate)).format('x');
            } else if (this.formDataUser.effectiveDate.length === 13) {
                effectiveDate = this.formDataUser.effectiveDate ? this.formDataUser.effectiveDate : '';
            } else {
                effectiveDate = this.formDataUser.effectiveDate ? moment(this.formDataUser.effectiveDate).format('x') : '';
            }
        }
        if(this.formDataUser.userName && this.formDataUser.effectiveDate) {
            let setData = {
                userName: this.formDataUser.userName,
                firstName: this.formDataUser.firstName,
                lastName: this.formDataUser.lastName,
                requestNo: this.formDataUser.requestNo,
                effectiveDate: effectiveDate,
                expiredDate: expiredDate
            }
            let header = {
                'action': 'add',
                'lang': 'en',
                'navigate': this.router.url
            }
            this.HttpService.call(this.endpoint.API_USER_MASTER, 'POST',
            header,
            null,
            null,
            setData,
            this.addUserSuccessCallback.bind(this),
            null)
        } else {
            this.errorDesc = "กรุณากรอกข้อมูลให้ครบถ้วน"
        }
    }

    addUserSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.errorDesc = '';
        if(snap.data.statusCode == 'S0000') {
            this.formSearch();
            this.errorDesc = '';
        } else {
            this.errorDesc = 'ชื่อผู้ใช้ซ้ำ';
        }
    }

    editUser(data:any, edit:any) {
        this.formUser();
        this.formDataUser = {
            userName: data.userName,
            firstName: data.firstName,
            lastName: data.lastName,
            effectiveDate: data.effectiveDate ? moment(data.effectiveDate).format("DD/MM/YYYY") : '',
            expiredDate: data.expiredDate ? moment(data.expiredDate).format("DD/MM/YYYY") : '',
            requestNo: data.requestNo
        };
        this.edit = edit;
    }

    update() {
        let expiredDate = '';
        let effectiveDate = '';
        if (this.formDataUser.effectiveDate.length === 10) {
            let splitDate = this.formDataUser.effectiveDate.split('/');
            let newDate = splitDate[1] + ',' + splitDate[0] + ',' + splitDate[2];
            effectiveDate = moment(new Date(newDate)).format('x');
        } else if (this.formDataUser.effectiveDate.length === 13) {
            effectiveDate = this.formDataUser.effectiveDate ? this.formDataUser.effectiveDate : '';
        } else {
            effectiveDate = this.formDataUser.effectiveDate ? moment(this.formDataUser.effectiveDate).format('x') : '';
        }
        let setData = {
            userName: this.formDataUser.userName,
            firstName: this.formDataUser.firstName,
            lastName: this.formDataUser.lastName,
            requestNo: this.formDataUser.requestNo,
            effectiveDate: effectiveDate,
            expiredDate: expiredDate
        }
        let header = {
            'action': 'update',
            'lang': 'en',
            'navigate': this.router.url
        }
        this.HttpService.call(this.endpoint.API_USER_MASTER, 'PUT',
        header,
        null,
        null,
        setData,
        this.updateUserSuccessCallback.bind(this),
        null)
    }

    updateUserSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.formSearch();
    }

    clearForm() {
        this.formDataUser = {
            userName: '',
            firstName: '',
            lastName: '',
            effectiveDate: '',
            expiredDate: '',
            requestNo: ''
        };
    }

    deleteUser() {
        let header = {
            'action': 'delete',
            'lang': 'en',
            'navigate': this.router.url
        }
        this.HttpService.call(this.endpoint.API_USER_MASTER, 'DELETE',
        header,
        null,
        null,
        {username: this.deleteUsername},
        this.deleteUserSuccessCallback.bind(this),
        null)
    }

    setDeleteUser(username: string, edit:any) {
        this.edit = edit;
        this.deleteUsername = username;
    }

    deleteUserSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.formSearch();
    }

    paginate(event:any) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.queryData(this.filterStatus,event.page + 1);
        }
    }
}
