import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  formUserActive;
  date = '';
  page: string = 'main';
  dateOfDay = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
  dbData = [{ day: 1, notiRole: "Coco, FoFo , SVH, TM" },
            { day: 7, notiRole: "BC, PMO" },
            { day: 14, notiRole: "CBH,Head" },
            { day: 30, notiRole: "ZBH" }
            ];

  dbData2 = [{ day: 1, notiRole: "Coco, FoFo , SVH, TM" },
  { day: 7, notiRole: "BC, PMO" },
  { day: 14, notiRole: "CBH,Head" },
  { day: 30, notiRole: "ZBH" }
  ];

  paginator = {
    totalRecords: 0,
    recordPerPage: 3,
    currentPage: 0,
    totalPages: 0
};


    cars: SelectItem[];
    selectedCars1: string[] = [];
    item: string;

    constructor() {
        this.cars = [
            {label: 'CoCo', value: 'CoCo'},
            {label: 'FoFo', value: 'FoFo'},
            {label: 'SVH', value: 'SVH'},
            {label: 'TM', value: 'TM'},
            {label: 'BC', value: 'BC'},
            {label: 'PMO', value: 'PMO'},
            {label: 'CBH', value: 'CBH'},
            {label: 'Head', value: 'Head'},
            {label: 'ZBH', value: 'ZBH'},
        ];
    }

    




  ngOnInit() {
  }



  editOst(data) {
    this.page = 'editOst';
  }



  editClosing(data){
    this.page = 'editClosing';
  }

  backTomain(){
    this.page = 'main';
  }

  saveUser() {
    
  }

}
