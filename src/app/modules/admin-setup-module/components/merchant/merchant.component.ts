import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { v4 as uuid } from 'uuid';
import { MenuService } from '../../../../views/layout/menubar/menu.service';
import { AppComponent } from '../../../../app.component';
import { Endpoint } from '../../../../common/const/endpoint';
import { Router } from '@angular/router';
import { HttpService } from '../../../../common/util/http-service';
import { timingSafeEqual } from 'crypto';


@Component({
    selector: 'app-merchant',
    templateUrl: './merchant.component.html',
    styleUrls: ['./merchant.component.css']
})
export class MerchantComponents implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));

    mockLocationId = this.rawMockLocationId[2];
    locationCode: string = this.rawMockLocationId[2];

    msg : string = '';
    msgs: Message[] = [];
    formActive = false;
    dataActive = true;
    edit = '';
    status = '';
    filterBank = [];
    dbDataBank = [];
    deleteData = '';
    filterData = [];
    dbData: any = [];
    formData = {
        locationCode : '',
        merchantType: '',
        bankName: '',
        merchantId: ''
    };
    company : string = '';
    rawData: object;
    paginator = {
        totalRecords: 0,
        recordPerPage: 3,
        currentPage: 0,
        totalPages: 0
    };
    dataSearch = {
        findBy: null,
        search: null
    };

    constructor(private router: Router, private Endpoint: Endpoint, private menuService: MenuService, private global : AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        this.queryData(1);
    }

    queryData(page) {
        this.dbData = [];
        var header = {
            'action': 'load-all',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        if(this.dataSearch.findBy == 'locationCode'){
            this.HttpService.call(this.Endpoint.API_MERCHANT, 'GET', header,null,
            {currentPage: page,
            locationCode: this.dataSearch.search}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        } else if(this.dataSearch.findBy == 'bankName'){
            this.HttpService.call(this.Endpoint.API_MERCHANT, 'GET', header,null,
            {currentPage: page,
            bankName: this.dataSearch.search}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }else if(this.dataSearch.findBy == 'merchantId'){
            this.HttpService.call(this.Endpoint.API_MERCHANT, 'GET', header,null,
            {currentPage: page,
            merchantId: this.dataSearch.search}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        } else {
            this.HttpService.call(this.Endpoint.API_MERCHANT, 'GET', header,null,
            {currentPage: page}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null); 
        }
        
    }

    querySuccessCallback (snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode == 'S0000'){
           this.dbData = snap.data.data.merchantMappingLocationInfoList;
           this.paginator = snap.data.data.pagingInfo;
        }
    }

    formBank() {
        this.edit = '0';
        this.dataActive = false;
        this.formActive = true;
    }

    cancel() {
        this.formActive = false;
        this.dataActive = true;
        this.queryData(1);
        this.clearForm();
    }

    editBank(data) {
        this.edit = '1';
        this.dataActive = false;
        this.formActive = true;
        this.formData = data;
    }

    update(){
        this.msg = '';
        if((this.formData.locationCode == null||"")
            ||(this.formData.bankName == null||"")
            ||(this.formData.merchantType == null||"")
            ||(this.formData.merchantId == null||""))
        {
            this.msg = 'notInput';
        } else {
            var header ={
                'action': 'update',
                'lang': 'EN',
                'navigate': this.router.url,
            }
            this.HttpService.call(this.Endpoint.API_MERCHANT, 'PUT', header,null, 
            null, 
            this.formData, 
            this.updateSuccessCallback.bind(this), 
            null);
        }
    }

    updateSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if (snap.data.statusCode == 'S0000') {
            this.msg = 'pass';
        }else{
            this.msg = 'fail';
        }
    }

    formSearch(){
        this.clearForm();
        this.queryData('1');
    }

    clearForm() {
        this.formData = {
            locationCode : '',
            merchantType: '',
            bankName: '',
            merchantId: ''
        };
        this.paginator = {
            totalRecords: 0,
            recordPerPage: 3,
            currentPage: 0,
            totalPages: 0
        };
    }
    
    delete(data){
        this.deleteData = data;
    }

    deleteBank(){
        var header = {
            'action': 'delete',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.Endpoint.API_MERCHANT, 'DELETE', header,null, 
        null, 
        {locationMerchantId: this.deleteData}, 
        this.deleteSuccessCallback.bind(this), 
        null);
    }

    deleteSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if (snap.data.statusCode == 'S0000') {
            this.msg = 'pass';
            this.cancel();
            this.queryData(1);
        } else {
            this.msg = 'fail';
        }
    }

    save() {
        var header =  {
            'action': 'add',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.Endpoint.API_MERCHANT, 'POST', header,null, 
        null, 
        this.formData, 
        this.saveSuccessCallback.bind(this), 
        null);
    }

    saveSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if (snap.data.statusCode == 'S0000') {
            this.msg = 'pass';
        } else {
            this.msg = 'fail';
        }
    }

    dropDown() {
        // company dropdown
        var headers = {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.Endpoint.API_LOAD_COMP, 'GET', headers,null, 
        null, 
        this.formData, 
        this.companySuccessCallback.bind(this), 
        null);
    }

    companySuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.company = snapData.data.data.companyInfoList.map(snap => {
                return {
                    companyCode: snap.companyCode,
                }
            });
        }
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.queryData(event.page + 1);
        }
    }

    clearSearchValue(){
        this.dataSearch.search = null;
    }
}
