import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { v4 as uuid } from 'uuid';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import { Router } from '@angular/router';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-pay-in-mapping',
    templateUrl: './pay-in-mapping.component.html',
    styleUrls: ['./pay-in-mapping.component.css']
})
export class PayInMappingComponent implements OnInit {

    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    formActive = false;
    dataActive = true;
    dataUserActive = true;
    formUserActive = false;
    msg: string = '';
    msguser: string = '';
    edit = '';
    edteuser = '';
    status = '';
    pageUser = 0;
    userData = [];
    location: any;
    bank: any;
    errorDesc = '';
    errorCode = '';
    deleteData = '';
    locationUser: any;
    alert = {
        username: '',
        name: '',
        ip: '',
    };
    dataSearch = {
        findBy: null,
        search: null
    };
    paging = 0;
    page: number = 0;
    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };

    filterData = [];
    dbData = [];
    roleData = [];
    formData = {
        locationCode: undefined,
        paymentTypeCode: undefined,
        defaultBank: undefined,
        bankMasterId: null
    };
    formUserData = {
        locationCode: '',
        username: ''
    };
    roleRawData: object;
    rawData: object;

    constructor(private router: Router, private endpoint: Endpoint,private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        this.queryData(1, 'paging');
        this.dropdown();
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            // this.loading = true
            this.queryData(event.page + 1, 'paging');
        }
    }

    queryData(page, click) {
        this.rawData = [];
        this.dbData = [];
        this.roleData = [];
        this.roleRawData = [];

        var headers = {
            'action': 'load-all',
            'lang': 'en',
            'navigate': this.router.url,
        }
        if (this.dataSearch.findBy == 'locationCode') {
            this.HttpService.call(this.endpoint.API_PAYIN_SETUP, 'GET', headers, null,
                {
                    currentPage: page ? page : null,
                    locationCode: this.dataSearch.search
                },
                null, this.searchSuccessCallback.bind(this), null)
        } else if (this.dataSearch.findBy == 'payinLine1') {
            this.HttpService.call(this.endpoint.API_PAYIN_SETUP, 'GET', headers, null,
                {
                    currentPage: page ? page : null,
                    payinLine1: this.dataSearch.search
                },
                null, this.searchSuccessCallback.bind(this), null)
        } else if (this.dataSearch.findBy == 'defaultBank') {
            this.HttpService.call(this.endpoint.API_PAYIN_SETUP, 'GET', headers, null,
                {
                    currentPage: page ? page : null,
                    defaultBank: this.dataSearch.search
                },
                null, this.searchSuccessCallback.bind(this), null)
        } else if (this.dataSearch.findBy == 'paymentTypeCode') {
            this.HttpService.call(this.endpoint.API_PAYIN_SETUP, 'GET', headers, null,
                {
                    currentPage: page ? page : null,
                    paymentTypeCode: this.dataSearch.search
                },
                null, this.searchSuccessCallback.bind(this), null)
        } else {
            this.HttpService.call(this.endpoint.API_PAYIN_SETUP, 'GET', headers, null,
                { currentPage: page ? page : null },
                null, this.searchSuccessCallback.bind(this), null)
        }

    }

    searchSuccessCallback(snapData: any) {
        this.global.checkTokenExpired(snapData.data.statusCode);
        this.dbData = snapData.data.data.payinSetupInfoList;
        this.paginator = snapData.data.data.pagingInfo;
        if (snapData.data.data.pagingInfo.totalRecords != 0) {
            // this.loading = false;
            // this.checkData = 1;
            this.paging = 1
        } else {
            // this.loading = false;
            // this.checkData = 0;
            this.paging = 0;
        }
    }

    formSearch() {
        this.clearForm();
        this.queryData(1, 'paging');
        this.dataActive = true;
        this.formActive = false;
        this.filterData = [];
        if (this.dataSearch.findBy.trim() && this.dataSearch.search.trim()) {
            this.filterData = this.dbData.filter(data => {
                if (this.dataSearch.findBy === '1') return data.userName.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '2') return data.name.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '3') return data.statusActive.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '4') return data.roleId == this.dataSearch.findBy ? 0 : data
            });
            if (!this.filterData.length) {
                this.msgs = [];
                this.msgs.push({ severity: 'info', summary: 'Alert', detail: 'No Data' });
            } else {
                this.msgs = [];
                this.msgs.push({ severity: 'success', summary: 'Alert', detail: `Total query ${this.filterData.length} rows` });
            }
        }
    }

    formBank() {
        this.dataActive = false;
        this.formActive = true;
    }

    editBank(data) {
        this.formBank();
        this.formData = data;
        this.edit = '1';
        this.setCnFlag()
    }

    update() {
        this.errorDesc = '';
        this.errorCode = '';
        if ((this.formData.locationCode == '' || null || undefined) ||
            (this.formData.paymentTypeCode == '' || null || undefined) ||
            (this.formData.defaultBank == '' || null || undefined) ||
            (this.formData.bankMasterId == '' || null || undefined)) {
                this.errorCode = 'inComplete';
                this.errorDesc = 'กรอกข้อมูลไม่ครบถ้วน';
        } else {

            this.dataActive = true;
            this.formActive = false;

            var headers = {
                'action': 'update',
                'lang': 'en',
                'navigate': this.router.url,
            }

            var datas = this.formData

            this.HttpService.call(this.endpoint.API_PAYIN_SETUP, 'PUT', headers, null, null, datas, this.updateSuccessCallback.bind(this), null)
        }
    }

    updateSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        if (data.data.statusCode == 'S0000') {
            this.msg = 'pass';
            this.clearForm();
            this.queryData(1, 'paging');
        } else {
            this.msg = 'fail';
        }
    }

    submitData() {
        if ((this.formData.locationCode == '' || null || undefined) ||
            (this.formData.paymentTypeCode == '' || null || undefined) ||
            (this.formData.defaultBank == '' || null || undefined) ||
            (this.formData.bankMasterId == '' || null || undefined)) {
                this.errorCode = 'inComplete';
                this.errorDesc = 'กรอกข้อมูลไม่ครบถ้วน';
        } else {
            this.dataActive = true;
            this.formActive = false;
            var headers = {
                'action': 'add',
                'lang': 'en',
                'navigate': this.router.url,
            }
            var datas = this.formData
            this.HttpService.call(this.endpoint.API_PAYIN_SETUP, 'POST', headers, null, null, datas, this.addSuccessCallback.bind(this), null)
        }

    }

    addSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        if (data.data.statusCode == 'S0000') {
            this.msg = 'pass';
            this.clearForm();
            this.queryData(1, 'paging');
        } else {
            this.msg = 'fail';
        }
    }

    clearForm() {
        this.formData = {
            locationCode: undefined,
            paymentTypeCode: undefined,
            defaultBank: undefined,
            bankMasterId: undefined,
        };
        this.alert = {
            username: '',
            name: '',
            ip: '',
        };
    }


    delete(data){
        this.deleteData = data;
    }

    deleteBank() {
        var headers = {
            'action': 'delete',
            'lang': 'en',
            'navigate': this.router.url,
        }
        var datas = {
            "locationPayinId": this.deleteData
        }
        this.HttpService.call(this.endpoint.API_PAYIN_SETUP, 'DELETE', headers, null, null, datas, this.deleteSuccessCallback.bind(this), null)
    }


    deleteSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        this.formSearch();
        this.queryData(1, 'paging');
    }



    dropdown() {
        //Location Code
        var headers = {
            'action': 'load-all-location',
            'lang': 'en',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_LOAD_ALL_LOCATION, 'Get', headers, null, null, null, this.allLocationSuccessCallback.bind(this), null)

        this.setCnFlag()
    }

    allLocationSuccessCallback(snapData: any) {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.location = snapData.data.data.locationTypeInfoList
        }
    }

    setCnFlag() {
        var headers = {
            'action': 'load-all-bank',
            'lang': 'en',
            'navigate': this.router.url,
        }
        var params = {
            paymentTypeCode: this.formData.paymentTypeCode ? this.formData.paymentTypeCode == undefined ?  null : this.formData.paymentTypeCode == 'undefined'? null : this.formData.paymentTypeCode : null
        }
        this.HttpService.call(this.endpoint.API_LOAD_ALL_BANK, 'Get', headers, null, params, null, this.bankmasterSuccessCallback.bind(this), null)
    }

    bankmasterSuccessCallback(snapData: any) {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.bank = snapData.data.data.bankInfoList
        }
    }

    clearSearchValue(){
        this.dataSearch.search = null;
    }
}
