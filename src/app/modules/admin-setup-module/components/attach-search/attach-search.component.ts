import { Component, OnInit } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { Endpoint } from '../../../../common/const/endpoint';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { HttpService } from '../../../../common/util/http-service';
import { AppComponent } from '../../../../app.component';

@Component({
  selector: 'app-attach-search',
  templateUrl: './attach-search.component.html',
  styleUrls: ['./attach-search.component.css']
})
export class AttachSearchComponent implements OnInit {
  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
  mockLocationId = this.rawMockLocationId[2];

  listDate: Array<object> = [];

  filterData: Array<object> = [];
  loading: boolean = false;
  checkData = 1;
  paging = 0;
  formSearch: {
    dateFrom: Date,
    dateTo: Date
  } = {
      dateFrom: new Date(),
      dateTo: new Date(),
    }
  paginator: any = {
    currentPage: 0,
    totalPages: 0,
    totalRecords: 0,
    recordPerPage: 3,
  };
  maxDate = new Date();
  numDate = -30;
  titleName = '';
  titleFile = '';
  constructor(private router: Router, private endpoint: Endpoint, private params: ActivatedRoute,private global : AppComponent, private HttpService: HttpService) { }

  ngOnInit() {
    this.params.queryParams.subscribe(params => {
      this.titleFile = params['doctype'];
    });
    this.loadListDocType('1');
  }

  paginate(event) {
    if ((event.page + 1) != this.paginator.currentPage) {
      //   this.loading = true
      this.search(event.page + 1);
    }
  }

  loadListDocType(page) {
    var headers = {
      'action': 'list',
      'lang': 'EN',
      'navigate': this.getNavigateWithoutQueryString(this.router.url),
    }
    this.HttpService.call(this.endpoint.API_LIST_DOC_TYPE,'GET', headers,null, null , null , this.loadListDocTypeSuccessCallback.bind(this),null);
  }

  loadListDocTypeSuccessCallback(snap: any){
    this.global.checkTokenExpired(snap.data.statusCode);
    if(snap.data.statusCode == 'S0000'){
      snap.data.data.attachMasterList.map(data => {
         if(this.titleFile == data.doctype){
           this.titleName = data.displayName;
         }
      })
    }
  }

  getNavigateWithoutQueryString(url) {
    let overstring = url.indexOf('?');
    if (overstring) {
        return url.substr(0, overstring);
    } else {
        return url;
    }
  }

  search(page){
    var headers = {
      'action': 'load-all-date',
      'lang': 'EN',
      'navigate': this.getNavigateWithoutQueryString(this.router.url),
    }

    var params = {
      dateFrom: this.formSearch.dateFrom.getTime() ? this.formSearch.dateFrom.getTime() : null,
      dateTo: this.formSearch.dateTo.getTime() ? this.formSearch.dateTo.getTime() : null
    }
    this.HttpService.call(this.endpoint.API_ATTACH_ALL_DATE+'/'+page,'GET', headers,null, params , null , this.searchSuccessCallback.bind(this),null);
  }

  searchSuccessCallback(snap: any){
    this.global.checkTokenExpired(snap.data.statusCode);
    if(snap.data.statusCode == 'S0000'){
      this.listDate = snap.data.data.dateInfo.map(data => {
          return {
           docDate: data.docDate
          }
       })
       this.paginator = snap.data.data.pagingInfo;
     }
  }

  submit(page) {
    this.filterData = [];
    this.filterData.push(
      {docDate: moment().add(this.numDate, 'days').format('DD/MM/YYYY')},
      {docDate: moment().add(this.numDate+1, 'days').format('DD/MM/YYYY')},
      {docDate: moment().add(this.numDate+2, 'days').format('DD/MM/YYYY')},
      {docDate: moment().add(this.numDate+3, 'days').format('DD/MM/YYYY')},
      {docDate: moment().add(this.numDate+4, 'days').format('DD/MM/YYYY')},
      {docDate: moment().add(this.numDate+5, 'days').format('DD/MM/YYYY')},
      {docDate: moment().add(this.numDate+6, 'days').format('DD/MM/YYYY')},
    )
  }
  link (url,date, type) {
    this.router.navigate(['/admin-setup/attach-upload'], { queryParams: {doctype: url, date: date , titleFile: type} } )
  }

  back(){
    this.router.navigate(['/admin-setup/attach-file'])
  }
}
