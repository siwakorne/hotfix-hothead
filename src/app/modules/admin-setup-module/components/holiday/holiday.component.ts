import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import * as moment from 'moment';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-holiday',
    templateUrl: './holiday.component.html',
    styleUrls: ['./holiday.component.css']
})
export class HolidayComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    formActive = false;
    dataActive = true;
    errorDesc:string = '';
    holidayBankDelId: string = '';
    edit = '';
    status = '';
    dataSearch = {
        findBy: null,
        search: null
    };
    action = '';
    filterData = [];
    dbData = [];
    formData: any = {
        holidayDate: '',
        holidayDesc: '',
        remark: '',
        activeFlag: undefined,
    };
    paginator = {
        totalRecords: 0,
        recordPerPage: 3,
        currentPage: 0,
        totalPages: 0
    };
    rawData: object;
    dateOfDay = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];

    constructor(private endpoint: Endpoint, private router: Router,private global: AppComponent,private HttpService: HttpService) { }

    ngOnInit() {
        this.queryData(1);
    };

    queryData(page) {
        this.rawData=[];
        this.dbData = [];
        let header = {
            'action': 'load-all',
            'lang':'EN',
            'navigate': this.router.url,
        }
        if(this.dataSearch.findBy == 'holidayDate'){
            let splitDate = this.dataSearch.search.split('/');
            let newDate = splitDate[1] + ',' + splitDate[0] + ',' + splitDate[2];
            let search = moment(new Date(newDate)).format('x');
            this.HttpService.call(this.endpoint.API_HOLIDAY_SETUP, 'GET', header, null,
            {currentPage: page,
            holidayDate: search }, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }else if(this.dataSearch.findBy == 'actionFlag'){
            this.HttpService.call(this.endpoint.API_HOLIDAY_SETUP, 'GET', header, null,
            {currentPage: page,
                actionFlag: this.dataSearch.search}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }else if(this.dataSearch.findBy == 'holidayDesc'){
            this.HttpService.call(this.endpoint.API_HOLIDAY_SETUP, 'GET', header, null,
            {currentPage: page,
                holidayDesc: this.dataSearch.search}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        } else {
            this.HttpService.call(this.endpoint.API_HOLIDAY_SETUP, 'GET', header, null,
            {currentPage: page}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }
        
    }

    querySuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.dbData = snap.data.data.holidaySetupInfoList;
        this.paginator = snap.data.data.pagingInfo;
    }

    formSearch() {
        this.clearForm();
        this.queryData(1);
        this.dataActive = true;
        this.formActive = false;
        this.filterData = [];
        if (this.dataSearch.findBy.trim() && this.dataSearch.search.trim()) {
            this.filterData = this.dbData.filter(data => {
                if (this.dataSearch.findBy === '1') return data.userName.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '2') return data.name.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '3') return data.statusActive.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '4') return data.roleId == this.dataSearch.findBy ? 0 : data
            });
            if (!this.filterData.length) {
                this.msgs = [];
                this.msgs.push({ severity: 'info', summary: 'Alert', detail: 'No Data' });
            } else {
                this.msgs = [];
                this.msgs.push({ severity: 'success', summary: 'Alert', detail: `Total query ${this.filterData.length} rows` });
            }
        }
    }

    formBank() {
        this.action = 'add';
        this.dataActive = false;
        this.formActive = true;
    }

    editBank(data) {
        this.formBank();
        this.action = 'edit';
        this.formData.holidayDate = new Date(data.holidayDate);
        this.formData.holidayDesc = data.holidayDesc;
        this.formData.remark = data.remark;
        this.formData.activeFlag = data.activeFlag;
    }

    update(){
        this.errorDesc = '';
        let vm = this;   
        if(this.formData.holidayDate && this.formData.holidayDesc && this.formData.activeFlag) {
            var header = {
                'action': this.action === 'edit' ? 'update' : 'add',
                'lang': 'EN',
                'navigate': this.router.url,
            }
            this.HttpService.call(this.endpoint.API_HOLIDAY_SETUP, 'POST', header, null,
            null, 
            {
                holidayDate: this.formData.holidayDate ?  moment(new Date( this.formData.holidayDate)).add(+7, 'hours').format('x') : null,
                holidayDesc: this.formData.holidayDesc,
                remark: this.formData.remark,
                activeFlag: this.formData.activeFlag ? this.formData.activeFlag == 'undefined' ? null:this.formData.activeFlag == undefined ? null : this.formData.activeFlag  : null
            }, 
            this.updateSuccessCallback.bind(this), 
            null);
        } else {
            this.errorDesc = 'กรุณากรอกให้ครบถ้วน'
        }
    }

    updateSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode === 'S0000') {
            this.dataActive = true;
            this.formActive = false;
            this.clearForm();
        }
    }

    clearForm() {
        this.formData = {
            holidayDate: '',
            holidayDesc: '',
            remark: '',
            status: undefined, 
        };
        this.paginator = {
            totalRecords: 0,
            recordPerPage: 3,
            currentPage: 0,
            totalPages: 0
        };
        this.queryData(1);
    }

    deleteBank(){
        var header = {
            'action': 'delete',
            'lang':'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_HOLIDAY_SETUP, 'DELETE', header, null,
        {holidayDate: this.holidayBankDelId}, 
        null, 
        this.deleteSuccessCallback.bind(this), 
        null);
    }

    setDeleteBank(data) {
        this.holidayBankDelId = data;
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.queryData(event.page + 1);
        }
    }

    deleteSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.queryData(1);
    }

    clearSearchValue(){
        this.dataSearch.search = null;
    }
}
