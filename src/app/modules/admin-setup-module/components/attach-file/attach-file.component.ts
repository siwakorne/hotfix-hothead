import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { Endpoint } from '../../../../common/const/endpoint';
import { v4 as uuid } from 'uuid';
import { HttpService } from '../../../../common/util/http-service';
import { AppComponent } from '../../../../app.component';
@Component({
  selector: 'app-attach-file',
  templateUrl: './attach-file.component.html',
  styleUrls: ['./attach-file.component.css']
})
export class AttachFileComponent implements OnInit {
  constructor(private router: Router, private endpoint: Endpoint,private global : AppComponent, private HttpService: HttpService) { }

  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
  mockLocationId = this.rawMockLocationId[2];

  listDoc: any;

  ngOnInit() {
    this.loadListDocType()
  }



  loadListDocType() {
    var headers = {
      'action': 'list',
      'lang': 'EN',
      'navigate': this.router.url,
    }
    this.HttpService.call(this.endpoint.API_LIST_DOC_TYPE, 'GET', headers ,null, 
        null, 
        null, 
        this.loadListDocTypeSuccessCallback.bind(this), 
        null);
  }

  loadListDocTypeSuccessCallback(snap: any){
    this.global.checkTokenExpired(snap.data.statusCode);
    if(snap.data.statusCode == 'S0000'){
      this.listDoc = snap.data.data.attachMasterList.map(data => {
          return {
            displayName: data.displayName,
            doctype: data.doctype
          }
      })
    }
  }

  link(url) {
    this.router.navigate(['/admin-setup/attach-search'], { queryParams: { doctype: url } });
  }
}
