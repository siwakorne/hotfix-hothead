import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';

@Component({
    selector: 'app-loc-vs-bank',
    templateUrl: './locvsbank.component.html',
    styleUrls: ['./locvsbank.component.css']
})
export class LocVsBankComponents implements OnInit {
    msgs: Message[] = [];
    formActive = false;
    dataActive = true;
    edit = '';
    status = '';
    dataSearch = {
        findBy: '',
        search: ''
    };
    filterData = [];
    dbData = [];
    locationCode = '';
    locationSearch = '';
    locData = [
        {bankName: 'test'},
        {bankName: 'test2'}
    ];
    bankData = [];
    rawData: object;

    constructor() { }

    ngOnInit() {
        this.queryData();
    }

    queryData() {
        this.locData = [
            {bankName: 'test'},
            {bankName: 'test2'}
        ];
        this.bankData = [];
        this.locationCode = '';
    }

    formSearch() {
        this.filterData = [];
        this.dbData.filter(snap => {
            if(snap.locationCode == this.locationSearch.toString()) {
                this.filterData = snap.bank
            }
        });
    }

    cancel() {
        this.dataActive = true;
        this.formActive = false;
    }

    formBank() {
        this.dataActive = false;
        this.formActive = true;
        if (!this.filterData.length) {
            this.queryData();
        } else {
            this.formSearch()
        }
    }

    update(){
        this.dbData = [{locationCode: this.locationCode, bank: this.bankData}];
        this.locationSearch = this.locationCode;
        this.formSearch();
        this.dataActive = true;
        this.formActive = false;
    }

    
}
