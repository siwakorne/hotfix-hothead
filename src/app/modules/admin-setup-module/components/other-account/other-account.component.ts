import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import { v4 as uuid } from 'uuid';
import { HttpService } from '../../../../common/util/http-service';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-other-account',
    templateUrl: './other-account.component.html',
    styleUrls: ['./other-account.component.css']
})
export class OtherAccountComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    formActive = false;
    dataActive = true;
    errorDesc: string = '';
    deleteId: string = '';
    edit = '';
    status = '';
    dataSearch = {
        findBy: null,
        search: null
    };
    action = '';
    filterData = [];
    dbData = [];
    formData = {
        glGroup: null,
        accountName: null,
        glAccount: null,
        glComp: null,
        glProduct: null,
        glProject: null,
        glRc: null,
        glCs: null,
        glFs: null,
        glSs: null,
        glRs: null,
        glNature: null,
        glIntComp: null,
        remark: null
    };
    paginator = {
        totalRecords: 0,
        recordPerPage: 3,
        currentPage: 0,
        totalPages: 0
    };
    rawData: object;

    constructor(private endpoint: Endpoint, private router: Router, private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        this.queryData(1);
    }

    queryData(page) {
        this.rawData = [];
        this.dbData = [];
        let header = {
            'action': 'load-all',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        if (this.dataSearch.findBy == 'glGroup') {
            this.HttpService.call(this.endpoint.API_OTHER_ACCOUNT_MAPPING, 'GET', header, null,
                {
                    currentPage: page,
                    glGroup: this.dataSearch.search
                },
                null,
                this.querySuccessCallback.bind(this),
                null);
        } else if (this.dataSearch.findBy == 'glComp') {
            this.HttpService.call(this.endpoint.API_OTHER_ACCOUNT_MAPPING, 'GET', header, null,
                {
                    currentPage: page,
                    glComp: this.dataSearch.search
                },
                null,
                this.querySuccessCallback.bind(this),
                null);
        } else if (this.dataSearch.findBy == 'accountName') {
            this.HttpService.call(this.endpoint.API_OTHER_ACCOUNT_MAPPING, 'GET', header, null,
                {
                    currentPage: page,
                    accountName: this.dataSearch.search
                },
                null,
                this.querySuccessCallback.bind(this),
                null);
        } else {
            this.HttpService.call(this.endpoint.API_OTHER_ACCOUNT_MAPPING, 'GET', header, null,
                { currentPage: page },
                null,
                this.querySuccessCallback.bind(this),
                null);
        }

    }

    querySuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.dbData = snap.data.data.otherAccountMappingInfoList;
        this.paginator = snap.data.data.pagingInfo;
    }

    formSearch() {
        this.clearForm();
        this.dataActive = true;
        this.formActive = false;
        this.filterData = [];
        if (this.dataSearch.findBy.trim() && this.dataSearch.search.trim()) {
            this.filterData = this.dbData.filter(data => {
                if (this.dataSearch.findBy === '1') return data.userName.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '2') return data.name.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '3') return data.statusActive.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '4') return data.roleId == this.dataSearch.findBy ? 0 : data
            });
            if (!this.filterData.length) {
                this.msgs = [];
                this.msgs.push({ severity: 'info', summary: 'Alert', detail: 'No Data' });
            } else {
                this.msgs = [];
                this.msgs.push({ severity: 'success', summary: 'Alert', detail: `Total query ${this.filterData.length} rows` });
            }
        }
    }

    formBank() {
        this.action = 'add';
        this.dataActive = false;
        this.formActive = true;
    }

    editBank(data) {
        this.formBank();
        this.action = 'edit';
        this.formData = data;
    } Í

    update() {
        this.errorDesc = '';
        if (this.formData.glGroup &&
            this.formData.accountName &&
            this.formData.glAccount &&
            this.formData.glComp &&
            this.formData.glProduct &&
            this.formData.glProject &&
            this.formData.glCs &&
            this.formData.glFs &&
            this.formData.glSs &&
            this.formData.glRc &&
            this.formData.glRs &&
            this.formData.glNature &&
            this.formData.glIntComp) {
            var header = {
                'action': this.action === 'edit' ? 'update' : 'add',
                'lang': 'EN',
                'navigate': this.router.url,
            }
            this.HttpService.call(this.endpoint.API_OTHER_ACCOUNT_MAPPING, 'POST', header, null,
                null,
                this.formData,
                this.updateSuccessCallback.bind(this),
                null);
        } else {
            this.errorDesc = 'กรุณากรอกให้ครบถ้วน'
        }
    }

    updateSuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if (snap.data.statusCode === 'S0000') {
            this.dataActive = true;
            this.formActive = false;
            this.clearForm();
        }
    }

    clearForm() {
        this.formData = {
            glGroup: null,
            accountName: null,
            glAccount: null,
            glComp: null,
            glProduct: null,
            glProject: null,
            glRc: null,
            glCs: null,
            glFs: null,
            glSs: null,
            glRs: null,
            glNature: null,
            glIntComp: null,
            remark: null
        };
        this.paginator = {
            totalRecords: 0,
            recordPerPage: 3,
            currentPage: 0,
            totalPages: 0
        };
        this.queryData(1);
    }

    deleteBank(data) {
        var header = {
            'action': 'delete',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_OTHER_ACCOUNT_MAPPING, 'DELETE', header, null,
            null,
            { glOthSeq: data },
            this.deleteSuccessCallback.bind(this),
            null);
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.queryData(event.page + 1);
        }
    }

    deleteSuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.queryData(1);
    }

    clearSearchValue() {
        this.dataSearch.search = null;
    }

    setDeleteData(id) {
        this.deleteId = id;
    }

}
