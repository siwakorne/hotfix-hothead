import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from 'primeng/api';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import { AppComponent } from '../../../../app.component';

@Component({
  selector: 'app-attach-mapping',
  templateUrl: './attach-mapping.component.html',
  styleUrls: ['./attach-mapping.component.css']
})
export class AttachMappingComponent implements OnInit {

  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
  formActive = false;
  dataActive = true;
  
  errorDesc:string = '';
  errorCode = '';
  edit = '';
  status = '';
  action ='';
  alertError = false
  dbData = [];
  dbDataBackup = [];
  dbDataUpdate =[];
  showButton = false;
  loading= false;
  msg='';

  formData = {
      documentType: '',
      prefixName: '',
      displayName: '',
      attachMasterId: 0,
      };
  paginator = {
      totalRecords: 0,
      recordPerPage: 10,
      currentPage: 0,
      totalPages: 0
  };
  rawData: object;

  constructor(private endpoint: Endpoint, private router: Router,private global: AppComponent, private HttpService: HttpService) { }

  ngOnInit() {
    this.queryData(1);
  }
  queryData(page) {
    this.loading = true;
    this.rawData=[];
    this.dbData = [];
    let header = {
        'action': 'load-all',
        'lang':'EN',
        'navigate': this.router.url,
    }
      this.HttpService.call(this.endpoint.API_ATTACH_MAPPING, 'GET', header,null, 
      {currentPage: page}, 
      null, 
      this.querySuccessCallback.bind(this), 
      null);
  }
  querySuccessCallback(snap:any) {
    this.global.checkTokenExpired(snap.data.statusCode);
    if (snap.data.statusCode == 'S0000') {
    this.dbData = snap.data.data.attachMappingInfoList;
    this.paginator = snap.data.data.pagingInfo;
    this.setData(snap.data.data.attachMappingInfoList);
    this.loading = false;
    this.msg = 'success';
        }     
    else {
    this.loading = false;
    this.msg = 'fail';
      }
    }
 
  
  
  setData(data){
    this.dbDataBackup = data.map(snap => {
       return {
        documentTypeBackup: snap.displayName,
        displayNameBackup: snap.displayName,
        attachMasterIdBackup: snap.attachMasterId
       }
    })
      console.log(this.dbDataBackup)
  }

  checkDataUpdate(){
    this.dbDataBackup.map(data1 => {
            if(data1.documentTypeBackup != data1.displayNameBackup){
              if(data1.displayNameBackup != null && data1.displayNameBackup != ''){
              this.dbDataUpdate.push({
              attachMasterId: data1.attachMasterIdBackup,
              displayName: data1.displayNameBackup
              })
              } 
              else {
                this.alertError = true
              }
              
        }})
      
    }
  update(){
    this.alertError = false
    this.errorCode = 'Loading';
    this.errorDesc = '';
    this.checkDataUpdate();
    console.log(this.dbDataUpdate)
    if(this.alertError == false){
        var header = {
            'action': 'update',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_ATTACH_MAPPING, 'PUT', header,null, null, {attachMappingUpdate: this.dbDataUpdate}, this.updateSuccessCallback.bind(this), this.updateFailCallback.bind(this));
      }
      else {
        this.errorCode = 'Update Fail';
      }
}

updateSuccessCallback(snap:any) {
    this.global.checkTokenExpired(snap.data.statusCode);
    if(snap.data.statusCode === 'S0000') {
        this.dataActive = true;
        this.formActive = false;
        this.clearForm();
        this.errorCode="Success";
        
    }
    else {
        this.errorCode = 'Update Fail';
    }
}

updateFailCallback(error){
  this.errorCode = 'Update Fail';
}

clearForm() {
  this.showButton = false;
  this.formData = {
      documentType: '',
      prefixName: '',
      displayName: '',
      attachMasterId: 0,
  };
  this.queryData(1);
}
changeValue(){
  this.showButton = true;
}
  paginate(event) {
    if ((event.page + 1) != this.paginator.currentPage) {
                this.queryData(event.page + 1);
      }
      
  }
}
