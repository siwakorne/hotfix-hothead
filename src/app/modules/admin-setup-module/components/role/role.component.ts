import { Component, OnInit } from '@angular/core';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import { Router } from '@angular/router';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-role',
    templateUrl: './role.component.html',
    styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
    items = [];
    sourceData = [];
    backupSource = [];
    targetData = [];
    backupTarget = [];
    reloaded: boolean = false;
    currentClick = '';
    loading: boolean = false;
    dataArray: string[] = [];
    selectData: string[];
    msgs: any = [];
    display = false;
    box = 0;
    editAtRole =0;
    errorDesc: string = '';
    errorCode: string = '';
    oldRole=0;
    trackMove: boolean = false;
    checked =false;
    roleName = '';
    roleLabelName = [];
    roleData = [];
    data = [];
    rawData = {
        roleId : 0,
        roleStatus : '',
        roleName : '',
        data : []
    };

    constructor(private endpoint: Endpoint, private HttpService: HttpService,private global: AppComponent, private router: Router) {
    }


    ngOnInit() {
        this.queryData();
    }

    querySuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.items = [];
        this.roleData = [];
        snap.data.data.map(list => {
            this.roleData.push({
                roleId: list.roleId,
                roleName: list.roleName
            });
            this.items.push({
                label: list.roleName, 
                command: (click) => {
                    this.targetData = [];                    
                    this.currentClick = list.roleId;
                    this.oldRole = this.editAtRole;
                    this.editAtRole = list.roleId;
                    this.roleName = list.roleName;
                    list.permitList[0] != null 
                    ? list.permitList.map(data => {
                        let name = '';
                        if(data == '01')        name = 'Confirm Sale & Payment';
                        else if(data == '02')   name = 'Closing End of Day';
                        else if(data == '03')   name = 'Search Adjust';
                        else if(data == '04')   name = 'Pay in slip Menu';
                        else if(data == '0401') name = 'Pay in slip print';
                        else if(data == '0402') name = 'Pay in slip rePrint';
                        else if(data == '0403') name = 'Pay in slip cancel';
                        else if(data == '0404') name = 'Pay in slip Search';
                        else if(data == '05')   name = 'Search Adjust Credit Card';
                        else if(data == '06')   name = 'My View AR';
                        else if(data == '07')   name = 'Login';
                        else if(data == '08')   name = 'Master Setup Menu';
                        else if(data == '0801') name = 'Adjust Master';
                        else if(data == '0802') name = 'Bank Account Master';
                        else if(data == '0803') name = 'Barcode Master';
                        else if(data == '0804') name = 'PaymentType Master';
                        else if(data == '0805') name = 'Location Master';
                        else if(data == '0806') name = 'Merchant Master';
                        else if(data == '0807') name = 'Suspense Master';
                        else if(data == '0808') name = 'Company Master';
                        else if(data == '0809') name = 'Pay in Master';
                        else if(data == '0810') name = 'Global Master';
                        else if(data == '0811') name = 'Bank Sla Master';
                        else if(data == '0812') name = 'Holiday Master';
                        else if(data == '0813') name = 'User Master';
                        else if(data == '0814') name = 'Other Account Master';
                        else if(data == '0815') name = 'Role Master';
                        else if(data == '1001') name = 'Summary Report';
                        else if(data == '1002') name = 'Cover WHT Report';
                        else if(data == '11')   name = 'Manage Pending';
                        else if(data == '12')   name = 'Attach File'
                        else name = `Not Found at Perm ${data}`;
                        this.targetData.push({
                            name: name,
                            perm: data
                        })
                    }) : [];
                    this.backupTarget = this.targetData;
                }
            });
        })
        var filterSource = this.sourceData.filter(this.comparer(this.targetData));
        // var filterTarget = this.targetData.filter(this.comparer(this.sourceData));
        this.sourceData = filterSource;
        this.backupSource = filterSource;
        // this.sourceData = this.arr_diff(this.sourceData, this.targetData);
        this.reloaded = true;
    }

    queryData() {
        this.loading = true;
        this.data = [];
        this.roleLabelName = [];
        this.reloaded = false;
        this.HttpService.call(this.endpoint.API_ROLE_MASTER, 'GET', 
        {
            action: 'list',
            lang: 'en',
            navigate: this.router.url,
        },
        null, 
        null, 
        null,
        this.querySuccessCallback.bind(this), 
        null);
        this.loading = false;
    }

    comparer(otherArray){
        return function(current){
          return otherArray.filter(function(other){
            return other.name == current.name && other.perm == current.perm
          }).length == 0;
        }
    }

    arr_diff (a1, a2) {
        var a = [], diff = [];
        for (var i = 0; i < a1.length; i++) {
            a[a1[i]] = true;
        }
        for (var i = 0; i < a2.length; i++) {
            if (a[a2[i]]) {
                delete a[a2[i]];
            } else {
                a[a2[i]] = true;
            }
        }
        for (var k in a) {
            diff.push(k);
        }
        return diff;
    }

    setPerm() {
        this.sourceData = [
            {perm: "01",   name: 'Confirm Sale & Payment'},
            {perm: "02",   name: 'Closing End of Day'},
            {perm: "03",   name: 'Search Adjust'},
            {perm: "04",   name: 'Pay in slip Menu'},
            {perm: "0401", name: 'Pay in slip print'},
            {perm: "0402", name: 'Pay in slip rePrint'},
            {perm: "0403", name: 'Pay in slip cancel'},
            {perm: "0404", name: 'Pay in slip Search'},
            {perm: "05",   name: 'Search Adjust Credit Card'},
            {perm: "06",   name: 'My View AR'},
            {perm: "07",   name: 'Login'},
            {perm: "08",   name: 'Master Setup Menu'},
            {perm: "0801", name: 'Adjust Master'},
            {perm: "0802", name: 'Bank Account Master'},
            {perm: "0803", name: 'Barcode Master'},
            {perm: "0804", name: 'PaymentType Master'},
            {perm: "0805", name: 'Location Master'},
            {perm: "0806", name: 'Merchant Master'},
            {perm: "0807", name: 'Suspense Master'},
            {perm: "0808", name: 'Company Master'},
            {perm: "0809", name: 'Pay in Master'},
            {perm: "0810", name: 'Global Master'},
            {perm: "0811", name: 'Bank Sla Master'},
            {perm: "0812", name: 'Holiday Master'},
            {perm: "0813", name: 'User Master'},
            {perm: "0814", name: 'Other Account Master'},
            {perm: "0815", name: 'Role Master'},
            {perm: "1001", name: 'Summary Report'},
            {perm: "1002", name: 'Cover WHT Report'},
            {perm: "11",   name: 'Manage Pending'},
            {perm: "12",   name: 'Attach File'},
        ]
    }

    showDialog() {
        this.trackMove = false;
        this.setPerm();
        this.queryData();
        this.checked = true;
        this.display = true;
    }

    save() {
        this.checked = false;
        this.rawData.data = [];
        let setTarget = [];
        if(this.targetData.length){
            this.targetData.map(target => {
                setTarget.push(target.perm);
            });
        }
        this.HttpService.call(this.endpoint.API_ROLE_MASTER, 'PUT', 
        {
            action: 'update',
            lang: 'en',
            navigate: this.router.url,
        },
        null, 
        null, 
        {
            permitList: setTarget,
            roleId: this.editAtRole,
        },
        this.saveSuccessCallback.bind(this), 
        null);
    }

    saveSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.checked = true;
    }

    // cancel() {
    //     this.sourceData = this.backupSource;
    //     this.targetData = this.backupTarget;
    //     this.queryData();
    // }
    
    close() {
        this.clearData();
        this.display = false;
        this.box = 0;
    }

    showAdd() {
        this.rawData.roleId = this.roleLabelName.length+1
        this.display = true;
        this.box = 1;
    }

    showDelete() {
        this.display = true;
        this.box = 2;
    }

    add() {
        this.rawData.data = [];
        this.HttpService.call(this.endpoint.API_ROLE_MASTER, 'POST', 
        {
            action: 'add',
            lang: 'en',
            navigate: this.router.url,
        },
        null,
        null, 
        {
            roleName: this.rawData.roleName
        },
        this.addSuccessCallback.bind(this), 
        null);
    }

    delete() {
        this.rawData.data = [];
        this.HttpService.call(this.endpoint.API_ROLE_MASTER, 'DELETE', 
        {
            action: 'delete',
            lang: 'en',
            navigate: this.router.url,
        },
        null,
        null, 
        {roleId : this.rawData.roleId},
        this.deleteSuccessCallback.bind(this), 
        null);
    }

    clearData() {
        this.rawData = {
            roleId : 0,
            roleStatus : '',
            roleName : '',
            data : []
        };
    }

    addSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode == 'S0000') {
            this.targetData = [];
            this.queryData();
            this.rawData = {
                roleId : 0,
                roleStatus : '',
                roleName : '',
                data : []
            };
            this.box = 0;
        } else {
            this.errorCode = snap.data.statusCode
            this.errorDesc = snap.data.statusMessage
        }
    }

    deleteSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.errorCode = '';
        this.errorDesc = '';
        if(snap.data.statusCode == 'S0000') {
            this.targetData = [];
            this.queryData();
            this.box = 0;
        } else {
            this.errorCode = snap.data.statusCode
            this.errorDesc = snap.data.statusMessage
        }
    }
    
    checkMove(){
        this.trackMove = true;
    }
}
