import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from 'primeng/api';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-bank',
    templateUrl: './bank.component.html',
    styleUrls: ['./bank.component.css']
})
export class BankComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    locationName = '';
    formActive = false;
    bankActive = true;
    edit = '';
    status = '';
    dataSearch = {
        findBy: null,
        search: null
    };
    filterBank = [];
    dbDataBank = [];
    errorDesc = '';
    errorCode = '';
    deleteData = '';
    dpsType = '';
    formDataBank = {
        bankAccountId: 0,
        companyCode: '',
        bankShortName:null,
        bankAccountName: '',
        bankAccountNo: '',
        glAccount: '',
        glRc: '',
        glProduct: '',
        glProject: '',
        glCs: '',
        glFs: '',
        glSs: '',
        glRs: ''
    };
    paginator = {
        totalRecords: 0,
        recordPerPage: 3,
        currentPage: 0,
        totalPages: 0
    };
    rawData: object;

    constructor(private endpoint: Endpoint, private router: Router,private global : AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        this.queryData(1);
    }

    queryData(page) {
        var header = {
            'action': 'load-all',
            'lang':'EN',
            'navigate': this.router.url,
        }
        this.dbDataBank = [];
        if(this.dataSearch.findBy == 'companyCode'){
            this.HttpService.call(this.endpoint.API_BANK_MASTER, 'GET', header,null, 
            {currentPage: page,
             companyCode: this.dataSearch.search
            }, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        } else if (this.dataSearch.findBy == 'bankShortName'){
            this.HttpService.call(this.endpoint.API_BANK_MASTER, 'GET', header,null, 
            {currentPage: page,
             bankShortName: this.dataSearch.search }, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        } else if (this.dataSearch.findBy == 'bankAccountNo'){
            this.HttpService.call(this.endpoint.API_BANK_MASTER, 'GET', header,null, 
            {currentPage: page,
                bankAccountNo: this.dataSearch.search }, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }else {
            this.HttpService.call(this.endpoint.API_BANK_MASTER, 'GET', header,null, 
            {currentPage: page
            }, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }
       
    }
    
    querySuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode === 'S0000') {
            let data = snap.data.data;
            this.dbDataBank = data.masterBankEdcInfoList;
            this.paginator = data.pagingInfo;
        }
    }

    formSearch() {
        this.clearForm();
        this.queryData(1);
        this.bankActive = true;
        this.formActive = false;
        this.filterBank = [];
        if (this.dataSearch.findBy.trim() && this.dataSearch.search.trim()) {
            this.filterBank = this.dbDataBank.filter(data => {
                if (this.dataSearch.findBy === '1') return data.companyCode.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data;
                else if (this.dataSearch.findBy === '2') return data.name.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data;
                else if (this.dataSearch.findBy === '3') return data.statusActive.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data;
                else if (this.dataSearch.findBy === '4') return data.roleId == this.dataSearch.findBy ? 0 : data;
            });
            if (!this.filterBank.length) {
                this.msgs = [];
                this.msgs.push({ severity: 'info', summary: 'Alert', detail: 'No Data' });
            } else {
                this.msgs = [];
                this.msgs.push({ severity: 'success', summary: 'Alert', detail: `Total query ${this.filterBank.length} rows` });
            }
        }
    }

    formBank() {
        this.bankActive = false;
        this.formActive = true;
    }

    editBank(data) {
        this.formBank();
        this.formDataBank = data;
    }

    update(){
        this.errorCode = '';
        this.errorDesc = '';
        if(this.formDataBank.bankAccountId == 0)
            this.formDataBank.bankAccountId = undefined;
        if(this.formDataBank.bankAccountName.indexOf(')') != -1 
           && this.formDataBank.companyCode
           && this.formDataBank.bankShortName
           && this.dpsType
           && this.formDataBank.glAccount
           && this.formDataBank.glCs
           && this.formDataBank.glFs
           && this.formDataBank.glProduct
           && this.formDataBank.glProject
           && this.formDataBank.glRc
           && this.formDataBank.glRs
           && this.formDataBank.glSs) {
               var header = {
                'action': this.formDataBank.bankAccountId == undefined ? 'add' : 'update',
                'lang':'EN',
                'navigate': this.router.url
            }
            this.HttpService.call(this.endpoint.API_BANK_MASTER, 'POST', header,null, 
            null, 
            this.formDataBank, 
            this.updateSuccessCallback.bind(this), 
            null);
        } else {
            this.errorCode = 'inComplete';
            this.errorDesc = 'กรอกข้อมูลไม่ครบถ้วน';
        }
    }

    updateSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode != 'S0000') {
            this.errorCode = snap.data.statusCode;
            this.errorDesc = snap.data.statusMessage;
        }
    }

    clearForm() {
        this.locationName = '';
        this.dpsType = '';
        this.formDataBank = {
            bankAccountId: 0,
            companyCode: '',
            bankShortName:'',
            bankAccountName: '',
            bankAccountNo: '',
            glAccount: '',
            glRc: '',
            glProduct: '',
            glProject: '',
            glCs: '',
            glFs: '',
            glSs: '',
            glRs: ''
        };
        this.paginator = {
            totalRecords: 0,
            recordPerPage: 3,
            currentPage: 0,
            totalPages: 0
        };
    }

    success() {
        this.bankActive = true;
        this.formActive = false;
        this.queryData(1);
        this.clearForm();
    }

    delete(data){
        this.deleteData = data;
    }

    deleteBank(){
        var header = {
            'action': 'delete',
            'lang':'EN',
            'navigate': this.router.url
        };
        this.HttpService.call(this.endpoint.API_BANK_MASTER, 'DELETE', header,null, 
        null, 
        {bankAccountId: this.deleteData}, 
        this.deleteSuccessCallback.bind(this), 
        null);
    }

    deleteSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.queryData(1);
    }

    autoFillAcc() {
        this.formDataBank.bankAccountName = this.dpsType + ' ' + this.formDataBank.bankShortName + ' ' + this.locationName + ' ' ;
        if (this.formDataBank.bankAccountNo) {
            this.formDataBank.bankAccountName += this.bankMask(this.formDataBank.bankAccountNo) ? this.bankMask(this.formDataBank.bankAccountNo) : '';
        }
    }

    bankMask(bankCode) {
        let preCode = '';
        if(bankCode.length <= 10) {
            if (bankCode.length === 10) {
                preCode = '(' + bankCode.substr(0,3) + '-'
                + bankCode.substr(3,1) + '-'
                + bankCode.substr(4,5) + '-'
                + bankCode.substr(9,1) + ')';
                return preCode;
            }
        } else {
            if (bankCode.length === 12) {
                preCode = '(' + bankCode.substr(0,3) + '-'
                + bankCode.substr(3,6) + '-'
                + bankCode.substr(9,3) + ')';
                return preCode;
            }
        }
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.queryData(event.page + 1);
        }
    }
}
