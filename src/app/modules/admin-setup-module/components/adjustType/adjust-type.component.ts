import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { Endpoint } from '../../../../../app/common/const/endpoint';
import { Router } from '@angular/router';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service'; 
@Component({
    selector: 'app-adjust-type',
    templateUrl: './adjust-type.component.html',
    styleUrls: ['./adjust-type.component.css']
})

export class AdjustTypeComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    msg:string = '';
    formActive = false;
    userActive = true;
    adjustType;
    adjustDetail;
    edit = '0';
    paging = 0;
    status = '';
    deleteData = '';
    modalAdjustTypeSutup = false;
    company: Array<Object> = [];
    bankShortName: Array<Object> = [];
    alert = {
        username: '',
        name: '',
        ip: '',
    };
    dataSearch = {
        findBy: null,
        search: null
    };
    page: number = 0;
    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };
    paginatorAdjustDetail: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };

    filterUser = [];
    dbDataUser = [];
    formDataUser = {
        userName: '',
        name: '',
        roleId: '',
        ip: '',
        effcDate: '',
        expDate: '',
        statusActive: ''
    };
    formData = {
        statusActive: null,
        adjustDocType: null,
        adjustMasterId: null,
        adjustStatusCode: null,
        adjustStatusDesc: null,
        glAccount: null,
        glProduct: null,
        glProject: null,
        onetimePostFlag: null,
        adjustType: null,
        status: null,
        glAcc: null,
        Flag: null,
        glRc: null,
        glPd: null,
        glPj: null,
        glCs: null,
        glFs: null,
        glSs: null,
        glRs: null,
        cnFlag : null,
        reverseMasterId : null,
        reverseAccFlag : null,
        checkAmtFlag : null,
        checkAmtValue : null
    }
    formDataAdjustDetail = {
        adjustDetailId: null,
	    adjustCode: null,
	    companyCode: null,
	    bankShortName: null,
	    bankAccountName: null,
	    bankAccountNo: null,
	    glAccount: null,
	    glRc: null,
	    glProduct: null,
	    glProject: null,
	    glCs: null,
	    glFs: null,
	    glSs: null,
	    glRs: null,
        statusActive: null,
    }
    rawData: object;

    constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        this.queryData(1, 'paging');
        this.dropdown();
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            // this.loading = true
            this.queryData(event.page + 1, 'paging');
        }
    }

    paginateAdjudtDetail(event) {
        if ((event.page + 1) != this.paginatorAdjustDetail.currentPage) {
            // this.loading = true
            this.queryDataAdjustDetail(event.page + 1, 'paging');
        }
    }


    searchSuccessCallback(snapData:any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.adjustType = snapData.data.data.adjustTypeInfoList.map(snap => {
                return {
                    adjustDocType: snap.adjustDocType,
                    adjustMasterId: snap.adjustMasterId,
                    adjustStatusCode: snap.adjustStatusCode,
                    adjustStatusDesc: snap.adjustStatusDesc,
                    adjustType: snap.adjustType,
                    glAccount: snap.glAccount,
                    glCs: snap.glCs,
                    glFs: snap.glFs,
                    glProduct: snap.glProduct,
                    glProject: snap.glProject,
                    glRc: snap.glRc,
                    glRs: snap.glRs,
                    glSs: snap.glSs,
                    onetimePostFlag: snap.onetimePostFlag,
                    checkAmtFlag: snap.checkAmtFlag
                }
              });
                this.paginator = snapData.data.data.pagingInfo;
            }
            if (snapData.data.data.pagingInfo.totalRecords != 0) {
                // this.loading = false;
                // this.checkData = 1;
                this.paging = 1
            } else {
                // this.loading = false;
                // this.checkData = 0;
                this.paging = 0;
            }
        }
    

    queryData(page, click) {  
        var headers ={
            'action': 'load-all',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        var params = {
            'adjustDocType': null,
            'currentPage': page ? page : null
        }   
        if(this.dataSearch.findBy == 'adjustDocType'){
            this.HttpService.call(this.endpoint.API_LOAD_ADJ_TYPE_MASTER,'GET', headers,null, 
            {
                adjustDocType: this.dataSearch.search,
                currentPage: page ? page : null
            }    , null , this.searchSuccessCallback.bind(this) ,null)
        } else if(this.dataSearch.findBy == 'adjustType'){
            this.HttpService.call(this.endpoint.API_LOAD_ADJ_TYPE_MASTER,'GET', headers,null, 
            {
                adjustType: this.dataSearch.search,
                currentPage: page ? page : null
            }    , null , this.searchSuccessCallback.bind(this) ,null)
        } else if(this.dataSearch.findBy == 'onetimePostFlag'){
            this.HttpService.call(this.endpoint.API_LOAD_ADJ_TYPE_MASTER,'GET', headers,null, 
            {
                onetimePostFlag: this.dataSearch.search,
                currentPage: page ? page : null
            }    , null , this.searchSuccessCallback.bind(this) ,null)
        }else if(this.dataSearch.findBy == 'glAccount'){
            this.HttpService.call(this.endpoint.API_LOAD_ADJ_TYPE_MASTER,'GET', headers,null, 
            {
                glAccount: this.dataSearch.search,
                currentPage: page ? page : null
            }    , null , this.searchSuccessCallback.bind(this) ,null)
        }else if(this.dataSearch.findBy == 'adjustStatusDesc'){
            this.HttpService.call(this.endpoint.API_LOAD_ADJ_TYPE_MASTER,'GET', headers,null, 
            {
                adjustStatusDesc: this.dataSearch.search,
                currentPage: page ? page : null
            }    , null , this.searchSuccessCallback.bind(this) ,null)
        } else {
            this.HttpService.call(this.endpoint.API_LOAD_ADJ_TYPE_MASTER,'GET', headers,null, 
            {
                currentPage: page ? page : null
            }    , null , this.searchSuccessCallback.bind(this) ,null)
        }
           
                
    }

    formSearch() {
        this.clearForm();
        this.queryData('1', 'paging') 
        this.userActive = true;
        this.formActive = false;
        this.filterUser = [];
        if (this.dataSearch.findBy.trim() && this.dataSearch.search.trim()) {
            this.filterUser = this.dbDataUser.filter(data => {
                if (this.dataSearch.findBy === '1') return data.role.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '2') return data.adjustType.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '3') return data.status.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : this.global.setStatus(data)
                else if (this.dataSearch.findBy === '4') return data.glAcc.toLowerCase().indexOf(this.dataSearch.findBy.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '4') return data.Flag.toLowerCase().indexOf(this.dataSearch.findBy.toLowerCase()) ? '' : data
            });
            if (!this.filterUser.length) {
                this.msgs = [];
                this.msgs.push({ severity: 'info', summary: 'Alert', detail: 'No Data' });
            } else {
                this.msgs = [];
                this.msgs.push({ severity: 'success', summary: 'Alert', detail: `Total query ${this.filterUser.length} rows` });
            }
        }
    }

    formUser() {
        this.edit = '0';
        this.userActive = false;
        this.formActive = true;
        this.queryDataAdjustDetail(1, 'paging');
    }

    addType(data) {
        this.msg = '';
        if((data.adjustDocType == null||"")||(data.adjustType == null||"")||(data.adjustStatusCode == null||"")||(data.onetimePostFlag == null||""))
        { this.msg = 'notInput'; }
        
        if(data.onetimePostFlag == 'Y'){
            if((data.glAccount == null||"")||(data.glRc == null||"")||(data.glProduct == null||"")||(data.glProject == null||"")||(data.glCs == null||"")||(data.glFs == null||"")||(data.glSs == null||"")||(data.glRs == null||"")){
                this.msg = 'notInput';
            }
        }else{
            var headers = {
                'action': 'add',
                'lang': 'EN',
                'navigate': this.router.url,
            }
            var datas= {
                "adjustDocType": data.adjustDocType,
                "adjustType": data.adjustType,
                "adjustStatusCode": data.adjustStatusCode,
                "glAccount": data.glAccount,
                "glRc": data.glRc,
                "glProduct": data.glProduct,
                "glProject": data.glProject,
                "glCs": data.glCs,
                "glFs": data.glFs,
                "glSs": data.glSs,
                "glRs": data.glRs,
                "checkAmtFlag" : data.checkAmtFlag,
                "checkAmtValue" : data.checkAmtValue,
                "oneTimePostFlag" : data.onetimePostFlag
            }
      
            this.HttpService.call(this.endpoint.API_ADD_ADJ_TYPE_MASTER,'POST', headers,null, null , datas ,this.addTypeSuccessCallback.bind(this) ,null)
        }
    }

    addTypeSuccessCallback(snapData:any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.msg = 'pass';
            this.queryData(1, 'paging');
        }else{
            this.msg = 'fail';
        }
    }

    async editUser(data) {
        var  headers = {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        var params = {
            'adjustMasterId': data
        }
        this.HttpService.call(this.endpoint.API_LOAD_ADJ_TYPE_MASTER_DETAIL,'GET', headers,null, params , null ,this.edituserSuccessCallback.bind(this) ,null)
        this.userActive = false;
        this.formActive = true;
        this.edit = '1';
    }

    edituserSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.formData = snapData.data.data.adjustTypeInfo;
            // this.queryData(1, 'paging');
        }
    }

    update(data) {
        var headers = {
            'action': 'update',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        var datas = data
        this.HttpService.call(this.endpoint.API_UPDATE_ADJ_TYPE_MASTER,'PUT', headers,null, null , datas ,this.updateSuccessCallback.bind(this) ,null)
    }

    updateSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.formData = snapData.data.data.adjustTypeInfo
            this.msg = 'pass';
            this.queryData(1, 'paging');
        }else{
            this.msg = 'fail';
        }
    }

    clearForm() {
        this.formData = {
            statusActive: null,
            adjustDocType: null,
            adjustMasterId: null,
            adjustStatusCode: null,
            adjustStatusDesc: null,
            glAccount: null,
            glProduct: null,
            glProject: null,
            onetimePostFlag: null,
            adjustType: null,
            status: null,
            glAcc: null,
            Flag: null,
            glRc: null,
            glPd: null,
            glPj: null,
            glCs: null,
            glFs: null,
            glSs: null,
            glRs: null,
            cnFlag : null,
            reverseMasterId : null,
            reverseAccFlag : null,
            checkAmtFlag : null,
            checkAmtValue : null
        }
        this.formDataAdjustDetail = {
            adjustDetailId: null,
            adjustCode: null,
            companyCode: null,
            bankShortName: null,
            bankAccountName: null,
            bankAccountNo: null,
            glAccount: null,
            glRc: null,
            glProduct: null,
            glProject: null,
            glCs: null,
            glFs: null,
            glSs: null,
            glRs: null,
            statusActive: null,
        }
    }

    clearFormAdjustDetail() {
        this.modalAdjustTypeSutup = true;
        this.formDataAdjustDetail = {
            adjustDetailId: null,
            adjustCode: null,
            companyCode: null,
            bankShortName: null,
            bankAccountName: null,
            bankAccountNo: null,
            glAccount: null,
            glRc: null,
            glProduct: null,
            glProject: null,
            glCs: null,
            glFs: null,
            glSs: null,
            glRs: null,
            statusActive: null,
        }
    }

    closeFormAdjustDetail() {
        this.modalAdjustTypeSutup = false;
        this.formDataAdjustDetail = {
            adjustDetailId: null,
            adjustCode: null,
            companyCode: null,
            bankShortName: null,
            bankAccountName: null,
            bankAccountNo: null,
            glAccount: null,
            glRc: null,
            glProduct: null,
            glProject: null,
            glCs: null,
            glFs: null,
            glSs: null,
            glRs: null,
            statusActive: null,
        }
    }

    delete(data){
        this.deleteData = data;
    }

    deleteAdjustType() {
        var headers= {
            'action': 'delete',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var datas = {
            "adjustMasterId": this.deleteData
                }
        this.HttpService.call(this.endpoint.API_DELETE_ADJ_TYPE_MASTER,'DELETE', headers,null, null , datas ,this.deleteSuccessCallback.bind(this) ,null)
    }


    deleteSuccessCallback(data: any){
        this.global.checkTokenExpired(data.data.statusCode);
        this.queryData(1, 'paging');
    }

    clearReverse(){
        this.formData.reverseMasterId = null;
        this.formData.reverseAccFlag = null;
    }

    clearAmt(){
        this.formData.checkAmtValue = null
    }

    isOnlyNumber(event, data, type) {
        if (data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                data = data.replace(/,/g, '');
                if ((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    if(type == 'amount') {
                        this.formData.checkAmtValue = Math.abs(this.setDecimal(parseFloat(data))).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                    } 
                }
            }
        }
    }

    setDecimal(number: number) {
        number = isNaN(number) ? 0 : number;
            if (number) {
                var digit = number.toString().indexOf('.');
                if (digit != -1) {
                    number = parseFloat(number.toString().substr(0, (digit + 3)));
                }
            }
            return number;
    }

    clearSearchValue(){
        this.dataSearch.search = null;
    }

    
    searchAdjustDetailSuccessCallback(snapData:any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.adjustDetail = snapData.data.data.adjustDetailInfoList.map(snap => {
                return {
                    adjustDetailId: snap.adjustDetailId,
	                adjustCode: snap.adjustCode,
	                companyCode: snap.companyCode,
	                bankShortName: snap.bankShortName,
	                bankAccountName: snap.bankAccountName,
	                bankAccountNo: snap.bankAccountNo,
	                glAccount: snap.glAccount,
	                glRc: snap.glRc,
	                glProduct: snap.glProduct,
	                glProject: snap.glProject,
	                glCs: snap.glCs,
	                glFs: snap.glFs,
	                glSs: snap.glSs,
	                glRs: snap.glRs,
                }
              });
                this.paginatorAdjustDetail = snapData.data.data.pagingInfo;
            }
            if (snapData.data.data.pagingInfo.totalRecords != 0) {
                // this.loading = false;
                // this.checkData = 1;
                this.paging = 1
            } else {
                // this.loading = false;
                // this.checkData = 0;
                this.paging = 0;
            }
        }

    queryDataAdjustDetail(page, click) {
        this.closeFormAdjustDetail();
        var headers ={
            'action': 'load-all',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_ADJUSTDETAIL_MASTER,'GET', headers,null, 
        { currentPage: page}, 
        null , this.searchAdjustDetailSuccessCallback.bind(this) ,null)
    }

    async editUserAdjustDetail(data) {
        this.modalAdjustTypeSutup = true;
        var  headers = {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        var params = {
            'adjustDetailId': data
        }
        this.HttpService.call(this.endpoint.API_ADJUSTDETAIL_MASTER,'GET', headers,null, params , null ,this.edituserAdjustDetailSuccessCallback.bind(this) ,null)
        this.userActive = false;
        this.formActive = true;
        this.edit = '1';
    }

    edituserAdjustDetailSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.formDataAdjustDetail = snapData.data.data.adjustDetailInfo;
        }
    }

    dropdown() {
        // company dropdown
        this.HttpService.call(this.endpoint.API_LOAD_COMP, 'GET', 
        {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        },null, null, null,this.companyDropdownSuccessCallback.bind(this), null);
        //bank short name dropdown
        this.HttpService.call(this.endpoint.API_LOAD_BANK_SHORTNAME, 'GET', 
        {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        },null, null, null,this.bankShortNameDropdownSuccessCallback.bind(this), null);
    
        
    }
    
    companyDropdownSuccessCallback(snapData:any) {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.company = snapData.data.data.companyInfoList.map(snap => {
                return {
                    companyCode: snap.companyCode,
                }
            });
        }
    }
    bankShortNameDropdownSuccessCallback(snapData:any) {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.bankShortName = snapData.data.data.infoList.map(snap => {
            
                return {
                    bankShortName: snap.bankShortName,
                }
            });
        }
    }

    displayModel(){
            this.modalAdjustTypeSutup = true;
    }
    
    updateAdjustDetail(data){
        this.modalAdjustTypeSutup = false;
        this.msg = '';
        if((data.adjustCode == null||data.adjustCode == "")||
        (data.companyCode == null||data.companyCode == "")||
        (data.bankShortName == null||data.bankShortName == "")||
        (data.bankAccountNo == null||data.bankAccountNo == "")||
        (data.glAccount == null||data.glAccount == "")||
        (data.glRc == null||data.glRc == "")||
        (data.glProduct == null||data.glProduct == "")||
        (data.glProject == null||data.glProject == "")||
        (data.glCs == null||data.glCs == "")||
        (data.glFs == null||data.glFs == "")||
        (data.glSs == null||data.glSs == "")||
        (data.glRs == null||data.glRs == ""))
        {
            this.msg = 'notInputAdj';
        } else {
        var headers = {
            'action': 'update',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        var datas = data
        this.HttpService.call(this.endpoint.API_ADJUSTDETAIL_MASTER,'PUT', headers,null, null , datas ,this.updateAdjustDetailSuccessCallback.bind(this) ,null)
        }
    }

    updateAdjustDetailSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.formDataAdjustDetail = snapData.data.data.adjustDetailInfo
            this.msg = 'passAdj';
            this.queryDataAdjustDetail(this.paginatorAdjustDetail.currentPage, 'paging');
        }else{
            this.msg = 'fail';
        }
    }

    addAdjustDetail(data){
        this.modalAdjustTypeSutup = false;
        this.msg = '';
        if((data.adjustCode == null||data.adjustCode == "")||
        (data.companyCode == null||data.companyCode == "")||
        (data.bankShortName == null||data.bankShortName == "")||
        (data.bankAccountNo == null||data.bankAccountNo == "")||
        (data.glAccount == null||data.glAccount == "")||
        (data.glRc == null||data.glRc == "")||
        (data.glProduct == null||data.glProduct == "")||
        (data.glProject == null||data.glProject == "")||
        (data.glCs == null||data.glCs == "")||
        (data.glFs == null||data.glFs == "")||
        (data.glSs == null||data.glSs == "")||
        (data.glRs == null||data.glRs == ""))
        {
        this.msg = 'notInputAdj';
        } else {
        var headers = {
            'action': 'add',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        var datas = data
        this.HttpService.call(this.endpoint.API_ADJUSTDETAIL_MASTER,'POST', headers,null, null , datas ,this.addAdjustDetailSuccessCallback.bind(this) ,null)
        }
    }

    addAdjustDetailSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.formDataAdjustDetail = snapData.data.data.adjustDetailInfo
            this.msg = 'passAdj';
            this.queryDataAdjustDetail(this.paginatorAdjustDetail.currentPage, 'paging');
        }else{
            this.msg = 'fail';
        }
    }

    deleteAdjustSetup(data){
        this.deleteData = data;
    }

    deleteAdjustDetail(){
        var header = {
            'action': 'delete',
            'lang':'EN',
            'navigate': this.router.url
        };
        this.HttpService.call(this.endpoint.API_ADJUSTDETAIL_MASTER, 'DELETE', header,null, 
        null, 
        {adjustDetailId: this.deleteData}, 
        this.deleteAdjustDetailSuccessCallback.bind(this), 
        null);
    }

    deleteAdjustDetailSuccessCallback(snapData: any) {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.formDataAdjustDetail = snapData.data.data.adjustDetailInfo
            this.msg = 'pass';
            this.queryDataAdjustDetail(this.paginatorAdjustDetail.currentPage, 'paging');
        }else{
            this.msg = 'fail';
        }
    }
}
