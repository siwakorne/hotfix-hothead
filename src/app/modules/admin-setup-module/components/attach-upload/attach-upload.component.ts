import { Component, OnInit, Sanitizer } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { Endpoint } from '../../../../common/const/endpoint';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { HttpService } from '../../../../common/util/http-service';
import { AppComponent } from '../../../../app.component';


@Component({
  selector: 'app-attach-upload',
  templateUrl: './attach-upload.component.html',
  styleUrls: ['./attach-upload.component.css']
})
export class AttachUploadComponent implements OnInit {
  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
  mockLocationId = this.rawMockLocationId[2];
  filterData: Array<object> = [];
  loading: boolean = false;
  checkData = 1;
  msg = '';
  msgDelete = '';
  paging = 0;
  display = false;
  formSearch: {
    dateFrom: Date,
    dateTo: Date
  } = {
      dateFrom: new Date(),
      dateTo: new Date(),
    }
  paginator: any = {
    currentPage: 0,
    totalPages: 0,
    totalRecords: 0,
    recordPerPage: 3,
  };
  showFilePdf: Array<any> = [];
  uploadedFiles: Array<any> = [];
  titleFile = '';
  dateFile = '';
  doctype: string;
  pdf: any;
  docType = '';
  listPdf: Array<any> = [];
  attachUploadInfoList: any;
  maxsizePdf = 1;
  constructor(private router: Router, private endpoint: Endpoint, private params: ActivatedRoute,private global: AppComponent, private HttpService: HttpService) { }

  ngOnInit() {
    this.maxsizePdf = 1;
    this.params.queryParams.subscribe(params => {
      this.titleFile = params['doctype'];
      this.dateFile = params['date']
      this.docType = params['titleFile'];
    });
    this.loadListDocType();
    this.maxsize();
  }


  async onUpload(event) {
    let vm = this;
    let pdf = [];
    for (let file of event.files) {
      let fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = function () {
        pdf.push({
          "filename": file.name,
          "base64Pdf": fileReader.result.toString().split(',')[1]
        })
        if (pdf.length == event.files.length) {
          vm.upload(pdf);
          vm.display = true;
        }
      };
    }
    // this.messageService.add({severity: 'info', summary: 'File Uploaded', detail: ''});
  }


  upload(pdf) {
    this.msg = '';
    var headers = {
      'action': 'upload',
      'lang': 'EN',
      'navigate': this.getNavigateWithoutQueryString(this.router.url),
    }

    var data = {
      'docdate': this.dateFile,
      'doctype': this.doctype,
      'attachUploadInfoList': pdf
    }
    this.HttpService.call(this.endpoint.API_UPLOAD,'POST', headers,null, null , data , this.uploadSuccessCallback.bind(this),this.uploadFailCallback.bind(this));
  }

  uploadSuccessCallback(snap: any){
    this.global.checkTokenExpired(snap.data.statusCode);
    if (snap.data.statusCode == 'S0000') {
      this.msg = 'success';
      this.pdf = [];
      this.listPdf = [];
      this.loadList('1')
    } else {
      this.msg = 'fail'
      this.pdf = [];
      this.listPdf = [];
      this.loadList('1')
    }
  }

  uploadFailCallback(err: any){
    this.pdf = [];
    this.listPdf = [];
    this.msg = 'fail';
  }


  async loadListDocType() {
    var headers = {
      'action': 'list',
      'lang': 'EN',
      'navigate': this.getNavigateWithoutQueryString(this.router.url),
    }
    await this.HttpService.call(this.endpoint.API_LIST_DOC_TYPE,'GET', headers,null, null , null , this.loadListDocTypeSuccessCallback.bind(this),null);
  }

  loadListDocTypeSuccessCallback(snap: any){
    this.global.checkTokenExpired(snap.data.statusCode);
    if (snap.data.statusCode == 'S0000') {
      snap.data.data.attachMasterList.map(data => {
        if (this.titleFile == data.displayName) {
          this.doctype = data.doctype;
        }
      })
      this.loadList('1');
    }
  }

  getNavigateWithoutQueryString(url) {
    let overstring = url.indexOf('?');
    if (overstring) {
        return url.substr(0, overstring);
    } else {
        return url;
    }
  }

  maxsize() {
    var headers = {
      'action': 'maxsize',
      'lang': 'EN',
      'navigate': this.getNavigateWithoutQueryString(this.router.url),
    }
    this.HttpService.call(this.endpoint.API_MAXSIZE,'GET', headers,null, null , null , this.maxsizeSuccessCallback.bind(this),null);
  }

  maxsizeSuccessCallback(snap: any){
    this.global.checkTokenExpired(snap.data.statusCode);
    if (snap.data.statusCode == 'S0000') {
      let size: any = (snap.data.data.attachMaxSize) * 1000;
      this.maxsizePdf = size;
    }
  }

  paginate(event) {
    if ((event.page + 1) != this.paginator.currentPage) {
      this.loadList(event.page + 1);
    }
  }

  loadList(page) {
    var headers = {
      'action': 'list',
      'lang': 'EN',
      'navigate': this.getNavigateWithoutQueryString(this.router.url),
    }

    var params = {
      'currentPage': page,
      'docdate': this.dateFile,
      'doctype': this.doctype,
    }
    this.HttpService.call(this.endpoint.API_UPLOAD,'GET', headers,null, params , null , this.loadListSuccessCallback.bind(this),null);
  }

  loadListSuccessCallback(snap: any){
    this.global.checkTokenExpired(snap.data.statusCode);
    if (snap.data.statusCode == 'S0000') {
      this.listPdf = snap.data.data.attachInfoList.map(data => {
        return {
          filename: data.filename,
          path: data.path
        }
      })
      this.paginator = snap.data.data.pagingInfo;
    }
  }

  preview(data) {
    var headers = {
      'action': 'preview',
      'lang': 'EN',
      'navigate': this.getNavigateWithoutQueryString(this.router.url),
    }

    var params = {
      'docdate': this.dateFile,
      'doctype': this.doctype,
      'filename': data.filename,
      'path': data.path
    }
    this.HttpService.call(this.endpoint.API_PREVIEW_PDF_FILE,'GET', headers,'blob', params , null , this.previewSuccessCallback.bind(this),null);
  }

  previewSuccessCallback(snap: any){
    if (snap.status == 200) {
      let blob = new Blob([snap.data], { type: 'application/pdf', });
      let fileURL = URL.createObjectURL(blob);
      window.open(fileURL, '_blank');
    }
  }


  delete(data) {
    this.msgDelete = '';
    var headers = {
      'Content-Type': 'application/json',
      'action': 'delete',
      'transactionId': uuid(),
      'locationId': JSON.parse(localStorage.getItem('loginData'))[2],
      'lang': 'EN',
      'username': this.rawMockLocationId[1],
      'navigate': this.getNavigateWithoutQueryString(this.router.url),
      'token': this.rawMockLocationId[3]
    }
    var datas = {
      docdate: this.dateFile,
      doctype: this.doctype,
      filename: data.filename,
      path: data.path
    }
    this.HttpService.call(this.endpoint.API_UPLOAD,'DELETE', headers,null, null , datas , this.deleteSuccessCallback.bind(this),this.deleteFailCallback.bind(this));
  }

  deleteSuccessCallback(snap: any){
    this.global.checkTokenExpired(snap.data.statusCode);
    if (snap.data.statusCode == 'S0000') {
      this.loadList(this.paginator.currentPage);
      this.msgDelete = 'success';
      this.display = true;     
    } else {
      this.msgDelete = 'fail';
      this.display = true;
    }
  }

  deleteFailCallback(err: any){
    this.msgDelete = 'fail';
    this.display = true;
  }


  reOpen(){
    this.loadList(this.paginator.currentPage);
    this.display = false;

  }

  back(){
    this.router.navigate(['/admin-setup/attach-search'], { queryParams: { doctype: this.docType } })
  }

}
