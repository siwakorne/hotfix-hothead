import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-global',
    templateUrl: './global.component.html',
    styleUrls: ['./global.component.css']
})
export class GlobalComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    formActive = false;
    dataActive = true;
    errorDesc: string = '';
    edit = '';
    status = '';
    deleteData = '';
    dataSearch = {
        findBy: null,
        search: null
    };
    action = '';
    filterData = [];
    dbData = [];
    formData = {
        confKey: '',
        confValue: '',
        remark: ''
    };
    paginator = {
        totalRecords: 0,
        recordPerPage: 3,
        currentPage: 0,
        totalPages: 0
    };
    rawData: object;

    constructor(private endpoint: Endpoint, private router: Router,private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        this.queryData(1);
    }

    queryData(page) {
        this.rawData = [];
        this.dbData = [];
        let header = {
            'action': 'load-all',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        if (this.dataSearch.findBy == 'confKeys') {
            this.HttpService.call(this.endpoint.API_GLOBAL_CONFIG_MASTER, 'GET', header, null,
                {
                    currentPage: page,
                    confKeys: this.dataSearch.search
                },
                null,
                this.querySuccessCallback.bind(this),
                null);
        } else if (this.dataSearch.findBy == 'confValue') {
            this.HttpService.call(this.endpoint.API_GLOBAL_CONFIG_MASTER, 'GET', header, null,
                {
                    currentPage: page,
                    confValue: this.dataSearch.search
                },
                null,
                this.querySuccessCallback.bind(this),
                null);
        } else if (this.dataSearch.findBy == 'remark') {
            this.HttpService.call(this.endpoint.API_GLOBAL_CONFIG_MASTER, 'GET', header, null,
                {
                    currentPage: page,
                    remark: this.dataSearch.search
                },
                null,
                this.querySuccessCallback.bind(this),
                null);
        } else {
            this.HttpService.call(this.endpoint.API_GLOBAL_CONFIG_MASTER, 'GET', header, null,
                { currentPage: page },
                null,
                this.querySuccessCallback.bind(this),
                null);
        }

    }

    querySuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.dbData = snap.data.data.gloBalConfigurationInfoList;
        this.paginator = snap.data.data.pagingInfo;
    }

    formSearch() {
        this.clearForm();
        this.queryData(1);
        this.dataActive = true;
        this.formActive = false;
        this.filterData = [];
        if (this.dataSearch.findBy.trim() && this.dataSearch.search.trim()) {
            this.filterData = this.dbData.filter(data => {
                if (this.dataSearch.findBy === '1') return data.userName.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '2') return data.name.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '3') return data.statusActive.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '4') return data.roleId == this.dataSearch.findBy ? 0 : data
            });
            if (!this.filterData.length) {
                this.msgs = [];
                this.msgs.push({ severity: 'info', summary: 'Alert', detail: 'No Data' });
            } else {
                this.msgs = [];
                this.msgs.push({ severity: 'success', summary: 'Alert', detail: `Total query ${this.filterData.length} rows` });
            }
        }
    }

    formBank() {
        this.action = 'add';
        this.dataActive = false;
        this.formActive = true;
    }

    editBank(data) {
        this.formBank();
        this.action = 'edit';
        this.formData = data;
    } Í

    update() {
        this.errorDesc = '';
        if (this.formData.confKey && this.formData.confValue) {
            var header = {
                'action': this.action === 'edit' ? 'update' : 'add',
                'lang': 'EN',
                'navigate': this.router.url,
            }
            this.HttpService.call(this.endpoint.API_GLOBAL_CONFIG_MASTER, 'POST', header, null,
                null,
                this.formData,
                this.updateSuccessCallback.bind(this),
                null);
        } else {
            this.errorDesc = 'กรุณากรอกให้ครบถ้วน'
        }
    }

    updateSuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if (snap.data.statusCode === 'S0000') {
            this.dataActive = true;
            this.formActive = false;
            this.clearForm();
        }
    }

    clearForm() {
        this.formData = {
            confKey: '',
            confValue: '',
            remark: ''
        };
        this.paginator = {
            totalRecords: 0,
            recordPerPage: 3,
            currentPage: 0,
            totalPages: 0
        };
        this.queryData(1);
    }

    delete(data){
        this.deleteData = data;
    }

    deleteBank() {
        var header = {
            'action': 'delete',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_GLOBAL_CONFIG_MASTER, 'DELETE', header, null,
            null,
            { seqnId: this.deleteData },
            this.deleteSuccessCallback.bind(this),
            null);
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.queryData(event.page + 1);
        }
    }

    deleteSuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.queryData(1);
    }

    clearSearchValue() {
        this.dataSearch.search = null;
    }
}
