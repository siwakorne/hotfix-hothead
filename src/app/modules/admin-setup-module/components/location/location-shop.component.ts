import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { Endpoint } from '../../../../common/const/endpoint';
import { ConfirmationService } from 'primeng/api';
import { HttpService } from '../../../../common/util/http-service';
import { Router } from '@angular/router';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-location-shop',
    templateUrl: './location-shop.component.html',
    styleUrls: ['./location-shop.component.css'],
    providers: [ConfirmationService]
})
export class LocationShopComponents implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    formActive = false;
    dataActive = true;
    dataUserActive = true;
    formUserActive = false;
    msg: string = '';
    loading: boolean = true;
    msguser: string = '';
    edit = '';
    edteuser = '';
    status = '';
    pageUser = 0;
    findBy: string = null;
    findSearch: string = null;
    filterStatus: boolean = false;
    filterBy: string = 'all';
    allUserData = [];
    userData = [];
    refreshPaging: boolean = true;
    currentPaging: number = 0;
    locationUser: any;
    filterData = [];
    dbData = [];
    deleteData: any = '';
    roleData = [];
    formData = {
        locationCode: '',
        locationNameEn: '',
        locationNameTh: '',
        locationType: '',
        locationArea: '',
        managerNameEn: '',
        managerNameTh: '',
        managerMobileNo: '',
        managerEmail: '',
        hierachyMapping: '',
        clusterCode: '',
        clusterName: '',
        zoneCode: '',
        zoneName: '',
        /* locationStatus: undefined, */
        reconciledFlag: undefined
    };
    formUserData = {
        locationCode: '',
        username: ''
    };
    roleRawData: object;
    rawData: object;
    paginator = {
        location: {
            totalRecords: 0,
            recordPerPage: 3,
            currentPage: 0,
            totalPages: 0
        },
        user: {
            totalRecords: 0,
            recordPerPage: 3,
            currentPage: 0,
            totalPages: 0
        }
    };

    constructor(private router: Router, private endpoint: Endpoint, private HttpService: HttpService, private global: AppComponent, private confirmationService: ConfirmationService) { }

    ngOnInit() {
        this.queryData(this.filterStatus, 1);
    }

    queryData(filter, page) {
        this.loading = true;
        this.rawData = [];
        this.dbData = [];
        this.roleData = [];
        this.roleRawData = [];
        this.userData = [];
        var headers = {
            'action': 'load-all',
            'lang': 'en',
            'navigate': this.router.url,
        }
        var params = {};
        if (this.findBy == 'locationCode') {
            params = {
                currentPage: page,
                locationCode: this.findSearch
            }
            this.HttpService.call(this.endpoint.API_LOCATION_MASTER, 'GET', headers, null, params, null, this.searchSuccessCallback.bind(this), this.searchFailCallback.bind(this));
        } else if (this.findBy == 'locationType') {
            params = {
                currentPage: page,
                locationType: this.findSearch
            }
            this.HttpService.call(this.endpoint.API_LOCATION_MASTER, 'GET', headers, null, params, null, this.searchSuccessCallback.bind(this), this.searchFailCallback.bind(this));
        } else {
            params = {
                currentPage: page,
            }
            this.HttpService.call(this.endpoint.API_LOCATION_MASTER, 'GET', headers, null, params, null, this.searchSuccessCallback.bind(this), this.searchFailCallback.bind(this));
        }
    }

    searchSuccessCallback(snapData: any) {
        this.global.checkTokenExpired(snapData.data.statusCode);
        this.loading = false;
        this.dbData = snapData.data.data.locationMasterInfoList;
        this.paginator.location = snapData.data.data.pagingInfo;
    }

    searchFailCallback(error: any) {
        this.loading = false;
    }

    formBank() {
        this.dataActive = false;
        this.formActive = true;
    }

    editBank(data) {
        this.formBank();
        this.formData = data;
        this.edit = '1';
    }

    update() {
        if ((this.formData.locationCode == '' || null || undefined) ||
            (this.formData.locationNameEn == '' || null || undefined) ||
            (this.formData.locationNameTh == '' || null || undefined) ||
            (this.formData.locationType == '' || null || undefined) ||
            (this.formData.locationArea == '' || null || undefined) ||
            (this.formData.managerNameEn == '' || null || undefined) ||
            (this.formData.managerNameTh == '' || null || undefined) ||
            (this.formData.managerMobileNo == '' || null || undefined) ||
            (this.formData.managerEmail == '' || null || undefined) ||
            (this.formData.hierachyMapping == '' || null || undefined) ||
            (this.formData.clusterCode == '' || null || undefined) ||
            (this.formData.clusterName == '' || null || undefined) ||
            (this.formData.zoneCode == '' || null || undefined ||
                (this.formData.zoneName == '' || null || undefined) ||
              /*   (this.formData.locationStatus == '' || null || undefined) || */
                (this.formData.reconciledFlag == '' || null || undefined))) {
            this.msg = 'notInput'
        } else {

            this.dataActive = true;
            this.formActive = false;

            var headers = {
                'action': 'update',
                'lang': 'en',
                'navigate': this.router.url,
            }

            var datas = this.formData

            this.HttpService.call(this.endpoint.API_LOCATION_MASTER, 'PUT', headers, null, null, datas, this.updateSuccessCallback.bind(this), null)
        }
    }

    updateSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        if (data.data.statusCode == 'S0000') {
            this.msg = 'pass';
            this.clearForm();
            this.queryData(this.filterStatus, 1);
        } else {
            this.msg = 'fail';
        }
    }

    submitData() {
        if ((this.formData.locationCode == '' || null || undefined) ||
            (this.formData.locationNameEn == '' || null || undefined) ||
            (this.formData.locationNameTh == '' || null || undefined) ||
            (this.formData.locationType == '' || null || undefined) ||
            (this.formData.locationArea == '' || null || undefined) ||
            (this.formData.managerNameEn == '' || null || undefined) ||
            (this.formData.managerNameTh == '' || null || undefined) ||
            (this.formData.managerMobileNo == '' || null || undefined) ||
            (this.formData.managerEmail == '' || null || undefined) ||
            (this.formData.hierachyMapping == '' || null || undefined) ||
            (this.formData.clusterCode == '' || null || undefined) ||
            (this.formData.clusterName == '' || null || undefined) ||
            (this.formData.zoneCode == '' || null || undefined ||
                (this.formData.zoneName == '' || null || undefined) ||
                (this.formData.reconciledFlag == '' || null || undefined))) {
            this.msg = 'notInput'
        } else {
            this.dataActive = true;
            this.formActive = false;
            var headers = {
                'action': 'add',
                'lang': 'en',
                'navigate': this.router.url,
            }
            var datas = this.formData
            this.HttpService.call(this.endpoint.API_LOCATION_MASTER, 'POST', headers, null, null, datas, this.addSuccessCallback.bind(this), null)
        }

    }

    addSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        if (data.data.statusCode == 'S0000') {
            this.msg = 'pass';
            this.clearForm();
            this.queryData(this.filterStatus, 1);
        } else {
            this.msg = 'fail';
        }
    }

    clearForm() {
        this.formData = {
            locationCode: '',
            locationNameEn: '',
            locationNameTh: '',
            locationType: '',
            locationArea: '',
            managerNameEn: '',
            managerNameTh: '',
            managerMobileNo: '',
            managerEmail: '',
            hierachyMapping: '',
            clusterCode: '',
            clusterName: '',
            zoneCode: '',
            zoneName: '',
           /*  locationStatus: undefined, */
            reconciledFlag: undefined,
        };
        this.paginator = {
            location: {
                totalRecords: 0,
                recordPerPage: 3,
                currentPage: 0,
                totalPages: 0
            },
            user: {
                totalRecords: 0,
                recordPerPage: 3,
                currentPage: 0,
                totalPages: 0
            }
        };
    }
    delete(data) {
        this.deleteData = data;
    }

    deleteBank() {
        /* data.status = "D"; */
        var headers = {
            'action': 'delete',
            'lang': 'en',
            'navigate': this.router.url,
        }
        var datas = {
            "locationCode": this.deleteData.locationCode
        }
        this.HttpService.call(this.endpoint.API_LOCATION_MASTER, 'DELETE', headers, null, null, datas, this.deleteSuccessCallback.bind(this), null)
    }


    deleteSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        this.queryData(this.filterStatus, 1);
    }

    formUserBank() {
        this.dataUserActive = false;
        this.formUserActive = true;
    }

    formSearch() {
        this.clearForm()
        this.dataActive = true;
        this.formActive = false;
        if (this.findBy) {
            this.filterStatus = true;
            this.queryData(this.filterStatus, 1);
        } else {
            this.filterStatus = false;
            this.queryData(this.filterStatus, 1);
        }
    }

    formSearchUser() {
        this.clearForm()
        this.dataActive = true;
        this.formActive = false;
        this.currentPaging = 0;
        this.refreshPaging = false;
        if (this.findBy) {
            this.filterStatus = true;
            this.queryUser(this.locationUser, this.filterStatus, 1);
        } else {
            this.filterStatus = false;
            this.queryUser(this.locationUser, this.filterStatus, 1);
        }
        this.refreshPaging = true;

    }

    user(data) {
        this.filterBy = 'all';
        this.findBy = '';
        this.findSearch = '';
        this.filterStatus = false;
        this.locationUser = data.locationCode;
        this.formUserData.locationCode = data.locationCode;
        this.pageUser = 1;
        this.queryUser(this.locationUser, this.filterStatus, 1);
    }

    backToLocation() {
        this.pageUser = 0;
        this.findBy = null;
        this.filterStatus = false;
        this.findSearch = null;
        this.filterBy = 'all';
        this.queryData(this.filterStatus, 1);
    }

    clearUserForm() {
        this.formUserData = {
            locationCode: '',
            username: ''
        };
    }

    queryUser(locationCode, filter, page) {
        this.loading = true;
        this.rawData = [];
        this.dbData = [];
        this.roleData = [];
        this.roleRawData = [];
        this.userData = [];
        this.dataUserActive = true;
        this.formUserActive = false;
        var headers = {
            'action': 'load-all',
            'lang': 'en',
            'navigate': this.router.url,
        }

        var param = {};

        if (filter) {
            param = {
                currentPage: page,
                filterBy: this.filterBy,
                locationCode: locationCode,
                searchBy: this.findBy,
                searchValue: this.findSearch
            }
            this.HttpService.call(this.endpoint.API_USER_MAPPING_LOCATION, 'GET', headers, null, param, null, this.queryUserSuccessCallback.bind(this), null)
        } else {
            param = {
                currentPage: page,
                filterBy: this.filterBy,
                locationCode: locationCode
            }
            this.HttpService.call(this.endpoint.API_USER_MAPPING_LOCATION, 'GET', headers, null, param, null, this.queryUserSuccessCallback.bind(this), null)
        }
    }

    queryUserSuccessCallback(snapData: any) {
        this.global.checkTokenExpired(snapData.data.statusCode);
        this.userData = snapData.data.data.userMappingLocationInfoList;
        this.paginator.user = snapData.data.data.pagingInfo;
        this.userData.filter(data => {
            if (data.locationCode != undefined) data.active = true;
            else data.active = false;
        })
        this.loading = false;
        this.refreshPaging = true;
    }

    addUser() {
        // if ((this.formUserData.locationCode == '' || null || undefined) ||
        //     (this.formUserData.username == '' || null || undefined) ) {
        //     this.msguser = 'notInput'
        // } else {
        //     this.dataActive = true;
        //     this.formActive = false;
        //     var headers = {
        //         'action': 'add',
        //         'lang': 'en',
        //         'navigate': this.router.url,
        //     }
        //     var datas = {
        //         "locationCode": this.formUserData.locationCode,
        //         "userName": this.formUserData.username
        //     }
        //     this.HttpService.call(this.endpoint.API_USER_MAPPING_LOCATION,'POST', headers,null, null , datas , this.addUserSuccessCallback.bind(this) ,null)
        // }
    }

    addUserSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        if (data.data.statusCode == 'S0000') {
            this.msguser = 'pass';
            this.clearUserForm()
            this.queryUser(this.locationUser, this.filterStatus, 1);
        } else {
            this.msguser = 'fail';
        }
    }

    paginate(event, onPage) {
        if (onPage == 'location') {
            if ((event.page + 1) != this.paginator.location.currentPage) {
                this.paginator.location.currentPage = this.paginator.location.currentPage
                this.queryData(this.filterStatus, event.page + 1);
            }
        } else {
            if ((event.page + 1) != this.paginator.user.currentPage) {
                var checkValidate = false;
                this.userData.map(data => {
                    if (data.active == true && data.locationCode == undefined) {
                        checkValidate = true;
                    }
                })
                if (checkValidate) {
                    this.confirmationService.confirm({
                        message: 'Do you want to save before change page?',
                        accept: async () => {
                            await this.saveUser();
                            this.paginator.location.currentPage = this.paginator.user.currentPage
                            this.queryUser(this.locationUser, this.filterStatus, event.page + 1);
                            this.currentPaging = event.page;
                        },
                        reject: () => {
                            this.paginator.location.currentPage = this.paginator.user.currentPage
                            this.queryUser(this.locationUser, this.filterStatus, event.page + 1);
                            this.currentPaging = event.page;
                        }
                    });
                } else {
                    this.paginator.location.currentPage = this.paginator.user.currentPage
                    this.queryUser(this.locationUser, this.filterStatus, event.page + 1);
                    this.currentPaging = event.page;
                }
            }
        }
    }

    deleteUse(data) {
        data.status = "D";
        var headers = {
            'action': 'delete',
            'lang': 'en',
            'navigate': this.router.url,
        }
        var datas = {
            "userLocationId": data.userLocationId
        }
        this.HttpService.call(this.endpoint.API_USER_MAPPING_LOCATION, 'DELETE', headers, null, null, datas, this.deleteUserSuccessCallback.bind(this), null)
    }


    deleteUserSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        this.queryUser(this.locationUser, this.filterStatus, 1);
    }

    filterRadio(status) {
        this.refreshPaging = false;
        if (this.filterBy != status) {
            this.filterBy = status;
            this.queryUser(this.locationUser, this.filterStatus, 1);
        }
        this.currentPaging = 0;
    }

    saveUser() {
        let preData = [];
        var headers = {
            'action': 'update',
            'lang': 'en',
            'navigate': this.router.url,
        }
        this.userData.map(snap => {
            snap.locationCode = this.formUserData.locationCode;
            preData.push({
                username: snap.userName,
                locationCode: snap.locationCode,
                status: snap.active ? '1' : '0'
            })
        })
        if (preData.length) {
            this.HttpService.call(this.endpoint.API_USER_MAPPING_LOCATION, 'PUT', headers, null, null, { locationShopInfoList: preData }, this.updateUserSuccessCallback.bind(this), this.updateUserFailCallback.bind(this))
        }
    }

    updateUserSuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if (snap.data.data.statusCode == 'S0000') {
            this.msguser = 'pass';
        } else {
            this.msguser = 'fail';
        }
    }

    updateUserFailCallback(error: any) {
        this.msguser = 'fail';
    }

    clearSearchValue() {
        this.findSearch = null;
    }
}
