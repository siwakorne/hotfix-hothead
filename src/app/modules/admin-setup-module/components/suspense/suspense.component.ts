import { Component, OnInit } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { Router } from '@angular/router';
import { Message } from 'primeng/api';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-suspense',
    templateUrl: './suspense.component.html',
    styleUrls: ['./suspense.component.css']
})
export class SuspenseComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    locationName = '';
    formActive = false;
    dataActive = true;
    editActive = '';
    suspenseActive: boolean = true;
    edit = ''
    status = '';
    dataSearch = {
        findBy: null,
        search: null
    };
    filter = [];
    dbData = [];
    errorDesc = '';
    errorCode = '';
    deleteData = '';
    dpsType = '';
    formData = {
        suspensesId: 0,
        legacyName: '',
        paymentType: '',
        accountName:'',
        remark: '',
        glAccount: '',
        glRc: '',
        glProduct: '',
        glProject: '',
        glCs: '',
        glFs: '',
        glSs: '',
        glRs: ''
    };
    rawData: object;
    paginator = {
        totalRecords: 0,
        recordPerPage: 3,
        currentPage: 0,
        totalPages: 0
    };

    constructor(private endpoint: Endpoint, private router: Router,private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        this.queryData(1);
    }

    queryData(page) {
        var header = {
            'action': 'load-all',
            'lang':'EN',
            'navigate': this.router.url
        }
        this.dbData = [];
        if(this.dataSearch.findBy == 'legacyName'){
            this.HttpService.call(this.endpoint.API_SUSPENSE, 'GET', header, null,
            {currentPage: page,
            legacyName: this.dataSearch.search}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        } else if (this.dataSearch.findBy == 'productType'){
            this.HttpService.call(this.endpoint.API_SUSPENSE, 'GET', header, null,
            {currentPage: page,
            productType: this.dataSearch.search}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        } else{
            this.HttpService.call(this.endpoint.API_SUSPENSE, 'GET', header, null,
            {currentPage: page}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }
        
    }
    
    querySuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode === 'S0000') {
            let data = snap.data.data;
            this.dbData = data.suspenseAccountInfoList;
            this.paginator = data.pagingInfo;
        }
    }

    formSearch() {
        this.clearForm();
        this.queryData(1);
        this.dataActive = true;
        this.formActive = false;
        this.filter = [];
        if (this.dataSearch.findBy.trim() && this.dataSearch.search.trim()) {
            this.filter = this.dbData.filter(data => {
                if (this.dataSearch.findBy === '1') return data.userName.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data;
                else if (this.dataSearch.findBy === '2') return data.name.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data;
                else if (this.dataSearch.findBy === '3') return data.statusActive.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data;
                else if (this.dataSearch.findBy === '4') return data.roleId == this.dataSearch.findBy ? 0 : data;
            });
            if (!this.filter.length) {
                this.msgs = [];
                this.msgs.push({ severity: 'info', summary: 'Alert', detail: 'No Data' });
            } else {
                this.msgs = [];
                this.msgs.push({ severity: 'success', summary: 'Alert', detail: `Total query ${this.filter.length} rows` });
            }
        }
    }

    form() {
        this.edit = '0';
        this.dataActive = false;
        this.formActive = true;
    }

    editForm(data) {
        this.edit = '1';
        this.form();
        this.formData = data;
    }

    update(){
        this.errorCode = '';
        this.errorDesc = '';
        if(this.formData.suspensesId == 0)
            this.formData.suspensesId = undefined;
        if(this.formData.legacyName
           && this.formData.paymentType
           && this.formData.accountName
           && this.formData.glAccount
           && this.formData.glCs
           && this.formData.glFs
           && this.formData.glProduct
           && this.formData.glProject
           && this.formData.glRc
           && this.formData.glRs
           && this.formData.glSs) {
               var header = {
                'action': this.formData.suspensesId == undefined ? 'add' : 'update',
                'lang':'EN',
                'navigate': this.router.url
            }
            this.HttpService.call(this.endpoint.API_SUSPENSE, 'POST', header, null,
            null, 
            this.formData, 
            this.updateSuccessCallback.bind(this), 
            null);
        } else {
            this.errorCode = 'inComplete';
            this.errorDesc = 'กรอกข้อมูลไม่ครบถ้วน';
        }
    }

    updateSuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode != 'S0000') {
            this.errorCode = snap.data.statusCode;
            this.errorDesc = snap.data.statusMessage;
        }
    }

    clearForm() {
        this.locationName = '';
        this.dpsType = '';
        this.formData = {
            suspensesId: 0,
            legacyName: '',
            accountName: '',
            paymentType:'',
            remark: '',
            glAccount: '',
            glRc: '',
            glProduct: '',
            glProject: '',
            glCs: '',
            glFs: '',
            glSs: '',
            glRs: ''
        };
        this.paginator = {
            totalRecords: 0,
            recordPerPage: 3,
            currentPage: 0,
            totalPages: 0
        };
    }

    success() {
        this.dataActive = true;
        this.formActive = false;
        this.queryData(1);
        this.clearForm();
    }

    delete(data){
        this.deleteData = data;
    }

    deleteSuspense(){
        var header = {
            'action': 'delete',
            'lang':'EN',
            'navigate': this.router.url
        };
        this.HttpService.call(this.endpoint.API_SUSPENSE, 'DELETE', header, null,
        null, 
        {suspensesId: this.deleteData}, 
        this.deleteSuccessCallback.bind(this), 
        null);
    }

    deleteSuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.queryData(1);
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.queryData(event.page + 1);
        }
    }

    clearSearchValue(){
        this.dataSearch.search = null;
    }
}
