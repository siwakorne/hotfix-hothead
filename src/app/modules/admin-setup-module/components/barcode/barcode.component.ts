import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
    selector: 'app-barcode',
    templateUrl: './barcode.component.html',
    styleUrls: ['./barcode.component.css']
})
export class BarcodeComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    formActive = false;
    barcodeActive = true;
    edit = '';
    status = '';
    deleteData = '';
    dataSearch = {
        findBy: null,
        search: null
    };
    filterBarcode = [];
    dbDataBarcode = [];
    bankMaster = [];
    bank={
        bankCode: '',
        bankDesc: '',
        bankMasterId: 0,
        bankShortName: '',
        companyCode: '',
        default_bank: '',
        paymentTypeCode: ''
    };
    bankId=[];
    bankDesc=[];
    action = '';
    formDataBarcode = {
        bankMasterId: 0,
        companyCode: '',
        taxId: '',
        serviceCode:'',
        fixAmtFlag: '',
        fixAmt: '',
        padAmtFlag: '',
        padLenght: '',
        padValue: ''
    };
    paginator = {
        totalRecords: 0,
        recordPerPage: 3,
        currentPage: 0,
        totalPages: 0
    };
    rawData: object;
    msg = '';

    constructor(private router: Router, private endpoint: Endpoint ,private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        this.queryData(1);
        this.dropDown();
    }

    queryData(page) {
        this.rawData=[];
        this.dbDataBarcode = [];
        var header = {
            'action': 'load-all',
            'navigate': this.router.url,
            'lang':'EN',
        }
        if(this.dataSearch.findBy == 'bankShortName'){
            this.HttpService.call(this.endpoint.API_BARCODE_MASTER, 'GET', header,null, 
            {currentPage: page,
            bankShortName: this.dataSearch.search}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }else if(this.dataSearch.findBy == 'companyCode'){
            this.HttpService.call(this.endpoint.API_BARCODE_MASTER, 'GET', header,null, 
            {currentPage: page,
            companyCode: this.dataSearch.search}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }else if(this.dataSearch.findBy == 'taxId'){
            this.HttpService.call(this.endpoint.API_BARCODE_MASTER, 'GET', header,null, 
            {currentPage: page,
            taxId: this.dataSearch.search}, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }else {
            this.HttpService.call(this.endpoint.API_BARCODE_MASTER, 'GET', header,null, 
            {currentPage: page
            }, 
            null, 
            this.querySuccessCallback.bind(this), 
            null);
        }
        
    }

    querySuccessCallback(snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode === 'S0000') {
            let data = snap.data.data;
            this.dbDataBarcode = data.barcodeSetupInfoList;
            this.paginator = data.pagingInfo;
        }
    }

    formSearch() {
        this.clearForm();
        this.queryData('1')
        this.barcodeActive = true;
        this.formActive = false;
        this.filterBarcode = [];
        if (this.dataSearch.findBy.trim() && this.dataSearch.search.trim()) {
            this.filterBarcode = this.dbDataBarcode.filter(data => {
                if (this.dataSearch.findBy === '1') return data.userName.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '2') return data.name.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '3') return data.statusActive.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '4') return data.roleId == this.dataSearch.findBy ? 0 : data
            });
            if (!this.filterBarcode.length) {
                this.msgs = [];
                this.msgs.push({ severity: 'info', summary: 'Alert', detail: 'No Data' });
            } else {
                this.msgs = [];
                this.msgs.push({ severity: 'success', summary: 'Alert', detail: `Total query ${this.filterBarcode.length} rows` });
            }
        }
    }

    formBarcode() {
        this.action = 'add';
        this.barcodeActive = false;
        this.formActive = true;
    }

    editBarcode(data) {
        this.formBarcode();
        this.action = 'edit'
        this.formDataBarcode = data;
    }

    updateSuccessCallback (snap:any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode == 'S0000') {
            this.barcodeActive = true;
            this.formActive = false;
            this.msg = 'success';
            this.queryData(1);
            this.clearForm();
        }
    }
    
    updateFailCallback (snap:any) {
        this.msg = 'fail';
    }

    update(){
        this.msg = '';
        if(this.formDataBarcode.bankMasterId 
            && this.formDataBarcode.companyCode 
            && this.formDataBarcode.taxId 
            && this.formDataBarcode.taxId.length == 13 
            && this.formDataBarcode.serviceCode 
            && this.formDataBarcode.fixAmtFlag == 'Y' 
            && this.formDataBarcode.fixAmt)
        {
            var header = {
                'action': this.action === 'edit' ? 'update' : 'add',
                'lang': 'EN',
                'navigate': this.router.url,
            }
            this.HttpService.call(this.endpoint.API_BARCODE_MASTER, 'POST', header,null, 
            null, 
            this.formDataBarcode, 
            this.updateSuccessCallback.bind(this), 
            this.updateFailCallback.bind(this));
        }else if(this.formDataBarcode.bankMasterId 
            && this.formDataBarcode.companyCode 
            && this.formDataBarcode.taxId 
            && this.formDataBarcode.taxId.length == 13 
            && this.formDataBarcode.serviceCode 
            && this.formDataBarcode.fixAmtFlag == 'N' 
            && this.formDataBarcode.padAmtFlag 
            && this.formDataBarcode.padLenght 
            && this.formDataBarcode.padValue)
        {
            var header = {
                'action': this.action === 'edit' ? 'update' : 'add',
                'lang': 'EN',
                'navigate': this.router.url
            }
            this.HttpService.call(this.endpoint.API_BARCODE_MASTER, 'POST', header,null, 
            null, 
            this.formDataBarcode, 
            this.updateSuccessCallback.bind(this), 
            this.updateFailCallback.bind(this))
        } else {
            this.msg = 'notInput'
        }
    }

    clearForm() {
        this.formDataBarcode = {
            bankMasterId: 0,
            companyCode: '',
            taxId: '',
            serviceCode:'',
            fixAmtFlag: '',
            fixAmt: '',
            padAmtFlag: '',
            padLenght: '',
            padValue: ''
        };
        this.paginator = {
            totalRecords: 0,
            recordPerPage: 3,
            currentPage: 0,
            totalPages: 0
        };
    }

    delete(data){
        this.deleteData = data;
    }

    deleteBarcode(){
        var header = {
            'action': 'delete',
            'lang':'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_BARCODE_MASTER, 'DELETE', header, null, 
        null, 
        {mappingId: this.deleteData}, 
        this.deleteSuccessCallback.bind(this), 
        this.deleteFailCallback.bind(this));
    }

    deleteSuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        this.msg = 'success';
        this.queryData(1);
    }

    deleteFailCallback() {
        this.msg = 'fail';
    }

    dropDown() {
        var headers = {
            'action': 'load-bank',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_LOAD_BANK,'GET', headers,null, null , null , this.dropDownSuccessCallback.bind(this),this.dropDownFailCallback.bind(this));
    }

    dropDownSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.bank = snapData.data.data.bankInfoList;
            this.msg = 'success';
        }
    }

    dropDownFailCallback(err: any){
        this.msg = 'fail';
    }

    changeFixAmt(flag) {
        if(flag == 'Y') {
            this.formDataBarcode.padAmtFlag = '';
            this.formDataBarcode.padLenght = '';
            this.formDataBarcode.padValue = '';
        } else if(flag == 'N') {
            this.formDataBarcode.fixAmt = '';
        }
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.queryData(event.page + 1);
        }
    }
    
    clearSearchValue(){
        this.dataSearch.search = null;
    }
}
