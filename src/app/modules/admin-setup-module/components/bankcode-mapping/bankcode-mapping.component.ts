import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from 'primeng/api';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import { AppComponent } from '../../../../app.component';
import { flush } from '@angular/core/testing';
import { declaredViewContainer } from '@angular/core/src/view/util';

@Component({
  selector: 'app-bankcode-mapping',
  templateUrl: './bankcode-mapping.component.html',
  styleUrls: ['./bankcode-mapping.component.css']
})
export class BankcodeMappingComponent implements OnInit {

  rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    formActive = false;
    dataActive = true;
    showButton = false;
    empty = false;
    errorDesc:string = '';
    msg = '';
    loading = false;
    edit = '';
    status = '';
    action ='';
    dbData = [];
    dbDataBackup = [];
    dbDataUpdate = [];
    formData = {
        mappingId: 0,
        legacyName: '',
        legacyValue: '',
        botValue: ''
        };
    paginator = {
        totalRecords: 0,
        recordPerPage: 10,
        currentPage: 0,
        totalPages: 0
    };
    rawData: object;
    
    constructor(private endpoint: Endpoint, private router: Router,private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
      this.queryData(1);
  }

  queryData(page) {
    this.loading = true;
    this.showButton = false;
    this.rawData=[];
    this.dbData = [];
    let header = {
        'action': 'load-all',
        'lang':'EN',
        'navigate': this.router.url,
    }
      this.HttpService.call(this.endpoint.API_BANK_CODE_MAPPING, 'GET', header,null, 
      {currentPage: page}, 
      null, 
      this.querySuccessCallback.bind(this), 
      null);
  }

  querySuccessCallback(snap:any) {
    this.global.checkTokenExpired(snap.data.statusCode);
      if (snap.data.statusCode == 'S0000') {
          this.dbData = snap.data.data.bankcodeMapMasterList;
          this.paginator = snap.data.data.pagingInfo;
          this.setData(snap.data.data.bankcodeMapMasterList);
          this.loading = false;
          this.msg = 'success';
      }
        else {
          this.loading = false;
          this.msg = 'fail';
      }
  } 

  setData(data){
    this.dbDataBackup = [];
    this.dbDataBackup = data.map(snap => {
       return {
        botValueBackup: snap.botValue,
        legacyNameBackup: snap.legacyName,
        legacyValueBackup: snap.legacyValue,
        mappingIdBackup: snap.mappingId
       }
    })
  }

  checkDataUpdate(){
  this.dbData.map(data1 => {
    this.dbDataBackup.map(data2 => {
      if(data1.mappingId == data2.mappingIdBackup){
          if(data1.botValue != data2.botValueBackup){
            if(data1.botValue != '' && data1.botValue != null){
              this.empty = false;
              this.dbDataUpdate.push({
              botValue: data1.botValue,
              mappingId: data1.mappingId
              })
            }
            else{
              this.empty = true;
              this.msg = 'fail';
            }
          }
      }})
    })
  }

  update(){
    this.msg = 'loading'
    this.checkDataUpdate();
    console.log(this.dbDataUpdate)
    if(this.empty == false){
        this.errorDesc = '';
        var header = {
            'action': 'update',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_BANK_CODE_MAPPING, 'PUT', header,null, null, {bankcodeMapping: this.dbDataUpdate}, this.updateSuccessCallback.bind(this), this.updateFailCallback.bind(this));
      }
  }

updateSuccessCallback(snap:any) {
    this.global.checkTokenExpired(snap.data.statusCode);
    if(snap.data.statusCode === 'S0000') {
        this.dataActive = true;
        this.formActive = false;
        this.dbDataUpdate = [];
        this.loading = false;
        this.msg = 'success';
        this.clearForm();
    }
    else {
      this.loading = false;
      this.msg = 'fail';
    }
    console.log(this.msg)
}

updateFailCallback(snap:any) {
    this.loading = false;
    this.msg = 'overflow';
}

async clearForm() {
  this.showButton = false;
  this.formData = {
    mappingId: 0,
    legacyName: '',
    legacyValue: '',
    botValue: ''  
  };
  this.queryData(this.paginator.currentPage);
}

changeValue() {
  this.showButton = true;
}

paginate(event) {
    if ((event.page + 1) != this.paginator.currentPage) {
                this.queryData(event.page + 1);
      }
  }
}

