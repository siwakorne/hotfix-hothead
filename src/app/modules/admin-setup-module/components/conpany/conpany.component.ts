import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';
import { Endpoint } from '../../../../common/const/endpoint';
import { HttpService } from '../../../../common/util/http-service';
import { Router } from '@angular/router';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-conpany',
    templateUrl: './conpany.component.html',
    styleUrls: ['./conpany.component.css']
})
export class ConpanyComponent implements OnInit {

    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    msgs: Message[] = [];
    formActive = false;
    dataActive = true;
    dataUserActive = true;
    formUserActive = false;
    msg: string = '';
    msguser: string = '';
    edit = '';
    edteuser = '';
    status = '';
    pageUser = 0;
    userData = [];
    errorDesc = '';
    errorCode = '';
    deleteData = '';
    locationUser: any;
    alert = {
        username: '',
        name: '',
        ip: '',
    };
    dataSearch = {
        findBy: null,
        search: null
    };
    paging = 0;
    page: number = 0;
    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };

    filterData = [];
    dbData = [];
    roleData = [];
    formData = {
        cashAccountMapping: '',
        companyCode: '',
        companyNameEn: '',
        companyNameTh: '',
        glEntityId: '',
        glSegment01: '',
        legalEntity: '',
    };
    formUserData = {
        locationCode: '',
        username: ''
    };
    roleRawData: object;
    rawData: object;

    constructor(private router: Router, private endpoint: Endpoint,private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {
        this.queryData(1, 'paging');
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            // this.loading = true
            this.queryData(event.page + 1, 'paging');
        }
    }

    queryData(page, click) {
        this.rawData = [];
        this.dbData = [];
        this.roleData = [];
        this.roleRawData = [];

        var headers = {
            'action': 'load-all',
            'lang': 'en',
            'navigate': this.router.url,
        }

        if (this.dataSearch.findBy == 'companyCode') {
            this.HttpService.call(this.endpoint.API_COMPANY_MASTER, 'GET', headers, null,
            {
                currentPage: page ? page : null,
                companyCode: this.dataSearch.search
            }, null, this.searchSuccessCallback.bind(this), null);
        } else if (this.dataSearch.findBy == 'companyNameEn') {
            this.HttpService.call(this.endpoint.API_COMPANY_MASTER, 'GET', headers, null,
            {
                currentPage: page ? page : null,
                companyNameEn: this.dataSearch.search
            }, null, this.searchSuccessCallback.bind(this), null);
        }else if (this.dataSearch.findBy == 'companyNameTh') {
            this.HttpService.call(this.endpoint.API_COMPANY_MASTER, 'GET', headers, null,
            {
                currentPage: page ? page : null,
                companyNameTh: this.dataSearch.search
            }, null, this.searchSuccessCallback.bind(this), null);
        } else {
            this.HttpService.call(this.endpoint.API_COMPANY_MASTER, 'GET', headers, null,
            {
                currentPage: page ? page : null
            }, null, this.searchSuccessCallback.bind(this), null);
        }
       
    }

    searchSuccessCallback(snapData: any) {
        this.global.checkTokenExpired(snapData.data.statusCode);
        this.dbData = snapData.data.data.companyMasterInfoList;
        this.paginator = snapData.data.data.pagingInfo;
        if (snapData.data.data.pagingInfo.totalRecords != 0) {
            // this.loading = false;
            // this.checkData = 1;
            this.paging = 1
        } else {
            // this.loading = false;
            // this.checkData = 0;
            this.paging = 0;
        }
    }

    formSearch() {
        this.clearForm();
        this.queryData(1, 'paging');
        this.dataActive = true;
        this.formActive = false;
        this.filterData = [];
        if (this.dataSearch.findBy.trim() && this.dataSearch.search.trim()) {
            this.filterData = this.dbData.filter(data => {
                if (this.dataSearch.findBy === '1') return data.userName.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '2') return data.name.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '3') return data.statusActive.toLowerCase().indexOf(this.dataSearch.search.toLowerCase()) ? '' : data
                else if (this.dataSearch.findBy === '4') return data.roleId == this.dataSearch.findBy ? 0 : data
            });
            if (!this.filterData.length) {
                this.msgs = [];
                this.msgs.push({ severity: 'info', summary: 'Alert', detail: 'No Data' });
            } else {
                this.msgs = [];
                this.msgs.push({ severity: 'success', summary: 'Alert', detail: `Total query ${this.filterData.length} rows` });
            }
        }
    }

    formBank() {
        this.dataActive = false;
        this.formActive = true;
    }

    editBank(data) {
        this.formBank();
        this.formData = data;
        this.edit = '1';
    }

    update() {
        this.errorDesc = '';
        this.errorCode = '';
        if ((this.formData.cashAccountMapping == '' || null || undefined) ||
            (this.formData.companyCode == '' || null || undefined) ||
            (this.formData.companyNameEn == '' || null || undefined) ||
            (this.formData.companyNameTh == '' || null || undefined) ||
            (this.formData.glEntityId == '' || null || undefined) ||
            (this.formData.glSegment01 == '' || null || undefined) ||
            (this.formData.legalEntity == '' || null || undefined)) {
                this.errorCode = 'inComplete';
                this.errorDesc = 'กรอกข้อมูลไม่ครบถ้วน';
        } else {

            this.dataActive = true;
            this.formActive = false;

            var headers = {
                'action': 'update',
                'lang': 'en',
                'navigate': this.router.url,
            }

            var datas = this.formData

            this.HttpService.call(this.endpoint.API_COMPANY_MASTER, 'PUT', headers, null, null, datas, this.updateSuccessCallback.bind(this), null)
        }
    }

    updateSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        if (data.data.statusCode == 'S0000') {
            this.errorCode = data.data.statusCode;
            this.errorDesc = data.data.statusMessage;
            this.msg = 'pass';
            this.clearForm();
            this.queryData(1, 'paging');
        } else {
            this.msg = 'fail';
        }
    }

    submitData() {
        this.errorDesc = '';
        this.errorCode = '';
        if ((this.formData.cashAccountMapping == '' || null || undefined) ||
            (this.formData.companyCode == '' || null || undefined) ||
            (this.formData.companyNameEn == '' || null || undefined) ||
            (this.formData.companyNameTh == '' || null || undefined) ||
            (this.formData.glEntityId == '' || null || undefined) ||
            (this.formData.glSegment01 == '' || null || undefined) ||
            (this.formData.legalEntity == '' || null || undefined)) {
                this.errorCode = 'inComplete';
                this.errorDesc = 'กรอกข้อมูลไม่ครบถ้วน';
        } else {
            this.dataActive = true;
            this.formActive = false;
            var headers = {
                'action': 'add',
                'lang': 'en',
                'navigate': this.router.url,
            }
            var datas = this.formData
            this.HttpService.call(this.endpoint.API_COMPANY_MASTER, 'POST', headers, null, null, datas, this.addSuccessCallback.bind(this), null)
        }

    }

    addSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        if (data.data.statusCode == 'S0000') {
            this.msg = 'pass';
            this.clearForm();
            this.queryData(1, 'paging');
        } else {
            this.msg = 'fail';
        }
    }

    clearForm() {
        this.formData = {
            cashAccountMapping: '',
            companyCode: '',
            companyNameEn: '',
            companyNameTh: '',
            glEntityId: '',
            glSegment01: '',
            legalEntity: '',
        };
        this.alert = {
            username: '',
            name: '',
            ip: '',
        };
    }

    delete(data){
        this.deleteData = data;
    }

    deleteBank() {
       /*  data.status = "D" */;
        var headers = {
            'action': 'delete',
            'lang': 'en',
            'navigate': this.router.url,
        }
        var datas = {
            "companyCode": this.deleteData
        }
        this.HttpService.call(this.endpoint.API_COMPANY_MASTER, 'DELETE', headers, null, null, datas, this.deleteSuccessCallback.bind(this), null)
    }


    deleteSuccessCallback(data: any) {
        this.global.checkTokenExpired(data.data.statusCode);
        this.formSearch();
        this.queryData(1, 'paging');
    }


    clearSearchValue(){
        this.dataSearch.search = null;
    }
}
