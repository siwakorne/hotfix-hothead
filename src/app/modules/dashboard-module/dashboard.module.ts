import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DASHBOARD_ROUTING } from '../routing-modules/auth/dashboard-routing';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(DASHBOARD_ROUTING)
    ],
    declarations: [DashboardComponent]
})
export class DashboardModule {
    constructor() { }
} 
