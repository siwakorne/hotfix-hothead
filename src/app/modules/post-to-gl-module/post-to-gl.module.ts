import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PostToGlComponent } from './components/post-to-gl/post-to-gl.component';
import { POST_TO_GL_ROUTING } from '../routing-modules/auth/post-to-gl-routing';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(POST_TO_GL_ROUTING)
    ],
    declarations: [PostToGlComponent]
})
export class PostToGlModule {
    constructor() { }
}
