import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { TabMenuModule } from 'primeng/tabmenu';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';
import { TooltipModule } from 'primeng/tooltip';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { CONFRM_SALE_PAYMNT_ROUTING } from '../routing-modules/auth/confrm-sale-paymnt-routing';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

import { ConfirmSalePaymentComponent } from './components/confirm-sale-payment/confirm-sale-payment.component';
import { ClosingEndOfDayComponent } from './components/closing-end-of-day/closing-end-of-day.component';

import { FormSalePaymentComponent } from './components/form-sale-payment/form-sale-payment.component';
import { AdjustViewComponent } from './components/adjust-view/adjust-view.component';
import { AdjustListComponent } from './components/adjust-list/adjust-list.component';
import { AdjustComponent } from '../../common/components/adjust/adjust.component';
import { SearchAdjustComponent } from '../../common/components/search-adjust/search-adjust.component';
import { UpdateAdjustComponent } from './components/update-adjust/update-adjust.component';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { KeyFilterModule } from 'primeng/keyfilter';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CONFRM_SALE_PAYMNT_ROUTING),
        CalendarModule,
        CurrencyMaskModule,
        FormsModule,
        TabMenuModule,
        PaginatorModule,
        DialogModule,
        ProgressSpinnerModule,
        ConfirmDialogModule,
        ScrollPanelModule,
        KeyFilterModule,
        TooltipModule
    ],
    declarations: [
        ConfirmSalePaymentComponent,
        ClosingEndOfDayComponent,
        AdjustComponent,
        FormSalePaymentComponent,
        AdjustViewComponent,
        AdjustListComponent,
        SearchAdjustComponent,
        UpdateAdjustComponent
    ],
    exports: [ AdjustComponent, AdjustListComponent, AdjustViewComponent]
})
export class ConfirmSalePaymentModule {
    constructor() { }
}
