import { Component, OnInit, EventEmitter, Input, Output, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as axios from 'axios';
import { Endpoint } from '../../../../common/const/endpoint';
import { v4 as uuid } from 'uuid';
import * as moment from 'moment';
import { TableBody } from 'primeng/primeng';
import { AppComponent } from '../../../../app.component';

@Component({
    selector: 'app-adjust-view',
    templateUrl: './adjust-view.component.html',
    styleUrls: ['./adjust-view.component.css']
})
export class AdjustViewComponent implements OnInit, OnChanges {

    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];
    view: any;
    adjustType: any;
    adjustStatus: any;
    company: any;
    loading: boolean;
    msg;
    merchantType: string;
    msgSave: string = '';
    bank: string;
    bankCn: any;
    merchantId: string;
    merchantCn: any;

    @Input() data: {
        company: string,
        payment: string,
        adjustNo: string,
        adjustTypeCode: string,
        status: string,
        diffAmt: string,
        bankEdc: string,
        locationCode: string,
        locationName: string,
        // merchartId: string,
        bankDoc: string,
        reverseMasterId: number,
        reverseAccFlag: string,
        note: string,
        key: string,
        docDate: null,
        amount: number,
        adjustNote: string,
        saleSlipDate: string,
        whtFlag: string,
        merchartId: string;
    };
    @Output() displayClose = new EventEmitter<boolean>();
    @Output() searchRe = new EventEmitter<boolean>();

    dateVal = new Date();
    detail: any;
    display
    bankRef = '';
    url: string;
    urlHead: string;

    constructor(private router: Router, private params: ActivatedRoute, private endpoint: Endpoint, private global: AppComponent) { }

    onCancel(agreed: boolean) {
        this.data.whtFlag = '';
        this.bankRef = '';
        this.displayClose.emit(agreed);
    }

    ngOnInit() {
        this.bankRef = '';
        let overstring = this.router.url.indexOf('?');
        if(this.mockLocationId) {
            if (overstring) {
                this.urlHead = this.router.url.substr(0, overstring);
                this.dropDown();
                this.queryFromReal();
            } else {
                this.urlHead = this.router.url;
                this.dropDown();
                this.queryFromReal();
            }
        }
        if (this.router.url === '/credit-note/search') {
            this.url = 'Treasury';
        } else {
            this.url = 'shop';
        }
        
    }

    ngOnChanges() {
        this.queryFromReal();
    }

    async queryFromReal() {
        this.loading = true;
        this.msg = 'success';
        this.view = [];
        await axios.default.request({
            url: this.endpoint.API_VIEW_ADJ,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'view',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.urlHead,
                'token': this.rawMockLocationId[3]
            },
            params: {
                adjustDocId: this.data.adjustNo
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.view = snapData.data.data.adjSrchRespList.map(snap => {
                    this.bank = snap.edcBank;
                    this.data.bankEdc = snap.edcBank;
                    this.merchantId = snap.merchantId ? snap.merchantId : null;
                    this.data.merchartId = snap.merchantId ? snap.merchantId : null;
                    return {
                        companyCode: snap.companyCode,
                        adjustAmt: snap.adjustAmt,
                        adjustDate: snap.adjustDate,
                        adjustDocId: snap.adjustDocId,
                        adjustMasterId: snap.adjustMasterId,
                        adjustStatus: snap.adjustStatus,
                        locationCode: snap.locationCode,
                        mainDocDate: snap.mainDocDate,
                        mainDocId: snap.mainDocId,
                        paymentType: snap.paymentTypeCode === 'CSH' ? 'Cash' : snap.paymentTypeCode === 'CHQ' ? 'Cheque' : snap.paymentTypeCode === 'CC' ? 'Credit Card' : '',
                        adjustNote: snap.adjustNote,
                        refBankDoc: snap.refBankDoc ? snap.refBankDoc : '-',
                        saleSlipDate: snap.saleSlipDate ? moment(snap.saleSlipDate).format('DD/MM/YYYY') : null,
                        edcBank: snap.edcBank,
                        last4Digits: snap.last4Digits,
                        paymentTypeCode: snap.paymentTypeCode,
                        refAdjustDocId: snap.refAdjustDocId != '0' ? snap.refAdjustDocId : '-',
                        chequeNo: snap.chequeNo,
                        whtFlag: snap.whtFlag == 'N' ?'No' : 'Yes',
                    };
                });
                this.loading = false;
                this.msg = 'success';
                this.loadMerchant();
                this.loadMerchantCn();
            }
            else {
                this.loading = false;
                this.msg = 'fail';
            }
        }).catch(err => {
            console.error(err.response);
        });
    }


    onSave(view) {
        this.msg = '';
        this.msgSave = '';
        var date = new Date(view.saleSlipDate);
        if (this.bankRef == '') {
            this.msg = 'overflow';
        } else {


            axios.default.request({
                url: this.endpoint.API_UPDATE_ADJ,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'save-cn',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'username': this.rawMockLocationId[1],
                    'lang': 'EN',
                    'navigate': this.urlHead,
                    'token': this.rawMockLocationId[3]
                },
                data: {
                    adjUpdInfo: {
                        'companyCode': view.companyCode,
                        'adjustDocId': view.adjustDocId,
                        'refBankDoc': this.bankRef,
                        'edcBank' : this.data.bankEdc,
                        'merchantId' : this.data.merchartId

                    }
                }
            }).then(response => {
                this.global.checkTokenExpired(response.data.statusCode);
                if (response.data.statusCode == 'S0000') {
                    this.msgSave = 'success';
                } else {
                    this.msgSave = 'fail';
                }
                this.queryFromReal();
            })
                .catch(err => {
                    if (err.response.data.statusCode == 'S0000') {
                        this.msgSave = 'success';
                    } else {
                        this.msgSave = 'fail';
                    }

                })
        }
    }

    dropDown() {

        // adjustType dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_TYPE,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.urlHead,
                'token': this.rawMockLocationId[3]
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.adjustType = snapData.data.data.adjMastInfoList.map(snap => {
                    return {
                        adjustType: snap.adjustType,
                        adjustDocType: snap.adjustDocType,
                        adjustMasterId: snap.adjustMasterId
                    };
                });
            }
        }).catch(err => {
            console.error(err.response);
        });

        // adjustStatus dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_STTS,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.urlHead,
                'token': this.rawMockLocationId[3]
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.adjustStatus = snapData.data.data.adjSttsInfoList.map(snap => {
                    return {
                        adjustStatusCode: snap.adjustStatusCode,
                        adjustStatusDesc: snap.adjustStatusDesc
                    };
                });
            }
        });

        // company dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_COMP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.company = snapData.data.data.companyInfoList.map(snap => {
                    return {
                        companyCode: snap.companyCode,
                    };
                });
            }
        }).catch(error => {
            console.error(error.response);
        });

        //bank
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK_MERCHANT,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank',
                'transactionId': uuid(),
                'locationId': this.data.locationCode ? this.data.locationCode : this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },params: {
                locationCode: this.data.locationCode,
                companyCode: this.data.company
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.bankCn = snapData.data.data.locMrchntInfoList.map(snap => {
                    return {
                        bankName: snap.bankName,
                    };
                });
            }
        });
    }

    loadMerchant() {
        //load merchant
        this.merchantType = '';
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK_MERCHANT,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-merchant',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.urlHead,
                'token': this.rawMockLocationId[3]
            },
            params: {
                bankName: this.data.bankEdc,
                locationCode: this.data.locationCode,
                companyCode: this.data.company
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    snapData.data.data.locMrchntInfoList.map(snap => {
                        if (this.merchantId == snap.merchantId) {
                            this.merchantType = snap.merchantType                           
                        }
                    });
                }
            });
    }

    loadMerchantCn() {
        this.merchantType = '';
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK_MERCHANT,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-merchant',
                'transactionId': uuid(),
                'locationId': this.data.locationCode ? this.data.locationCode : this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.urlHead,
                'token': this.rawMockLocationId[3]
            },
            params: {
                bankName: this.data.bankEdc,
                locationCode: this.data.locationCode,
                companyCode: this.data.company
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.merchantCn =  snapData.data.data.locMrchntInfoList.map(snap => {
                      return {
                        merchantId: snap.merchantId,
                        merchantType: snap.merchantType
                      }

                    });
                }
            });
    }
}
