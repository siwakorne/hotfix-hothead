import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import { MenuItem } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import * as axios from 'axios';
import * as moment from 'moment';
import { v4 as uuid } from 'uuid';
import { MenuService } from '../../../../views/layout/menubar/menu.service';
import { AppComponent } from '../../../../app.component';
import { Paginator } from 'primeng/primeng';
import { HttpService } from '../../../../common/util/http-service';
@Component({
    selector: 'app-form-sale-payment',
    templateUrl: './form-sale-payment.component.html',
    styleUrls: ['./form-sale-payment.component.css'],
    providers: [ConfirmationService]
})
export class FormSalePaymentComponent implements OnInit {
    @ViewChild('pageChq') pageChq: Paginator;
    @ViewChild('pageWht') pageWht: Paginator;
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));

    regExFilterWhtNo = /^[-/\\.,()[\]{}:;#$!?_<>*\w]*$/;
    mockLocationId = this.rawMockLocationId[2];
    pendingApi: boolean = false;
    pendingWHT: boolean = false;
    changePagingError: boolean = false;
    locationCode: string = this.rawMockLocationId[2];
    navigatorToMain = '/';
    fetchPaging: boolean = false;
    errorCheckSwitch: boolean = false;
    items: MenuItem[];
    activeItem: MenuItem;
    backupDateNewList: string = '';
    selectForm = 1;
    activeForm1 = '';
    activeForm2 = '';
    errorText: string = '';
    activeForm3 = '';
    messageAlert = '';
    test = 0;
    btnState = '';
    whtCheck = [];
    chqCheck = [];
    clickChange = false;
    backupTab1: string = '';
    backupTab2: string = '';
    backupTab3: string = '';
    statusTab1: string = '';
    statusTab2: string = '';
    statusTab3: string = '';
    errorStatus: boolean = false;
    totalAmount: 0;
    mearg = null;
    mergLoad = false;
    docStatus = null
    lastSyncDate = null;
    eodPayment = {
        docId: null,
        chq: {
            dtn: null,
            dtac: null
        },
        wht: {
            dtn: {
                csh: null,
                chq: null,
                cc: null
            },
            dtac: {
                csh: null,
                chq: null,
                cc: null
            }
        }
    }
    timedate = '';
    dateVal = '';
    docDate = '';
    closed = 'closed';
    // failCondition: boolean = false;
    loading = false;
    clickLoad = false;
    legacyAmount = {
        DTACChequeTotal: 0,
        DTNChequeTotal: 0,
        DTACWhtCashTotal: 0,
        DTACWhtChequeTotal: 0,
        DTACWhtCreditTotal: 0,
        DTNWhtCashTotal: 0,
        DTNWhtChequeTotal: 0,
        DTNWhtCreditTotal: 0,
        cashTotal: 0,
        CCDtacTotal: 0,
        CCDtnTotal: 0
    };
    objectData = {
        cashCC: {
            dtnCsh: {
                shopAmt: null,
                paymentTypeCode: 'CSH',
                companyCode: 'DTN',
                paymentId: 0
            },
            dtacCC: {
                shopAmt: null,
                paymentTypeCode: 'CC',
                companyCode: 'DTAC',
                paymentId: 0
            },
            dtnCC: {
                shopAmt: null,
                paymentTypeCode: 'CC',
                companyCode: 'DTN',
                paymentId: 0
            }
        },
        cashcredit: [],
        cheque: [],
        wht: []
    };
    brandData: any;
    legacyData: any;
    datainput = {
        dtncash: 0,
        dtncredit: 0,
        dtaccredit: 0
    }
    defaultDtnCash;
    defaultDtacCredit;
    defaultDtnCredit;
    whtPagination = [];
    chequePagination = [];
    defaultWhtPagination = [];
    defaultChequePagination = [];
    paginator = {
        firstCheque: 0,
        cheque: {
            totalRecords: 0,
            recordPerPage: 3,
            currentPage: 0,
            totalPages: 0
        },
        firstWht: 0,
        wht: {
            totalRecords: 0,
            recordPerPage: 10,
            currentPage: 0,
            totalPages: 0
        }
    };
    display = false;
    loadCsh = false;
    loadChq = false;
    loadWht = false;
    company = [];
    bank = [];
    form = {
        active: '',
        wht: {
            shopEodDocId: 0,
            // shopEodPaymentId: 0,
            companyCode: '',
            taxId: '',
            custName: '',
            whtNo: '',
            whtDate: '',
            paymentTypeCode: '',
            shopAmt: null,
            paymentAmt: null,
            whtFlag: 'Y',
        },
        cheque: {
            shopEodDocId: 0,
            // shopEodPaymentId: 0,
            company: '',
            bankCode: '',
            chequeNo: '',
            shopAmt: null,
            paymentTypeCode: 'CHQ',
            whtFlag: 'N',
        }
    };

    constructor(private router: Router, private Endpoint: Endpoint, private confirmationService: ConfirmationService, private menuService: MenuService, private global : AppComponent, private httpService: HttpService) { }

    async ngOnInit() {
        this.mearg = 'fail';
        this.docStatus = null;
        this.dropDown();
        this.items = [
            {
                label: 'Cash & Credit Card', command: event => {
                    this.selectForm = 1;
                }
            },
            {
                label: 'Cheque', command: event => {
                    this.selectForm = 2;
                }
            },
            {
                label: 'Withholding Tax', command: event => {
                    this.selectForm = 3;
                }
            }
        ];
        this.activeItem = this.items[0];
        this.loading = true;
        this.queryData('cash', 0);
        this.queryData('cheque', 1);
        this.queryData('wht', 1);
        this.lastSync()
    }

    lastSyncDateSuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode == 'S0000'){
            this.lastSyncDate = snap.data.data.lastSyncDate
        }
    }

    lastSync(){

        this.httpService.call(
            this.Endpoint.API_MEARG,
            "GET",
            {
                'lang': 'EN',
                'navigate': this.router.url,
                'action': 'merge-last-sync'
            },
            null,
            null,
            {
                locationCode: this.mockLocationId
            },
            this.lastSyncDateSuccessCallback.bind(this),
            null
        );
    }
    
    mergeConfirmSuccessCallback(snap: any) {
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode == 'S0000'){
            this.mearg == 'success'
            this.loading = true;
            this.mergLoad = false;
            this.queryData('cash', 0);
            this.queryData('cheque', 1);
            this.queryData('wht', 1);
        }
    }

    meargConfirm(){
        this.mergLoad = true;
        this.httpService.call(
            this.Endpoint.API_MEARG,
            "GET",
            {
                'lang': 'EN',
                'navigate': this.router.url,
                'action': 'merge'
            },
            null,
            {
                docDate :this.timedate
            },
            null,
            this.mergeConfirmSuccessCallback.bind(this),
            null
        );
    }

    activeForm(form) {
        if (this.statusTab1 !== 'E' && this.statusTab2 !== 'E' && this.statusTab3 !== 'E') {
            if (form === 1 && this.selectForm != 1) {
                this.selectForm = 1;
                this.activeForm1 = 'ui-state-active';
                this.queryData('cash', 0);
            } else if (form === 2 && this.selectForm != 2) {
                this.selectForm = 2;
                this.activeForm2 = 'ui-state-active';
                this.queryData('cheque', (this.paginator.firstCheque / 10) + 1);
            } else if (form === 3 && this.selectForm != 3) {
                this.selectForm = 3;
                this.activeForm3 = 'ui-state-active';
                this.queryData('wht', (this.paginator.firstWht / 10) + 1);
            }
        } else {
            this.confirmationService.confirm({
                message: 'Do you want to save-draft before Change tabs?',
                accept: async () => {
                    if (this.statusTab1 === 'E') {
                        await this.saveDraft('cash');
                    } else if (this.statusTab2 === 'E') {
                        await this.saveDraft('cheque');
                    } else if (this.statusTab3 === 'E') {
                        await this.saveDraft('wht');
                    }
                    if(this.errorCheckSwitch) {
                        this.display = true;
                    } else {
                        if(this.statusTab1 === 'E') this.statusTab1 = 'D';
                        else if (this.statusTab2 === 'E') this.statusTab2 = 'D';
                        else if (this.statusTab3 === 'E') this.statusTab3 = 'D';
                        if (form === 1) {
                            this.selectForm = 1;
                            this.activeForm1 = 'ui-state-active'
                            this.queryData('cash', 0);
                        } else if (form === 2) {
                            this.selectForm = 2;
                            this.activeForm2 = 'ui-state-active'
                            this.queryData('cheque', (this.paginator.firstCheque / 10) + 1);
                        } else if (form === 3) {
                            this.selectForm = 3;
                            this.activeForm3 = 'ui-state-active'
                            this.queryData('wht', (this.paginator.firstWht / 10) + 1);
                        }
                    }
                },
                reject: () => {
                    if (this.statusTab1 === 'E') {
                        this.statusTab1 = this.backupTab1;
                    } else if (this.statusTab2 === 'E') {
                        this.statusTab2 = this.backupTab2;
                    } else if (this.statusTab3 === 'E') {
                        this.statusTab3 = this.backupTab3;
                    }
                    if (form === 1) {
                        this.selectForm = 1;
                        this.activeForm1 = 'ui-state-active';
                        this.queryData('cash', 0);
                    } else if (form === 2) {
                        this.selectForm = 2;
                        this.activeForm2 = 'ui-state-active';
                        this.queryData('cheque', (this.paginator.firstCheque / 10) + 1);
                    } else if (form === 3) {
                        this.selectForm = 3;
                        this.activeForm3 = 'ui-state-active';
                        this.queryData('wht', (this.paginator.firstWht / 10) + 1);
                    }
                }
            });
        }
    }

    paginate(event, type) {
        this.clickChange = true;
        if (type === 'cheque') {
            if (this.statusTab2 !== 'E') {
                if (this.paginator.firstCheque != event.first) {
                    this.paginator.firstCheque = event.first;
                    this.queryData(type, (event.page + 1));
                }
            } else {
                this.confirmationService.confirm({
                    message: 'Do you want to save-draft before change pagination?',
                    accept: () => {
                        if (this.paginator.firstCheque != event.first) {
                            this.paginator.firstCheque = event.first;
                        }
                        this.saveDraft('cheque');
                    },
                    reject: () => {
                        this.statusTab2 = 'C';
                        if (this.paginator.firstCheque != event.first) {
                            this.paginator.firstCheque = event.first;
                            this.queryData(type, (event.page + 1));
                        }
                    }
                });
            }
            this.clickChange = false;
        } else {
            if (this.statusTab3 !== 'E') {
                if (this.paginator.firstWht != event.first) {
                    this.paginator.firstWht = event.first;
                    this.queryData(type, (event.page + 1));
                }
                this.clickChange = false;
            } else {
                var backupPaging: number = 0;
                if(!this.changePagingError) {
                    this.confirmationService.confirm({
                        message: 'Do you want to save-draft before change pagination?',
                        accept: async () => {
                            if (this.paginator.firstWht != event.first) {
                                backupPaging = this.paginator.firstWht;
                                this.paginator.firstWht = event.first;
                            }
                            await this.saveDraft('wht');
                            if(this.errorCheckSwitch) {
                                this.display = true;
                                this.changePagingError = true;
                                this.pageWht.changePage(backupPaging > 0 ? backupPaging/10 : backupPaging);
                                this.paginator.firstWht = backupPaging;
                                // this.queryData(type, (event.page + 1));
                            }
                            this.clickChange = false;
                        },
                        reject: () => {
                            this.statusTab3 = 'C'
                            if (this.paginator.firstWht != event.first) {
                                this.paginator.firstWht = event.first;
                                this.queryData(type, (event.page + 1));
                            }
                            this.clickChange = false;
                        }
                    });
                } else {
                    this.paginator.firstWht = backupPaging;
                    this.changePagingError = false;
                    this.clickChange = false;
                }
            }
        }
    }

    async saveDraft(type) {
        this.errorCheckSwitch = false;
        this.errorText = '';
        this.pendingApi = true;
        // this.failCondition = false;
        this.btnState = 'Save';
        let config = {
            headers: {
                'action': 'save-draft',
                'lang': 'EN',
                'navigate': this.router.url,
            }
        };
        if (type === 'wht') {
            let data = this.objectData.wht
            // let data ;
            // let checkStatus  = true;
            // let forMapA = this.objectData.wht;
            // let forMapB = this.objectData.wht;
            // forMapA.map(A=>{
            //     forMapB.map(B=>{
            //         if(A.taxId == B.taxId && A.whtNo == B.whtNo){
            //             this.errorStatus = true;
            //             this.errorText = 'WHT No และ Tax ID ซ้ำกัน';
            //             checkStatus  = false;
            //         }
            //     })
            // })
            // if(checkStatus){
            //     data = this.objectData.wht
            //     console.log(data)
            // }
            data = data.filter(dateConvert => {
                // if (!dateConvert.whtNo || !dateConvert.whtDate || !dateConvert.shopAmt) {
                //     this.failCondition = true;
                // }
                if(dateConvert.whtDate != null) {
                    if (dateConvert.whtDate.length === 10) {
                        let splitDate = dateConvert.whtDate.split('/');
                        let newDate = splitDate[1] + ',' + splitDate[0] + ',' + splitDate[2];
                        dateConvert.whtDate = moment(new Date(newDate)).format('x');
                    } else if (dateConvert.whtDate.length === 13) {
                        dateConvert.whtDate = dateConvert.whtDate ? dateConvert.whtDate : null;
                    } else {
                        dateConvert.whtDate = dateConvert.whtDate ? moment( moment.now()).format('x') : moment( moment.now()).format('x');
                    }
                }
                dateConvert.whtNo = dateConvert.whtNo ? dateConvert.whtNo : null;
                dateConvert.shopAmt = dateConvert.shopAmt ? dateConvert.shopAmt.replace(/,/g, '') : null;
                return dateConvert;
            });
            // Require request blankSpace to null data (in data send need to require)
            // if (!this.failCondition) {
                // @ts-check(Continute here)
                // this.httpService.call(
                //     this.Endpoint.API_COMFRM_WHT,
                //     "PUT",
                //     config.headers,
                //     null,
                //     null,
                //     {
                //         locationCode: this.mockLocationId
                //     },
                //     this.confirmWhtSuccessCallback.bind(this),
                //     this.confirmWhtFailCallback.bind(this)
                // );
                axios.default.request({
                    url: this.Endpoint.API_COMFRM_WHT,
                    method: 'PUT',
                    data: {
                        pmntsWHTList: data,
                        docDate: this.docDate
                    },
                    headers: config.headers
                }).then(response => {
                    data.filter(dateConvert => {
                        dateConvert.whtDate = dateConvert.whtDate ? moment.unix(dateConvert.whtDate / 1000).format('DD/MM/YYYY'): null;
                    });
                    if(response.data.statusCode == 'S0000') {
                        this.queryData(type, (this.paginator.firstWht / 10) + 1)
                        this.errorStatus = false;
                        this.statusTab3 = response.data.data.shopEodDocPaymentStts.whtStatus;
                    }
                    if(response.data.statusCode == 'B0029') {
                        this.errorCheckSwitch = true;
                        this.errorStatus = true;
                        this.errorText = 'WHT No และ Tax ID ซ้ำกัน';
                    }
                    this.pendingApi = false;
                }).catch(error => {
                    data.filter(dateConvert => {
                        dateConvert.whtDate = dateConvert.whtDate ? moment.unix(dateConvert.whtDate / 1000).format('DD/MM/YYYY'): null;
                    });
                    console.error(error.response);
                    this.errorStatus = true;
                    this.pendingApi = false;
                })
                // this.queryData(type, (this.paginator.firstWht / 10) + 1)
            // }
        } else if (type === 'cheque') {
            // Require request blankSpace to null data (in objectData.cheque need to require)
            let data = this.objectData.cheque
            data = data.map(money => {
                // if (!money.chequeNo || !money.shopAmt) {
                //     this.failCondition = true;
                // }
                money.chequeNo = money.chequeNo ? money.chequeNo : null;
                money.shopAmt = money.shopAmt ? money.shopAmt.replace(/,/g, '') : null;
                return money;
            });
            // if (!this.failCondition) {
                await axios.default.request({
                    url: this.Endpoint.API_COMFRM_CHEQUE,
                    method: 'PUT',
                    data: {
                        confrmChqSave: data,
                        docDate: this.docDate
                    },
                    headers: config.headers
                })
                .then(response => {
                    if(response.data.statusCode == 'S0000') {
                        this.queryData(type, (this.paginator.firstCheque / 10) + 1)
                        this.statusTab2 = response.data.data.shopEodDocPaymentStts.chqStatus;
                    }
                    this.pendingApi = false;
                })
                .catch(err => {
                    this.errorStatus = true;
                    console.log(err.response.data);
                    this.pendingApi = false;
                })
                // this.queryData(type, (this.paginator.firstCheque / 10) + 1)
            // }
        } else if (type === 'cash') {
            // let preObject = [];
            // Object.keys(this.objectData.cashCC).forEach(keys => {
            //     preObject.push(this.objectData.cashCC[keys]);
            // })
            let data = this.objectData.cashcredit;
            data = data.map(money => {
                // if (!money.shopAmt) {
                //     this.failCondition = true;
                // }
                money.shopAmt = money.shopAmt ? money.shopAmt.replace(/,/g, '') : null;
                return money;
            });
            // if (!this.failCondition) {
                await axios.default.request({
                    url: this.Endpoint.API_COMFRM_CASH,
                    method: 'PUT',
                    data: {
                        pmntsCshCrd: data,
                        docDate: this.docDate
                    },
                    headers: config.headers
                })
                .then(response => {
                    if(response.data.statusCode == 'S0000') {
                        this.statusTab1 = response.data.data.shopEodDocPaymentStts.cshCCStatus;
                    }
                    this.pendingApi = false;
                })
                .catch(err => {
                    console.log(err.response.data);
                    this.pendingApi = false;
                })
                // this.queryData(type, '')
            // }
        }
    }

    submit() {
        this.confirmationService.confirm({
            message: 'Do you want to submit data ?',
            accept: async () => {
                this.btnState = 'Submit';
                let config = {
                    headers: {
                        'Content-Type': 'application/json',
                        'action': 'save',
                        'transactionId': uuid(),
                        // 'locationId': this.mockLocationId,
                        'locationId': JSON.parse(localStorage.getItem('loginData'))[2],
                        'lang': 'EN',
                        'username': this.rawMockLocationId[1],
                        'navigate': this.router.url,
                        'token': this.rawMockLocationId[3]
                    }
                };
                this.objectData.wht.filter(dateConvert => {
                    // if (dateConvert.whtDate.length === 10) {
                    //     let splitDate = dateConvert.whtDate.split('/');
                    //     let newDate = splitDate[1] + ',' + splitDate[0] + ',' + splitDate[2];
                    //     dateConvert.whtDate = moment(new Date(newDate)).format('x');
                    // }
                });
                let cash = this.objectData.cashcredit;
                if (cash.length) {
                    cash = cash.map(money => {
                        money.shopAmt = money.shopAmt.replace(/,/g, '');
                        return money;
                    });
                    await axios.default.request({
                        url: this.Endpoint.API_COMFRM_CASH,
                        method: 'PUT',
                        data: {
                            pmntsCshCrd: this.objectData.cashcredit,
                            docDate: this.docDate
                        },
                        headers: config.headers
                    })
                    .catch(err => {
                        console.log(err.response.data);
                    });
                }
                let chq = this.objectData.cheque;
                if (chq.length) {
                    chq = chq.map(money => {
                        money.shopAmt = money.shopAmt.replace(/,/g, '');
                        return money;
                    });
                    await axios.default.request({
                        url: this.Endpoint.API_COMFRM_CHEQUE,
                        method: 'PUT',
                        data: {
                            confrmChqSave: this.objectData.cheque,
                            docDate: this.docDate
                        },
                        headers: config.headers
                    })
                    .catch(err => {
                        console.log(err.response.data);
                    });
                }
                let wht = this.objectData.wht;
                if (wht.length) {
                    wht = wht.map(money => {
                        money.shopAmt = money.shopAmt.replace(/,/g, '');
                        return money;
                    });
                    await axios.default.request({
                        url: this.Endpoint.API_COMFRM_WHT,
                        method: 'PUT',
                        data: {
                            pmntsWHTList: this.objectData.wht,
                            docDate: this.docDate
                        },
                        headers: config.headers
                    })
                    .catch(err => {
                        console.log(err.response.data);
                    });
                }
                await this.SubmitData();
            },
            reject: () => {
                
            }
        });
    }

    editValue(status) {
        if (status === 'statusTab1') {
            this.statusTab1 = 'E';
        } else if (status === 'statusTab2') {
            this.statusTab2 = 'E';
        } else if (status === 'statusTab3') {
            this.statusTab3 = 'E';
        }
    }

    queryData(type, page) {
        let config = {
            loadDraft: {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'load-draft',
                    'transactionId': uuid(),
                    // 'locationId': this.mockLocationId,
                    'locationId': JSON.parse(localStorage.getItem('loginData'))[2],
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.router.url,
                    'token': this.rawMockLocationId[3]
                }
            },
            list: {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'list',
                    'transactionId': uuid(),
                    // 'locationId': this.mockLocationId,
                    'locationId': JSON.parse(localStorage.getItem('loginData'))[2],
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.router.url,
                    'token': this.rawMockLocationId[3]
                }
            }
        };
        let preData = [];
        if (type === 'cash') {
            this.objectData.cashcredit = []
            this.clickLoad = true;
            this.objectData.cashcredit = [];
            this.loadCsh = true;
            // axios.default.get(`${this.Endpoint.API_COMFRM_CASH}`, config.loadDraft)
            axios.default.request({
                url: this.Endpoint.API_COMFRM_CASH,
                method: 'GET',
                headers: config.loadDraft.headers
            })
                .then(snapData => {
                    this.global.checkTokenExpired(snapData.data.statusCode);
                    if (snapData.data.data.docDate) {
                        this.timedate = snapData.data.data.docDate
                        this.dateVal = moment(snapData.data.data.docDate).format('DD/MM/YYYY');
                        this.docDate = snapData.data.data.docDate
                        
                    } else {
                        this.dateVal = 'No Data.'
                    }
                    if (snapData.data.data.shopEodDocPaymentStts) {
                        this.statusTab1 = snapData.data.data.shopEodDocPaymentStts.cshCCStatus
                        this.statusTab2 = snapData.data.data.shopEodDocPaymentStts.chqStatus;
                        this.statusTab3 = snapData.data.data.shopEodDocPaymentStts.whtStatus;
                        this.backupTab1 = snapData.data.data.shopEodDocPaymentStts.cshCCStatus;
                        this.backupTab2 = snapData.data.data.shopEodDocPaymentStts.chqStatus;
                        this.backupTab3 = snapData.data.data.shopEodDocPaymentStts.whtStatus;
                    }
                    preData = snapData.data.data.pmntsCshCrdList;
                    if (preData.length) {
                        this.global.checkTokenExpired(snapData.data.statusCode);
                        if (snapData.data.statusCode === 'S0000') {
                            this.loadCsh = false;
                        }
                        preData.filter(data => {
                            this.docStatus = data.docStatus;
                            this.eodPayment.docId = data.shopEodDocId;
                            if (data.paymentTypeCode === 'CSH' && data.companyCode !== 'PAYSBUY') {

                                let preProcess = data.shopAmt ? this.setDecimal(parseFloat(data.shopAmt)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : data.shopAmt == 0 ? this.setDecimal(parseFloat(data.shopAmt)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null ;

                                // this.objectData.cashCC.dtnCsh = data;
                                data.shopAmt = preProcess;
                                this.objectData.cashcredit.push(data);
                               
                                if (data.docStatus !== 'S') {
                                    // this.statusTab1 = data.docStatus;
                                    // this.backupTab1 = data.docStatus;
                                    this.closed = 'form';
                                } else {
                                    this.messageAlert = 'ได้ทำการ Confirm Sale & Payment เรียบร้อยแล้ว';                                    
                                }
                            }
                        });
                        preData.filter(data => {
                            this.docStatus = data.docStatus;
                            if (data.paymentTypeCode === 'CC' && data.companyCode !== 'PAYSBUY') {
                                if (data.companyCode === 'DTN') {

                                    let preProcess = data.shopAmt ? this.setDecimal(parseFloat(data.shopAmt)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : data.shopAmt == 0 ? this.setDecimal(parseFloat(data.shopAmt)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null;

                                    // this.objectData.cashCC.dtnCC= data;
                                    data.shopAmt = preProcess;
                                    this.objectData.cashcredit.push(data);
                                    this.docStatus = data.docStatus;
                                    if (data.docStatus !== 'S') {
                                        // this.statusTab1 = data.docStatus;
                                        // this.backupTab1 = data.docStatus;
                                        this.closed = 'form';
                                    } else {
                                        this.messageAlert = 'ได้ทำการ Confirm Sale & Payment เรียบร้อยแล้ว';
                                    }
                                }
                            }
                        });
                        preData.filter(data => {
                            this.docStatus = data.docStatus;
                            if (data.paymentTypeCode === 'CC' && data.company !== 'PAYSBUY') {
                                if (data.companyCode === 'DTAC') {
                                    // this.objectData.cashCC.dtacCC = data;

                                    let preProcess = data.shopAmt ? this.setDecimal(parseFloat(data.shopAmt)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : data.shopAmt == 0 ? this.setDecimal(parseFloat(data.shopAmt)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null;

                                    data.shopAmt = preProcess;
                                    this.objectData.cashcredit.push(data);
                                    this.docStatus = data.docStatus;
                                    if (data.docStatus !== 'S') {
                                        // this.statusTab1 = data.docStatus;
                                        // this.backupTab1 = data.docStatus;
                                        this.closed = 'form';
                                    } {
                                        this.messageAlert = 'ได้ทำการ Confirm Sale & Payment เรียบร้อยแล้ว';
                                    }
                                }
                            }
                        });
                        preData = [];
                    } else {
                        this.messageAlert = 'No Data';
                        this.loadCsh = false;
                        this.statusTab1 = 'D';
                    }
                    this.clickLoad = false;
                }).catch(err => {
                    this.messageAlert = 'No Data';
                    if (err.response) {
                        this.loading = false;
                        this.loadCsh = false;
                        this.statusTab1 = 'D';
                        this.clickLoad = false;
                        console.log(err.response);
                    } else {
                        this.loading = false;
                        this.loadCsh = false;
                        this.statusTab1 = 'D';
                    }
                })
        } else if (type === 'cheque') {
            this.objectData.cheque = [];
            this.loadChq = true;
            this.clickLoad = true;
            this.objectData.cheque = [];
            axios.default.request({
                url: this.Endpoint.API_COMFRM_CHEQUE + '/' + page,
                method: 'GET',
                headers: config.loadDraft.headers,
                params: {
                    whtFlag: 'N',
                    pmntTypeCode: 'CHQ'
                }

            }).then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode === 'S0000') {
                    this.loadChq = false;
                }
                if (snapData.data.data.shopEodDocPaymentStts) {
                    this.statusTab1 = snapData.data.data.shopEodDocPaymentStts.cshCCStatus
                    this.statusTab2 = snapData.data.data.shopEodDocPaymentStts.chqStatus;
                    this.statusTab3 = snapData.data.data.shopEodDocPaymentStts.whtStatus;
                    this.backupTab1 = snapData.data.data.shopEodDocPaymentStts.cshCCStatus;
                    this.backupTab2 = snapData.data.data.shopEodDocPaymentStts.chqStatus;
                    this.backupTab3 = snapData.data.data.shopEodDocPaymentStts.whtStatus;
                }
                preData = snapData.data.data.confirmChequeList;
                this.totalAmount = snapData.data.data.totalAmount;
                let data = preData;
                data = Object.assign({}, data, {
                    paymentType: 'CHQ',
                    whtFlag: 'N'
                });

                if (preData.length) {
                    preData.map(data => {
                        this.docStatus = data.docStatus;
                        data.shopAmt = data.shopAmt != undefined ? this.setDecimal(parseFloat(data.shopAmt)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : undefined;
                        if (data.docStatus !== 'S') {
                            // this.statusTab2 = data.docStatus;
                            // this.backupTab2 = data.docStatus;
                            this.closed = 'form';
                        } else {
                            this.messageAlert = 'ได้ทำการ Confirm Sale & Payment เรียบร้อยแล้ว';
                        }
                        if (data.company === 'DTN') {
                            this.eodPayment.docId = data.shopEodDocId;
                            this.eodPayment.chq.dtn = data.shopEodPaymentId;
                        }
                        if (data.company === 'DTAC') {
                            this.eodPayment.docId = data.shopEodDocId;
                            this.eodPayment.chq.dtac = data.shopEodPaymentId;
                        }
                        if (data.company === 'DTN' || data.company === 'DTAC') {
                            preData = Object.assign({}, data, { whtFlag: 'N', paymentTypeCode: 'CHQ' })
                            this.objectData.cheque.push(preData)
                        }
                    })
                    this.paginator.cheque = snapData.data.data.pagingInfo;
                    preData = [];
                } else {
                    this.messageAlert = 'No Data';
                    this.statusTab2 = 'D';
                }
                this.clickLoad = false;
            }).catch(err => {
                this.messageAlert = 'No Data';
                if (err.response) {
                    this.loadChq = false;
                    this.loading = false;
                    this.statusTab2 = 'D';
                    this.clickLoad = false;
                    console.log(err.response);
                } else {
                    this.loading = false;
                    this.statusTab2 = 'D';
                    this.loadChq = false;
                }
            })
        } else if (type === 'wht') {
            this.objectData.wht = [];
            this.clickLoad = true;
            this.loadWht = true;
            this.objectData.wht = [];
            // axios.default.get(`${this.Endpoint.API_COMFRM_WHT}/${page}`, config.loadDraft)
            axios.default.request({
                url: this.Endpoint.API_COMFRM_WHT+'/'+page,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'load-draft',
                    'transactionId': uuid(),
                    // 'locationId': this.mockLocationId,
                    'locationId': JSON.parse(localStorage.getItem('loginData'))[2],
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.router.url,
                    'token': this.rawMockLocationId[3]
                }
            })
                .then(snapData => {
                    preData = snapData.data.data.pmntsWHTList;
                    this.global.checkTokenExpired(snapData.data.statusCode);
                    if (snapData.data.statusCode === 'S0000') {
                        this.loadWht = false;
                    }
                    if (snapData.data.data.shopEodDocPaymentStts) {
                        this.statusTab1 = snapData.data.data.shopEodDocPaymentStts.cshCCStatus
                        this.statusTab2 = snapData.data.data.shopEodDocPaymentStts.chqStatus;
                        this.statusTab3 = snapData.data.data.shopEodDocPaymentStts.whtStatus;
                        this.backupTab1 = snapData.data.data.shopEodDocPaymentStts.cshCCStatus;
                        this.backupTab2 = snapData.data.data.shopEodDocPaymentStts.chqStatus;
                        this.backupTab3 = snapData.data.data.shopEodDocPaymentStts.whtStatus;
                    }
                    if (preData.length) {
                        preData.map(snapEod => {

                            snapEod.shopAmt = snapEod.shopAmt ? this.setDecimal(parseFloat(snapEod.shopAmt)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : snapEod.shopAmt == 0  ? this.setDecimal(parseFloat(snapEod.shopAmt)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null ;

                            if (snapEod.companyCode === 'DTN') {
                                if (snapEod.paymentTypeCode === 'CSH') {
                                    this.eodPayment.docId = snapEod.shopEodDocId;
                                    this.eodPayment.wht.dtn.csh = snapEod.shopEodPaymentId
                                } else if (snapEod.paymentTypeCode === 'CHQ') {
                                    this.eodPayment.docId = snapEod.shopEodDocId;
                                    this.eodPayment.wht.dtn.chq = snapEod.shopEodPaymentId
                                } else {
                                    this.eodPayment.docId = snapEod.shopEodDocId;
                                    this.eodPayment.wht.dtn.cc = snapEod.shopEodPaymentId
                                }
                            }
                            if (snapEod.companyCode === 'DTAC') {
                                if (snapEod.paymentTypeCode === 'CSH') {
                                    this.eodPayment.docId = snapEod.shopEodDocId;
                                    this.eodPayment.wht.dtac.csh = snapEod.shopEodPaymentId
                                } else if (snapEod.paymentTypeCode === 'CHQ') {
                                    this.eodPayment.docId = snapEod.shopEodDocId;
                                    this.eodPayment.wht.dtac.chq = snapEod.shopEodPaymentId
                                } else {
                                    this.eodPayment.docId = snapEod.shopEodDocId;
                                    this.eodPayment.wht.dtac.cc = snapEod.shopEodPaymentId
                                }
                            }
                        })
                        this.totalAmount = snapData.data.data.totalAmount;
                        this.paginator.wht = snapData.data.data.pagingInfo;
                        preData.map(data => {
                            this.docStatus = data.docStatus;
                            if (data.companyCode === 'DTN' || data.companyCode === 'DTAC') {
                                if (data.docStatus !== 'S') {
                                    // this.backupTab3 = data.docStatus;
                                    // this.statusTab3 = data.docStatus;
                                    this.closed = 'form';
                                } else {
                                    this.messageAlert = 'ได้ทำการ Confirm Sale & Payment เรียบร้อยแล้ว';                                    
                                }
                                data.whtDate = data.whtDate ? moment(data.whtDate).format('DD/MM/YYYY') : moment( moment.now()).format('x');
                                this.objectData.wht.push(data);
                            }
                        });
                        preData = [];
                    } else {
                        this.messageAlert = 'No Data';
                        this.statusTab3 = 'D';
                    }
                    this.loading = false;
                    this.clickLoad = false;
                }).catch(err => {
                    this.messageAlert = 'No Data';
                    if (err.response) {
                        this.loading = false;
                        this.loadWht = false;
                        this.statusTab3 = 'D';
                        this.clickLoad = false;
                        console.log(err.response);
                    } else {
                        this.loading = false;
                        this.loadWht = false;
                        this.statusTab3 = 'D';
                    }
                })
        }
    }

    SubmitData() {
        this.btnState = '';
        this.router.navigate(['/confirm-sale-payment/closing-end-of-day']);
        this.menuService.queryDataCloud(this.router.url);
    }

    addNewList(type) {
        this.display = true;
        this.errorCheckSwitch = false;
        if (type === 'Withholding Tax') {
            this.form.active = 'Withholding Tax';
        } else {
            this.form.active = 'Cheque';
        }
        this.clearNewList();
    }

    submitNewList(type) {
        this.errorStatus = false;
        this.pendingWHT = true;
        if (type === 'Withholding Tax') {
            if (this.form.wht.companyCode === 'DTN') {
                if (this.form.wht.paymentTypeCode === 'CSH') {
                    this.form.wht.shopEodDocId = this.eodPayment.docId
                } else if (this.form.wht.paymentTypeCode === 'CHQ') {
                    this.form.wht.shopEodDocId = this.eodPayment.docId
                } else {
                    this.form.wht.shopEodDocId = this.eodPayment.docId
                }
            }
            if (this.form.wht.companyCode === 'DTAC') {
                if (this.form.wht.paymentTypeCode === 'CSH') {
                    this.form.wht.shopEodDocId = this.eodPayment.docId
                } else if (this.form.wht.paymentTypeCode === 'CHQ') {
                    this.form.wht.shopEodDocId = this.eodPayment.docId
                } else {
                    this.form.wht.shopEodDocId = this.eodPayment.docId
                }
            }
            this.form.wht.shopAmt = this.form.wht.shopAmt.replace(/,/g, '');
            this.form.wht.paymentAmt = this.form.wht.paymentAmt.replace(/,/g, '');
            if(this.form.wht.taxId.length == 13){
            if((Math.abs(parseFloat(this.form.wht.shopAmt)) <= Math.abs(parseFloat(this.form.wht.paymentAmt))) && ((this.form.wht.paymentAmt < 0  && this.form.wht.shopAmt < 0 ) || (this.form.wht.paymentAmt > 0 && this.form.wht.shopAmt > 0))) {
                if (this.form.wht.whtDate.length === 10) {
                    let splitDate = this.form.wht.whtDate.split('/');
                    let newDate = splitDate[1] + ',' + splitDate[0] + ',' + splitDate[2];
                    this.form.wht.whtDate = moment(new Date(newDate)).format('x');
                } else if (this.form.wht.whtDate.length === 13) {
                    this.form.wht.whtDate = this.form.wht.whtDate ? this.form.wht.whtDate : moment( moment.now()).format('x');
                } else {
                    this.form.wht.whtDate = this.form.wht.whtDate ? moment(this.form.wht.whtDate).format('x') : moment( moment.now()).format('x');
                }
                this.errorText = '';
                axios.default.request({
                    url: this.Endpoint.API_COMFRM_WHT,
                    method: 'POST',
                    data: {
                        pmntWHT: {
                        shopEodDocId: this.form.wht.shopEodDocId,
                        companyCode: this.form.wht.companyCode,
                        taxId: this.form.wht.taxId,
                        custName: this.form.wht.custName,
                        whtNo: this.form.wht.whtNo,
                        whtDate: this.form.wht.whtDate,
                        paymentTypeCode: this.form.wht.paymentTypeCode,
                        shopAmt: parseFloat(this.form.wht.shopAmt),
                        paymentAmt: parseFloat(this.form.wht.paymentAmt),
                        whtFlag: 'Y'
                        }
                    },
                    headers: {
                        'Content-Type': 'application/json',
                        'action': 'add',
                        'transactionId': uuid(),
                        // 'locationId': this.mockLocationId,
                        'locationId': JSON.parse(localStorage.getItem('loginData'))[2],
                        'lang': 'EN',
                        'username': this.rawMockLocationId[1],
                        'navigate': this.router.url,
                        'token': this.rawMockLocationId[3]
                    }
                })
                .then((response) => {
                    if(response.data.statusCode == 'S0000') {
                        this.display = false;
                        this.errorStatus = false;
                        this.queryData('wht', (this.paginator.firstWht / 10) + 1)
                        this.clearNewList();
                    }
                    if(response.data.statusCode == 'B0029') {
                        this.form.wht.whtDate = this.form.wht.whtDate ? moment.unix(Number(this.form.wht.whtDate) / 1000).format('DD/MM/YYYY'): null
                        this.errorStatus = true;
                        this.errorText = 'WHT No และ Tax ID ซ้ำกัน';
                    }
                })
                .catch(() => {
                    this.display = false;
                    this.errorStatus = true;
                    this.clearNewList();
                })
            } else {
                this.errorStatus = true;
                this.errorText = 'WHT Amount ต้องน้อยกว่า Payment Amount';
            }
        }else{
            this.errorStatus = true;
                this.errorText = 'Tax ID  ไม่ถูกต้อง';
        }
        } else {
            if (this.form.cheque.company === 'DTN') {
                this.form.cheque.shopEodDocId = this.eodPayment.docId
            }
            if (this.form.cheque.company === 'DTAC') {
                this.form.cheque.shopEodDocId = this.eodPayment.docId
            }
            this.form.cheque.shopAmt = this.form.cheque.shopAmt.replace(/,/g, '');
            axios.default.request({
                url: this.Endpoint.API_COMFRM_CHEQUE,
                method: 'POST',
                data: {
                confrmInfo: {
                    shopEodDocId: this.form.cheque.shopEodDocId,
                    company: this.form.cheque.company,
                    bankCode: this.form.cheque.bankCode,
                    chequeNo: this.form.cheque.chequeNo,
                    shopAmt: parseFloat(this.form.cheque.shopAmt),
                    paymentTypeCode: this.form.cheque.paymentTypeCode,
                    whtFlag: this.form.cheque.whtFlag
                    },
                },
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'add',
                    'transactionId': uuid(),
                    // 'locationId': this.mockLocationId,
                    'locationId': JSON.parse(localStorage.getItem('loginData'))[2],
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.router.url,
                    'token': this.rawMockLocationId[3]
                }
            })
            .then(() => {
                this.errorStatus = false;
                this.display = false;
                this.queryData('cheque', (this.paginator.firstCheque / 10) + 1)
                this.clearNewList();
            })
            .catch(() => {
                this.display = false;
                this.errorStatus = true;
                this.clearNewList();
            })
        }
        this.pendingWHT = false;
    }

    dropDown() {
        // company dropdown
        axios.default.request({
            url: this.Endpoint.API_LOAD_COMP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                // 'locationId': this.mockLocationId,
                'locationId': JSON.parse(localStorage.getItem('loginData'))[2],
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.company = snapData.data.data.companyInfoList.map(snap => {
                    return {
                        companyCode: snap.companyCode,
                    };
                });
            }
        }).catch(error => {
            console.error(error.response);
        });
        // bank dropdown
        axios.default.request({
            url: this.Endpoint.API_LOAD_BANKCODE,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank-code',
                'transactionId': uuid(),
                // 'locationId': this.mockLocationId,
                'locationId': JSON.parse(localStorage.getItem('loginData'))[2],
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.bank = snapData.data.data.bankCodeInfoList.map(snap => {
                    return {
                        bankCode: snap.bankCode,
                        bankNameTh: snap.bankNameTh,
                        initialName: snap.initialName
                    }
                });
            }
        });
    }

    clearNewList() {
        this.form.wht = {
            shopEodDocId: 0,
            // shopEodPaymentId: 3,
            companyCode: '',
            taxId: '',
            custName: '',
            whtNo: '',
            whtDate: '',
            paymentTypeCode: '',
            shopAmt: '',
            paymentAmt: '',
            whtFlag: 'Y',
        };
        this.form.cheque = {
            shopEodDocId: 0,
            //shopEodPaymentId: 2,
            company: '',
            bankCode: '',
            chequeNo: '',
            shopAmt: '',
            paymentTypeCode: 'CHQ',
            whtFlag: 'N',
        };
    }

    changeState() {
        this.btnState = '';
    }

    isOnlyNumber(event, data, index, type) {
        if(data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                let minus = 1;  
                let minusString = data.indexOf('-')
                if(minusString != -1) {
                    minus = -1;
                    data = data.replace(/-/g, '');
                }
                if (type == 'cash') {
                    this.statusTab1 = 'E';
                } else if (type == 'cheque') {
                    this.statusTab2 = 'E';
                } else if (type == 'wht') {
                    this.statusTab3 = 'E';
                }
                data = data.replace(/,/g, '');
                if((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    let preProcess = data ? this.setDecimal(parseFloat(data) * minus).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null;
                    preProcess = preProcess ? preProcess : null;
                    if (type == 'cash') {
                        this.objectData.cashcredit[index].shopAmt = preProcess;
                    } else if (type == 'cheque') {
                        this.objectData.cheque[index].shopAmt = preProcess;
                    } else if (type == 'wht') {
                        this.objectData.wht[index].shopAmt = preProcess;
                    } else if (type == 'formChq') {
                        this.form.cheque.shopAmt = preProcess;
                    } else if (type == 'formWht') {
                        this.form.wht.shopAmt = preProcess;
                    } else if (type == 'paymentWht') {
                        this.form.wht.paymentAmt = preProcess;
                    }
                }
            }
        }
    }

    setDecimal(number: number) {
        number = isNaN(number) ? 0 : number;
        if(number) {
            var digit = number.toString().indexOf('.');
            if(digit != -1) {
                number = parseFloat(number.toString().substr(0,(digit+3)));
            }
        }
        return number;
    }
}
