import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
    selector: 'app-update-adjust',
    templateUrl: './update-adjust.component.html',
    styleUrls: ['./update-adjust.component.css']
})
export class UpdateAdjustComponent implements OnInit, OnChanges {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    locId: string = this.rawMockLocationId[2];
    mockLocationId: string = this.rawMockLocationId[2];
    @Output() disClose = new EventEmitter<boolean>();
    //  @Output() changeAmount = new EventEmitter<number>();
    @Output() displayClose = new EventEmitter<boolean>();

    display: boolean = false;
    type: any = [];
    date: any;
    status: any = [];
    bank: any = [];
    merchantId: any = [];
    merchantIdFirst: any;
    merchantTypeFirst: any;
    cnFlag = '';
    filterData = [];
    dateVal = Date.now();
    adjustStatus: any;
    company: any;
    adjustType: Array<any> = [];
    detail: any;
    msg: any;
    ostDiff: any;
    adjustData: any;
    amount: string;
    key = '';
    paymentType: string;
    dataAdjustType: string = '';
    loading: boolean = true;
    search: boolean;
    checkData = 1;
    clickLoad = false;
    checkAmtFlag: any;
    checkAmtValue: any;
    newData = {
        company: null,
        status: null,
        amount: null,
        adjustType: null,
        paymentType: null,
        saleSlipDate: null,
        adjustNo: null,
        bankEdc: null,
        merchantId: null,
        last: null,
        taxId: null,
        chequeNo: '',
        customerName: '',
        ostDiff: '',
        note: null,
        diff: '',
        legacyAmount: '',
        adjustMasterId: null,
        paymentTypeCode: null,
        statusCode: null,
        edcBank: null,
        adjustDocId: null,
        docDate: null,
        whtFlag: null,
        shopEodDocId: null,
        shopEodDetId: null,
        last4Digits: null,
        outstandingDiffAmt: null,
        companyCode: null,
        adjustNote: null,
        refBankDoc: null,
        shopEodPaymentId: null,
        glAccount: null,
        glCs: null,
        glFs: null,
        glProduct: null,
        glProject: null,
        glRc: null,
        glRs: null,
        glSs: null
    }
    form = {
        amount: '',
        note: '',
        adjustNo: '',
        company: '',
        paymentType: '',
        paymentTypeCode: '',
        locationCode: '',
        adjustMasterId: '',
        status: '',
        adjustType: '',
        key: ''
    };
    @Input() data = {
        company: null,
        status: null,
        amount: null,
        adjustType: null,
        paymentType: null,
        saleSlipDate: null,
        adjustNo: null,
        bankEdc: null,
        merchantId: null,
        last: null,
        taxId: null,
        chequeNo: '',
        customerName: '',
        ostDiff: '',
        note: null,
        diff: '',
        legacyAmount: '',
        adjustMasterId: null,
        paymentTypeCode: null,
        statusCode: null,
        edcBank: null,
        adjustDocId: null,
        docDate: null,
        whtFlag: null,
        shopEodDocId: null,
        shopEodDetId: null,
        last4Digits: null,
        outstandingDiffAmt: null,
        companyCode: null,
        adjustNote: null,
        refBankDoc: null,
        shopEodPaymentId: null,
        refAdjustDocId: null,
        glAccount: null,
        glCs: null,
        glFs: null,
        glProduct: null,
        glProject: null,
        glRc: null,
        glRs: null,
        glSs: null
    };
    paginator: any = {
        pageCount: 0,
        rows: 3,
        data: [],
        offset: 0
    };
    locationList = []

    banks = [
        { bank: 'kbank' },
        { bank: 'scb' },
    ];
    constructor(private router: Router, private global: AppComponent, private endpoint: Endpoint, private HttpService: HttpService) {
    }



    checkAmtWithFlag(){
        this.msg = '';
        let amt = Number(this.amount.replace(/,/g, ''));
        if(this.checkAmtFlag == 'Y'){
            if(this.checkAmtValue < Math.abs(amt)){
                this.msg = 'overflow';
            } else {
               this.onSave();
            }
          }else{
            this.onSave();
          }
    }



    onSave() {
        let ost = Number(this.data.outstandingDiffAmt);
        let amt = Number(this.amount.replace(/,/g, ''));
       /*  if (Math.abs(ost) < Math.abs(amt)) {
            this.msg = 'overflow';
        } else { */
            if (this.newData.whtFlag == 'Y') {
                if (this.newData.adjustMasterId == null || this.amount == null || this.amount == '0.00'|| this.amount == '' || this.amount == undefined || this.data.adjustType == 'undefined') {
                    this.msg = 'fail';
                } else {
                    this.upDateData();
                }
            } else {
                if (this.data.paymentTypeCode == 'CSH') {
                    if (this.newData.adjustMasterId == null || this.amount == null || this.amount == '0.00' || this.amount == '' || this.amount == undefined || this.data.adjustType == 'undefined') {
                        this.msg = 'fail';
                    } else {
                        this.upDateData();
                    }
                } else if (this.data.paymentTypeCode == 'CHQ') {
                    if (this.newData.adjustMasterId == null || this.amount == null || this.amount == '0.00' || this.amount == '' || this.amount == undefined || this.data.adjustType == 'undefined') {
                        this.msg = 'fail';
                    } else {
                        this.upDateData();
                    }
                } else if (this.data.paymentTypeCode == 'CC') {
                    if (this.cnFlag == 'Y') {
                        if (this.newData.adjustMasterId == null ||
                            this.newData.edcBank == null ||
                            this.newData.merchantId == null ||
                            this.newData.saleSlipDate == '' ||
                            this.newData.saleSlipDate == undefined ||
                            this.newData.saleSlipDate == null ||
                            this.newData.last4Digits == null ||
                            this.newData.last4Digits.length != 4 ||
                            this.amount == null ||
                            this.amount == '0.00' || 
                            this.amount == '' ||
                            this.amount == undefined || 
                            this.data.adjustType == 'undefined') {
                            this.msg = 'fail';
                        } else {
                            this.upDateData();
                        }
                    } else {
                        if (this.newData.adjustMasterId == null ||
                            this.amount == null) {
                            this.msg = 'fail';
                        } else {
                            this.upDateData();
                        }
                    }
                }
            /* } */
        }
    }

    upDateData() {
        this.clickLoad = true;
        let ost = Number(this.newData.outstandingDiffAmt);
        let oldAmt = Number(this.data.amount)
        let amt = Number(this.amount.toString().replace(/,/g, ''));
            var date = this.newData.saleSlipDate ?  new Date(this.newData.saleSlipDate).getTime() : null;

            var headers = {
                'action': 'save',
                'lang': 'EN',
                'navigate': this.router.url,
            }

            var data = {
                adjUpdInfo: {
                    "adjustDocId": this.newData.adjustNo,
                    "adjustMasterId": this.newData.adjustMasterId,
                    "adjustStatus": this.newData.statusCode,
                    "adjustAmt": Number(this.amount.toString().replace(/,/g, '')),
                    "adjustNote": this.newData.adjustNote,
                    "mainDocId": this.newData.shopEodDocId,
                    "mainDocDate": this.newData.docDate,
                    "companyCode": this.newData.companyCode,
                    "locationCode": this.mockLocationId,
                    "paymentTypeCode": this.newData.paymentTypeCode,
                    "whtFlag": this.newData.whtFlag,
                    "chequeNo": this.newData.chequeNo,
                    "shopEodDetId": this.newData.shopEodDetId,
                    "edcBank": this.newData.edcBank,
                    "merchantId": this.newData.merchantId,
                    "saleSlipDate":  date,
                    "last4Digits": this.newData.last4Digits,
                    "outstandingDiffAmt": this.newData.outstandingDiffAmt,
                    "shopEodPaymentId": this.newData.shopEodPaymentId,
                    "glAccount": this.newData.glAccount,
                    "glCs": this.newData.glCs,
                    "glFs": this.newData.glFs,
                    "glProduct": this.newData.glProduct,
                    "glProject": this.newData.glProject,
                    "glRc": this.newData.glRc,
                    "glRs": this.newData.glRs ,
                    "glSs": this.newData.glSs 
                }
            }
            this.HttpService.call(this.endpoint.API_UPDATE_ADJ,'PUT', headers,null, null , data , this.upDateDataSuccessCallback.bind(this) ,this.upDateDataFailCallback.bind(this));
    }

    upDateDataSuccessCallback(err: any){
        this.global.checkTokenExpired(err.data.statusCode);
        if (err.data.statusCode == 'S0000') {
            this.msg = 'success';
            this.clickLoad = false;
        } else {
            this.msg = 'fail';
            this.msg = err.data.statusMessage
        }
    }


    upDateDataFailCallback(err: any){
        this.clickLoad = false;
        if (err.response.data.statusCode == 'B0000') {
            this.msg = 'overflow';
        }
        this.msg = 'fail';
    }


    showDialog() {
        this.display = true;
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.loading = true
            this.onSearch(event.page + 1, 'paging');
        }
    }

    clear() {
        this.form = {
            amount: '',
            note: '',
            adjustNo: '',
            company: '',
            paymentType: '',
            paymentTypeCode: '',
            locationCode: '',
            adjustMasterId: '',
            status: '',
            adjustType: '',
            key: ''
        };
        this.filterData = [];
    }

    onCancel(agreed: boolean) {
        this.displayClose.emit(agreed);
    }

    add(data) {
        this.display = false;
        this.data.refBankDoc = data.adjustNo;
        this.data.adjustMasterId = data.adjustMasterId;
        this.form.key = data.key;
        this.data.statusCode = 'Resolved';
    }

    ngOnInit() {
        this.data.saleSlipDate = this.data.saleSlipDate ? new Date(this.data.saleSlipDate) : null;
        this.setData();
        this.dropDown();
        this.amount = Math.abs(this.setDecimal(parseFloat(this.data.amount))).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.ostDiff = '';
    }

    ngOnChanges() {
        this.data.saleSlipDate = this.data.saleSlipDate ?  new Date(this.data.saleSlipDate) : null; 
        this.setData();
        this.dropDown();
        this.amount = Math.abs(this.setDecimal(parseFloat(this.data.amount))).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.ostDiff = '';
    }

    onSearch(page, click) {
        if (click == 'paging') {
            this.checkData = 1;
        } else { this.checkData = 0 }
        this.search = true;
        let config = {
            headers: {
                'action': 'search',
                'lang': 'EN',
                'navigate': this.router.url,
            }
        }

        var  params = {
            currentPage: page ? page : null,
            paymentTypeCode: this.form.paymentTypeCode ? this.form.paymentTypeCode : null,
            adjustMasterId: this.form.adjustMasterId ? this.form.adjustMasterId : null,
            locationCode:this.form.locationCode ? this.form.locationCode : null,
            companyCode: this.form.company ? this.form.company : null,
            adjustStatus: this.form.status ? this.form.status : null,
            adjustAmt: this.form.amount ? this.form.amount : null,
            adjustDocType: 'SH',
        }

        this.HttpService.call(this.endpoint.API_SRCH_ADJ,'GET', config.headers,null, params , null , this.onSearchSuccessCallback.bind(this) ,null);
    }

    onSearchSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.filterData = snapData.data.data.adjSrchRespList.map(snap => {
                this.loading = false;
                Object.keys(this.adjustType).forEach(keys => {
                    if (snap.adjustMasterId == this.adjustType[keys].adjustMasterId) {
                        this.dataAdjustType = this.adjustType[keys].adjustType;
                    }
                });
                return {
                    adjustNo: snap.adjustDocId,
                    company: snap.companyCode,
                    locationCode: snap.locationCode,
                    adjustType: this.dataAdjustType,
                    paymentType: snap.paymentTypeCode === 'CSH' ? 'Cash' : snap.paymentTypeCode === 'CHQ' ? 'Cheque' : snap.paymentTypeCode === 'CC' ? 'Credit Card' : '',
                    status: this.global.setStatus(snap.adjustStatus),
                    amount: snap.adjustAmt ? snap.adjustAmt : 0,
                    merchantId: snap.merchantId,
                    edcBank: snap.edcBank,
                    saleSlipDate: snap.saleSlipDate,
                    adjustMasterId: snap.adjustMasterId,
                    adjustNote: snap.adjustNote,

                }

            });
            if (snapData.data.data.pagingInfo.currentPage == 0) {
                this.checkData = 0;
                this.loading = false
            } else {
                this.checkData = 1;
            }
            this.paginator = snapData.data.data.pagingInfo;


        }
    }

    async dropDown() {
         // location
            let preList = JSON.parse(localStorage.getItem('locationPermiss'));
            preList.map(key => {
                        this.locationList.push({
                            code: key.code,
                            name: key.name
                        }) 
                });
        /* }); */
        // adjustType dropdown
        var headersAdjustType = {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_LOAD_ADJ_TYPE,'GET', headersAdjustType,null, null , null , this.adjustTypeSuccessCallback.bind(this) ,null);

        // adjustStatus dropdown

        var headersAdjustStatus = {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_LOAD_ADJ_STTS,'GET', headersAdjustStatus,null, null , null , this.adjustStatusSuccessCallback.bind(this) ,null);

        // company dropdown
        var headersCompany = {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.endpoint.API_LOAD_COMP,'GET', headersCompany,null, null , null , this.companySuccessCallback.bind(this) ,null);


        //bank
        var headersBank = {
            'action': 'load-bank',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var paramsBank = {
            companyCode: this.newData.companyCode,
            locationCode: this.mockLocationId
        }
        this.HttpService.call(this.endpoint.API_LOAD_BANK_MERCHANT,'GET', headersBank,null, paramsBank , null , this.bankSuccessCallback.bind(this) ,null);

        //merchant one
        this.merchantId = [];
        //  this.merchantIdFirst = '';
        //  this.merchantTypeFirst = '';
        let vm = this;
         var headersMerchant = {
            'action': 'load-merchant',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        var paramsMerchant = {
            bankName: this.newData.edcBank,
            companyCode: this.newData.companyCode,
            locationCode: this.mockLocationId
        }
     await this.HttpService.call(this.endpoint.API_LOAD_BANK_MERCHANT,'GET', headersMerchant,null, paramsMerchant , null , this.merChantSuccessCallback.bind(this) ,null);
    }


    adjustTypeSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.adjustType = snapData.data.data.adjMastInfoList.map(snap => {
                if(this.newData.adjustMasterId == snap.adjustMasterId){
                    this.cnFlag = snap.cnFlag;
                }
                if(this.newData.adjustMasterId == snap.adjustMasterId){
                    this.checkAmtFlag = snap.checkAmtFlag;
                    this.checkAmtValue = snap.checkAmtValue;
                }
                return {
                    adjustType: snap.adjustType,
                    adjustDocType: snap.adjustDocType,
                    adjustMasterId: snap.adjustMasterId,
                    adjustCnFlag: snap.cnFlag,
                    defaultStatus: snap.defaultStatus,
                    checkAmtFlag: snap.checkAmtFlag,
                    checkAmtValue: snap.checkAmtValue,
                    glAccount: snap.glAccount,
                    glCs: snap.glCs,
                    glFs: snap.glFs,
                    glProduct: snap.glProduct,
                    glProject: snap.glProject,
                    glRc: snap.glRc,
                    glRs: snap.glRs,
                    glSs: snap.glSs
                };
            });
            if(this.adjustType.length) {
              
            }
        }
    }

    adjustStatusSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.adjustStatus = snapData.data.data.adjSttsInfoList.map(snap => {
                return {
                    adjustStatusCode: snap.adjustStatusCode,
                    adjustStatusDesc: snap.adjustStatusDesc
                };
            });
        }
    }

    companySuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.company = snapData.data.data.companyInfoList.map(snap => {
                return {
                    companyCode: snap.companyCode,
                };
            });
        }
    }

    bankSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.bank = snapData.data.data.locMrchntInfoList.map(snap => {
                return {
                    bankName: snap.bankName,
                };
            });
        }
    }

    merChantSuccessCallback(snapData: any){
        let vm = this;
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.merchantId = snapData.data.data.locMrchntInfoList.map(snap => {
                if (this.newData.merchantId == snap.merchantId) {
                 /*    this.merchantIdFirst = snap.merchantId,
                    this.merchantTypeFirst = snap.merchantType */
                    vm.newData.merchantId = snap.merchantId;
                }
                return {
                    merchantId: snap.merchantId,
                    merchantType: snap.merchantType
                };
              /* this.merchantId.push( {
                    merchantId: snap.merchantId,
                    merchantType: snap.merchantType
                }) */
             
            });
        }
    }

    setData() {
        this.newData = {
            company: this.data.company,
            status: this.data.status,
            amount: this.data.amount,
            adjustType: this.data.adjustType,
            paymentType: this.data.paymentType,
            saleSlipDate: this.data.saleSlipDate,
            adjustNo: this.data.adjustNo,
            bankEdc: this.data.bankEdc,
            merchantId: this.data.merchantId,
            last: this.data.last,
            taxId: this.data.taxId,
            chequeNo: this.data.chequeNo,
            customerName: this.data.customerName,
            ostDiff: this.data.ostDiff,
            note: this.data.note,
            diff: this.data.diff,
            legacyAmount: this.data.legacyAmount,
            adjustMasterId: this.data.adjustMasterId,
            paymentTypeCode: this.data.paymentTypeCode,
            statusCode: this.data.statusCode,
            edcBank: this.data.edcBank,
            adjustDocId: this.data.adjustDocId,
            docDate: this.data.docDate,
            whtFlag: this.data.whtFlag,
            shopEodDocId: this.data.shopEodDocId,
            shopEodDetId: this.data.shopEodDetId,
            last4Digits: this.data.last4Digits,
            outstandingDiffAmt: this.data.outstandingDiffAmt,
            companyCode: this.data.companyCode,
            adjustNote: this.data.adjustNote,
            refBankDoc: this.data.refBankDoc,
            shopEodPaymentId: this.data.shopEodPaymentId,
            glAccount: this.data.glAccount,
            glCs: this.data.glCs,
            glFs: this.data.glFs,
            glProduct: this.data.glProduct,
            glProject: this.data.glProject,
            glRc: this.data.glRc,
            glRs: this.data.glRs,
            glSs: this.data.glSs
        }
    }

    loadMerchant() {
        this.newData.merchantId = null;
        //load merchant
        var headers = {
            'action': 'load-merchant',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var params = {
            bankName: this.newData.edcBank,
            companyCode: this.newData.companyCode,
            locationCode: this.mockLocationId
        }
        this.HttpService.call(this.endpoint.API_LOAD_BANK_MERCHANT,'GET', headers,null, params , null , this.loadMerChantSuccessCallback.bind(this) ,null);
    }
    
    loadMerChantSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.merchantId = snapData.data.data.locMrchntInfoList.map(snap => {
                /* this.merchantIdFirst = snap.merchantId,
                this.merchantTypeFirst = snap.merchantType */
                if(this.newData.merchantId == null){
                    this.newData.merchantId = snap.merchantId
                }
                return {
                    merchantId: snap.merchantId,
                    merchantType: snap.merchantType
                };
            });
        }
    }
    setCnFlag() {
        this.cnFlag = 'N';
        this.checkAmtValue = null;
        this.checkAmtFlag = null;
        this.adjustType.map(data => {
            if(this.newData.adjustMasterId == data.adjustMasterId && data.adjustCnFlag) {
                this.cnFlag = data.adjustCnFlag;
            }
            if(this.newData.adjustMasterId == data.adjustMasterId){
                this.checkAmtFlag = data.checkAmtFlag;
                this.checkAmtValue = data.checkAmtValue;
            }
        })
        if(this.cnFlag == 'N') {
            this.newData.edcBank = null;
            this.newData.merchantId = this.merchantIdFirst;
            this.newData.saleSlipDate = null;
            this.newData.last4Digits = null;
        } else if (this.cnFlag == 'Y'){
            this.newData.edcBank = this.data.edcBank;
            this.newData.merchantId = this.data.merchantId;
            this.newData.saleSlipDate = this.data.saleSlipDate;
            this.newData.last4Digits = this.data.last4Digits;
        }
        this.setGlAcc();
        this.setStatus();
    }

    setStatus() {
        this.adjustType.map(snap => {
            if (this.newData.adjustMasterId == snap.adjustMasterId) {
                this.newData.statusCode = snap.defaultStatus;
            }
        }); 
    }

    setGlAcc() {
        this.newData.glAccount = null,
        this.newData.glCs = null,
        this.newData.glFs = null,
        this.newData.glProduct = null,
        this.newData.glProject = null,
        this.newData.glRc = null,
        this.newData.glRs = null,
        this.newData.glSs = null
      
            this.adjustType.map(snap => {
                if (this.newData.adjustMasterId == snap.adjustMasterId) {
                        this.newData.glAccount = snap.glAccount,
                        this.newData.glCs = snap.glCs,
                        this.newData.glFs = snap.glFs,
                        this.newData.glProduct = snap.glProduct,
                        this.newData.glProject = snap.glProject,
                        this.newData.glRc = snap.glRc,
                        this.newData.glRs = snap.glRs,
                        this.newData.glSs = snap.glSs
                }

            })
    }

    isOnlyNumber(event, data, type) {
        if(data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                data = data.replace(/,/g, '');
                if((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    if(type == 'amount') {
                        this.amount = Math.abs(this.setDecimal(parseFloat(data))).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                    } else if (type == 'tranDate') {
                        this.newData.last4Digits = isNaN(Math.abs(data)) ? null : Math.abs(data) != 0 ? Math.abs(data).toString() : null ;
                    }
                }
            }
        }
    }

    setDecimal(number: number) {
        number = isNaN(number) ? 0 : number;
        if(number) {
            var digit = number.toString().indexOf('.');
            if(digit != -1) {
                number = parseFloat(number.toString().substr(0,(digit+3)));
            }
        }
        return number;
    }
}
