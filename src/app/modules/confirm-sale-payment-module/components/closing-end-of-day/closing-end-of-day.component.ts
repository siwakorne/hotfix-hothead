import { Component, OnInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Endpoint } from '../../../../common/const/endpoint';
import { ConfirmationService } from 'primeng/api';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
    selector: 'app-closing-end-of-day',
    templateUrl: './closing-end-of-day.component.html',
    styleUrls: ['./closing-end-of-day.component.css'],
    providers: [ConfirmationService]
})
export class ClosingEndOfDayComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    locationCode: string = this.rawMockLocationId[2];
    loading = true;
    errorCode = false;
    adjustdata: any;
    display: boolean = false;
    displaylist: boolean = false;
    closingData: any = [];
    closingId: number = 0;
    datashop: any;
    dataEod: any;
    dataList: any;
    box: number = 0;
    preCheque: object = [];
    preWht: object = [];
    preCCDtn: object = [];
    preCCDtac: object = [];
    preCash: object = [];
    DTNData: Array<object> = [];
    DTACData: Array<object> = [];
    DTNWhtData: Array<object> = [];
    DTACWhtData: Array<object> = [];
    PAYSData: Array<object> = [];
    closeEod: string;
    legacyData: any;
    P: any;
    company: any;
    dataModal: any = {
        paymentTypeCode: '',
        whtFlag: '',
        companyCode: '',
        cbs: '',
        dicovery: '',
        kiosk: '',
        pos: ''
    };
    listDataModal: any = [];
    adjDataModalObj: Array<any> = [];
    adjDataModal: Array<object> = [];
    adjCheque: any = [];
    modalType: string = 'N';
    cheque: Array<object> = [];
    whtData: Array<object> = [];
    allOstDiff: number = -1;
    adjAmount: number = 0;
    closed = '';
    mockLocationId: string = this.rawMockLocationId[2];
    constructor(private router: Router, private Endpoint: Endpoint, private confirmationService: ConfirmationService, private global:AppComponent, private HttpService: HttpService) {

    }
    async ngOnInit() {
        this.loading = true;
        await this.queryData();
    }

    async ngOnChanges() {
        this.loading = true;
        await this.queryData();
    }

    onOk() {
        this.router.navigate(['/']);
    }

    goComfirm() {
        this.router.navigate(['/confirmsale']);
    }

    queryData() {
        var headers = {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        this.HttpService.call(this.Endpoint.API_LOAD_CLOSING,'GET', headers,null, null , null , this.querySuccessCallback.bind(this) ,this.queryFailCallback.bind(this));
    }



    querySuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        this.closingData = snapData.data.data;
        this.closingData.eodDate = snapData.data.data.eodDate;
        this.closed = snapData.data.data.eodDate ? 'closed' : '';
        this.allOstDiff = 0;            
        this.closingData.closingEodInfoList.map(list => {
            if (list.whtFlag === 'N') {
                this.PAYSData = [];
                this.DTNData = [];
                this.DTACData = [];
                list.paymentEodInfoList.filter(data => {
                    this.closingId = data.shopEodDocId;
                    if(data.companyCode === 'DTAC' && (data.paymentTypeCode == 'CSH' || data.paymentTypeCode == 'CC' || data.paymentTypeCode == 'CHQ')) {
                        let preData = data;
                        preData = Object.assign({}, preData, {
                            whtFlag: list.whtFlag,
                           /*  docDate: this.closingData.docDate */
                            docDate: snapData.data.data.docDate
                        });
                        this.DTACData.push(preData);
                    } else if (data.companyCode === 'DTN' && (data.paymentTypeCode == 'CSH' || data.paymentTypeCode == 'CC' || data.paymentTypeCode == 'CHQ')) {
                        let preData = data;
                        preData = Object.assign({}, preData, {
                            whtFlag: list.whtFlag,
                            /*   docDate: this.closingData.docDate */
                            docDate: snapData.data.data.docDate
                        });
                        this.DTNData.push(preData);
                    }
                    // else if (data.companyCode === 'PAYSBUY' && data.paymentTypeCode == 'CSH') {
                    //     let preData = data;
                    //     preData = Object.assign({}, preData, {
                    //         whtFlag: list.whtFlag,
                    //       /*   docDate: this.closingData.docDate */
                    //         docDate: snapData.data.data.docDate
                    //     });
                    //     this.PAYSData.push(preData);
                    // }
                    if(data.checkDiffAmt) {
                        this.allOstDiff += Math.abs(data.checkDiffAmt);
                    } else {
                        this.allOstDiff += Math.abs(data.outstandingDiffAmt);
                    }
                });
            } else {
                this.DTACWhtData = [];
                this.DTNWhtData = [];
                list.paymentEodInfoList.filter(data => {
                    this.closingId = data.shopEodDocId;
                    let preData = {};
                    preData = Object.assign({}, data, {
                        whtFlag: list.whtFlag,
                        /*  docDate: this.closingData.docDate */
                        docDate: snapData.data.data.docDate
                    });
                    if (data.companyCode === 'DTAC' && (data.paymentTypeCode == 'CSH' || data.paymentTypeCode == 'CC' || data.paymentTypeCode == 'CHQ')) {
                        let preData = data;
                        preData = Object.assign({}, preData, {
                            whtFlag: list.whtFlag,
                            /*   docDate: this.closingData.docDate */
                            docDate: snapData.data.data.docDate
                        });
                        this.DTACWhtData.push(preData);
                    } else if (data.companyCode === 'DTN' && (data.paymentTypeCode == 'CSH' || data.paymentTypeCode == 'CC' || data.paymentTypeCode == 'CHQ')) {
                        let preData = data;
                        preData = Object.assign({}, preData, {
                            whtFlag: list.whtFlag,
                            /*    docDate: this.closingData.docDate */
                            docDate: snapData.data.data.docDate
                        });
                        this.DTNWhtData.push(preData);
                    }
                    if(data.checkDiffAmt) {
                        this.allOstDiff += Math.abs(data.checkDiffAmt);                            
                    } else {
                        this.allOstDiff += Math.abs(data.outstandingDiffAmt);
                    }
                })
            }
        })
        this.loading = false;
    }


    queryFailCallback(err: any){
        this.closed = 'closed'
        this.loading = false;
        if(err.response.data.statusCode == 'B0006'){
            this.router.navigate(['confirm-sale-payment/form-sale']);
        }
    }

    showLegacy(legacyData, P, company) {
        this.legacyData = legacyData;
        this.P = P;
        this.company = company;


        var headers = {
            "action": "load-detail",
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var params = {
            docDate: legacyData.docDate,
            whtFlag: legacyData.whtFlag,
            paymentTypeCode: legacyData.paymentTypeCode,
            companyCode: legacyData.companyCode
        }
        this.HttpService.call(this.Endpoint.API_LOAD_CLOSING+'/legacy-detail','GET', headers,null, params , null , this.showLegacySuccessCallback.bind(this) ,null);
    }


    showLegacySuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        this.modalType = 'N';
        this.display = true;
        this.box = 2;
        this.dataModal = [];
        this.listDataModal = [];
        this.dataModal.paymentTypeCode = this.legacyData.paymentTypeCode;
        this.dataModal.companyCode = this.company;
        this.dataModal.whtFlag = this.P;
        let data = snapData.data.data.legacyDetailEodInfoList;
        data.map(data => {
            if (data.legacyName === 'CBS') {
                this.dataModal.cbs = data.shopAmt
            } else if (data.legacyName === 'POS') {
                this.dataModal.pos = data.shopAmt
            } else if (data.legacyName === 'KIOSK') {
                this.dataModal.kiosk = data.shopAmt
            } else if (data.legacyName === 'DIS') {
                this.dataModal.discovery = data.shopAmt
            }
        });
    }

    adjust(adjustdata, type) {
        this.adjustdata = adjustdata;
        this.box = 0;
        this.datashop = '';
        if ((adjustdata.paymentTypeCode === "CC" && type !== 'W') || (adjustdata.paymentTypeCode === "CSH" && type !== 'W')) {
            this.datashop = adjustdata;
            this.box = 1;
            this.display = true;
        } else if (adjustdata.paymentTypeCode === "CHQ" && type !== 'W') {
            let vm = this;

            var headers = {
                'action': 'cheque-detail',
                'lang': 'EN',
                'navigate': this.router.url,
            }

            var  params = {
                docDate: adjustdata.docDate,
                whtFlag: adjustdata.whtFlag,
                paymentTypeCode: adjustdata.paymentTypeCode,
                companyCode: adjustdata.companyCode,
            }
            this.HttpService.call(this.Endpoint.API_CHEQUE_DETAIL,'GET', headers,null, params , null , this.adjustCHQSuccessCallback.bind(this) ,null);
            this.displaylist = true;
            this.box = 3;
        } else {
            let vm = this;
            var headerscsh = {
                'action': 'cheque-detail',
                'lang': 'EN',
                'navigate': this.router.url,
            }

            var paramscsh = {
                docDate: adjustdata.docDate,
                whtFlag: adjustdata.whtFlag,
                paymentTypeCode: adjustdata.paymentTypeCode,
                companyCode: adjustdata.companyCode,
            }
            this.HttpService.call(this.Endpoint.API_CHEQUE_DETAIL,'GET', headerscsh,null, paramscsh , null , this.adjustCSHSuccessCallback.bind(this) ,null);
            this.display = true;
            this.box = 4;
        }
        adjustdata = [];
    }
    adjustCHQSuccessCallback(snapData: any){
        var vm = this;
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.adjDataModal = snapData.data.data.closingAdjustDetail.map(snap => {
                let preData = snap;
                preData = Object.assign({}, preData, {
                    whtFlag: vm.adjustdata.whtFlag,
                    docDate: vm.adjustdata.docDate
                });
                return preData;
            });
        }
    }

    adjustCSHSuccessCallback(snapData: any){
        var vm = this;
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.adjDataModalObj = snapData.data.data.closingAdjustDetail.map(snap => {
                let preData = snap;
                preData = Object.assign({}, preData, {
                    whtFlag: vm.adjustdata.whtFlag,
                    docDate: vm.adjustdata.docDate,
                });
                return preData;

            });
        }
    }

    detailList(data) {
        this.datashop = data;
        this.display = true;
        this.box = 1;
    }

    adjustList(adjustDatas) {
        this.dataList = '';
        this.box = 5;
        this.dataList = adjustDatas
        this.display = true;
    }

    onClose(agreed: boolean) {
        agreed ? this.display = false : this.display = true;
        this.box = 0;
    }

    decrease(amount) {
        this.clear();
        this.queryData();
    }

    updateAmount(amount) {
        this.clear();
        this.queryData();
    }

    clear() {
        this.DTNData = [];
        this.DTACData = [];
        this.DTNWhtData = [];
        this.DTACWhtData = [];
    }

    submitClosing() {
        this.confirmationService.confirm({
            message: 'Do you want to closing end of day ?',
            accept: () => {
                let config = {
                    headers: {
                        'action': 'close',
                        'lang': 'EN',
                        'navigate': this.router.url,
                    }
                }

                this.HttpService.call(this.Endpoint.API_LOAD_CLOSING,'PUT', config.headers, null, null, {shopEodDocId: this.closingId}, this.submitClosingSuccessCallback.bind(this) ,this.submitClosingFailCallback.bind(this));
            },
            reject: () => {

            }
        });
    }

    submitClosingSuccessCallback(data: any){
        this.errorCode = false;
        this.display = true;
        this.box = 99;
    }

    submitClosingFailCallback(err: any){
        this.display = true;
        this.box = 99;
        this.errorCode = true;
    }

    closing() {
        this.loading = true;
        this.clear();
        this.queryData();
    }
}
