import { Component, OnInit, Input, Output, EventEmitter, OnChanges, Sanitizer } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { v4 as uuid } from 'uuid';
import { Endpoint } from '../../../../common/const/endpoint';
import { AppComponent } from '../../../../app.component';
import { HttpService } from '../../../../common/util/http-service';

@Component({
    selector: 'app-adjust-list',
    templateUrl: './adjust-list.component.html',
    styleUrls: ['./adjust-list.component.css']
})
export class AdjustListComponent implements OnInit, OnChanges {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));

    mockLocationId: string = this.rawMockLocationId[2];
    @Output() increaseAmount = new EventEmitter<number>();
    @Output() updateAmount = new EventEmitter<number>();
    display: boolean = false;
    box: number = 0;
    filterData: Array<any> = [];
    filterPagination: Array<object> = [];
    adjustData: Array<any> = [];
    key = null;
    msg = '';
    msgUd = '';
    dataEod: any;
    adjustTypes = [];
    rawAdjustType: string = '';
    editAdjust = [];
    dataDelete: any;
    data: any;
    listData: {
        company: string,
        paymentType: string,
        adjustNo: string,
        adjustType: string,
        status: string,
        amount: number,
        taxId: string,
        saleSlipDate: string,
        customerName: string,
        chequeNo: string,
        last: string;
        legacyAmount: string;
        diff: string;
    } = {
            company: '',
            paymentType: '',
            adjustNo: '',
            adjustType: '',
            status: '',
            amount: 0,
            taxId: '',
            saleSlipDate: '',
            customerName: '',
            chequeNo: '',
            last: '',
            legacyAmount: '',
            diff: ''
        };
    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };
    noData = '1'
    form: any = {
        pagingInfo: {
            currentPage: 1
        },
    };

    @Input() dataList = {
        companyCode: null,
        paymentTypeCode: null,
        docDate: null,
        whtFlag: null,
        shopEodDocId: null,
        outstandingDiffAmt: null,
        shopEodDetId: null,
        adjustAmt: null
    };

    constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent, private HttpService: HttpService) { }

    ngOnInit() {

    }

    async ngOnChanges() {
        this.filterData = [];
        this.paginator = [];
        this.noData = '0';
        this.querydata(1, '')
    }

    paginate(event) {
        this.form.pagingInfo.currentPage = (event.page + 1);
        this.querydata(event.page + 1, '');
    }

    async loadAdjustType() {
        var headersAdjustType = {
            'action': 'load',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        await this.HttpService.call(this.endpoint.API_LOAD_ADJ_TYPE,'GET', headersAdjustType,null, null , null , this.adjustTypeDataSuccessCallback.bind(this) ,null);
    }

    adjustTypeDataSuccessCallback(snapData: any) {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.adjustTypes = snapData.data.data.adjMastInfoList.map(snap => {
                return {
                    adjustType: snap.adjustType,
                    adjustMasterId: snap.adjustMasterId
                };
            });
        }
    }


    async querydata(page, type) {
        await this.loadAdjustType();
        if ((this.paginator.currentPage != this.form.pagingInfo.currentPage) || type == 'reload') {
            let date = moment(this.dataList.docDate).add(7, 'hours').format('x');

            var headers = {
                'action': 'search',
                'lang': 'EN',
                'navigate': this.router.url,
            }

            var params = {
                currentPage: page,
                companyCode: this.dataList.companyCode,
                paymentTypeCode: this.dataList.paymentTypeCode,
                mainDocDate: this.dataList.docDate,
                whtFlag: this.dataList.whtFlag,
                locationCode: this.mockLocationId,
                adjustDocType: 'SH'
            }
            await this.HttpService.call(this.endpoint.API_SRCH_ADJ,'GET', headers,null, params , null , this.queryDataSuccessCallback.bind(this) ,null);
        }
    }


    queryDataSuccessCallback(snapData: any){
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.filterData = snapData.data.data.adjSrchRespList.map(snap => {
                Object.keys(this.adjustTypes).map(key => {
                    if (this.adjustTypes[key].adjustMasterId == snap.adjustMasterId) {
                        this.rawAdjustType = this.adjustTypes[key].adjustType;
                    }
                })
                return {
                    adjustNo: snap.adjustDocId,
                    companyCode: snap.companyCode,
                    locationCode: snap.locationCode,
                    adjustType: this.rawAdjustType,
                    refAdjustDocId: snap.refAdjustDocId,
                    // adjustNote: snap.adjustNote,
                    paymentType: snap.paymentTypeCode == 'CSH' ? 'Cash' : snap.paymentTypeCode == 'CHQ' ? 'Cheque' : snap.paymentTypeCode == 'CC' ? 'Credit Card' : '',
                    status: this.global.setStatus(snap.adjustStatus),
                    amount: snap.adjustAmt ? snap.adjustAmt : 0,
                    adjustMasterId: snap.adjustMasterId,
                    statusCode: snap.adjustStatus,
                    edcBank: snap.edcBank != undefined ? snap.edcBank : null,
                    merchantId: snap.merchantId != undefined ? snap.merchantId : null,
                    saleSlipDate: snap.saleSlipDate != undefined || snap.saleSlipDate !== "Invalid date" ? snap.saleSlipDate : null,
                    adjustDocId: snap.adjustDocId,
                    docDate: this.dataList.docDate,
                    whtFlag: this.dataList.whtFlag,
                    paymentTypeCode: snap.paymentTypeCode,
                    shopEodDocId: this.dataList.shopEodDocId,
                    last4Digits: snap.last4Digits != undefined ? snap.last4Digits : null,
                    outstandingDiffAmt: snap.outstandingDiffAmt != undefined ? snap.outstandingDiffAmt : this.dataList.outstandingDiffAmt,
                    shopEodDetId: snap.shopEodDetId,
                    refBankDoc: snap.refBankDoc,
                    shopEodPaymentId: snap.shopEodPaymentId,
                    diffAmt: snap.diffAmt,
                    adjustAmt: snap.adjustAmt,
                    adjustNote: snap.adjustNote,
                    glAccount: snap.glAccount,
                    glCs: snap.glCs,
                    glFs: snap.glFs,
                    glProduct: snap.glProduct,
                    glProject: snap.glProject,
                    glRc: snap.glRc,
                    glRs: snap.glRs,
                    glSs: snap.glSs
                }
            });
            this.paginator = snapData.data.data.pagingInfo;
            if (!this.filterData.length) {
                this.noData = '1';
            }
        }
    }
    view(data) {
        this.listData = data;
        this.box = 2;
        this.display = true;
    }

    edit(data) {

        this.data = data;
        this.msgUd = '';
         
        var  headers = {
            'action': 'valid-upd-ref',
            'lang': 'EN',
            'navigate': this.router.url,
        }

        var datas = {
            adjUpdInfo: {
                adjustDocId: data.adjustDocId
            }
        }
        this.HttpService.call(this.endpoint.API_ADD_ADJ,'POST', headers,null, null , datas , this.editSuccessCallback.bind(this) ,null);
    }

    editSuccessCallback(snap: any){
        if (snap.data.data.adjustRefFlag == true) {
            this.editAdjust = this.data
            this.box = 3;
            this.display = true;
            
        }else{
            this.box = 6;
            this.display = true;
            this.msgUd = 'false';
        }
    }

    addData(data) {
        this.dataDelete = data;
        this.box = 4;
        this.display = true;
    }

    delete() {
        this.msg = '';
        var  headers = {
            'action': 'remove',
            'lang': 'EN',
            'navigate': this.router.url,
        }
        var  datas = {
            "adjUpdInfo": {
                "adjustDocId": this.dataDelete.adjustNo,
                "whtFlag": this.dataDelete.whtFlag,
                "shopEodPaymentId": this.dataDelete.shopEodPaymentId,
                "shopEodDetId": this.dataDelete.shopEodDetId,
                "paymentTypeCode": this.dataDelete.paymentTypeCode,
                "adjustAmt": this.dataDelete.adjustAmt,
                "outstandingDiffAmt": this.dataDelete.outstandingDiffAmt,
                "refAdjustDocId": this.dataDelete.refAdjustDocId ? this.dataDelete.refAdjustDocId : undefined,
                "adjustStatus": "1"
            }
        }
        this.HttpService.call(this.endpoint.API_DELETE_ADJ,'DELETE', headers,null, null , datas , this.deleteSuccessCallback.bind(this) ,this.deleteFailCallback.bind(this));
    }

    deleteSuccessCallback(snap: any){
        this.global.checkTokenExpired(snap.data.statusCode);
        if(snap.data.statusCode == 'S0000'){
            this.querydata(this.paginator.currentPage, 'reload');
            if(snap.data.data.affects == 0) {
                this.msg = 'fail';
            } else {
                this.msg = '';
                this.display = false
            }
            this.updateAmount.emit();
        }
    }

    deleteFailCallback(error: any){
        console.error(error.response);
        this.msg = 'fail';
    }

    changeAmount(amount) {
        this.updateAmount.emit(amount);
    }

    onClose(agreed: boolean) {
        this.updateAmount.emit();
        this.querydata(this.form.pagingInfo.currentPage, 'reload');
        agreed ? this.display = false : this.display = true;

    }

    cancel() {
        this.querydata(this.form.pagingInfo.currentPage, 'reload');
        this.box = null;
        this.display = false;
    }


}
