import { Component, OnInit } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { Router } from '@angular/router';

@Component({
    selector: 'app-confirm-sale-payment',
    templateUrl: './confirm-sale-payment.component.html',
    styleUrls: ['./confirm-sale-payment.component.css']
})

export class ConfirmSalePaymentComponent implements OnInit {
    constructor(private router: Router) { }

    ngOnInit() {
        this.checkAuth();
    }

    checkAuth() {
        if(!JSON.parse(localStorage.getItem('loginData'))) {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        } else if (JSON.parse(localStorage.getItem('loginData')) && JSON.parse(localStorage.getItem('loginData')).length < 4) {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        } 
    }
}
