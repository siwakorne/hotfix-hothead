import { environment } from '../../../environments/environment'
export class Endpoint {

    private buildUrl = (path) => {
        return `${environment.apiUrl}/${path}`;
    };

    readonly HOSTNAME: string = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
    readonly PAGE_NOT_FOUND: string = this.HOSTNAME + '/40x.html';
    readonly ACCESS_DENIED: string = this.HOSTNAME + '/access-denied.html';
    readonly ERROR_OCCURRED: string = this.HOSTNAME + '/error-occurred.html';
    // ++++ REAL API ++++

    // ++ Confirm Page ++
    readonly API_MEARG: string = this.buildUrl('load/merge-confirm');
    readonly API_COMFRM_CASH: string = this.buildUrl('cashcreditcard');
    readonly API_COMFRM_CHEQUE: string = this.buildUrl('confirm-cheque');
    readonly API_COMFRM_WHT: string = this.buildUrl('withholdingtax');

    // ++ Closing Page ++
    readonly API_LOAD_CLOSING: string = this.buildUrl('closing-eod');

    // ++ AR ++
    readonly API_LOAD_AR_DETAIL: string = this.buildUrl('ar');
    readonly API_LOAD_AR_COMPANY: string = this.buildUrl('ar/reconcile-comp');
    readonly API_POST_AR_ADJUST: string = this.buildUrl('ar/adjust');

    readonly API_LOAD_BANK_SLA: string = this.buildUrl('load/bank-sla')
    readonly API_LOAD_COMP: string = this.buildUrl('load/company');
    readonly API_LOAD_ADJ_TYPE: string = this.buildUrl('load/adjust-type');
    readonly API_LOAD_ADJ_STTS: string = this.buildUrl('load/adjust-status');
    readonly API_SRCH_ADJ: string = this.buildUrl('adjust');
    readonly API_CHEQUE_DETAIL: string = this.buildUrl('closing-eod/adjust-detail');
    readonly API_ADD_ADJ: string = this.buildUrl('adjust');
    readonly API_VIEW_ADJ: string = this.buildUrl('adjust');
    readonly API_UPDATE_ADJ: string = this.buildUrl('adjust');
    readonly API_DELETE_ADJ: string = this.buildUrl('adjust');
    readonly API_LOAD_EODID: string = this.buildUrl('shop/eod-det-id');
    readonly API_LOAD_DOC_TYPE: string = this.buildUrl('load/doc-type');

    //--print--
    readonly API_PRINT_SLIP: string = this.buildUrl('payin-slip-pdf/print');
    readonly API_PRINT_SLIP_load: string = this.buildUrl('payin-slip-pdf/print');
    readonly API_CANCLE_SLIP: string = this.buildUrl('payin-slip/cancel');
    readonly API_SEARCH_SLIP: string = this.buildUrl('payin-slip');
    readonly API_REPRINT_SLIP: string = this.buildUrl('payin-slip-pdf/reprint');
    readonly API_CHECK_LASTPRINT: string = this.buildUrl('payin-slip');
    readonly API_CHECK_DATA_PRINT: string = this.buildUrl('payin-slip/check-print');

    //--Adjust Type Master--
    readonly API_LOAD_ADJ_TYPE_MASTER: string = this.buildUrl('adjust-type-setup');
    readonly API_LOAD_ADJ_TYPE_MASTER_DETAIL: string = this.buildUrl('adjust-type-setup');
    readonly API_ADD_ADJ_TYPE_MASTER: string = this.buildUrl('adjust-type-setup');
    readonly API_UPDATE_ADJ_TYPE_MASTER: string = this.buildUrl('adjust-type-setup');
    readonly API_DELETE_ADJ_TYPE_MASTER: string = this.buildUrl('adjust-type-setup');

    // --Bank Account Master--
    readonly API_BANK_ACC_MASTER: string = this.buildUrl('bank-account');
    readonly API_BANK_MASTER: string = this.buildUrl('master-bank');

    // -- Barcode Master 
    readonly API_BARCODE_MASTER: string = this.buildUrl('barcode-setup');

    // -- PaymentType Master
    readonly API_PAYMENTTYPE_MASTER: string = this.buildUrl('payment-type-master');
    readonly API_LOAD_PAYMENTTYPE: string = this.buildUrl('load/payment-type');

    // -- Location Master --
    readonly API_LOCATION_MASTER: string = this.buildUrl('location-shop/location-master');

    // -- user mapping white location --
    readonly API_USER_MAPPING_LOCATION: string = this.buildUrl('location-shop/user-mapping-location');

    // --company-master --
    readonly API_LOCATION_TYPE: string = this.buildUrl('load/location-type');

    readonly API_COMPANY_MASTER: string = this.buildUrl('company-master');
    // -- User Master --
    readonly API_USER_MASTER: string = this.buildUrl('user');

    // -- Role Master --
    readonly API_ROLE_MASTER: string = this.buildUrl('role');

    //-- Merchant Master --
    readonly API_MERCHANT: string = this.buildUrl('merchant-mapping');

    // -- Suspense Master --
    readonly API_SUSPENSE: string = this.buildUrl('suspense-account');

    // -- other-account-mapping --
    readonly API_OTHER_ACCOUNT_MAPPING: string = this.buildUrl('other-account-mapping');

    // -- bank sla --
    readonly API_BANK_SLA_MASTER: string = this.buildUrl('bank-sla-master');

    // -- global-config --
    readonly API_GLOBAL_CONFIG_MASTER: string = this.buildUrl('global-config');

    // -- holiday-setup --
    readonly API_HOLIDAY_SETUP: string = this.buildUrl('holiday-setup');

    // -- payin-Setup --
    readonly API_PAYIN_SETUP: string = this.buildUrl('payin-Setup');

    //-- all location --
    readonly API_LOAD_ALL_LOCATION: string = this.buildUrl('load/location-type');

    //  --load/bank-master --
    readonly API_LOAD_ALL_BANK: string = this.buildUrl('load/bank-master');

     // -- bank code mapping --
     readonly API_BANK_CODE_MAPPING: string = this.buildUrl('master-bankcode-mapping');

    //report
    readonly API_REPORT_COVER_WHT: string = this.buildUrl('report');
    // readonly API_REPORT_LOAD_PDF: string = this.buildUrl('report/COVER_WHT_REPORT_');
    // readonly API_REPORT_SUMMARY_PDF: string = this.buildUrl('report/SUMMARY_REPORT_');
    readonly API_REPORT_LOAD_PDF: string = this.buildUrl('report-pdf/cover-wht');
    readonly API_REPORT_SUMMARY_PDF: string = this.buildUrl('report-pdf/summary');
    readonly API_REPORT_RECONCILE: string = this.buildUrl('report/reconcile-by-bank');
    readonly API_REPORT_RECONCILE_SHOP: string = this.buildUrl('report/reconcile-by-shop');
    readonly API_REPORT_OST: string = this.buildUrl('report/outstanding-adjust');

    //dropdown Bank
    readonly API_LOAD_BANK: string = this.buildUrl('load/bank-master');
    readonly API_LOAD_BANK_MERCHANT: string = this.buildUrl('load/merchant-type');
    readonly API_LOAD_COMP_PRINT: string = this.buildUrl('load/bank-master');
    readonly API_LOAD_BANKCODE: string = this.buildUrl('load/bank-code-master');

    //bank adjust ar
    readonly API_BANK_ADJUST_AR: string = this.buildUrl('load/adjust-type');

    //user management
    readonly API_USER_MANAGEMENT: string = this.buildUrl('user-management');

    //authorize
    readonly API_AUTH: string = this.buildUrl('admin/authorize');
    readonly API_LOGOUT: string = this.buildUrl('admin/logout');

    // -- Manage pending --
    readonly API_MANAGE_PENDING: string = this.buildUrl('manage-pending');
    

    //attach file
    readonly API_LIST_DOC_TYPE: string = this.buildUrl('attach/doctype');
    readonly API_LIST_DOC_DATE: string = this.buildUrl('attach');
    readonly API_UPLOAD: string = this.buildUrl('attach');
    readonly API_PREVIEW_PDF_FILE: string = this.buildUrl('attach/preview');
    readonly API_MAXSIZE: string = this.buildUrl('attach/maxsize');
    readonly API_ATTACH_ALL_DATE: string = this.buildUrl('attach/docdate');


    //attach mapping
    readonly API_ATTACH_MAPPING: string = this.buildUrl('attach-mapping');

    //Bank Master
    readonly API_MASTER_BANK_MASTER: string = this.buildUrl('master-bankmaster');
    
    // --adjust detail master--
    readonly API_ADJUSTDETAIL_MASTER: string = this.buildUrl('adjust-detail');

    //load bank short name
    readonly API_LOAD_BANK_SHORTNAME: string = this.buildUrl('load/bank-short-name');
  

    // ---- REAL API ----
}