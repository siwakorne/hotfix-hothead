import { Component, OnInit, EventEmitter, Input, Output, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Endpoint } from '../../../common/const/endpoint';
import * as moment from 'moment';
import * as axios from 'axios';
import { v4 as uuid } from 'uuid';
import { AppComponent } from '../../../app.component';

@Component({
    selector: 'app-adjust',
    templateUrl: './adjust.component.html',
    styleUrls: ['./adjust.component.css']
})
export class AdjustComponent implements OnInit, OnChanges {

    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    /* locId: string = '02131' */
    mockLocationId = this.rawMockLocationId[2];
    @Output() disClose = new EventEmitter<boolean>();
    @Output() decreaseAmount = new EventEmitter<number>();

    display = false;
    type: any = [];
    url: string;
    adjustType: Array<any> = [];
    company: Array<object> = [];
    adjustStatus: Array<object> = [];
    dataLocationName: string = '';
    dataAdjustType: string = '';
    loading: boolean = true;
    submitClick: boolean = false;
    checkData = 1;
    active = '';
    cnFlag = '';
    bank: any;
    merchant: any;
    maxAmount: number;
    filterData = [];
    search: boolean;
    dateVal = Date.now();
    detail: any;
    msg: any;
    ostDiff: any;
    amount: string;
    key = '';
    paymentType: string;
    clickLoad = false;
    paging = 0;
    page: number = 0;
    statusDefault = null;
    searchAmount = null;
    box = null;
    checkAmtFlag: any;
    checkAmtValue: any;
    paymentTypeDropdown: Array<any> =[];
    condition = {
        msg: ''
    };

    form: any = {
        pagingInfo: {
            currentPage: 1
        },
        adjSrchReq: {
            company: '',
            paymentType: null,
            locationCode: '',
            adjustType: null,
            status: '',
            amount: null,
            saleSlipDate: null,
            bankEdc: null,
            merchantId: null,
            Ostamount: null,
            adjustNo: null
        }
    };
    @Input() data = {
        key: '',
        companyCode: '',
        status: '1',
        amount: '',
        adjustType: null,
        paymentTypeCode: null,
        saleSlipDate: null,
        adjustNo: null,
        bankEdc: '',
        merchartId: null,
        taxId: null,
        chequeNo: '',
        custName: null,
        outstandingDiffAmt: '',
        note: null,
        diff: '',
        legacyAmount: '',
        shopEodDocId: null,
        whtFlag: null,
        docDate: null,
        cheque_no: null,
        shopEodDetId: null,
        last4Digits: null,
        shopEodPaymentId: null,
        diffAmt: null,
        glAccount: null,
        glCs: null,
        glFs: null,
        glProduct: null,
        glProject: null,
        glRc: null,
        glRs: null,
        glSs: null
    };

    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };
    locationList: Array<object> = [];
    constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent) {
    }


    checkAmtWithFlag() {
        this.msg = '';
        let amt = Number(this.amount.replace(/,/g, ''));
        if (this.checkAmtFlag == 'Y') {
            if (this.checkAmtValue < Math.abs(amt)) {
                this.msg = 'overflow';
                this.condition.msg = this.checkAmtValue.toString();
            } else {
                this.onSave();
            }
        } else {
            this.onSave();
        }
    }

    onSave() {
        this.msg = '';
        let ost = Number(this.data.outstandingDiffAmt);
        let amt = Number(this.amount.replace(/,/g, ''));
        if (Math.abs(ost) < Math.abs(amt)) {
            this.msg = 'overflow';
        } else {
            if (this.amount == '0.00') {
                this.msg = 'zero'
            } else {
                if (this.data.whtFlag == 'Y') {
                    if (this.data.adjustType == null || this.amount == null || this.amount == '0.00' || this.amount == '' || this.amount == undefined || this.data.adjustType == 'undefined') {
                        this.msg = 'fail';
                    } else {
                        this.upDateData();
                    }
                } else {
                    if (this.data.paymentTypeCode == 'CSH') {
                        if (this.data.adjustType == null || this.amount == null || this.amount == '0.00' || this.amount == '' || this.amount == undefined || this.data.adjustType == 'undefined') {
                            this.msg = 'fail';
                        } else {
                            this.upDateData();
                        }
                    } else if (this.data.paymentTypeCode == 'CHQ') {
                        if (this.data.adjustType == null || this.amount == null || this.amount == '0.00' || this.amount == '' || this.amount == undefined || this.data.adjustType == 'undefined') {
                            this.msg = 'fail';
                        } else {
                            this.upDateData();
                        }
                    } else if (this.data.paymentTypeCode == 'CC') {
                        if (this.cnFlag == 'Y') {
                            if (this.data.adjustType == null ||
                                this.data.bankEdc == null ||
                                this.data.merchartId == null ||
                                this.data.saleSlipDate == null ||
                                this.data.saleSlipDate == undefined ||
                                this.data.last4Digits == null ||
                                this.data.last4Digits.length != 4 ||
                                this.amount == null ||
                                this.amount == '0.00' ||
                                this.amount == '' ||
                                this.amount == undefined ||
                                this.data.adjustType == 'undefined') {
                                this.msg = 'fail';
                            } else {
                                this.upDateData();
                            }
                        } else {
                            if (this.data.adjustType == null || this.amount == null || this.amount == '0.00' || this.amount == '' || this.amount == undefined || this.data.adjustType == 'undefined') {
                                this.msg = 'fail';
                            } else {
                                this.upDateData();
                            }
                        }
                    }
                }
            }

        }
    }

    async upDateData() {
        this.clickLoad = true;
        this.submitClick = true;
        if (this.data.status == undefined) {
            this.data.status = '1';
        }
        let date = this.data.saleSlipDate ? moment(this.data.saleSlipDate).format('x') : null;

        axios.default.request({
            url: this.endpoint.API_ADD_ADJ,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'action': 'add',
                'transactionId': uuid(),
                'username': this.rawMockLocationId[1],
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            data: {
                adjUpdInfo: {
                    "adjustMasterId": this.data.adjustType,
                    "adjustStatus": this.data.status,
                    "adjustAmt": Number(this.amount.replace(/,/g, '')),
                    "adjustNote": this.data.note,
                    "mainDocId": this.data.shopEodDocId,
                    "mainDocDate": this.data.docDate,
                    "companyCode": this.data.companyCode,
                    "locationCode": this.mockLocationId,
                    "paymentTypeCode": this.data.paymentTypeCode,
                    "whtFlag": this.data.whtFlag,
                    "chequeNo": this.data.chequeNo,
                    "shopEodDetId": this.data.shopEodDetId,
                    "outstandingDiffAmt": this.data.outstandingDiffAmt,
                    "shopEodPaymentId": this.data.shopEodPaymentId,
                    "edcBank": this.data.bankEdc,
                    "merchantId": this.data.merchartId,
                    "saleSlipDate": date ? date : null,
                    "refAdjustDocId": this.form.adjustNo,
                    "last4Digits": this.data.last4Digits,
                    "diffAmt": this.data.diffAmt,
                    "glAccount": this.data.glAccount,
                    "glCs": this.data.glCs,
                    "glFs": this.data.glFs,
                    "glProduct": this.data.glProduct,
                    "glProject": this.data.glProject,
                    "glRc": this.data.glRc,
                    "glRs": this.data.glRs,
                    "glSs": this.data.glSs,
                }
            }
        }).then(() => {
            this.data.saleSlipDate = null;
            this.clickLoad = false;
            this.msg = 'success';
            this.decreaseAmount.emit(Number(this.amount.replace(/,/g, '')));
        }).catch(err => {
            if (err.response.data.data.statusCode == 'B2004') {
                this.msg = 'overflow';
            }
            this.clickLoad = false;
            this.submitClick = false;
        })
    }

    showDialog() {
        this.msg = '';
        this.filterData = [];
        this.active = 'clear';
        this.loading = false;
        this.checkData = 1;
        let ost = Number(this.data.outstandingDiffAmt);
        let amt = Number(this.amount.replace(/,/g, ''));
        if (this.ostDiff > 0) {
            this.searchAmount = Number(this.amount.replace(/,/g, '')) * (-1);
        } else {
            this.searchAmount = this.amount.replace(/,/g, '');
        }
        if (Math.abs(ost) >= Math.abs(amt)) {
            this.box = 0;
            this.display = true;
            this.form.adjSrchReq.amount = this.amount //(parseFloat(this.amount)*-1).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
            this.form.adjSrchReq.locationCode = this.mockLocationId;
            this.form.adjSrchReq.status = "1";
        } else {
            this.box = 1;
            this.display = true;
            this.msg = 'overref';
        }
    }

    reOpen() {
        this.box = null;
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.onSearch(event.page + 1);
        }
    }

    closing() {
        this.paginator.currentPage = 0;
        this.paginator.totalRecords = 0;
        this.display = false;
        this.filterData = [];
        this.loading = false;
        this.checkData = 1;
        this.active = 'clear';
        this.paging = 0;
    }


    onSearch(page) {
        this.filterData = [];
        this.loading = true;
        this.page = page;
        this.checkData = 1;
        this.active = 'search';
        let config = {
            headers: {
                'Content-Type': 'application/json',
                'action': 'search-ref',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        }
        /*   let query = '?';
         query += page ? 'currentPage=' + page : '';
         query += this.form.adjSrchReq.paymentType ? '&paymentTypeCode=' + this.form.adjSrchReq.paymentType : '';
         query += this.form.adjSrchReq.adjustType ? '&adjustMasterId=' + this.form.adjSrchReq.adjustType : '';
         query += this.form.adjSrchReq.locationCode ? '&locationCode=' + this.form.adjSrchReq.locationCode : '';
         query += this.form.adjSrchReq.company ? '&companyCode=' + this.form.adjSrchReq.company : '';
         query += this.form.adjSrchReq.status ? '&adjustStatus=' + this.form.adjSrchReq.status : '';
         query += this.form.adjSrchReq.bankEdc ? '&edcBank=' + this.form.adjSrchReq.bankEdc : '';
         query += this.form.adjSrchReq.amount ? '&adjustAmt=' + this.form.adjSrchReq.amount.replace(/,/g, '') : '';
        // query += this.amount ? '&adjustAmt=' + this.amount.replace(/,/g, '') : '';
         query += '&adjustDocType=SH';
 
         axios.default.get(`${this.endpoint.API_SRCH_ADJ}${query}`, config ) */
        axios.default.request({
            url: this.endpoint.API_SRCH_ADJ,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'search-ref',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                currentPage: page ? page : null,
                paymentTypeCode: this.form.adjSrchReq.paymentType ? this.form.adjSrchReq.paymentType : null,
                adjustMasterId: this.form.adjSrchReq.adjustType ? this.form.adjSrchReq.adjustType : null,
                locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode != 'undefined' ? this.form.adjSrchReq.locationCode : null : null,
                companyCode: this.form.adjSrchReq.company ? this.form.adjSrchReq.company : null,
                adjustStatus: this.form.adjSrchReq.status ? this.form.adjSrchReq.status : null,
                edcBank: this.form.adjSrchReq.bankEdc ? this.form.adjSrchReq.bankEdc : null,
                outstandingAmt: this.form.adjSrchReq.Ostamount ?  this.form.adjSrchReq.Ostamount : null,
                adjustAmt: this.searchAmount ? this.searchAmount : null,
                adjustDocType: 'SH',
                adjustDocId: this.form.adjSrchReq.adjustNo ? this.form.adjSrchReq.adjustNo : null
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.filterData = snapData.data.data.adjSrchRespList.map(snap => {
                        this.loading = false;
                        Object.keys(this.locationList).forEach(keys => {
                            if (snap.locationCode == this.locationList[keys].code) {
                                this.dataLocationName = this.locationList[keys].name;
                            }
                        });
                        Object.keys(this.adjustType).forEach(keys => {
                            if (snap.adjustMasterId == this.adjustType[keys].adjustMasterId) {
                                this.dataAdjustType = this.adjustType[keys].adjustType;
                            }
                        });
                        return {
                            adjustNo: snap.adjustDocId,
                            company: snap.companyCode,
                            locationCode: snap.locationCode,
                            adjustType: this.dataAdjustType,
                            paymentType: snap.paymentTypeCode === 'CSH' ? 'Cash' : snap.paymentTypeCode === 'CHQ' ? 'Cheque' : snap.paymentTypeCode === 'CC' ? 'Credit Card' : '',
                            status: this.global.setStatus(snap.adjustStatus),
                            amount: snap.adjustAmt ? snap.adjustAmt : 0,
                            merchantId: snap.merchantId,
                            edcBank: snap.edcBank,
                            saleSlipDate: snap.saleSlipDate,
                            locationName: this.dataLocationName,
                            adjustNote: snap.adjustNote,
                            last4Digits: snap.last4Digits,
                            adjustDate: snap.adjustDate,
                            adjustMasterId: snap.adjustMasterId,
                            outstandingAmt: snap.outstandingAmt

                        }

                    });
                    if (snapData.data.data.pagingInfo.totalRecords != 0) {
                        this.loading = false;
                        this.checkData = 1;
                        this.paging = 1
                    } else {
                        this.loading = false;
                        this.checkData = 0;
                        this.paging = 0;
                    }
                    this.paginator = snapData.data.data.pagingInfo;
                }
            })
    }

    clear() {
        this.active = 'clear';
        this.filterData = [];
        if (this.locationList.length >= 1) {
            this.form = {
                adjSrchReq: {
                    company: '',
                    paymentType: null,
                    amount: this.form.adjSrchReq.amount,
                    locationCode: this.form.adjSrchReq.locationCode,
                    status: this.form.adjSrchReq.status,
                    adjustType: null,
                    saleSlipDate: null,
                    bankEdc: null,
                    merchantId: null,
                    Ostamount: null,
                    adjustNo: null
                }
            };
        } else {
            this.form = {
                adjSrchReq: {
                    company: '',
                    paymentType: null,
                    amount: this.form.adjSrchReq.amount,
                    locationCode: this.form.adjSrchReq.locationCode,
                    status: this.form.adjSrchReq.status,
                    adjustType: null,
                    saleSlipDate: null,
                    bankEdc: null,
                    merchantId: null,
                    Ostamount: null,
                    adjustNo: null
                }
            };
        }
    }

    onCancel(agreed: boolean) {
        this.data = {
            key: '',
            companyCode: '',
            status: null,
            amount: '',
            adjustType: undefined,
            paymentTypeCode: null,
            saleSlipDate: null,
            adjustNo: null,
            bankEdc: '',
            merchartId: null,
            taxId: null,
            chequeNo: '',
            custName: null,
            outstandingDiffAmt: '',
            note: null,
            diff: '',
            legacyAmount: '',
            shopEodDocId: null,
            whtFlag: null,
            docDate: null,
            cheque_no: null,
            shopEodDetId: null,
            last4Digits: null,
            shopEodPaymentId: null,
            diffAmt: null,
            glAccount: null,
            glCs: null,
            glFs: null,
            glProduct: null,
            glProject: null,
            glRc: null,
            glRs: null,
            glSs: null
        };
        this.disClose.emit(agreed);
    }

    add(data) {
        this.paginator.currentPage = 0;
        this.paginator.totalRecords = 0;
        this.display = false;
        this.form.adjustNo = data.adjustNo;
        this.form.key = data.key;
        this.data.adjustType = data.adjustMasterId;
        this.data.status = '2';
        //   this.amount = Math.abs(parseFloat(data.amount)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
    }

    ngOnInit() {
        this.cnFlag = 'N';
        this.data.adjustType = undefined;
        this.data.bankEdc = undefined;
        this.data.merchartId = undefined;
        this.filterData = [];
        this.ostDiff = ''
        this.amount = null;
        this.dropDown();
        let legacys: any = this.data.outstandingDiffAmt;

        if (this.data.taxId) {
            this.ostDiff = this.data.outstandingDiffAmt;
            this.amount = Math.abs(parseFloat(legacys)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
            this.maxAmount = Math.abs(legacys);

        } else {
            if (this.data.paymentTypeCode === 'CHQ') {
                this.ostDiff = this.data.outstandingDiffAmt;
                this.amount = Math.abs(parseFloat(legacys)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                this.maxAmount = Math.abs(legacys);

            } else {
                this.ostDiff = this.data.outstandingDiffAmt;
                this.amount = Math.abs(parseFloat(legacys)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                this.maxAmount = Math.abs(legacys);

            }
        }

        if (this.router.url === '/confirm-sale-payment/search-adjust') {
            this.url = 'shop';
        } else if (this.router.url === '/credit-note/search') {
            this.url = 'Treasury';
            this.form.adjSrchReq.paymentType = 'CC';
        }
    }

    ngOnChanges() {
        this.cnFlag = 'N';
        this.data.adjustType = undefined;
        this.data.bankEdc = undefined;
        this.data.merchartId = undefined;
        this.filterData = [];
        this.ostDiff = ''
        this.amount = null;
        this.dropDown();
        let legacys: any = this.data.outstandingDiffAmt;
        this.data.saleSlipDate = null

        if (this.data.taxId) {
            this.ostDiff = this.data.outstandingDiffAmt;
            this.amount = Math.abs(parseFloat(legacys)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
            this.maxAmount = Math.abs(legacys);

        } else {
            if (this.data.paymentTypeCode === 'CHQ') {
                this.ostDiff = this.data.outstandingDiffAmt;
                this.amount = Math.abs(parseFloat(legacys)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                this.maxAmount = Math.abs(legacys);

            } else {
                this.ostDiff = this.data.outstandingDiffAmt;
                this.amount = Math.abs(parseFloat(legacys)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                this.maxAmount = Math.abs(legacys);

            }
        }

        if (this.router.url === '/confirm-sale-payment/search-adjust') {
            this.url = 'shop';
        } else if (this.router.url === '/credit-note/search') {
            this.url = 'Treasury';
            this.form.adjSrchReq.paymentType = 'CC';
        }
    }

    dropDown() {
        // adjustType dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_TYPE,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'username': this.rawMockLocationId[1],
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.adjustType = snapData.data.data.adjMastInfoList.map(snap => {
                    return {
                        adjustType: snap.adjustType,
                        adjustDocType: snap.adjustDocType,
                        adjustMasterId: snap.adjustMasterId,
                        adjustCnFlag: snap.cnFlag,
                        defaultStatus: snap.defaultStatus,
                        checkAmtFlag: snap.checkAmtFlag,
                        checkAmtValue: snap.checkAmtValue,
                        glAccount: snap.glAccount,
                        glCs: snap.glCs,
                        glFs: snap.glFs,
                        glProduct: snap.glProduct,
                        glProject: snap.glProject,
                        glRc: snap.glRc,
                        glRs: snap.glRs,
                        glSs: snap.glSs
                    };
                });
            }
        });

        // adjustStatus dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_STTS,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'username': this.rawMockLocationId[1],
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.adjustStatus = snapData.data.data.adjSttsInfoList.map(snap => {
                    return {
                        adjustStatusCode: snap.adjustStatusCode,
                        adjustStatusDesc: snap.adjustStatusDesc
                    };
                });
            }
        });


        // company dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_COMP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'username': this.rawMockLocationId[1],
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.company = snapData.data.data.companyInfoList.map(snap => {
                    return {
                        companyCode: snap.companyCode,
                    };
                });
            }
        });

        //bank
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK_MERCHANT,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                companyCode: this.data.companyCode ? this.data.companyCode : null,
                locationCode: this.mockLocationId

            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.bank = snapData.data.data.locMrchntInfoList.map(snap => {
                    return {
                        bankName: snap.bankName,
                        companyCode: snap.companyCodefalse
                    };
                });
            }
        });
        // paymentType dropdown
axios.default.request({
    url: this.endpoint.API_LOAD_PAYMENTTYPE,
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'action': 'load',
        'transactionId': uuid(),
        'locationId': this.mockLocationId,
        'lang': 'EN',
        'username': this.rawMockLocationId[1],
        'navigate': this.router.url,
        'token': this.rawMockLocationId[3],
    }

})
    .then(snapData => {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.paymentTypeDropdown = snapData.data.data.paymentTypeInfoList.map(snap => {
                return {
                    paymentTypeCode: snap.paymentTypeCode,
                    paymentTypeDesc: snap.paymentTypeDesc,
                    paymentTypeStatus: snap.paymentTypeStatus,
                    paymentMethod: snap.paymentMethod

                }
            });
        }
    })

        let preList = JSON.parse(localStorage.getItem('locationPermiss'));
        preList.map(key => {
            this.locationList.push({
                code: key.code,
                name: key.name
            })
        });
    }

    async setCnFlag() {
        this.cnFlag = 'N';
        this.checkAmtValue = null;
        this.checkAmtFlag = null;
        await this.adjustType.map(data => {
            if (this.data.adjustType == data.adjustMasterId && data.adjustCnFlag) {
                this.cnFlag = data.adjustCnFlag;
            }
            if (this.data.adjustType == data.adjustMasterId) {
                this.checkAmtFlag = data.checkAmtFlag;
                this.checkAmtValue = data.checkAmtValue;
            }
        })
        if (this.cnFlag == 'N') {
            this.data.bankEdc = undefined;
            this.data.merchartId = undefined;
            this.data.saleSlipDate = null;
            this.data.last4Digits = null;
        }
        this.setGlAcc()
        this.setStatus();
    }

    setStatus() {
        this.adjustType.map(snap => {
            if (this.data.adjustType == snap.adjustMasterId) {
                if (this.form.adjustNo) {
                    this.data.status = '2';
                } else {
                    this.data.status = snap.defaultStatus;
                }
            }
        });
    }

    Ok() {
        this.box = null;
        this.clickLoad = false;
        this.display = false;
    }


    setGlAcc() {
        this.data.glAccount = null,
            this.data.glCs = null,
            this.data.glFs = null,
            this.data.glProduct = null,
            this.data.glProject = null,
            this.data.glRc = null,
            this.data.glRs = null,
            this.data.glSs = null

        this.adjustType.map(snap => {
            if (this.data.adjustType == snap.adjustMasterId) {
                this.data.glAccount = snap.glAccount,
                    this.data.glCs = snap.glCs,
                    this.data.glFs = snap.glFs,
                    this.data.glProduct = snap.glProduct,
                    this.data.glProject = snap.glProject,
                    this.data.glRc = snap.glRc,
                    this.data.glRs = snap.glRs,
                    this.data.glSs = snap.glSs
            }

        })
    }



    loadMerchantt() {
        this.merchant = [];
        //load merchant
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK_MERCHANT,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-merchant',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                bankName: this.data.bankEdc,
                companyCode: this.data.companyCode ? this.data.companyCode : null,
                locationCode: this.mockLocationId
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.merchant = snapData.data.data.locMrchntInfoList.map(snap => {
                    return {
                        merchantId: snap.merchantId,
                        merchantType: snap.merchantType
                    };
                });
            }
        });
    }

    isOnlyNumber(event, data, type) {
        if (data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                data = data.replace(/,/g, '');
                if ((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    if(type == 'amount') {
                        this.amount = Math.abs(this.setDecimal(parseFloat(data))).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
                    } else if (type == '4digit') {
                        this.data.last4Digits = isNaN(Math.abs(data)) ? null : Math.abs(data) != 0 ? Math.abs(data).toString() : null ;
                    }
                }
            }
        }
    }

    isOnlyNumberOst(event, data) {
        if (data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                let minus = 1;
                let minusString = data.indexOf('-')
                if (minusString != -1) {
                    minus = -1;
                    data = data.replace(/-/g, '');
                }
                data = data.replace(/,/g, '');
                if ((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    this.form.adjSrchReq.Ostamount = data ? this.setDecimal(parseFloat(data) * minus) : null;
                }
            }
        }
    }


    setDecimal(number: number) {
        number = isNaN(number) ? 0 : number;
        if (number) {
            var digit = number.toString().indexOf('.');
            if (digit != -1) {
                number = parseFloat(number.toString().substr(0, (digit + 3)));
            }
        }
        return number;
    }
}
