import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ParentExampleComponent } from './parent-example/parent-example.component';
import { ChildExampleComponent } from './child-example/child-example.component';
import { DialogModule } from 'primeng/dialog';
import { PaginatorModule } from 'primeng/paginator';
import { CalendarModule } from 'primeng/calendar';
import { CreditNoteModule } from '../../modules/credit-note-module/credit-note.module';
import {ScrollPanelModule} from 'primeng/scrollpanel';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        DialogModule,
        PaginatorModule,
        CalendarModule,
       CreditNoteModule,
       ScrollPanelModule
    ],
    declarations: [
        ParentExampleComponent,
        ChildExampleComponent
    ],
})
export class ShareCompModule { }
