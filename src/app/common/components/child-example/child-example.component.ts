import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-child-example',
    templateUrl: './child-example.component.html',
    styleUrls: ['./child-example.component.css']
})
export class ChildExampleComponent implements OnInit {

    @Input() data: { firstName: string, lastName: string };

    @Output() validateFullName: EventEmitter<string> = new EventEmitter<string>();

    constructor() { 
    }

    ngOnInit() {
    }

    isValidFullName() {
        let fullName: string = this.data.firstName.length > 0 && this.data.lastName.length > 0 ? `${this.data.firstName} ${this.data.lastName}` : 'invalid full name'; 
        this.validateFullName.emit(fullName);
    }

}
