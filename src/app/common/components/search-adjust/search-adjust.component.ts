import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as axios from 'axios';
import { v4 as uuid } from 'uuid';
import { Endpoint } from '../../const/endpoint';
import * as moment from 'moment';
import { AppComponent } from '../../../app.component';

@Component({
    selector: 'app-search-adjust',
    templateUrl: './search-adjust.component.html',
    styleUrls: ['./search-adjust.component.css']
})
export class SearchAdjustComponent implements OnInit {
    rawMockLocationId = JSON.parse(localStorage.getItem('loginData'));
    mockLocationId = this.rawMockLocationId[2];
    config;
    loading: boolean = true;
    loadingRef: boolean = true;
    display: boolean = false;
    refDisplay: boolean = false;
    url: string;
    listData = [];
    refData: any;
    refDocActive: boolean = false;
    checkData = 1;
    beforePaging = 1;
    paging = 0;
    page: number = 0;
    search: boolean;
    pagingRef = 0;
    pageRef: number = 0;
    searchRef: boolean;
    checkDataRef = 1;
    activeRef: string = '';
    dataAdjustType: string = '';
    dataLocationName: string = '';
    adjustData: Array<any> = [];
    filterData: Array<object> = [];
    filterRefData: Array<object> = [];
    filterPagination: Array<object> = [];
    company: Array<object> = [];
    adjustType: Array<object> = [];
    adjustStatus: Array<object> = [];
    bank: any;
    merchant: any;
    searchMaindoc = '0';
    searchAdjustdoc = '0';
    minMainDocDate: any;
    mintoDocDate: any;
    addRefData: any;
    msg: any = null;
    paymentType: Array<any> =[];
    paginator: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };
    paginatorRef: any = {
        currentPage: 0,
        totalPages: 0,
        totalRecords: 0,
        recordPerPage: 3,
    };
    box: number = 0;
    active: string = '';
    form: any = {
        pagingInfo: {
            currentPage: 1
        },
        adjSrchReq: {
            whtFlag: '',
            company: '',
            paymentType: '',
            locationCode: undefined,
            adjustType: '',
            status: 1,
            amount: null,
            saleSlipDate: null,
            bankEdc: '',
            merchantId: null,
            inputMerchantId: null,
            wht: '',
            adjustDateTo: null,
            adjustDate: null,
            mainDocDate: null,
            mainDocDateTo: null,
            Ostamount: null

        }
    };
    fromRefDoc: any = {
        pagingInfo: {
            currentPage: 1
        },
        adjSrchReq: {
            company: '',
            paymentType: '',
            locationCode: undefined,
            adjustType: '',
            status: 1,
            amount: null,
            adjustNo: null
        }
    };
    locationList = [];

    constructor(private router: Router, private endpoint: Endpoint, private global: AppComponent) { }

    ngOnInit() {
        Object.keys(this.rawMockLocationId[0].permissionIdList).map(key => {
            if (this.rawMockLocationId[0].permissionIdList[key] == "0301") { this.refDocActive = true; }
        });
        this.checkAuth();
        this.loading = true;
        this.checkData = 1;
        this.dropDow();
        if (this.router.url === '/confirm-sale-payment/search-adjust') {
            this.url = 'shop';
            this.form.adjSrchReq.locationCode = this.mockLocationId;
        } else if (this.router.url === '/credit-note/search') {
            this.url = 'Treasury';
            this.form.adjSrchReq.status == 1;
            this.form.adjSrchReq.paymentType = 'CC';
        }
    }

    dropDow() {
        // company dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_COMP,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3],
            }

        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.company = snapData.data.data.companyInfoList.map(snap => {
                        return {
                            companyCode: snap.companyCode,
                        }
                    });
                }
            })

        // adjustType dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_TYPE,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    snapData.data.data.adjMastInfoList.map(snap => {
                        if (snap.cnFlag == 'Y' && this.url == 'Treasury') {
                            this.form.adjSrchReq.adjustType = snap.adjustMasterId
                            this.adjustType.push({
                                adjustType: snap.adjustType,
                                adjustDocType: snap.adjustDocType,
                                adjustMasterId: snap.adjustMasterId
                            });
                        } else if (this.url == 'shop') {
                            this.adjustType.push({
                                adjustType: snap.adjustType,
                                adjustDocType: snap.adjustDocType,
                                adjustMasterId: snap.adjustMasterId
                            });
                        }
                    });
                }
            })
        // adjustStatus dropdown
        axios.default.request({
            url: this.endpoint.API_LOAD_ADJ_STTS,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }

        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.adjustStatus = snapData.data.data.adjSttsInfoList.map(snap => {
                        return {
                            adjustStatusCode: snap.adjustStatusCode,
                            adjustStatusDesc: snap.adjustStatusDesc
                        }
                    });
                }
            })


        //bank
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK_MERCHANT,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }, params: {
                locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode != 'undefined' ? this.form.adjSrchReq.locationCode : null : null,
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.bank = snapData.data.data.locMrchntInfoList.map(snap => {
                    return {
                        bankName: snap.bankName,
                        companyCode: snap.companyCode
                    };
                });
            }
        });
        // paymentType dropdown
axios.default.request({
    url: this.endpoint.API_LOAD_PAYMENTTYPE,
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'action': 'load',
        'transactionId': uuid(),
        'locationId': this.mockLocationId,
        'lang': 'EN',
        'username': this.rawMockLocationId[1],
        'navigate': this.router.url,
        'token': this.rawMockLocationId[3],
    }

})
    .then(snapData => {
        this.global.checkTokenExpired(snapData.data.statusCode);
        if (snapData.data.statusCode == 'S0000') {
            this.paymentType = snapData.data.data.paymentTypeInfoList.map(snap => {
                return {
                    paymentTypeCode: snap.paymentTypeCode,
                    paymentTypeDesc: snap.paymentTypeDesc,
                    paymentTypeStatus: snap.paymentTypeStatus,
                    paymentMethod: snap.paymentMethod

                }
            });
        }
    })


        // location
        let preList = JSON.parse(localStorage.getItem('locationPermiss'));
        preList.map(key => {
            this.locationList.push({
                code: key.code,
                name: key.name
            })
        });
    }

    paginate(event) {
        if ((event.page + 1) != this.paginator.currentPage) {
            this.loading = true
            this.Search(event.page + 1, 'paging');
        }
    }

    paginateRef(event) {
        if ((event.page + 1) != this.paginatorRef.currentPage) {
            this.loadingRef = true
            this.refSearch(event.page + 1);
        }
    }

    view(data) {
        this.box = 1;
        this.display = true;
        this.listData = data
    }
    refDoc(data) {
        this.box = 2;
        this.paginatorRef.currentPage = 0;
        this.paginatorRef.totalRecords = 0;
        this.filterRefData = [];
        this.pagingRef = 0;
        this.refData = data;
        this.fromRefDoc.adjSrchReq.adjustNo = null;
        this.fromRefDoc.adjSrchReq.locationCode = data.locationCode;
        this.fromRefDoc.adjSrchReq.amount = (this.refData.amount * -1).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.display = true;
    }
    ref(data) {
        this.box = 3;
        this.display = true;

        this.addRefData = data;
    }

    addRef() {
        this.msg = null;
        axios.default.request({
            url: this.endpoint.API_ADD_ADJ,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'action': 'save-ref',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            data: {
                adjUpdInfo: {
                    "adjustAmt": this.refData.amount,
                    "adjustDocId": this.refData.adjustNo,
                    "refAdjustDocId": this.addRefData.adjustNo,
                    "updatedBy": this.rawMockLocationId[1]
                }
            }
        }).then(snap => {
            if (snap.data.statusCode == 'S0000') {
                this.msg = 'success';
                this.Search(this.page, 'paging');
            } else {
                this.msg = 'fail';
            }
        })
    }

    cancelRef() {
        this.box = 2;
        this.display = true;
    }

    close() {
        this.box = 0;
        this.display = false;
    }

    refSearch(pageRef) {
        this.filterRefData = [];
        this.loadingRef = true;
        this.pageRef = pageRef;
        this.checkDataRef = 1;
        this.activeRef = 'search';
        let config = {
            headers: {
                'Content-Type': 'application/json',
                'action': 'search-ref',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'username': this.rawMockLocationId[1],
                'lang': 'EN',
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }
        }
        axios.default.request({
            url: this.endpoint.API_SRCH_ADJ,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'search-ref',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                currentPage: pageRef ? pageRef : null,
                paymentTypeCode: this.fromRefDoc.adjSrchReq.paymentType ? this.fromRefDoc.adjSrchReq.paymentType : null,
                adjustMasterId: this.fromRefDoc.adjSrchReq.adjustType ? this.fromRefDoc.adjSrchReq.adjustType : null,
                locationCode: this.fromRefDoc.adjSrchReq.locationCode ? this.fromRefDoc.adjSrchReq.locationCode != 'undefined' ? this.form.adjSrchReq.locationCode : null : null,
                companyCode: this.fromRefDoc.adjSrchReq.company ? this.fromRefDoc.adjSrchReq.company : null,
                adjustStatus: this.fromRefDoc.adjSrchReq.status ? this.fromRefDoc.adjSrchReq.status : null,
                edcBank: this.fromRefDoc.adjSrchReq.bankEdc ? this.fromRefDoc.adjSrchReq.bankEdc : null,
                adjustAmt: this.fromRefDoc.adjSrchReq.amount ? this.fromRefDoc.adjSrchReq.amount.replace(/,/g, '') : null,
                adjustDocId: this.fromRefDoc.adjSrchReq.adjustNo ? this.fromRefDoc.adjSrchReq.adjustNo : null,
                adjustDocType: 'SH'
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.filterRefData = snapData.data.data.adjSrchRespList.map(snap => {
                        this.loadingRef = false;
                        Object.keys(this.locationList).forEach(keys => {
                            if (snap.locationCode == this.locationList[keys].code) {
                                this.dataLocationName = this.locationList[keys].name;
                            }
                        });
                        Object.keys(this.adjustType).forEach(keys => {
                            if (snap.adjustMasterId == this.adjustType[keys].adjustMasterId) {
                                this.dataAdjustType = this.adjustType[keys].adjustType;
                            }
                        });
                        return {
                            adjustNo: snap.adjustDocId,
                            company: snap.companyCode,
                            locationCode: snap.locationCode,
                            adjustType: this.dataAdjustType,
                            paymentType: snap.paymentTypeCode === 'CSH' ? 'Cash' : snap.paymentTypeCode === 'CHQ' ? 'Cheque' : snap.paymentTypeCode === 'CC' ? 'Credit Card' : '',
                            status: this.global.setStatus(snap.adjustStatus),
                            amount: snap.adjustAmt ? snap.adjustAmt : 0,
                            merchantId: snap.merchantId,
                            edcBank: snap.edcBank,
                            saleSlipDate: snap.saleSlipDate,
                            locationName: this.dataLocationName,
                            adjustNote: snap.adjustNote,
                            last4Digits: snap.last4Digits,
                            adjustDate: snap.adjustDate,
                            adjustMasterId: snap.adjustMasterId,
                            outstandingAmt: snap.outstandingAmt
                        }
                    });
                    if (snapData.data.data.pagingInfo.totalRecords != 0) {
                        this.loadingRef = false;
                        this.checkDataRef = 1;
                        this.pagingRef = 1
                    } else {
                        this.loadingRef = false;
                        this.checkDataRef = 0;
                        this.pagingRef = 0;
                    }
                    this.paginatorRef = snapData.data.data.pagingInfo;
                }
            })
    }



    refClear() {
        this.active = 'clear';
        this.filterRefData = [];
        this.paginatorRef.currentPage = 0;
        this.paginatorRef.totalRecords = 0;
        this.pagingRef = 0;
        this.fromRefDoc = {
            adjSrchReq: {
                company: '',
                paymentType: '',
                locationCode: this.refData.locationCode,
                adjustType: '',
                status: 1,
                amount: (this.refData.amount * -1).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }),
                adjustNo: null
            }
        };
    }

    Search(page, click) {
        this.filterData = [];
        this.loading = true;
        this.page = page;
        this.checkData = 1;
        let status = this.form.adjSrchReq.status
        if (click != 'paging') {
            this.paging = 0;
        }
        this.search = true;

        if (this.url == 'shop') {
            this.config = {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'search-close-shop',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.router.url,
                    'token': this.rawMockLocationId[3]
                }
            }
        } else if (this.url == 'Treasury') {
            this.config = {
                headers: {
                    'Content-Type': 'application/json',
                    'action': 'search-close-shop',
                    'transactionId': uuid(),
                    'locationId': this.mockLocationId,
                    'lang': 'EN',
                    'username': this.rawMockLocationId[1],
                    'navigate': this.router.url,
                    'token': this.rawMockLocationId[3]
                }
            }
        }
        let date = '';
        if (this.form.adjSrchReq.saleSlipDate) {
            let preData = new Date(this.form.adjSrchReq.saleSlipDate);
            date = moment(preData).format('x');
        }
        let dateAdjust = '';
        if (this.form.adjSrchReq.adjustDate) {
            let preDataAdjust = new Date(this.form.adjSrchReq.adjustDate);
            dateAdjust = moment(preDataAdjust).add(-7, 'hours').format('x');
        }
        let preLocation = [];
        axios.default.request({
            url: this.endpoint.API_SRCH_ADJ,
            method: 'GET',
            headers: this.config.headers,
            params: {
                currentPage: page ? page : null,
                paymentTypeCode: this.form.adjSrchReq.paymentType ? this.form.adjSrchReq.paymentType : null,
                adjustMasterId: this.form.adjSrchReq.adjustType ? this.form.adjSrchReq.adjustType : null,
                locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode != 'undefined' ? this.form.adjSrchReq.locationCode : null : null,
                companyCode: this.form.adjSrchReq.company ? this.form.adjSrchReq.company : null,
                adjustStatus: this.form.adjSrchReq.status ? this.form.adjSrchReq.status : null,
                edcBank: this.form.adjSrchReq.bankEdc ? this.form.adjSrchReq.bankEdc : null,
                adjustAmt: this.form.adjSrchReq.amount ? this.form.adjSrchReq.amount.replace(/,/g, '') : null,
                merchantId: this.form.adjSrchReq.merchantId ? this.form.adjSrchReq.merchantId : this.form.adjSrchReq.inputMerchantId ? this.form.adjSrchReq.inputMerchantId : null,
                saleSlipDate: date ? date : null,
                // adjustData: dateAdjust ? dateAdjust : null,
                adjustDocType: 'SH',//this.url == 'shop'?  'SH' : 'TR',
                whtFlag: this.form.adjSrchReq.wht ? this.form.adjSrchReq.wht : null,
                adjustDate: this.form.adjSrchReq.adjustDate ? moment(new Date(this.form.adjSrchReq.adjustDate)).add(+7, 'hours').format('x') : null,
                adjustDateTo: this.form.adjSrchReq.adjustDateTo == this.form.adjSrchReq.adjustDate ? null : this.form.adjSrchReq.adjustDateTo ? moment(new Date(this.form.adjSrchReq.adjustDateTo)).add(+7, 'hours').format('x') : null,
                mainDocDate: this.form.adjSrchReq.mainDocDate ? moment(new Date(this.form.adjSrchReq.mainDocDate)).add(+7, 'hours').format('x') : null,
                mainDocDateTo: this.form.adjSrchReq.mainDocDateTo == this.form.adjSrchReq.mainDocDate ? null : this.form.adjSrchReq.mainDocDateTo ? moment(new Date(this.form.adjSrchReq.mainDocDateTo)).add(+7, 'hours').format('x') : null,
                outstandingAmt: this.form.adjSrchReq.Ostamount ? this.form.adjSrchReq.Ostamount.replace(/,/g, '') : null,
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.filterData = snapData.data.data.adjSrchRespList.map(snap => {
                    this.loading = false;
                    Object.keys(this.locationList).forEach(keys => {
                        if (snap.locationCode == this.locationList[keys].code) {
                            this.dataLocationName = this.locationList[keys].name;
                        }
                    });
                    Object.keys(this.adjustType).forEach(keys => {
                        if (snap.adjustMasterId == this.adjustType[keys].adjustMasterId) {
                            this.dataAdjustType = this.adjustType[keys].adjustType;
                        }
                    });
                    return {
                        whtFlag: snap.whtFlag,
                        adjustNo: snap.adjustDocId,
                        company: snap.companyCode,
                        locationCode: snap.locationCode,
                        adjustType: this.dataAdjustType,
                        paymentType: snap.paymentTypeCode === 'CSH' ? 'Cash' : snap.paymentTypeCode === 'CHQ' ? 'Cheque' : snap.paymentTypeCode === 'CC' ? 'Credit Card' : '',
                        status: this.global.setStatus(snap.adjustStatus),
                        amount: snap.adjustAmt ? snap.adjustAmt : 0,
                        merchantId: snap.merchantId,
                        edcBank: snap.edcBank,
                        saleSlipDate: moment(snap.saleSlipDate).add(7, 'hours'),
                        locationName: this.dataLocationName,
                        adjustNote: snap.adjustNote,
                        refAdjustDocId: snap.refAdjustDocId != 0 ? snap.refAdjustDocId : null,
                        last4Digits: snap.last4Digits,
                        adjustDate: snap.adjustDate,
                        Ostamount : snap.outstandingAmt,
                        cnFlag: snap.cnFlag

                    }
                })
                this.paginator = snapData.data.data.pagingInfo;
            }
            if (snapData.data.data.pagingInfo.totalRecords != 0) {
                this.loading = false;
                this.checkData = 1;
                this.paging = 1
            } else {
                this.loading = false;
                this.checkData = 0;
                this.paging = 0;
            }
        })
    }

    clear() {
        this.filterData = [];
        this.form.adjSrchReq.company = '';
        if (this.url !== 'Treasury') {
            this.form.adjSrchReq.paymentType = '';
            this.form.adjSrchReq.adjustType = '';
            this.form.adjSrchReq.locationCode = this.mockLocationId;
        } else {
            this.form.adjSrchReq.locationCode = undefined;
        }
        this.form.adjSrchReq.locationName = undefined;
        this.form.adjSrchReq.status = 1;
        this.form.adjSrchReq.amount = null;
        this.form.adjSrchReq.Ostamount = null;
        this.form.adjSrchReq.saleSlipDate = null;
        this.form.adjSrchReq.bankEdc = '';
        this.form.adjSrchReq.merchantId = null;
        this.form.adjSrchReq.adjustDate = null;
        this.paging = 0;
        this.search = false;
        this.form.adjSrchReq.inputMerchantId = null;
        this.form.adjSrchReq.wht = '';
        this.form.adjSrchReq.adjustDateTo = null;
        this.form.adjSrchReq.adjustDate = null;
        this.form.adjSrchReq.mainDocDate = null;
        this.form.adjSrchReq.mainDocDateTo = null;
        this.searchMaindoc = '0';
        this.searchAdjustdoc = '0';
    }

    onClose(agreed: boolean) {
        agreed ? this.display = false : this.display = true;
        this.box = null;
        /* if(agreed)
        {
            this.box = 1 ;
        }
        else{
            this.box = 0 ;
        } */
        this.loading = true
        this.Search(this.page, 'paging');

    }

    loadBank() {
        this.bank = [];
        this.form.adjSrchReq.bankEdc = '';
        this.form.adjSrchReq.merchantId = null;
        this.form.adjSrchReq.inputMerchantId = null;
        //bank
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK_MERCHANT,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-bank',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            }, params: {
                locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode != 'undefined' ? this.form.adjSrchReq.locationCode : null : null,
                companyCode: this.form.adjSrchReq.company ? this.form.adjSrchReq.company : null
            }
        }).then(snapData => {
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.bank = snapData.data.data.locMrchntInfoList.map(snap => {
                    return {
                        bankName: snap.bankName,
                        companyCode: snap.companyCode
                    };
                });
            }
        });
    }

    loadMerchantt() {
        this.form.adjSrchReq.merchantId = null;
        this.form.adjSrchReq.inputMerchantId = null;
        //load merchant
        axios.default.request({
            url: this.endpoint.API_LOAD_BANK_MERCHANT,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'action': 'load-merchant',
                'transactionId': uuid(),
                'locationId': this.mockLocationId,
                'lang': 'EN',
                'username': this.rawMockLocationId[1],
                'navigate': this.router.url,
                'token': this.rawMockLocationId[3]
            },
            params: {
                bankName: this.form.adjSrchReq.bankEdc,
                locationCode: this.form.adjSrchReq.locationCode ? this.form.adjSrchReq.locationCode != 'undefined' ? this.form.adjSrchReq.locationCode : null : null,
                companyCode: this.form.adjSrchReq.company ? this.form.adjSrchReq.company : null
            }
        })
            .then(snapData => {
                this.global.checkTokenExpired(snapData.data.statusCode);
                if (snapData.data.statusCode == 'S0000') {
                    this.merchant = snapData.data.data.locMrchntInfoList.map(snap => {
                        if (snap != '') {
                            this.form.adjSrchReq.merchantId = snap.merchantId;
                            this.form.adjSrchReq.inputMerchantId = this.form.adjSrchReq.merchantId;
                        }

                        return {
                            merchantId: snap.merchantId,
                            merchantType: snap.merchantType,
                        };
                    });
                    if (snapData.data.data.locMrchntInfoList == '') {
                        this.form.adjSrchReq.merchantId = null;
                    }
                }
            });
    }

    setMerchantId() {
        this.form.adjSrchReq.inputMerchantId = this.form.adjSrchReq.merchantId;
    }
    clearMerchantId() {
        this.form.adjSrchReq.merchantId = null;
    }

    isOnlyNumber(event, data) {
        if (data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                let minus = 1;
                let minusString = data.indexOf('-')
                if (minusString != -1) {
                    minus = -1;
                    data = data.replace(/-/g, '');
                }
                data = data.replace(/,/g, '');
                if ((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    this.form.adjSrchReq.amount = data ? this.setDecimal(parseFloat(data) * minus).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null;
                }
            }
        }
    }
    
    isOnlyNumberOst(event, data) {
        if (data) {
            if ((event.type == 'blur' || event.which == 13) || data) {
                let minus = 1;
                let minusString = data.indexOf('-')
                if (minusString != -1) {
                    minus = -1;
                    data = data.replace(/-/g, '');
                }
                data = data.replace(/,/g, '');
                if ((event.type == 'blur' || event.which == 13) || (data.toString().indexOf('.') != -1 && data.toString().length - data.toString().indexOf('.') > 3)) {
                    this.form.adjSrchReq.Ostamount = data ? this.setDecimal(parseFloat(data) * minus).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 }) : null;
                }
            }
        }
    }

    


    toDocDate() {
        if (this.form.adjSrchReq.adjustDate > this.form.adjSrchReq.adjustDateTo) {
            this.form.adjSrchReq.adjustDateTo = null;
        }
        if (this.form.adjSrchReq.adjustDate) {
            this.mintoDocDate = this.form.adjSrchReq.adjustDate
            this.searchAdjustdoc = '1';
            if (this.form.adjSrchReq.adjustDateTo == null || this.form.adjSrchReq.adjustDateTo == undefined) {
                this.form.adjSrchReq.adjustDateTo = this.form.adjSrchReq.adjustDate;
            }
        } else {
            this.searchAdjustdoc = '0';
            this.form.adjSrchReq.adjustDateTo = null;
        }
    }


    toMainDocDate() {
        if (this.form.adjSrchReq.mainDocDate > this.form.adjSrchReq.mainDocDateTo) {
            this.form.adjSrchReq.mainDocDateTo = null;
        }

        if (this.form.adjSrchReq.mainDocDate) {
            this.minMainDocDate = this.form.adjSrchReq.mainDocDate;
            this.searchMaindoc = '1';
            if (this.form.adjSrchReq.mainDocDateTo == null || this.form.adjSrchReq.mainDocDateTo == undefined) {
                this.form.adjSrchReq.mainDocDateTo = this.form.adjSrchReq.mainDocDate;
            }

        } else {
            this.searchMaindoc = '0';
            this.form.adjSrchReq.mainDocDateTo = null;
        }
    }

    setDecimal(number: number) {
        number = isNaN(number) ? 0 : number;
        if (number) {
            var digit = number.toString().indexOf('.');
            if (digit != -1) {
                number = parseFloat(number.toString().substr(0, (digit + 3)));
            }
        }
        return number;
    }

    checkAuth() {
        if (!JSON.parse(localStorage.getItem('loginData'))) {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        } else if (JSON.parse(localStorage.getItem('loginData')) && JSON.parse(localStorage.getItem('loginData')).length < 4) {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        }
    }
}
