import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-parent-example',
    templateUrl: './parent-example.component.html',
    styleUrls: ['./parent-example.component.css']
})
export class ParentExampleComponent implements OnInit {

    fullName: string;
    fname: string;
    lname: string;    
    parentData: { firstName: string, lastName: string } = { firstName: '', lastName: '' };

    constructor() { }

    ngOnInit() {
    }

    onChangedName() {
        this.parentData.firstName = this.fname;
        this.parentData.lastName = this.lname;
    }

    onValidateFullName(fullName: string) {
        this.fullName = fullName;
    }

}
