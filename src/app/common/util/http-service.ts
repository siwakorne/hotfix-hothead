import { Injectable } from '@angular/core';
import * as axios from 'axios';
import { v4 as uuid } from 'uuid';

@Injectable()
export class HttpService {

    

    constructor() {
    }

    public call(url:string, method:string, headers:any, responseType:string, params:any, data:any, 
        successCallback:(snapData:any)=>void, errorCallback:(error:any)=>void) {
          var  headerss = {
                'Content-Type': 'application/json',
                'transactionId': uuid(),
                'username': JSON.parse(localStorage.getItem('loginData'))[1],
                'locationId': JSON.parse(localStorage.getItem('loginData'))[2],
                'token': JSON.parse(localStorage.getItem('loginData'))[3]
            };
        axios.default.request({
        url: url,
        method: method,
        headers: Object.assign(headerss, headers),
        responseType: responseType,
        params: params,
        data: data
        })
        .then(successCallback)
        .catch(errorCallback);
        }

}
