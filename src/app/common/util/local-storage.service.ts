import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

    private storage: any;

    constructor() {
        this.storage = {
            "logged": true,
            "userDetl": {
                "name": "John",
                "role": "admin",
                "permList": ["010101001", "010101002", "010101003", "010101004"]
            }
        };
    }

    public getStorage() : any {
        return this.storage;
    }

}
