import { Injectable, ErrorHandler } from '@angular/core';
import { Endpoint } from '../const/endpoint';

@Injectable()
export class CustomErrorHandler implements ErrorHandler {

    constructor(private Endpoint: Endpoint) { }
    handleError(error) {
        console.log(error);
        // if (error.status) {
        //     window.location.href = this.Endpoint.ERROR_OCCURRED;
        // } else  {
        //     console.log(error);
        //     let statusCode = (error || {}).statusCode || '';
        //     if (statusCode.search(/^B/g) == -1 ) {
        //         console.error(error);
        //     }
        // }
    }
}
