import { Component, OnInit, OnChanges } from '@angular/core';
import { Endpoint } from '../../../common/const/endpoint';
import { Router } from '@angular/router';
import { MenuService } from './menu.service';
const version = require('../../../../../package.json').version;
@Component({
    selector: 'app-menubar',
    templateUrl: './menubar.component.html',
    styleUrls: ['./menubar.component.css']
})
export class MenubarComponent implements OnInit {

    constructor(private Endpoint: Endpoint, private router: Router, private menuService: MenuService) { }
    service: any;
    mockLocationId: string ;
    locationName: string ;
    dateVal: any;
    appVersion:string = version;

    ngOnInit() {
        this.dateVal = new Date();
        this.service = this.menuService
        this.menuService.mockLocationId = this.menuService.getRawMockLocationId ? this.menuService.getRawMockLocationId()[2]: '1';
        this.mockLocationId = JSON.parse(localStorage.getItem('loginData')) ? JSON.parse(localStorage.getItem('loginData'))[2] : null;
        this.menuService.clearActive();
        this.menuService.queryDataCloud(this.router.url);
    }

    ngOnChanges() {
        this.dateVal = new Date();
        this.menuService.mockLocationId = this.menuService.getRawMockLocationId() ? this.menuService.getRawMockLocationId()[2]: '1';
        this.mockLocationId = JSON.parse(localStorage.getItem('loginData')) ? JSON.parse(localStorage.getItem('loginData'))[2] : null;
        this.menuService.clearActive();
        this.menuService.queryDataCloud(this.router.url);
    }

    activeMenu(type) {
        this.menuService.clearActive();
        if (type === "form") this.menuService.getActiveMenu().confirm = 'is-active';
        else if (type === "closing") this.menuService.getActiveMenu().closing = 'is-active';
        else if (type === "searchAdj") this.menuService.getActiveMenu().searchAdj = 'is-active';
        else if (type === "payin") this.menuService.getActiveMenu().payin = 'is-active';
        else if (type === "searchCdn") this.menuService.getActiveMenu().searchCdn = 'is-active';
        else if (type === "ar") this.menuService.getActiveMenu().ar = 'is-active';
        else if (type === "user") this.menuService.getActiveMenu().user = 'is-active';
        else if (type === 'master') this.menuService.getActiveMenu().master = 'is-active';
        else if (type === 'report') this.menuService.getActiveMenu().report = 'is-active';
        else if (type === 'manage') this.menuService.getActiveMenu().managePending = 'is-active';
        else if (type === 'attact') this.menuService.getActiveMenu().attactFile = 'is-active';
        else if (type === "searchAdjAR") this.menuService.getActiveMenu().searchAdjAR = 'is-active';
        this.menuService.queryDataCloud(this.router.url);
    }

    editSession(){
        localStorage.clear();
        this.menuService.getRawMockLocationId()[2] =  this.menuService.mockLocationId //this.menuService.getMockLocationId();
        localStorage.setItem('loginData', JSON.stringify(this.menuService.getRawMockLocationId()));
        this.menuService.queryDataCloud(this.router.url);
        location.reload();
    }
    redirectToLogin(){
        this.router.navigate(['/login']);
    }

    logout() {
        this.menuService.logout();
    }

    active() {
        return this.menuService.getActive();
    }

    menuDisplay() {
        return this.menuService.getMenuDisplay();
    }

    rawMockLocationId() {
        return this.menuService.getRawMockLocationId();
    }

    status() {
        return this.menuService.getStatus();
    }

    closeDate() {
        return this.menuService.getCloseDate();
    }

}
