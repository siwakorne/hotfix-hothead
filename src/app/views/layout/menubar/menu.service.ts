import { Injectable } from '@angular/core';
import * as axios from 'axios';
import { Endpoint } from '../../../common/const/endpoint';
import { v4 as uuid } from 'uuid';
import { Router } from '@angular/router';
import { HttpService } from '../../../common/util/http-service';

@Injectable()
export class MenuService {

    constructor(private Endpoint: Endpoint, private router: Router, private HttpService: HttpService) {
    }
    rawMockLocationId = localStorage.getItem('loginData') ? JSON.parse(localStorage.getItem('loginData')) : '1';
   // mockLocationId : string =  '1' 
    mockLocationId =  this.rawMockLocationId[2];
    /* locationCode: string = '02131'; */
    roleData = [];
    status: string = '';
    dateVal = Date.now();
    active = {
        confirm: '',
        closing: '',
        searchAdj: '',
        payin: '',
        searchCdn: '',
        ar: '',
        user: '',
        master: '',
        report: '',
        managePending: '',
        attactFile: '',
        searchAdjAR: '',
    };
    closeDate = '';
    menuDisplay = {
        confirmSale: false,
        closingEnd: false,
        searchAdjust: false,
        payInSlip: false,
        payInPrint: false,
        payInReprint: false,
        payInCancel: false,
        payInSearch: false,
        summaryReport : false,
        coverWHT : false,
        searchCN: false,
        myViewAr: false,
        ArTracking : false,
        login: false,
        loginUser: false,
        loginRole: false,
        masterSetup: false,
        masterAdjustType: false,
        masterBankAccount: false,
        masterBarcode: false,
        masterPaymentType: false,
        masterLocationShop: false,
        masterMerchant: false,
        masterLocVsBank: false,
        managePending: false,
        attachFile: false,
        suspense: false,
        company: false,
        payinLocation: false,
        global: false,
        bankSla: false,
        holiday: false,
        user: false,
        OtherAccount: false,
        role: false,
        ReportReconcileSale: false,
        ReportReconcileBank: false,
        ReportOutstanding: false,
        notification: false,
        attachMapping: false,
        bankcodeMapping: false,
        bankMaster: false,
        reportArAging: false,
        reportShortageSurplus: false,

        searchAdjAR: false
    };
    role: object;
    locationName:string = '';

    getRawMockLocationId() {
        return this.rawMockLocationId;
    }

    getMockLocationId() {
        return this.mockLocationId;
    }

    getCurrentDate() {
        return this.dateVal;
    }

    getActiveMenu() {
        return this.active;
    }

    setStatus(status) {
        this.status = status;
    }

    getStatus() {
        return this.status;
    }

    getCloseDate() {
        return this.closeDate;
    }

    getRoleData() {
        return this.roleData;
    }

    activeMenu(type) {
        this.clearActive();
        if (type === "form") this.active.confirm = 'is-active';
        else if (type === "closing") this.active.closing = 'is-active';
        else if (type === "searchAdj") this.active.searchAdj = 'is-active';
        else if (type === "payin") this.active.payin = 'is-active';
        else if (type === "searchCdn") this.active.searchCdn = 'is-active';
        else if (type === "ar") this.active.ar = 'is-active';
        else if (type === "user") this.active.user = 'is-active';
        else if (type === 'master') this.active.master = 'is-active';
        else if (type === 'report') this.active.report = 'is-active';
        else if (type === 'manage') this.active.managePending = 'is-active';
        else if (type === 'attact') this.active.attactFile = 'is-active';
        else if (type === "searchAdjAR") this.active.searchAdjAR = 'is-active';
    }

    clearActive() {
        this.active = {
            confirm: '',
            closing: '',
            searchAdj: '',
            payin: '',
            searchCdn: '',
            ar: '',
            user: '',
            master: '',
            report: '',
            managePending: '',
            attactFile: '',
            searchAdjAR: '',
        };
    }

    queryDataCloud(navigate) {
        this.rawMockLocationId = localStorage.getItem('loginData') ? JSON.parse(localStorage.getItem('loginData')) : '1';
        if(this.rawMockLocationId[1] != undefined) {
            // mockLocationId : string =  '1' 
            this.mockLocationId =  this.rawMockLocationId[2];
            this.status = '';
            this.closeDate = '';
            this.roleData = [];
            
            let config = {
                headers: {
                    'action': 'load',
                    'lang':'EN',
                    'navigate': navigate,
                }
            };
    
            //axios.default.get(`${this.Endpoint.API_LOAD_CLOSING}`, config)
            this.HttpService.call(this.Endpoint.API_LOAD_CLOSING,'GET', config.headers,null, null , null , this.queryDataCloudSuccessCallback.bind(this) ,this.queryDataCloudFailCallback.bind(this));
            var stroage = JSON.parse(localStorage.getItem('loginData')) ? JSON.parse(localStorage.getItem('loginData'))[0] : null;
            if(stroage && stroage != '1'){
                Object.keys(this.rawMockLocationId[0].permissionIdList).map(key => {
                    if (this.rawMockLocationId[0].permissionIdList[key]== "01")   { this.menuDisplay.confirmSale = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "02")   { this.menuDisplay.closingEnd = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "03")   { this.menuDisplay.searchAdjust = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "04")   { this.menuDisplay.payInSlip = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0401") { this.menuDisplay.payInPrint = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0402") { this.menuDisplay.payInReprint = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0403") { this.menuDisplay.payInCancel = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0404") { this.menuDisplay.payInSearch = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0405") { this.menuDisplay.searchAdjAR = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "05")   { this.menuDisplay.searchCN = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "06")   { this.menuDisplay.myViewAr = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "09")   { this.menuDisplay.ArTracking = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "07")   { this.menuDisplay.login = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0701") { this.menuDisplay.loginUser = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0702") { this.menuDisplay.loginRole = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "08")   { this.menuDisplay.masterSetup = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0801") { this.menuDisplay.masterAdjustType = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0802") { this.menuDisplay.masterBankAccount = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0803") { this.menuDisplay.masterBarcode = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0804") { this.menuDisplay.masterPaymentType = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0805") { this.menuDisplay.masterLocationShop = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0806") { this.menuDisplay.masterMerchant = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0807") { this.menuDisplay.suspense = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0808") { this.menuDisplay.company = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0809") { this.menuDisplay.payinLocation = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0810") { this.menuDisplay.global = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0811") { this.menuDisplay.bankSla = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0812") { this.menuDisplay.holiday = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0813") { this.menuDisplay.user = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0814") { this.menuDisplay.OtherAccount = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0815") { this.menuDisplay.role = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0816") { this.menuDisplay.notification = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0818") { this.menuDisplay.attachMapping = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0817") { this.menuDisplay.bankcodeMapping = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "0819") { this.menuDisplay.bankMaster = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "1001") { this.menuDisplay.summaryReport = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "1002") { this.menuDisplay.coverWHT = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "1003") { this.menuDisplay.ReportReconcileSale = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "1004") { this.menuDisplay.ReportReconcileBank = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "1005") { this.menuDisplay.ReportOutstanding = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "1006") { this.menuDisplay.reportArAging = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "1007") { this.menuDisplay.reportShortageSurplus = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "11") { this.menuDisplay.managePending = true; }
                    if (this.rawMockLocationId[0].permissionIdList[key]== "12") { this.menuDisplay.attachFile = true; }
                });
                this.roleData = this.rawMockLocationId[0].locationCodeList;
                Object.keys(this.roleData).map(key => {
                    if(this.roleData[key].code == this.mockLocationId){
                        this.locationName = this.roleData[key].name;
                    };
                });
                localStorage.setItem('locationPermiss', JSON.stringify(this.roleData));
            }
        } else {
            localStorage.clear();
            this.router.navigate(['login']);
        }
    }

    checkTokenExpired(statusCode){
        if (statusCode == 'B1004') {
            localStorage.clear();
            this.router.navigate(['login']);
            location.reload();
        }
    }

    queryDataCloudSuccessCallback(snapData: any){
        this.status = 'S'
        this.closeDate = snapData.data.data.eodDate;
        this.checkTokenExpired(snapData.data.statusCode);
    }
    
    queryDataCloudFailCallback(err: any){
        this.status = 'D';
        this.closeDate = '';
        this.checkTokenExpired(err.data.statusCode != undefined ? err.data.statusCode : null);
    }
    

    async logout() {
        this.menuDisplay = {
            confirmSale: false,
            closingEnd: false,
            searchAdjust: false,
            payInSlip: false,
            payInPrint: false,
            payInReprint: false,
            payInCancel: false,
            payInSearch: false,
            summaryReport : false,
            coverWHT : false,
            searchCN: false,
            myViewAr: false,
            ArTracking : false,
            login: false,
            loginUser: false,
            loginRole: false,
            masterSetup: false,
            masterAdjustType: false,
            masterBankAccount: false,
            masterBarcode: false,
            masterPaymentType: false,
            masterLocationShop: false,
            masterMerchant: false,
            masterLocVsBank: false,
            managePending: false,
            attachFile: false,
            suspense: false,
            company: false,
            payinLocation: false,
            global: false,
            bankSla: false,
            holiday: false,
            user: false,
            OtherAccount: false,
            role: false,
            ReportReconcileSale: false,
            ReportReconcileBank: false,
            ReportOutstanding: false,
            notification: false,
            attachMapping: false,
            bankcodeMapping: false,
            bankMaster: false,
            reportArAging: false,
            reportShortageSurplus: false,
            searchAdjAR: false
        };
        await axios.default.request({
            url: this.Endpoint.API_LOGOUT,
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'transactionId': uuid(),
                'action': 'logout',
                'lang': 'EN',
                'navigate': '/logout',
                'username': this.rawMockLocationId ? this.rawMockLocationId[1] : ''
            }
        }).then(() => {
            localStorage.clear();
            location.reload();
        }).catch(() => {
            localStorage.clear();
            location.reload();
        })
    }

    getActive() {
        return this.active;
    }

    getMenuDisplay() {
        return this.menuDisplay;
    }
}
