import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Endpoint } from '../../../common/const/endpoint';
import { v4 as uuid } from 'uuid';
import * as axios from 'axios';
import { MenuService } from './../../layout/menubar/menu.service';
import { AppComponent } from '../../../app.component';
import { HttpService } from '../../../common/util/http-service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    constructor(private router: Router ,private endpoint: Endpoint,private menuService: MenuService,private global:AppComponent, private HttpService: HttpService) { }

    username: string = '';
    logined = JSON.parse(localStorage.getItem('loginData'));
    rawData  = [];
    rawsData = [];
    roleData = [];
    token = '';
    password: string = '';
   /*  listDomain=[{
        name : 'tac_bangkok',
        name : 'dcenter'
    }] */
    domain : string = '';
    sendData = {
        objectData:[],
        locationCode : ''
    }
    logInStatus : boolean;
    locationCode = '';    
    display = false;
    alert = false;
    clickSingin = false;

    ngOnInit() {
        if (this.logined) {
            this.router.navigate(['/admin-config'])
        }
    }

    async submit() {
        this.clickSingin = true;
        this.roleData = [];
        this.rawsData = [];
        this.rawData = [];
        this.rawData = [];
        await axios.default.request({
            url: this.endpoint.API_AUTH,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'transactionId': uuid(),
                'action': 'login',
                'lang': 'EN',
                'navigate': this.router.url,
                'username':this.username
            }, 
            data: {
                "username":this.username,
                "password": this.password,
                "domain":this.domain
            }
        }).then(snapData => {
            this.clickSingin = false;
            this.global.checkTokenExpired(snapData.data.statusCode);
            if (snapData.data.statusCode == 'S0000') {
                this.logInStatus = true;
                this.rawData.push(snapData.data.data.authorization);
                Object.keys(snapData.data.data.authorization.locationCodeList).map(keys => {
                    this.roleData.push(snapData.data.data.authorization.locationCodeList[keys].code);
                });
                if (snapData.data.data.authorization.locationCodeList.length == 1) {
                    this.locationCode = snapData.data.data.authorization.locationCodeList[0].code;
                }
                this.rawData.push(this.username.toLowerCase());
                this.token = snapData.data.data.authorization.token;
                if(this.roleData.length == 1) {
                    this.locationCode = this.roleData[0];
                    this.rawData.push(this.locationCode);
                    this.rawData.push(this.token);
                    localStorage.setItem('loginData', JSON.stringify(this.rawData));
                    this.menuService.queryDataCloud(this.router.url);
                    this.router.navigate(['/admin-config']);
                    this.display = false;
                } else {
                    this.display = true;
                }
            }
        }).catch(() => {
            this.clickSingin = false;
            this.logInStatus = false; 
            this.display = true;
        })
    }
    
    onKeyUser(event: any) {
        if ((event.key == 'Enter'||event.key == 'NumpadEnter') && this.clickSingin == false) {
            this.submit();
        }
    }

    ok() {
        this.rawData.push(this.locationCode);
        this.rawData.push(this.token);
        localStorage.setItem('loginData', JSON.stringify(this.rawData));
        this.menuService.queryDataCloud(this.router.url);
        this.router.navigate(['/admin-config']) 
       // location.reload();
    }
}
